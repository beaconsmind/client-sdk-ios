require 'xcodeproj'

def add_files (direc, relative_path, current_group, main_target)
    Dir.glob(direc) do |item|
        next if item == '.' or item == '.DS_Store'
 
        if File.directory?(item)
            new_folder = File.basename(item)
            created_group = current_group.new_group(new_folder, new_folder, source_tree = :group)
            add_files("#{item}/*", item, created_group, main_target)
        else
            filename = item.sub "#{relative_path}/", ''
            i = current_group.new_file(filename)
            if item.end_with? ".swift"
                main_target.add_file_references([i])
            end
        end
    end
 end
 

project_root_dir = 'Beaconsmind'
project_path = 'Beaconsmind/Beaconsmind.xcodeproj'
project = Xcodeproj::Project.open(project_path)
groupToDelete = nil

topGroup = project.groups.find { |g|
    g.path == project_root_dir
 }
target = project.native_targets.first
apiGroup = topGroup['API']

if apiGroup != nil
    apiGroup.recursive_children.each do |item|
        target.source_build_phase.remove_file_reference(item)
        item.remove_from_project
    end
    
    apiGroup.remove_from_project
end

group_contents_path = File.join(project_root_dir, project_root_dir, 'API')
add_files(group_contents_path, group_contents_path, topGroup, target)

project.sort(:groups_position => :above)
project.save