{% include "Includes/Header.stencil" %}

import Foundation
import Alamofire

protocol APIClient {

    func apiRequest<T: APIResponseValue>(
        _ request: APIRequest<T>,
        emptyResponseCodes: Set<Int>,
        complete: @escaping (SDKApiResult<T>) -> Void
    ) -> CancellableRequest?
}

// MARK: - Default parameter helpers

extension APIClient {

    func apiRequest<T: APIResponseValue>(
        _ request: APIRequest<T>,
        complete: @escaping (SDKApiResult<T>) -> Void
    ) -> CancellableRequest? {
        apiRequest(
            request,
            emptyResponseCodes: [],
            complete: complete
        )
    }
}

protocol APIClientDelegate: AnyObject {
    func handleUnauthenticatedResponse()
}

/// Manages and sends APIRequests
final class APIClientImpl: APIClient {

    /// A list of RequestBehaviours that can be used to monitor and alter all requests
    private(set) var behaviours: [RequestBehaviour] = []

    /// The base url prepended before every request path
    private(set) var baseURL: String

    /// The Alamofire Session used for each request
    private var session: Session

    private weak var delegate: APIClientDelegate?

    /// These headers will get added to every request
    private var defaultHeaders: [String: String]

    private var jsonDecoder = JSONDecoder()
    private var jsonEncoder = JSONEncoder()

    private var decodingQueue = DispatchQueue(label: "apiClient", qos: .utility, attributes: .concurrent)

    init(
        baseURL: String,
        session: Session = .default,
        delegate: APIClientDelegate? = nil,
        defaultHeaders: [String: String] = [:],
        behaviours: [RequestBehaviour] = []
    ) {
        self.baseURL = baseURL
        self.session = session
        self.delegate = delegate
        self.behaviours = behaviours
        self.defaultHeaders = defaultHeaders
        jsonDecoder.dateDecodingStrategy = .custom(dateDecoder)
        jsonEncoder.dateEncodingStrategy = .formatted(API.dateEncodingFormatter)
    }

    func apiRequest<T: APIResponseValue>(
        _ request: APIRequest<T>,
        emptyResponseCodes: Set<Int> = [],
        complete: @escaping (SDKApiResult<T>) -> Void
    ) -> CancellableRequest? {
        makeRequest(
            request,
            emptyResponseCodes: emptyResponseCodes
        ) { response in
            switch (response.result) {
            case let .success(value):
                complete(.success(value))
            case let .failure(error):
                if
                    case let .unexpectedStatusCode(code, data) = error,
                    let strFromData = String(data: data, encoding: .utf8)
                {
                    complete(.failure(SDKApiError.apiError(code, strFromData)))
                } else {
                    complete(.failure(SDKApiError.generic(error)))
                }
            }
        }
    }

    @discardableResult
    private func makeRequest<T: APIResponseValue>(
        _ request: APIRequest<T>,
        behaviours: [RequestBehaviour] = [],
        emptyResponseCodes: Set<Int> = [],
        completionQueue: DispatchQueue = DispatchQueue.main,
        complete: @escaping (APIResponse<T>) -> Void
    ) -> CancellableRequest? {
        // create composite behaviour to make it easy to call functions on array of behaviours
        let requestBehaviour = RequestBehaviourGroup(request: request, behaviours: self.behaviours + behaviours)

        // create the url request from the request
        var urlRequest: URLRequest
        do {
            guard let safeURL = URL(string: baseURL) else {
                throw InternalError.malformedURL
            }

            urlRequest = try request.createURLRequest(baseURL: safeURL, encoder: jsonEncoder)
        } catch {
            let error = APIClientError.requestEncodingError(error)
            requestBehaviour.onFailure(error: error)
            let response = APIResponse<T>(request: request, result: .failure(error))
            complete(response)
            return nil
        }

        // add the default headers
        if urlRequest.allHTTPHeaderFields == nil {
            urlRequest.allHTTPHeaderFields = [:]
        }
        for (key, value) in defaultHeaders {
            urlRequest.allHTTPHeaderFields?[key] = value
        }

        urlRequest = requestBehaviour.modifyRequest(urlRequest)

        let cancellableRequest = CancellableRequest(request: request.asAny())

        requestBehaviour.validate(urlRequest) { result in
            switch result {
            case .success(let urlRequest):
                self.makeNetworkRequest(
                    request: request,
                    urlRequest: urlRequest,
                    cancellableRequest: cancellableRequest,
                    requestBehaviour: requestBehaviour,
                    emptyResponseCodes: emptyResponseCodes,
                    completionQueue: completionQueue,
                    complete: complete)
            case .failure(let error):
                let error = APIClientError.validationError(error)
                let response = APIResponse<T>(request: request, result: .failure(error), urlRequest: urlRequest)
                requestBehaviour.onFailure(error: error)
                complete(response)
            }
        }
        return cancellableRequest
    }

    private func makeNetworkRequest<T>(
        request: APIRequest<T>,
        urlRequest: URLRequest,
        cancellableRequest: CancellableRequest,
        requestBehaviour: RequestBehaviourGroup,
        emptyResponseCodes: Set<Int> = [],
        completionQueue: DispatchQueue,
        complete: @escaping (APIResponse<T>) -> Void
    ) {
        requestBehaviour.beforeSend()

        if request.service.isUpload {
            let formData: (MultipartFormData) -> Void = { multipartFormData in
                for (name, value) in request.formParameters {
                    if let file = value as? UploadFile {
                        switch file.type {
                        case let .url(url):
                            if let fileName = file.fileName, let mimeType = file.mimeType {
                                multipartFormData.append(url, withName: name, fileName: fileName, mimeType: mimeType)
                            } else {
                                multipartFormData.append(url, withName: name)
                            }
                        case let .data(data):
                            if let fileName = file.fileName, let mimeType = file.mimeType {
                                multipartFormData.append(data, withName: name, fileName: fileName, mimeType: mimeType)
                            } else {
                                multipartFormData.append(data, withName: name)
                            }
                        }
                    } else if let settings = value as? UploadSettings {
                        multipartFormData.append(settings.encoded, withName: name, fileName: settings.fileName, mimeType: settings.mimeType)
                    } else if let url = value as? URL {
                        multipartFormData.append(url, withName: name)
                    } else if let data = value as? Data {
                        multipartFormData.append(data, withName: name)
                    } else if let string = value as? String {
                        multipartFormData.append(Data(string.utf8), withName: name)
                    }
                }
            }
            let request = session
                .upload(multipartFormData: formData, with: urlRequest)
                .responseData(queue: decodingQueue, emptyResponseCodes: emptyResponseCodes) { dataResponse in
                    self.handleResponse(request: request, requestBehaviour: requestBehaviour, dataResponse: dataResponse, completionQueue: completionQueue, complete: complete)
                }
            cancellableRequest.networkRequest = request
        } else {
            let request = session.request(urlRequest)
                .responseData(queue: decodingQueue, emptyResponseCodes: emptyResponseCodes) { dataResponse in
                    self.handleResponse(request: request, requestBehaviour: requestBehaviour, dataResponse: dataResponse, completionQueue: completionQueue, complete: complete)
            }
            cancellableRequest.networkRequest = request
        }
    }

    private func handleResponse<T>(request: APIRequest<T>, requestBehaviour: RequestBehaviourGroup, dataResponse: DataResponse<Data, AFError>, completionQueue: DispatchQueue, complete: @escaping (APIResponse<T>) -> Void) {

        let result: APIResult<T>

        switch dataResponse.result {
        case .success(let value):
            do {
                guard let statusCode = dataResponse.response?.statusCode else {
                    throw InternalError.emptyResponse
                }

                let decoded = try T(statusCode: statusCode, data: value, decoder: jsonDecoder)
                result = .success(decoded)
                if decoded.successful {
                    requestBehaviour.onSuccess(result: decoded.response as Any)
                }
            } catch let error {
                let apiError: APIClientError
                if let error = error as? DecodingError {
                    apiError = APIClientError.decodingError(error)
                } else if let error = error as? APIClientError {
                    apiError = error
                } else {
                    apiError = APIClientError.unknownError(error)
                }

                result = .failure(apiError)
                requestBehaviour.onFailure(error: apiError)
            }
        case .failure(let error):
            let apiError = APIClientError.networkError(error)
            result = .failure(apiError)
            requestBehaviour.onFailure(error: apiError)
        }
        let response = APIResponse<T>(request: request, result: result, urlRequest: dataResponse.request, urlResponse: dataResponse.response, data: dataResponse.data, metrics: dataResponse.metrics)
        requestBehaviour.onResponse(response: response.asAny())

        /// Logging user out in case of 401 or 403
        if let statusCode = response.urlResponse?.statusCode, [401, 403].contains(statusCode) {
            delegate?.handleUnauthenticatedResponse()
        }

        completionQueue.async {
            complete(response)
        }
    }
}

private extension APIClientImpl {
    enum InternalError: Error {
        case malformedURL
        case emptyResponse
    }
}

public class CancellableRequest {
    /// The request used to make the actual network request
    public let request: AnyRequest

    init(request: AnyRequest) {
        self.request = request
    }

    var networkRequest: Request?

    /// cancels the request
    public func cancel() {
        networkRequest?.cancel()
    }
}

// Create URLRequest
extension APIRequest {
    public func createURLRequest(baseURL: String, encoder: RequestEncoder = JSONEncoder()) throws -> URLRequest {
        try createURLRequest(baseURL: URL(string: baseURL)!, encoder: encoder)
    }

    public func createURLRequest(baseURL: URL, encoder: RequestEncoder = JSONEncoder()) throws -> URLRequest {
        var urlRequest = URLRequest(url: baseURL.appendingPathComponent(path))
        urlRequest.httpMethod = service.method
        urlRequest.allHTTPHeaderFields = headers

        // filter out parameters with empty string value
        var queryParams: [String: Any] = [:]
        for (key, value) in queryParameters {
            if !String(describing: value).isEmpty {
                queryParams[key] = value
            }
        }

        if !queryParams.isEmpty {
            urlRequest = try URLEncoding.queryString.encode(urlRequest, with: queryParams)
        }

        var formParams: [String: Any] = [:]
        for (key, value) in formParameters {
            if !String(describing: value).isEmpty {
                formParams[key] = value
            }
        }

        if !formParams.isEmpty {
            urlRequest = try URLEncoding.httpBody.encode(urlRequest, with: formParams)
        }

        if let encodeBody = encodeBody {
            urlRequest.httpBody = try encodeBody(encoder)
            urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        }
        return urlRequest
    }
}
