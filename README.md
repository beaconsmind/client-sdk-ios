# Beaconsmind

Welcome to the SDK readme guide for Beaconsmind. This quick guide assumes knowledge about iOS develoment, package managers, application signing and debug/release versions of the apps.

## Guides

- Get started with our [Installation guide](./Documentation/Installing.md)
- SDK integration is described in [usage guide](./Documentation/UsageGuide.md)
- In-depth SDK API documentation can be found in the [API docs](./Documentation/API.md)
- About [Using Beacons](./Documentation/Beacons.md) in general
- Documentation of the [exposed UI Widgets](./Documentation/UI.md)
- [Registering a new APNS key](./Documentation/RegisteringAPNSKey.md) on Apple’s developer portal

### Supporting versions before 3.0.0

Until version `3.0.0`, the SDK was using static functions to make our API-s as clean as possible, but this introduced some limitations, so we decided to deprecate this approach and start a more flexible, delegate-based setup.

- Migration guide from previous versions to `3.0.0` can be found in the [Migrating to 3.0.0 docs](./Documentation/Deprecated/Migrating-to-3.0.0.md)
- Former setup documentation can be found in the [SDK Setup](./Documentation/Deprecated/Setup.md)
- Former API documentation can be found in the [API docs](./Documentation/Deprecated/API.md)
- Former Beacons usage description can be found in [Using Beacons](./Documentation/Deprecated/Beacons.md)
- Former offers description can be found in the [Offers docs](./Documentation/Deprecated/Offers.md)
- Former Push notifications description can be found in the [Push Notifications guide](./Documentation/Deprecated/Push-notifications.md)

## Troubleshooting

- `BeaconListenerDelegate` methods are not properly called when beacon enters proximity of the device with versions below 1.0.3. Please use 1.0.3 and above to properly get beacon messages
- `BeaconsmindSDK.startListeningBeacons` needs to be invoked on the _main_ app thread. SDK versions below 1.0.3 will not throw an error.

## Developers

Beaconsmind AG

- Augustin Juricic (augustinjuricic@beaconsmind.com)
- Tamas Dancsi (tamasdancsi@beaconsmind.com)

## License

Beaconsmind SDK is available under the MIT license. See the LICENSE file for more info.
