//
//  BeaconContactsSummaryProxy.swift
//  BeaconsmindProxy
//
//  Created by Ivo Leko on 14.03.2023..
//

import UIKit
import Beaconsmind

@objc public class BeaconContactsSummaryProxy: NSObject {
    
    private let beaconContactsSummary: BeaconContactsSummary
    
    init(summary: BeaconContactsSummary) {
        self.beaconContactsSummary = summary
    }
    
    @objc public var store: String {
        self.beaconContactsSummary.store
    }
    
    @objc public var name: String {
        self.beaconContactsSummary.name
    }
    
    @objc public var uuid: String {
        self.beaconContactsSummary.uuid
    }
    
    @objc public var major: String {
        self.beaconContactsSummary.major
    }
    
    @objc public var minor: String {
        self.beaconContactsSummary.minor
    }
    
    @objc public var rssi: NSNumber? {
        if let rssi = self.beaconContactsSummary.rssi {
            return NSNumber(value: rssi)
        } else {
            return  nil
        }
    }
    
    @objc public var averageRSSI: NSNumber? {
        if let averageRSSI = self.beaconContactsSummary.averageRSSI {
            return NSNumber(value: averageRSSI)
        } else {
            return nil
        }
    }
    
    @objc public var timestamp: NSNumber? {
        if let timestamp = self.beaconContactsSummary.timestamp {
            return NSNumber(value: timestamp)
        } else {
            return nil
        }
    }
    
    @objc public var frequency: Int {
        return beaconContactsSummary.frequency
    }
    
    @objc public var contactCount: Int {
        return beaconContactsSummary.contactCount
    }
    
    @objc public var isInRange: Bool {
        return beaconContactsSummary.isInRange
    }
}
