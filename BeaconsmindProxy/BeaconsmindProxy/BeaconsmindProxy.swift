//
//  BeaconsmindProxy.swift
//  BeaconsmindProxy
//
//  Created by Ivo Leko on 26.02.2023..
//

import Foundation
import Beaconsmind

@objc public protocol BeaconsmindDelegateProxy {
    func beaconsmind(_ beaconsmind: BeaconsmindProxy, onContextChanged context: APIContextProxy?)
}

@objc public enum PlatformTypeProxy: Int {
    case Unknown = 0
    case Fcm = 1
    case Apns = 2
    
    func getType() -> PlatformType {
        switch self {
        case .Unknown:
            return .Unknown
        case .Fcm:
            return .Fcm
        case .Apns:
            return .Apns
        }
    }
}



@objc public class BeaconsmindProxy: NSObject, BeaconsmindDelegate {
    
    weak var delegate: BeaconsmindDelegateProxy? = nil
    
    @objc public static let `default` = BeaconsmindProxy()
    
    
    //MARK: - START
    @objc public func start(delegate: BeaconsmindDelegateProxy,
                            appVersion: String,
                            hostname: String) -> Bool {
        do {
            self.delegate = delegate
            try Beaconsmind.default.start(delegate: self,
                                          appVersion: appVersion,
                                          hostname: hostname)
            return true
        } catch {
            self.delegate = nil
            return false
        }
    }
    
    func startDevelop(delegate: BeaconsmindDelegateProxy,
                      appVersion: String,
                      hostName: String,
                      token: String?) -> Bool {
        do {
            self.delegate = delegate
            try Beaconsmind.default.startDevelop(delegate: self,
                                                 appVersion: appVersion,
                                                 hostname: hostName,
                                                 token: token)
            return true
        } catch {
            self.delegate = nil
            return false
        }
    }
    
    
    public func beaconsmind(_ beaconsmind: Beaconsmind,
                            onContextChanged
                            context: APIContext?) {
        
        let contextProxy: APIContextProxy?
        if let context = context {
            contextProxy = APIContextProxy(context)
        } else {
            contextProxy = nil
        }
        
        self.delegate?.beaconsmind(self, onContextChanged: contextProxy)
    }
    
    
    // MARK: - Authentication
    @objc public func login(username: String,
                            password: String,
                            completion: ((_ result: APIContextProxy?,
                                          _ error: SDKApiErrorProxy?) -> Void)?) {
        
        do {
            try Beaconsmind.default.login(username: username,
                                          password: password,
                                          callback: { result in
                switch result {
                case .success(let apiContext):
                    completion?(APIContextProxy(apiContext), nil)
                case .failure(let apiError):
                    completion?(nil, SDKApiErrorProxy(apiError))
                }
            })
        } catch {
            completion?(nil, SDKApiErrorProxy(error: error))
        }
    }
    
    @objc public func signUp(username: String,
                firstName: String,
                lastName: String,
                password: String,
                confirmPassword: String,
                avatarThumbnailURL: String? = nil,
                avatarURL: String? = nil,
                language: String? = nil,
                gender: String? = nil,
                favoriteStoreID: NSNumber? = nil,
                birthDate: NSDate? = nil,
                completion: ((_ result: APIContextProxy?,
                              _ error: SDKApiErrorProxy?) -> Void)?) {
        
        do {
            var dateDay: DateDay? = nil
            if let birthDate = birthDate {
                dateDay = DateDay(date: birthDate as Date)
            }
            
            try Beaconsmind.default.signup(username: username,
                                           firstName: firstName,
                                           lastName: lastName,
                                           password: password,
                                           confirmPassword: confirmPassword,
                                           avatarThumbnailURL: avatarThumbnailURL,
                                           avatarURL: avatarURL,
                                           language: language,
                                           gender: gender,
                                           favoriteStoreID: favoriteStoreID?.intValue,
                                           birthDate: dateDay) { result in
                
                switch result {
                case .success(let apiContext):
                    completion?(APIContextProxy(apiContext), nil)
                case .failure(let apiError):
                    completion?(nil, SDKApiErrorProxy(apiError))
                }
            }
        } catch {
            completion?(nil, SDKApiErrorProxy(error: error))
        }
    }
    
    
    @objc public func logout(completion: ((_ result: Bool,
                                        _ error: SDKApiErrorProxy?) -> Void)?) {
        do {
            try Beaconsmind.default.logout { result in
                switch result {
                case .success(let success):
                    completion?(success, nil)
                case .failure(let apiError):
                    completion?(false, SDKApiErrorProxy(apiError))
                }
            }
        } catch {
            completion?(false, SDKApiErrorProxy(error: error))
        }
    }
    
    @objc public func getAPIContext() -> APIContextProxy? {
        guard let context = Beaconsmind.default.getAPIContext() else {
            return nil
        }
        return APIContextProxy(context)
    }
    
    @objc public func updateHostname(hostname: String) {
        Beaconsmind.default.updateHostname(hostname: hostname)
    }
    
    @objc public func importAccount(
        id: String,
        email: String,
        firstName: String? = nil,
        lastName: String? = nil,
        birthDate: NSDate? = nil,
        language: String? = nil,
        gender: String? = nil,
        completion: ((_ result: APIContextProxy?,
                      _ error: SDKApiErrorProxy?) -> Void)?) {
        
        do {
            try Beaconsmind.default.importAccount(id: id,
                                                  email: email,
                                                  firstName: firstName,
                                                  lastName: lastName,
                                                  birthDate: birthDate as DateTime?,
                                                  language: language,
                                                  gender: gender,
                                                  callback: { result in
                
                switch result {
                case .success(let apiContext):
                    completion?(APIContextProxy(apiContext), nil)
                case .failure(let apiError):
                    completion?(nil, SDKApiErrorProxy(apiError))
                }
            })
        } catch {
            completion?(nil, SDKApiErrorProxy(error: error))
        }
    }
    
    
    //MARK: - Beacons
    
    @objc public func startListeningBeacons() -> SDKApiErrorProxy? {
        do {
            try Beaconsmind.default.startListeningBeacons()
            return nil
        } catch {
            return SDKApiErrorProxy(error: error)
        }
    }
    
    @objc public func stopListeningBeacons() {
        Beaconsmind.default.stopListeningBeacons()
    }
    
    @objc public func getBeaconsContactsSummary() -> [BeaconContactsSummaryProxy] {
        return Beaconsmind.default.beaconContactsSummaries.map { summary in
            BeaconContactsSummaryProxy(summary: summary)
        }
    }

    
    //MARK: - Notifications
    
    @objc public func registerForPushNotifications() {
        Beaconsmind.default.registerForPushNotifications()
    }
    
   
    @objc public func register(deviceToken: Data,
                               platformType: PlatformTypeProxy = .Apns,
                               completion: ((_ result: Bool,
                                             _ error: SDKApiErrorProxy?) -> Void)?) {
        
//        guard let data = deviceToken.data(using: .utf8) else {
//            completion?(false, SDKApiErrorProxy(error: NSError(domain: "BeaconsmindProxy", code: 1, userInfo: [NSLocalizedDescriptionKey: "Cannot create data from token"])))
//            return
//        }
        
        do {
            try Beaconsmind.default.register(deviceToken: deviceToken,
                                             platformType: platformType.getType()) { result in
                switch result {
                case .success(let response):
                    completion?(response.successful, nil)
                case .failure(let apiError):
                    completion?(false, SDKApiErrorProxy(apiError))
                }
            }
        } catch {
            completion?(false, SDKApiErrorProxy(error: error))
        }
    }
    
    // MARK: - Locations
    @objc public func requestLocationPermission(callback: LocationPermissionRequestCompletionHandler?) {
        Beaconsmind.default.requestLocationPermission(callback: callback)
    }
    
    //MARK: - Logs
    @objc public func setMinLogLevel(level: LogLevelProxy) {
        Beaconsmind.default.setMinLogLevel(level: level.getLogLevel())
    }
    
    // MARK: - Profile

    @objc public func getProfile(completion: ((_ result: ProfileResponseProxy?,
                                               _ error: SDKApiErrorProxy?) -> Void)?) {
        
        do {
            try _ = Beaconsmind.default.getProfile(callback: { result in
                switch result {
                case .success(let profileResponse):
                    if let profileResponse = profileResponse {
                        completion?(ProfileResponseProxy(profileResponse: profileResponse), nil)
                    } else {
                        completion?(nil, SDKApiErrorProxy(SDKApiError.unknown))
                    }
                case .failure(let apiError):
                    completion?(nil, SDKApiErrorProxy(apiError))
                }
            })
        }
        catch {
            completion?(nil, SDKApiErrorProxy(error: error))
        }
        
    }
    
    
    @objc public func updateProfile(firstName: String,
                                    lastName: String,
                                    avatarThumbnailURL: String? = nil,
                                    avatarURL: String? = nil,
                                    birthDate: NSDate? = nil,
                                    city: String? = nil,
                                    country: String? = nil,
                                    disablePushNotifications: NSNumber? = nil,
                                    favoriteStoreID: NSNumber? = nil,
                                    gender: String? = nil,
                                    houseNumber: String? = nil,
                                    landlinePhone: String? = nil,
                                    language: String? = nil,
                                    phoneNumber: String? = nil,
                                    street: String? = nil,
                                    zipCode: String? = nil,
                                    completion: ((_ result: ProfileResponseProxy?,
                                                  _ error: SDKApiErrorProxy?) -> Void)?) {
        
        do {
            _ = try Beaconsmind.default.updateProfile(firstName: firstName,
                                                      lastName: lastName,
                                                      avatarThumbnailURL: avatarThumbnailURL,
                                                      avatarURL: avatarURL,
                                                      birthDate: birthDate as DateTime?,
                                                      city: city,
                                                      country: country,
                                                      disablePushNotifications: disablePushNotifications?.boolValue,
                                                      favoriteStoreID: favoriteStoreID?.intValue,
                                                      gender: gender,
                                                      houseNumber: houseNumber,
                                                      landlinePhone: landlinePhone,
                                                      language: language,
                                                      phoneNumber: phoneNumber,
                                                      street: street,
                                                      zipCode: zipCode) { result in
                switch result {
                case .success(let profileResponse):
                    if let profileResponse = profileResponse {
                        completion?(ProfileResponseProxy(profileResponse: profileResponse), nil)
                    } else {
                        completion?(nil, SDKApiErrorProxy(SDKApiError.unknown))
                    }
                case .failure(let apiError):
                    completion?(nil, SDKApiErrorProxy(apiError))
                }
                
            }
        } catch {
            completion?(nil, SDKApiErrorProxy(error: error))
        }
        
    }

    
    // MARK: - Offers

    @objc public func loadOffer(offerID: Int, completion: ((_ result: OfferResponseProxy?,
                                                            _ error: SDKApiErrorProxy?) -> Void)?) {
        do {
            let request = API.Offers.OffersGetOffer.Request(offerId: offerID)
            _ = try Beaconsmind.default.apiRequest(request) { result in

                switch result {
                case .success(let response):
                    if let offerResponse = response.success {
                        completion?(OfferResponseProxy(offerResponse: offerResponse), nil)
                    } else {
                        completion?(nil, SDKApiErrorProxy(SDKApiError.unknown))
                    }

                case .failure:
                    completion?(nil, SDKApiErrorProxy(error: NSError(domain: "BeaconsmindProxy",
                                                                     code: 2,
                                                                     userInfo: [NSLocalizedDescriptionKey:  "Couldn't load offers, please try again"]))
                    )
                }
            }
        } catch {
            completion?(nil, SDKApiErrorProxy(error: error))
        }
    }
    
    
    @objc public func loadOffers(completion: ((_ result: [OfferResponseProxy]?,
                                  _ error: SDKApiErrorProxy?) -> Void)?) {
        do {
            let request = API.Offers.OffersGetOffers.Request()
            _ = try Beaconsmind.default.apiRequest(request) { result in
                switch result {
                case let .success(response):
                    if let offerResponses = response.success {
                        let offerResponsesProxy = offerResponses.map { offerResponse in
                            OfferResponseProxy(offerResponse: offerResponse)
                        }
                        completion?(offerResponsesProxy, nil)
                    } else {
                        completion?(nil, SDKApiErrorProxy(SDKApiError.unknown))
                    }
                case .failure:
                    completion?(nil, SDKApiErrorProxy(error: NSError(domain: "BeaconsmindProxy",
                                                                     code: 2,
                                                                     userInfo: [NSLocalizedDescriptionKey:  "Couldn't load offers, please try again"]))
                    )
                }
            }
        } catch {
            completion?(nil, SDKApiErrorProxy(error: error))
        }
    }
    
    
    
    @objc public func markOfferAsRead(offerID: Int,
                         completion: ((_ result: Bool,
                                       _ error: SDKApiErrorProxy?) -> Void)?) {
        do {
            try Beaconsmind.default.markOfferAsRead(offerID: offerID) { result in
                switch result {
                case .success(let success):
                    completion?(success, nil)
                case .failure(let apiError):
                    completion?(false, SDKApiErrorProxy(apiError))
                }
            }
        } catch {
            completion?(false, SDKApiErrorProxy(error: error))
        }
    }
    
    @objc public func markOfferAsReceived(offerID: Int,
                             completion: ((_ result: Bool,
                                       _ error: SDKApiErrorProxy?) -> Void)?) {
        do {
            try Beaconsmind.default.markOfferAsReceived(offerID: offerID, callback: { result in
                switch result {
                case .success(let success):
                    completion?(success, nil)
                case .failure(let apiError):
                    completion?(false, SDKApiErrorProxy(apiError))
                }
            })
        } catch {
            completion?(false, SDKApiErrorProxy(error: error))
        }
    }
    
    @objc public func markOfferAsRedeemed(offerID: Int,
                             completion: ((_ result: Bool,
                                       _ error: SDKApiErrorProxy?) -> Void)?) {
        do {
            try Beaconsmind.default.markOfferAsRedeemed(offerID: offerID, callback: { result in
                switch result {
                case .success(let success):
                    completion?(success, nil)
                case .failure(let apiError):
                    completion?(false, SDKApiErrorProxy(apiError))
                }
            })
        } catch {
            completion?(false, SDKApiErrorProxy(error: error))
        }
    }
    
    @objc public func markOfferAsClicked(offerID: Int,
                             completion: ((_ result: Bool,
                                       _ error: SDKApiErrorProxy?) -> Void)?) {
        do {
            try Beaconsmind.default.markOfferAsClicked(offerID: offerID, callback: { result in
                switch result {
                case .success(let success):
                    completion?(success, nil)
                case .failure(let apiError):
                    completion?(false, SDKApiErrorProxy(apiError))
                }
            })
        } catch {
            completion?(false, SDKApiErrorProxy(error: error))
        }
    }
}

