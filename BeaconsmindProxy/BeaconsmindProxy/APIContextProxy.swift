//
//  APIContextProxy.swift
//  BeaconsmindProxy
//
//  Created by Ivo Leko on 26.02.2023..
//

import Foundation
import Beaconsmind

@objc public class APIContextProxy: NSObject {
    
    let apiContext: APIContext
    init(_ apiContext: APIContext) {
        self.apiContext = apiContext
    }
    
    @objc public var hostname: String {
        get {
            apiContext.hostname
        }
    }
    
    @objc public var accessToken: String {
        get {
            apiContext.accessToken
        }
    }
    
    @objc public var userID: String {
        get {
            apiContext.userID
        }
    }
    
    @objc public var tokenType: String? {
        get {
            apiContext.tokenType
        }
    }
    
    @objc public var expiresIn: String? {
        get {
            apiContext.expiresIn
        }
    }
}
