//
//  SDKApiErrorProxy.swift
//  BeaconsmindProxy
//
//  Created by Ivo Leko on 26.02.2023..
//

import Foundation
import Beaconsmind


@objc public enum ErrorType: Int {
    case unknown
    case wrongSelf
    case notStarted
    case needsMainThread
    case generic
    case location
    case network
    case parse
    case apierror
    case apiErrorMessage
    case invalidContext
    case invalidStore
    case invalidImage
}


@objc public class SDKApiErrorProxy: NSObject {
    
    let apiError: SDKApiError
    @objc public let errorType: ErrorType
    @objc public let error: Error?
    @objc public let httpStatusCode: NSNumber?
    @objc public let httpPayload: String?
    
    init (_ apiError: SDKApiError) {
        self.apiError = apiError
        
        var error: Error? = nil
        var httpStatusCode: Int? = nil
        var httpPayload: String? = nil
        
        switch apiError {
        case .unknown:
            self.errorType = .unknown
        case .wrongSelf:
            self.errorType = .wrongSelf
        case .notStarted:
            self.errorType = .notStarted
        case .needsMainThread:
            self.errorType = .needsMainThread
        case .generic(let errorT):
            self.errorType = .generic
            error = errorT
        case .location(let locationError):
            self.errorType = .location
            error = locationError
        case .network(let apiClientError):
            self.errorType = .network
            error = apiClientError
        case .parse(let stringError):
            self.errorType = .parse
            error = NSError(domain: "SDKApiErrorProxy", code: 1, userInfo: [NSLocalizedDescriptionKey: stringError])
        case .apiError(let code, let payload):
            self.errorType = .apierror
            httpStatusCode = code
            httpPayload = payload
        case .apiErrorMessage(let payload):
            self.errorType = .apiErrorMessage
            httpPayload = payload
        case .invalidContext:
            self.errorType = .invalidContext
        case .invalidStore:
            self.errorType = .invalidStore
        case .invalidImage:
            self.errorType = .invalidImage
        }
        
        if let code = httpStatusCode {
            self.httpStatusCode = NSNumber(integerLiteral: code)
        } else {
            self.httpStatusCode = nil
        }
        self.httpPayload = httpPayload
        self.error = error
    }
    
    convenience init (error: Error) {
        self.init(SDKApiError.generic(error))
    }
}
