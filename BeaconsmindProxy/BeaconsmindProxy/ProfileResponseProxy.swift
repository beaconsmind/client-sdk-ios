//
//  ProfileResponseProxy.swift
//  BeaconsmindProxy
//
//  Created by Ivo Leko on 09.03.2023..
//

import UIKit
import Beaconsmind

@objc public class ProfileResponseProxy: NSObject {
    let profileResponse: ProfileResponse
    
    init(profileResponse: ProfileResponse) {
        self.profileResponse = profileResponse
    }
    
    @objc public var disablePushNotifications: Bool {
        self.profileResponse.disablePushNotifications
    }

    @objc public var firstName: String {
        self.profileResponse.firstName
    }

    @objc public var joinDate: DateTime {
        self.profileResponse.joinDate
    }

    @objc public var lastName: String {
        self.profileResponse.lastName
    }

    @objc public var newsLetterSubscription: Bool {
        self.profileResponse.newsLetterSubscription
    }

    @objc public var avatarThumbnailUrl: String? {
        self.profileResponse.avatarThumbnailUrl
    }

    @objc public var avatarUrl: String? {
        self.profileResponse.avatarUrl
    }

    @objc public var birthDate: DateTime? {
        self.profileResponse.birthDate
    }

    @objc public var city: String? {
        self.profileResponse.city
    }

    @objc public var claims: [String]? {
        self.profileResponse.claims
    }

    @objc public var clubId: String? {
        self.profileResponse.clubId
    }

    @objc public var country: String? {
        self.profileResponse.country
    }

    @objc public var favoriteStore: String? {
        self.profileResponse.favoriteStore
    }

    @objc public var favoriteStoreId: NSNumber? {
        if let favoriteStoreId = self.profileResponse.favoriteStoreId {
            return NSNumber(integerLiteral: favoriteStoreId)
        }
        return nil
    }

    @objc public var fullName: String? {
        self.profileResponse.fullName
    }

    @objc public var gender: String? {
        self.profileResponse.gender
    }

    @objc public var houseNumber: String? {
        self.profileResponse.houseNumber
    }

    @objc public var id: String? {
        self.profileResponse.id
    }

    @objc public var landlinePhone: String? {
        self.profileResponse.landlinePhone
    }

    @objc public var language: String? {
        self.profileResponse.language
    }

    @objc public var phoneNumber: String? {
        self.profileResponse.phoneNumber
    }

    @objc public var roles: [String]? {
        self.profileResponse.roles
    }

    @objc public var street: String? {
        self.profileResponse.street
    }

    @objc public var url: String? {
        self.profileResponse.url
    }

    @objc public var userName: String? {
        self.profileResponse.userName
    }

    @objc public var zipCode: String? {
        self.profileResponse.zipCode
    }
}
