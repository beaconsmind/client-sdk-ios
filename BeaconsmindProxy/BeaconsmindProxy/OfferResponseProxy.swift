//
//  OfferResponseProxy.swift
//  BeaconsmindProxy
//
//  Created by Ivo Leko on 14.03.2023..
//

import UIKit
import Beaconsmind


@objc public class OfferResponseProxy: NSObject {
    
    @objc public class ButtonModelProxy: NSObject {
        
        private let buttonModel: ButtonModel
        
        init(buttonModel: ButtonModel) {
            self.buttonModel = buttonModel
        }
        
        @objc public var link: String? {
            buttonModel.link
        }
        
        @objc public var title: String? {
            buttonModel.title
        }
    }

    private let offerResponse: OfferResponse

    init(offerResponse: OfferResponse) {
        self.offerResponse = offerResponse
    }
    
    @objc public var isRedeemable: Bool {
        self.offerResponse.isRedeemable
    }
    
    @objc public var isRedeemed: Bool {
        self.offerResponse.isRedeemed
    }
    
    @objc public var isVoucher: Bool {
        self.offerResponse.isVoucher
    }
    
    @objc public var messageId: Int {
        self.offerResponse.messageId
    }
    
    @objc public var offerId: Int {
        self.offerResponse.messageId
    }
    
    @objc public var tileAmount: Int {
        self.offerResponse.tileAmount
    }
    
    @objc public var callToAction: ButtonModelProxy? {
        guard let callToAction = self.offerResponse.callToAction else {
            return nil
        }
        return ButtonModelProxy(buttonModel: callToAction)
    }
    
    @objc public var imageUrl: String? {
        self.offerResponse.imageUrl
    }
    
    @objc public var text: String? {
        self.offerResponse.text
    }
    
    @objc public var thumbnailUrl: String? {
        self.offerResponse.thumbnailUrl
    }
    
    @objc public var title: String? {
        self.offerResponse.title
    }
    
    @objc public var validity: String? {
        self.offerResponse.validity
    }
}
