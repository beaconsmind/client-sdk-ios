//
//  LogLevelProxy.swift
//  BeaconsmindProxy
//
//  Created by Ivo Leko on 15.03.2023..
//

import UIKit
import Beaconsmind

@objc public enum LogLevelProxy: Int {
    case silent = 0
    case error = 1
    case warning = 2
    case info = 3
    case debug = 4
    
    func getLogLevel() -> LogLevel {
        switch self {
        case .silent:
            return .silent
        case .error:
            return .error
        case .debug:
            return .debug
        case .info:
            return .info
        case .warning:
            return .warning
        }
    }
}
