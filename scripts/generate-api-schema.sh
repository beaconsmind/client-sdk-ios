#!/usr/bin/env bash

set -euo pipefail
main() {
    REPOSRC=https://github.com/yonaskolb/SwagGen.git
    LOCALREPO=swift-swagger-gen
    SWAGGER_GEN_PATH=swagger-gen
    LIB_ROOT_PATH="Beaconsmind/Beaconsmind"
    OUTPUT_DIR="./generated"
    SWAGGER_SPEC=https://test-develop-suite.azurewebsites.net/swagger/docs/client.v1.json

    # We do it this way so that we can abstract if from just git later on
    LOCALREPO_VC_DIR=$LOCALREPO/.git

    if [ ! -d $LOCALREPO_VC_DIR ]
    then
        git clone "${REPOSRC}" "${LOCALREPO}"
    else
        git clone "${REPOSRC}" "${LOCALREPO}" 2> /dev/null || git -C "${LOCALREPO}" pull
    fi

    rm -rvf "${LIB_ROOT_PATH}/API"

    pushd "${LOCALREPO}" > /dev/null

    rm -rf "${OUTPUT_DIR}"
    swaggen generate "${SWAGGER_SPEC}" --destination "${OUTPUT_DIR}" --template "../${SWAGGER_GEN_PATH}/Template"
    mv "${OUTPUT_DIR}/Sources/APIClient.swift" "../${LIB_ROOT_PATH}/Services/API/APIClient.swift"
    cp -av "${OUTPUT_DIR}/Sources/" "../${LIB_ROOT_PATH}/API"

    popd > /dev/null

    cp -R "${SWAGGER_GEN_PATH}/Overrides/" "${LIB_ROOT_PATH}/API"
    ruby "${SWAGGER_GEN_PATH}/update_xcodeproj.rb"
}

main "$@"
