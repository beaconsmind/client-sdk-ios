#!/usr/bin/env bash

set -euo pipefail
main() {
  bundle install
  ./scripts/carthage.sh

  local is_m1=""

  while getopts ":m" opt; do
    case "${opt}" in
    m)
      is_m1=true
      ;;
    esac
  done

  shift $((OPTIND - 1))
  if [[ -n "${is_m1}" ]]; then
    echo "Installing with M1"
    arch -x86_64 pod install
  else 
    echo "Installing without M1"
    bundle exec pod install
  fi
}

main "$@"
