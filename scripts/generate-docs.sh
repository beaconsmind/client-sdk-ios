#!/usr/bin/env bash

DOCS_DIR=Docs

rm -rf "${DOCS_DIR}"

BUNDLE_VERSION=$(defaults read `pwd`/Beaconsmind/Beaconsmind/Info.plist CFBundleShortVersionString)
echo "Setting up ${BUNDLE_VERSION}"

sed s/'${BUNDLE_VERSION}'/"${BUNDLE_VERSION}/" .jazzy.yaml.template > .jazzy.yaml

bundle exec jazzy
