//
//  ViewController.swift
//  Beaconsmind Demo App
//
//  Created by Bruno on 23.06.2021..
//

import UIKit
import Beaconsmind
import CoreLocation

class LoginViewController: BeaconsmindViewController {

    @IBOutlet var usernameField: UITextField!
    @IBOutlet var passwordField: UITextField!
    @IBOutlet var loginButton: UIButton!
    @IBOutlet var environmentField: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()

        environmentField.text = AppContext.default.host
    }

    @IBAction func onLoginTapped(_ sender: Any) {
        login()
    }

    private func login() {
        guard
            let username = usernameField.text, username.count > 0,
            let password = passwordField.text, password.count > 0
        else {
            showMessage(message: "Please enter username, password")
            return
        }

        startLoading()
        do {
            try Beaconsmind.default.login(username: username, password: password) { [weak self] result in
                self?.stopLoading()
                if case .failure = result {
                    self?.showMessage(message: "Couldn't log in, please check your credentials and try again")
                }
            }
        } catch {
            print(error)
            stopLoading()
        }
    }

    @IBAction func onChangeEnvironment(_ sender: Any) {
        guard
            let host = environmentField.text,
            let delegate = UIApplication.shared.delegate as? AppDelegate else
        {
            return
        }
        AppContext.default.update(host: host)
        delegate.restartBeaconsmind()
    }
}
