//
//  Copyright © 17.12.21 BeaconsmindSdkDemo. All rights reserved.
//

import UIKit
import Beaconsmind
import CoreLocation

class SignupViewController: BeaconsmindViewController {
    @IBOutlet var emailField: UITextField!
    @IBOutlet var firstNameField: UITextField!
    @IBOutlet var lastNameField: UITextField!
    @IBOutlet var passwordField: UITextField!
    @IBOutlet var confirmPasswordField: UITextField!
    @IBOutlet var languageField: UITextField!
    @IBOutlet var genderField: UITextField!
    @IBOutlet var storeIDField: UITextField!
    @IBOutlet var birthDateField: UIDatePicker!

    override func viewDidLoad() {
        super.viewDidLoad()

        birthDateField.maximumDate = Date()
    }

    @IBAction func onSignupTapped(_ sender: Any) {
        startSignup()
    }

    private func startSignup() {
        guard
            let email = emailField.text, email.count > 0,
            let firstName = firstNameField.text, firstName.count > 0,
            let lastName = lastNameField.text, lastName.count > 0,
            let password = passwordField.text, password.count > 0,
            let confirmPassword = confirmPasswordField.text, confirmPassword.count > 0
        else {
            showMessage(message: "Please enter email, first name, last name, password and password confirmation")
            return
        }

        /// Only sending birth date if it's not today, since date picker component is not too flexible
        let birthDate: DateDay? = Calendar.current.isDateInToday(birthDateField.date) ? nil : .init(date: birthDateField.date)

        let favoriteStoreID: Int? = storeIDField.text != nil ? Int(storeIDField.text!) : nil

        startLoading()
        do {
            try Beaconsmind.default.signup(
                username: email,
                firstName: firstName,
                lastName: lastName,
                password: password,
                confirmPassword: confirmPassword,
                language: languageField.text,
                gender: genderField.text,
                favoriteStoreID: favoriteStoreID,
                birthDate: birthDate) { [weak self] result in
                    self?.stopLoading()

                    if case let .failure(error) = result {
                        self?.showMessage(message: error.localizedDescription)
                    }
                }
        } catch {
            print(error)
            stopLoading()
        }
    }
}
