//
//  ViewController.swift
//  Beaconsmind Demo App
//

import UIKit
import Beaconsmind
import CoreLocation

class ResetPasswordViewController: BeaconsmindViewController {

    @IBOutlet var emailField: UITextField!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet var codeField: UITextField!
    @IBOutlet weak var codeLabel: UILabel!
    @IBOutlet var passwordField: UITextField!
    @IBOutlet weak var passwordLabel: UILabel!
    @IBOutlet var passwordConfirmationField: UITextField!
    @IBOutlet weak var passwordConfirmationLabel: UILabel!
    @IBOutlet var requestButton: UIButton!
    @IBOutlet var resetButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Password Reset"
        updateUI(codeSent: false)
    }

    @IBAction func onRequestPasswordResetTapped(_ sender: Any) {
        guard let email = emailField.text, !email.isEmpty else {
            showMessage(message: "Please enter your email")
            return
        }

        startLoading()
        do {
            _ = try Beaconsmind.default.resetPasswordRequest(email: email) { [weak self] result in
                self?.stopLoading()

                switch result {
                    case .success:
                        self?.updateUI(codeSent: true)
                    case .failure(let error):
                        self?.showMessage(message: "Couldn't request password reset, please check your email and try again: \(error.localizedDescription)")
                }
            }
        } catch {
            print(error)
            stopLoading()
        }
    }

    @IBAction func onResetPasswordTapped(_ sender: Any) {
        guard let email = emailField.text, !email.isEmpty else {
            showMessage(message: "Please enter your email")
            return
        }

        guard let code = codeField.text, !code.isEmpty else {
            showMessage(message: "Please enter the code you received")
            return
        }

        guard
            let password = passwordField.text, !password.isEmpty,
            let passwordConfirmation = passwordConfirmationField.text, !passwordConfirmation.isEmpty,
            password == passwordConfirmation
        else {
            showMessage(message: "Please enter to matching passwords")
            return
        }

        startLoading()
        do {
            _ = try Beaconsmind.default.resetPassword(
                email: email,
                password: password,
                code: code,
                confirmPassword: passwordConfirmation
            ) { [weak self] result in
                self?.handleResetPasswordResult(result: result)
            }
        } catch {
            print(error)
            stopLoading()
        }
    }

    private func handleResetPasswordResult(result: SDKApiResult<PasswordChangedResponse?>) {
        stopLoading()

        switch result {
            case .success:
                showMessage(message: "Your password has been successfully reset", okButtonText: "Go back") { [weak self] in
                    self?.navigationController?.popViewController(animated: true)
                }
            case .failure(let error):
                showMessage(message: "Couldn't request password reset, please check your email and try again: \(error.localizedDescription)")
        }
    }

    private func updateUI(codeSent: Bool) {
        let firstPartEnabled = !codeSent
        let firstPartOpacity: Float = firstPartEnabled ? 1 : 0.2
        let secondPartEnabled = codeSent
        let secondPartOpacity: Float = secondPartEnabled ? 1 : 0.2

        emailField.isEnabled = firstPartEnabled
        emailField.layer.opacity = firstPartOpacity
        emailLabel.layer.opacity = firstPartOpacity
        requestButton.isEnabled = firstPartEnabled
        requestButton.layer.opacity = firstPartOpacity

        codeField.isEnabled = secondPartEnabled
        codeField.layer.opacity = secondPartOpacity
        codeLabel.layer.opacity = secondPartOpacity
        passwordField.isEnabled = secondPartEnabled
        passwordField.layer.opacity = secondPartOpacity
        passwordLabel.layer.opacity = secondPartOpacity
        passwordConfirmationField.isEnabled = secondPartEnabled
        passwordConfirmationField.layer.opacity = secondPartOpacity
        passwordConfirmationLabel.layer.opacity = secondPartOpacity
        resetButton.isEnabled = secondPartEnabled
        resetButton.layer.opacity = secondPartOpacity
    }
}
