//
//  Copyright © 30.11.21 BeaconsmindSdkDemo. All rights reserved.
//

import UIKit
import Beaconsmind
import CoreLocation

class AccountImportViewController: BeaconsmindViewController {
    @IBOutlet var idField: UITextField!
    @IBOutlet var emailField: UITextField!
    @IBOutlet var firstNameField: UITextField!
    @IBOutlet var lastNameField: UITextField!
    @IBOutlet var birthDateField: UIDatePicker!
    @IBOutlet var languageField: UITextField!
    @IBOutlet var genderField: UITextField!
    @IBOutlet var deviceIDField: UITextField!
    @IBOutlet var platformField: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()

        birthDateField.maximumDate = Date()
    }

    @IBAction func onImportTapped(_ sender: Any) {
        startImport()
    }

    private func startImport() {
        guard
            let id = idField.text, id.count > 0,
            let email = emailField.text, email.count > 0
        else {
            showMessage(message: "Please enter id, email")
            return
        }

        /// Only sending birth date if it's not today, since date picker component is not too flexible
        let birthDate: DateTime? = Calendar.current.isDateInToday(birthDateField.date) ? nil : birthDateField.date

        startLoading()
        do {
            if
                let deviceID = deviceIDField.text, deviceID.count > 0,
                let platform = platformField.text, platform.count > 0
            {
                /// Sending with custom push notification token
                let deviceRegistration = DeviceRegistrationModel(
                    nativeId: deviceIDField.text,
                    platform: platformField.text
                )

                try Beaconsmind.default.importAccount(
                    id: id,
                    email: email,
                    firstName: firstNameField.text,
                    lastName: lastNameField.text,
                    birthDate: birthDate,
                    language: languageField.text,
                    gender: genderField.text,
                    deviceRegistration: deviceRegistration
                ) { [weak self] result in
                    guard let self = self else { return }
                    self.stopLoading()

                    if case let .failure(error) = result {
                        self.showMessage(message: error.localizedDescription)
                    }
                }
            } else {
                /// Sending default account import call
                try Beaconsmind.default.importAccount(
                    id: id,
                    email: email,
                    firstName: firstNameField.text,
                    lastName: lastNameField.text,
                    birthDate: birthDate,
                    language: languageField.text,
                    gender: genderField.text
                ) { [weak self] result in
                    guard let self = self else { return }
                    self.stopLoading()

                    if case let .failure(error) = result {
                        self.showMessage(message: error.localizedDescription)
                    }
                }
            }
        } catch {
            print(error)
            stopLoading()
        }
    }
}
