//
//  UIApplication+Beacons.swift
//  BeaconsmindSdkDemo
//
//  Created by Bruno on 14/07/2021.
//

import UIKit
import CoreLocation
import Beaconsmind

extension UIApplication {

    static var MainStoryboard: UIStoryboard = {
        UIStoryboard(name: "Main", bundle: Bundle(for: AppDelegate.self))
    }()

    static var AppStoryboard: UIStoryboard = {
        UIStoryboard(name: "App", bundle: Bundle(for: AppDelegate.self))
    }()

    var appDelegate: AppDelegate {
        delegate as! AppDelegate
    }
}

extension Notification.Name {
    static let receivedOffer = Notification.Name("received-offer-push")
}

extension BeaconContactsSummary {

    var lastContactString: String {
        guard let timestamp = timestamp else {
            return ""
        }
        return Date(milliseconds: timestamp).timeAgo()
    }

    var infoString: String {
        guard timestamp != nil else {
            return ""
        }
        return
"""
Contacts: \(contactCount) times, \(frequency) per minute
Signal: \(rssi?.dbString ?? "-") (avg \(averageRSSI?.dbString ?? "-"))
"""
    }
}

extension Date {

    func timeAgo() -> String {
        let formatter = DateComponentsFormatter()
        formatter.unitsStyle = .full
        formatter.allowedUnits = [.year, .month, .day, .hour, .minute, .second]
        formatter.zeroFormattingBehavior = .dropAll
        formatter.maximumUnitCount = 1
        return "\(String(format: formatter.string(from: self, to: Date()) ?? "", locale: .current)) ago"
    }
}

extension Double {

    var dbString: String {
        String(format: "%.0f Db", self)
    }
}
