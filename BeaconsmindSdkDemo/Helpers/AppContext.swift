//
//  Copyright © 2022 beaconsmind. All rights reserved.
//

import Foundation

struct AppContext {

    static let `default` = AppContext()

    private let defaults = UserDefaults.standard
    private let storeKeyHost = "key.com.beaconsmind.demo.hostname"

    var host: String {
        if let host = defaults.string(forKey: storeKeyHost) {
            return host
        }
        return "https://test-develop-suite.azurewebsites.net"
    }

    func update(host: String) {
        defaults.set(host, forKey: storeKeyHost)
        defaults.synchronize()
    }
}
