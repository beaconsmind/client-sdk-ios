//
//  Copyright © 18.12.21 BeaconsmindSdkDemo. All rights reserved.
//

import UIKit
import Beaconsmind

class ProfileViewController: BeaconsmindViewController {

    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var urlLabel: UILabel!
    @IBOutlet weak var firstNameField: UITextField!
    @IBOutlet weak var lastNameField: UITextField!
    @IBOutlet weak var birthDateField: UIDatePicker!
    @IBOutlet weak var cityField: UITextField!
    @IBOutlet weak var countryField: UITextField!
    @IBOutlet weak var pushSwitch: UISwitch!
    @IBOutlet weak var storeField: UITextField!
    @IBOutlet weak var genderField: UITextField!
    @IBOutlet weak var houseNumberField: UITextField!
    @IBOutlet weak var landlinePhoneField: UITextField!
    @IBOutlet weak var languageField: UITextField!
    @IBOutlet weak var phoneNumberField: UITextField!
    @IBOutlet weak var streetField: UITextField!
    @IBOutlet weak var zipCodeField: UITextField!

    var textFields: [UITextField] {
        [firstNameField,
         lastNameField,
         languageField,
         genderField,
         landlinePhoneField,
         phoneNumberField,
         storeField,
         countryField,
         cityField,
         streetField,
         houseNumberField,
         zipCodeField]
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Profile"

        birthDateField.maximumDate = Date()
        
        textFields.forEach { $0.delegate = self }
        textFields.forEach { $0.returnKeyType = .continue }
        textFields.last?.returnKeyType = .go
        
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard)))
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        loadProfile()
    }

    func loadProfile() {
        startLoading()

        do {
            _ = try Beaconsmind.default.getProfile(callback: { [weak self] result in
                switch result {
                case let .success(profile):
                    self?.handleProfileResponse(profile: profile)

                case let .failure(error):
                    self?.showMessage(message: error.localizedDescription)
                }
                self?.stopLoading()
            })
        } catch {
            stopLoading()
            showMessage(message: error.localizedDescription)
        }
    }

    @IBAction func onUpdateButtonTap(_ sender: Any) {
        guard
            let firstName = firstNameField.text, firstName.count > 0,
            let lastName = lastNameField.text, lastName.count > 0
        else {
            showMessage(message: "Please enter first name and last name")
            return
        }

        /// Only sending birth date if it's not today, since date picker component is not too flexible
        let birthDate: DateTime? = Calendar.current.isDateInToday(birthDateField.date) ? nil : birthDateField.date

        var favoriteStoreID: Int? = nil
        if let storeID = storeField.text, storeID.count > 0 {
            favoriteStoreID = Int(storeID)
        }

        dismissKeyboard()
        startLoading()
        do {
            _ = try Beaconsmind.default.updateProfile(
                firstName: firstName,
                lastName: lastName,
                birthDate: birthDate,
                city: cityField.text,
                country: countryField.text,
                disablePushNotifications: !pushSwitch.isOn,
                favoriteStoreID: favoriteStoreID,
                gender: genderField.text,
                houseNumber: houseNumberField.text,
                landlinePhone: landlinePhoneField.text,
                language: languageField.text,
                phoneNumber: phoneNumberField.text,
                street: streetField.text,
                zipCode: zipCodeField.text
            ) { [weak self] result in
                switch result {
                case let .success(profile):
                    self?.handleProfileResponse(profile: profile)

                case let .failure(error):
                    self?.showMessage(message: error.localizedDescription)
                }
                self?.stopLoading()
            }
        } catch  {
            stopLoading()
            showMessage(message: error.localizedDescription)
        }
    }

    // MARK: Helpers

    private func handleProfileResponse(profile: ProfileResponse?) {
        var favoriteStoreID = ""
        if let storeID = profile?.favoriteStoreId {
            favoriteStoreID = "\(storeID)"
        }

        emailLabel.text = profile?.userName
        firstNameField.text = profile?.firstName
        lastNameField.text = profile?.lastName
        birthDateField.date = profile?.birthDate ?? Date()
        languageField.text = profile?.language
        genderField.text = profile?.gender
        landlinePhoneField.text = profile?.landlinePhone
        phoneNumberField.text = profile?.phoneNumber
        storeField.text = favoriteStoreID
        pushSwitch.isOn = !(profile?.disablePushNotifications ?? true)
        countryField.text = profile?.country
        cityField.text = profile?.city
        streetField.text = profile?.street
        houseNumberField.text = profile?.houseNumber
        zipCodeField.text = profile?.zipCode
        urlLabel.text = profile?.url
    }
}

extension ProfileViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let selectedTextFieldIndex = textFields.firstIndex(of: textField), selectedTextFieldIndex < textFields.count - 1 {
            textFields[selectedTextFieldIndex + 1].becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        return true
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
