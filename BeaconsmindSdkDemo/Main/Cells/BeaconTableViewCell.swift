//
//  Copyright © 30.11.21 BeaconsmindSdkDemo. All rights reserved.
//

import UIKit
import Beaconsmind

class BeaconTableViewCell: UITableViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var uuidLabel: UILabel!
    @IBOutlet weak var storeLabel: UILabel!
    @IBOutlet weak var lastRangedLabel: UILabel!
    @IBOutlet weak var detailedInfoLabel: UILabel!
    @IBOutlet weak var icon: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()

        backgroundColor = .clear
        selectionStyle = .none

        icon.tintColor = Theme.colors.primary
        containerView.backgroundColor = Theme.colors.whiteToBlack
        uuidLabel.textColor = Theme.colors.secondary1

        containerView.clipsToBounds = true
        containerView.layer.cornerRadius = 5

        contentView.layer.shadowOpacity = 0.25
        contentView.layer.shadowOffset = CGSize(width: 0, height: 1)
        contentView.layer.shadowColor = Theme.colors.blackToWhite.cgColor
        contentView.layer.shadowRadius = 2
    }

    func updateData(summary: BeaconContactsSummary) {
        titleLabel.text = summary.name
        uuidLabel.text = summary.uuid
        storeLabel.text = summary.store

        if summary.isInRange {
            icon.tintColor = Theme.colors.blackToWhite
            containerView.backgroundColor = .green.withAlphaComponent(0.5)
            uuidLabel.textColor = Theme.colors.blackToWhite
        } else {
            icon.tintColor = Theme.colors.primary
            containerView.backgroundColor = Theme.colors.whiteToBlack
            uuidLabel.textColor = Theme.colors.secondary1
        }

        lastRangedLabel.text = summary.lastContactString
        detailedInfoLabel.text = summary.infoString
    }
}
