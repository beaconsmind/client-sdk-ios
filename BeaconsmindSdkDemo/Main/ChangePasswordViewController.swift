//
//  Copyright © 18.12.21 BeaconsmindSdkDemo. All rights reserved.
//

import UIKit
import Beaconsmind

class ChangePasswordViewController: BeaconsmindViewController {

    @IBOutlet weak var oldPasswordField: UITextField!
    @IBOutlet weak var newPasswordField: UITextField!
    @IBOutlet weak var newPasswordConfirmationField: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Change password"
    }

    @IBAction func onChangePasswordTap(_ sender: Any) {
        guard
            let oldPassword = oldPasswordField.text, !oldPassword.isEmpty,
            let newPassword = newPasswordField.text, !newPassword.isEmpty,
            let newPasswordConfirmation = newPasswordConfirmationField.text, !newPasswordConfirmation.isEmpty,
            newPassword == newPasswordConfirmation
        else {
            showMessage(message: "Please enter your old password and two matching new passwords")
            return
        }

        startLoading()
        do {
            _ = try Beaconsmind.default.changePassword(
                newPassword: newPassword,
                oldPassword: oldPassword
            ) { [weak self] result in
                self?.stopLoading()
                switch result {
                    case .success:
                        self?.showMessage(message: "Your password was successfully changed")

                    case .failure(let error):
                        self?.showMessage(message: "Error changing your password: \(error.localizedDescription)")
                }
            }
        } catch  {
            stopLoading()
            showMessage(message: error.localizedDescription)
        }
    }
}
