//
//  Copyright © 2022 beaconsmind. All rights reserved.
//

import UIKit
import Beaconsmind

class SettingsViewController: BeaconsmindViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Settings"
    }

    @IBAction func deleteAccountTap(_ sender: Any) {
        startLoading()
        do {
            try Beaconsmind.default.deleteAccount(callback: { [weak self] result in
                guard let self = self else {
                    return
                }
                
                if case let .failure(error) = result {
                    self.showMessage(message: error.localizedDescription)
                }
                self.stopLoading()
            })
        }
        catch {
            stopLoading()
            showMessage(message: error.localizedDescription)
        }
    }
    
    @IBAction func onSignOutTap(_ sender: Any) {
        startLoading()
        do {
            try Beaconsmind.default.logout(callback: { [weak self] result in
                guard let self = self else {
                    return
                }
                if case let .failure(error) = result {
                    self.showMessage(message: error.localizedDescription)
                }
                self.stopLoading()
            })
        } catch  {
            stopLoading()
            showMessage(message: error.localizedDescription)
        }
    }
}
