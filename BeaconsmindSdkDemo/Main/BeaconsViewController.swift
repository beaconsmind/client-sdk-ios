//
//  BeaconsViewController.swift
//  BeaconsmindSdkDemo
//
//  Created by Bruno on 09.07.2021..
//

import UIKit
import Beaconsmind
import CoreLocation

fileprivate let CellType = "BeaconTableViewCell"

class BeaconsViewController: BeaconsmindViewController {

    @IBOutlet var tableView: UITableView!

    private var list: [BeaconContactsSummary] = []
    private var refreshTimer: Timer?
    private let refreshInterval: TimeInterval = 1

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Beacons"

        tableView.dataSource = self
        tableView.delegate = self
        tableView.separatorStyle = .none
        tableView.estimatedRowHeight = 100
        tableView.separatorStyle = .none
        tableView.rowHeight = UITableView.automaticDimension
        tableView.contentInset.top = 4
        tableView.contentInset.bottom = 100
        tableView.showsVerticalScrollIndicator = false
        tableView.backgroundColor = UIColor.groupTableViewBackground

        tableView.register(
            UINib(nibName: CellType, bundle: Bundle.main),
            forCellReuseIdentifier: CellType
        )
    }

    deinit {
        refreshTimer?.invalidate()
        refreshTimer = nil
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        startLoading()
        refreshTimer = Timer.scheduledTimer(withTimeInterval: refreshInterval, repeats: true, block: { [weak self] _ in
            guard let self = self else {
                return
            }
            self.list = Beaconsmind.default.beaconContactsSummaries
            self.tableView.reloadData()

            if self.list.count != 0 {
                self.stopLoading()
            }
        })
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        refreshTimer?.invalidate()
        refreshTimer = nil
    }
}

extension BeaconsViewController: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        list.count
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CellType, for: indexPath) as! BeaconTableViewCell
        cell.updateData(summary: list[indexPath.row])
        return cell
    }
}
