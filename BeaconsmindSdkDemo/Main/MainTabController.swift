//
//  Copyright © 13.12.21 BeaconsmindSdkDemo. All rights reserved.
//

import UIKit
import Beaconsmind

class MainTabController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        Beaconsmind.default.requestLocationPermission { enabled in
            if enabled {
                DispatchQueue.main.async {
                    try? Beaconsmind.default.startListeningBeacons()
                }
            } else {
                print("TODO: No location permission given, update UI")
            }
        }

        NotificationCenter.default.addObserver(forName: .receivedOffer, object: nil, queue: .main) { [weak self] notification in
            guard let self = self else {
                return
            }
            if
                let id = notification.object as? Int,
                let controllers = self.viewControllers,
                controllers.count > 1,
                let offerList = controllers[1] as? UINavigationController
            {
                offerList.pushViewController(OfferDetailViewController(id: id), animated: true)
                self.selectedIndex = 1
            }
        }
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}
