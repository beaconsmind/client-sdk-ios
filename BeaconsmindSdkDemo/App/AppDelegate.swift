//
//  AppDelegate.swift
//  Beaconsmind Demo App
//
//  Created by Bruno on 23.06.2021..
//

import UIKit
import Beaconsmind

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    private var appVersion: String {
        Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String ?? "1.0"
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        UNUserNotificationCenter.current().delegate = self
        Theme.setUp()

        Beaconsmind.default.setMinLogLevel(level: .debug)
        BeaconsmindUI.default.styleSheet = StyleSheet(
            backgroundColor: Theme.colors.whiteToBlack,
            redeemButtonTitleColor: Theme.colors.whiteToBlack,
            redeemButtonBackgroundColor: Theme.colors.blackToWhite,
            primaryButtonTitleColor: Theme.colors.whiteToBlack,
            primaryButtonBackgroundColor: Theme.colors.blackToWhite,
            buttonCornerRadius: 8,
            loadingViewContentColor: Theme.colors.whiteToBlack,
            loadingViewBackgroundColor: Theme.colors.blackToWhite,
            cellTextColor: Theme.colors.blackToWhite,
            cellShadowColor: Theme.colors.blackToGray,
            cellCornerRadius: 8
        )

        restartBeaconsmind()
        return true
    }

    func restartBeaconsmind() {
        print("[AppDelegate] Starting Beaconsmind with host: \(AppContext.default.host)")
        do {
            try Beaconsmind.default.start(
                delegate: self,
                appVersion: appVersion,
                hostname: AppContext.default.host
            )
        } catch {
            print(error)
        }
    }

    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        do {
            try Beaconsmind.default.register(deviceToken: deviceToken)
        } catch {
            print(error.localizedDescription)
        }
    }

    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed registering remote notifications: \(error.localizedDescription)")
    }
}

// MARK: - BeaconsmindDelegate

extension AppDelegate: BeaconsmindDelegate {

    func beaconsmind(_ beaconsmind: Beaconsmind, onContextChanged context: APIContext?) {
        let mainController: UIViewController?
        if context != nil {
            mainController = UIApplication.AppStoryboard.instantiateInitialViewController()
            Beaconsmind.default.registerForPushNotifications()
        } else {
            mainController = UIApplication.MainStoryboard.instantiateInitialViewController()
        }

        let window = UIWindow(frame: UIScreen.main.bounds)
        window.rootViewController = mainController
        self.window = window
        window.makeKeyAndVisible()
    }
}

// MARK: - UNUserNotificationCenterDelegate

extension AppDelegate: UNUserNotificationCenterDelegate {
    /// If remote notifications background mode is enabled, then it's invoked when push notification
    /// is delivered while the app is in the killed, suspended or background state.
    /// 
    /// From the official documentation:
    ///
    /// "[...]  system calls this method when your app is running in the foreground or background.
    /// In addition, if you enabled the remote notifications background mode, the system launches your
    /// app (or wakes it from the suspended state) and puts it in the background state when a remote
    /// notification arrives.
    ///
    /// However, the system does not automatically launch your  app if the user has force-quit it.
    /// In that situation, the user must relaunch your app or restart the device before the system
    /// attempts to launch your app automatically again."
    ///
    /// https://developer.apple.com/documentation/uikit/uiapplicationdelegate/1623013-application
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) async -> UIBackgroundFetchResult {
        do {
            let notification = userInfo
            
            let offer = try OfferPayload.tryParse(data: notification)
            try Beaconsmind.default.handleReceivedPushNotification(offer: offer)
        } catch {
            /// When deserialization fails, it means that the notification does not come from Beaconsmind.
            print(error.localizedDescription)
        }
        
        return .newData
    }
    
    /// Invoked when user interacts with the delivered push notification, e.g. taps on it.
    /// 
    /// From the official documentation:
    /// 
    /// "Asks the delegate to process the user's response to a delivered notification."
    /// 
    /// https://developer.apple.com/documentation/usernotifications/unusernotificationcenterdelegate/1649501-usernotificationcenter
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        /// Needs some delay to make sure the app is displayed before handling the notification
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [weak self] in
            do {
                let notification = response.notification.request.content.userInfo
                
                let offer = try OfferPayload.tryParse(data: notification)
                try Beaconsmind.default.handleReceivedPushNotification(offer: offer)
            } catch {
                /// When deserialization fails, it means that the notification does not come from Beaconsmind.
                print(error.localizedDescription)
            }
        }
        completionHandler()
    }

    /// Invoked when push notification is delivered while the app is in the foreground.
    ///
    /// From the official documentation:
    /// 
    /// "Asks the delegate how to handle a notification that arrived while the app was running in the foreground."
    /// 
    /// https://developer.apple.com/documentation/usernotifications/unusernotificationcenterdelegate/1649518-usernotificationcenter
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        do {
            let notification = notification.request.content.userInfo
            
            let offer = try OfferPayload.tryParse(data: notification)
            try Beaconsmind.default.handleReceivedPushNotification(offer: offer)
        } catch {
            /// When deserialization fails, it means that the notification does not come from Beaconsmind.
            print(error.localizedDescription)
        }
        
        /// .list - Show the notification in Notification Center.
        /// .sound - Play the sound associated with the notification.
        if #available(iOS 14.0, *) {
            completionHandler([.list, .sound, .banner])
        } else {
            completionHandler([.sound, .alert])
        }
    }
}
