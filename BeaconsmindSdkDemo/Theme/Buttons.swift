//
//  Copyright © 2022 beaconsmind. All rights reserved.
//

import UIKit

class ActionButton: UIButton {

    override func awakeFromNib() {
        super.awakeFromNib()

        layer.cornerRadius = 5
        backgroundColor = Theme.colors.primary
        setTitleColor(Theme.colors.whiteToBlack, for: .normal)
    }
}

class BorderedButton: UIButton {

    override func awakeFromNib() {
        super.awakeFromNib()

        layer.cornerRadius = 5
        layer.borderWidth = 2
        layer.borderColor = Theme.colors.primary.cgColor
        backgroundColor = Theme.colors.whiteToBlack
        setTitleColor(Theme.colors.primary, for: .normal)
    }
}
