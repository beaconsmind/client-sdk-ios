//
//  Copyright © 2022 beaconsmind. All rights reserved.
//

import UIKit

struct Theme {

    static let colors = Colors()

    static func setUp() {
        if #available(iOS 15, *) {
            UINavigationBar.appearance().scrollEdgeAppearance = UINavigationBarAppearance()
        }

        if #available(iOS 13.0, *) {
            let navBarAppearance = UINavigationBarAppearance()
            navBarAppearance.configureWithOpaqueBackground()
            navBarAppearance.titleTextAttributes = [.foregroundColor: colors.whiteToBlack]
            navBarAppearance.backgroundColor = colors.primary
            UINavigationBar.appearance().standardAppearance = navBarAppearance
            UINavigationBar.appearance().scrollEdgeAppearance = navBarAppearance
        } else {
            UINavigationBar.appearance().barTintColor = colors.primary
            UINavigationBar.appearance().titleTextAttributes = [.foregroundColor: colors.whiteToBlack]
            UINavigationBar.appearance().tintColor = colors.whiteToBlack
        }

        UIButton.appearance().tintColor = colors.primary

        UITextField.appearance().tintColor = colors.primary

        UITabBar.appearance().barTintColor = colors.whiteToBlack
        UITabBar.appearance().tintColor = colors.primary
        UITabBar.appearance().unselectedItemTintColor = colors.secondary1

        UISwitch.appearance().onTintColor = colors.primary

        UIButton.appearance(whenContainedInInstancesOf: [UINavigationController.self]).tintColor = colors.whiteToBlack

        UIView.appearance().tintColor = colors.primary
    }
}
