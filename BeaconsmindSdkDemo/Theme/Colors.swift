//
//  Copyright © 2022 beaconsmind. All rights reserved.
//

import UIKit

struct Colors {

    let blackToGray: UIColor
    let blackToWhite: UIColor
    let secondary1: UIColor
    let primary: UIColor
    let whiteToBlack: UIColor

    init() {
        blackToGray = Colors.color(name: "blackToGray", fallback: .black)
        blackToWhite = Colors.color(name: "blackToWhite", fallback: .black)
        secondary1 = Colors.color(name: "secondary1", fallback: .gray)
        primary = Colors.color(name: "primary", fallback: .green)
        whiteToBlack = Colors.color(name: "whiteToBlack", fallback: .white)
    }

    private static func color(name: String, fallback: UIColor) -> UIColor {
        if #available(iOS 11.0, *) {
            return UIColor(named: "base/\(name)") ?? fallback
        }
        return fallback
    }
}
