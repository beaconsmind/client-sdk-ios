//
// BeaconsmindAPI
// Generated by SwagGen
//

import Foundation

extension API.Configuration {

    /** Return a list of configured beacons. */
    public enum ConfigurationGetBeacons {

        public static let service = APIService<Response>(id: "Configuration_GetBeacons", tag: "Configuration", method: "GET", path: "/api/configuration/beacons", hasBody: false, securityRequirements: [])

        public final class Request: APIRequest<Response> {

            public init() {
                super.init(service: ConfigurationGetBeacons.service)
            }
        }

        public enum Response: APIResponseValue, CustomStringConvertible, CustomDebugStringConvertible {
            public typealias SuccessType = [StoreBeaconInfo]

            /** Returns the list of beacons */
            case status200([StoreBeaconInfo])

            public var success: [StoreBeaconInfo]? {
                switch self {
                case .status200(let response): return response
                }
            }

            public var response: Any {
                switch self {
                case .status200(let response): return response
                }
            }

            public var statusCode: Int {
                switch self {
                case .status200: return 200
                }
            }

            public var successful: Bool {
                switch self {
                case .status200: return true
                }
            }

            public init(statusCode: Int, data: Data, decoder: ResponseDecoder) throws {
                switch statusCode {
                case 200: self = try .status200(decoder.decode([StoreBeaconInfo].self, from: data))
                default: throw APIClientError.unexpectedStatusCode(statusCode: statusCode, data: data)
                }
            }

            public var description: String {
                return "\(statusCode) \(successful ? "success" : "failure")"
            }

            public var debugDescription: String {
                var string = description
                let responseString = "\(response)"
                if responseString != "()" {
                    string += "\n\(responseString)"
                }
                return string
            }
        }
    }
}
