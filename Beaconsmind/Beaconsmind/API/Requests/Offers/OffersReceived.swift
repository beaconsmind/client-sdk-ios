//
// BeaconsmindAPI
// Generated by SwagGen
//

import Foundation

extension API.Offers {

    /** Marks the notification as Received. */
    public enum OffersReceived {

        public static let service = APIService<Response>(id: "Offers_Received", tag: "Offers", method: "POST", path: "/api/offers/received", hasBody: true, securityRequirements: [])

        public final class Request: APIRequest<Response> {

            public var body: OfferStatusRequest?

            public init(body: OfferStatusRequest?, encoder: RequestEncoder? = nil) {
                self.body = body
                super.init(service: OffersReceived.service) { defaultEncoder in
                    return try (encoder ?? defaultEncoder).encode(body)
                }
            }
        }

        public enum Response: APIResponseValue, CustomStringConvertible, CustomDebugStringConvertible {
            public typealias SuccessType = Void

            /** Empty */
            case status200

            public var success: Void? {
                switch self {
                case .status200: return ()
                }
            }

            public var response: Any {
                switch self {
                default: return ()
                }
            }

            public var statusCode: Int {
                switch self {
                case .status200: return 200
                }
            }

            public var successful: Bool {
                switch self {
                case .status200: return true
                }
            }

            public init(statusCode: Int, data: Data, decoder: ResponseDecoder) throws {
                switch statusCode {
                case 200: self = .status200
                default: throw APIClientError.unexpectedStatusCode(statusCode: statusCode, data: data)
                }
            }

            public var description: String {
                return "\(statusCode) \(successful ? "success" : "failure")"
            }

            public var debugDescription: String {
                var string = description
                let responseString = "\(response)"
                if responseString != "()" {
                    string += "\n\(responseString)"
                }
                return string
            }
        }
    }
}
