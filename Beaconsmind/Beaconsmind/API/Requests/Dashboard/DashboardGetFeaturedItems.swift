//
// BeaconsmindAPI
// Generated by SwagGen
//

import Foundation

extension API.Dashboard {

    public enum DashboardGetFeaturedItems {

        public static let service = APIService<Response>(id: "Dashboard_GetFeaturedItems", tag: "Dashboard", method: "GET", path: "/api/v1/dashboard/featured-items", hasBody: false, securityRequirements: [])

        public final class Request: APIRequest<Response> {

            public struct Options {

                public var search: String?

                public var skip: Int?

                public var limit: Int?

                public init(search: String? = nil, skip: Int? = nil, limit: Int? = nil) {
                    self.search = search
                    self.skip = skip
                    self.limit = limit
                }
            }

            public var options: Options

            public init(options: Options) {
                self.options = options
                super.init(service: DashboardGetFeaturedItems.service)
            }

            /// convenience initialiser so an Option doesn't have to be created
            public convenience init(search: String? = nil, skip: Int? = nil, limit: Int? = nil) {
                let options = Options(search: search, skip: skip, limit: limit)
                self.init(options: options)
            }

            public override var queryParameters: [String: Any] {
                var params: [String: Any] = [:]
                if let search = options.search {
                  params["search"] = search
                }
                if let skip = options.skip {
                  params["skip"] = skip
                }
                if let limit = options.limit {
                  params["limit"] = limit
                }
                return params
            }
        }

        public enum Response: APIResponseValue, CustomStringConvertible, CustomDebugStringConvertible {
            public typealias SuccessType = [FeaturedItemView]

            /** Success */
            case status200([FeaturedItemView])

            public var success: [FeaturedItemView]? {
                switch self {
                case .status200(let response): return response
                }
            }

            public var response: Any {
                switch self {
                case .status200(let response): return response
                }
            }

            public var statusCode: Int {
                switch self {
                case .status200: return 200
                }
            }

            public var successful: Bool {
                switch self {
                case .status200: return true
                }
            }

            public init(statusCode: Int, data: Data, decoder: ResponseDecoder) throws {
                switch statusCode {
                case 200: self = try .status200(decoder.decode([FeaturedItemView].self, from: data))
                default: throw APIClientError.unexpectedStatusCode(statusCode: statusCode, data: data)
                }
            }

            public var description: String {
                return "\(statusCode) \(successful ? "success" : "failure")"
            }

            public var debugDescription: String {
                var string = description
                let responseString = "\(response)"
                if responseString != "()" {
                    string += "\n\(responseString)"
                }
                return string
            }
        }
    }
}
