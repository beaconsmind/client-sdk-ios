//
// BeaconsmindAPI
// Generated by SwagGen
//

import Foundation

public class AppSettings: APIModel {

    public var beacons: [StoreBeaconInfo]?

    public var getInTouchUrl: String?

    public var stores: [StoreInfo]?

    public var termsAndConditionsUrl: String?

    public init(beacons: [StoreBeaconInfo]? = nil, getInTouchUrl: String? = nil, stores: [StoreInfo]? = nil, termsAndConditionsUrl: String? = nil) {
        self.beacons = beacons
        self.getInTouchUrl = getInTouchUrl
        self.stores = stores
        self.termsAndConditionsUrl = termsAndConditionsUrl
    }

    public required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: StringCodingKey.self)

        beacons = try container.decodeArrayIfPresent("beacons")
        getInTouchUrl = try container.decodeIfPresent("getInTouchUrl")
        stores = try container.decodeArrayIfPresent("stores")
        termsAndConditionsUrl = try container.decodeIfPresent("termsAndConditionsUrl")
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: StringCodingKey.self)

        try container.encodeIfPresent(beacons, forKey: "beacons")
        try container.encodeIfPresent(getInTouchUrl, forKey: "getInTouchUrl")
        try container.encodeIfPresent(stores, forKey: "stores")
        try container.encodeIfPresent(termsAndConditionsUrl, forKey: "termsAndConditionsUrl")
    }

    public func isEqual(to object: Any?) -> Bool {
      guard let object = object as? AppSettings else { return false }
      guard self.beacons == object.beacons else { return false }
      guard self.getInTouchUrl == object.getInTouchUrl else { return false }
      guard self.stores == object.stores else { return false }
      guard self.termsAndConditionsUrl == object.termsAndConditionsUrl else { return false }
      return true
    }

    public static func == (lhs: AppSettings, rhs: AppSettings) -> Bool {
        return lhs.isEqual(to: rhs)
    }
}
