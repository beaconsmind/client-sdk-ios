//
// BeaconsmindAPI
// Generated by SwagGen
//

import Foundation

public class CustomerCardModel: APIModel {

    public var barcodeData: String

    public var barcodeType: BarcodeType

    public var name: String

    public var loyaltyPoints: LoyaltyPoints?

    public init(barcodeData: String, barcodeType: BarcodeType, name: String, loyaltyPoints: LoyaltyPoints? = nil) {
        self.barcodeData = barcodeData
        self.barcodeType = barcodeType
        self.name = name
        self.loyaltyPoints = loyaltyPoints
    }

    public required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: StringCodingKey.self)

        barcodeData = try container.decode("barcodeData")
        barcodeType = try container.decode("barcodeType")
        name = try container.decode("name")
        loyaltyPoints = try container.decodeIfPresent("loyaltyPoints")
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: StringCodingKey.self)

        try container.encode(barcodeData, forKey: "barcodeData")
        try container.encode(barcodeType, forKey: "barcodeType")
        try container.encode(name, forKey: "name")
        try container.encodeIfPresent(loyaltyPoints, forKey: "loyaltyPoints")
    }

    public func isEqual(to object: Any?) -> Bool {
      guard let object = object as? CustomerCardModel else { return false }
      guard self.barcodeData == object.barcodeData else { return false }
      guard self.barcodeType == object.barcodeType else { return false }
      guard self.name == object.name else { return false }
      guard self.loyaltyPoints == object.loyaltyPoints else { return false }
      return true
    }

    public static func == (lhs: CustomerCardModel, rhs: CustomerCardModel) -> Bool {
        return lhs.isEqual(to: rhs)
    }
}
