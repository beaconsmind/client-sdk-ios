//
// BeaconsmindAPI
// Generated by SwagGen
//

import Foundation

public class SubscribeRequest: APIModel {

    public var phoneOS: PhoneOS

    public var platformType: PlatformType

    public var phoneManufacturer: String?

    public var phoneModel: String?

    public var phoneOsVersion: String?

    /** Device id for the notification platform (FCM token, APNS token) */
    public var platformId: String?

    public init(phoneOS: PhoneOS, platformType: PlatformType, phoneManufacturer: String? = nil, phoneModel: String? = nil, phoneOsVersion: String? = nil, platformId: String? = nil) {
        self.phoneOS = phoneOS
        self.platformType = platformType
        self.phoneManufacturer = phoneManufacturer
        self.phoneModel = phoneModel
        self.phoneOsVersion = phoneOsVersion
        self.platformId = platformId
    }

    public required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: StringCodingKey.self)

        phoneOS = try container.decode("phoneOS")
        platformType = try container.decode("platformType")
        phoneManufacturer = try container.decodeIfPresent("phoneManufacturer")
        phoneModel = try container.decodeIfPresent("phoneModel")
        phoneOsVersion = try container.decodeIfPresent("phoneOsVersion")
        platformId = try container.decodeIfPresent("platformId")
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: StringCodingKey.self)

        try container.encode(phoneOS, forKey: "phoneOS")
        try container.encode(platformType, forKey: "platformType")
        try container.encodeIfPresent(phoneManufacturer, forKey: "phoneManufacturer")
        try container.encodeIfPresent(phoneModel, forKey: "phoneModel")
        try container.encodeIfPresent(phoneOsVersion, forKey: "phoneOsVersion")
        try container.encodeIfPresent(platformId, forKey: "platformId")
    }

    public func isEqual(to object: Any?) -> Bool {
      guard let object = object as? SubscribeRequest else { return false }
      guard self.phoneOS == object.phoneOS else { return false }
      guard self.platformType == object.platformType else { return false }
      guard self.phoneManufacturer == object.phoneManufacturer else { return false }
      guard self.phoneModel == object.phoneModel else { return false }
      guard self.phoneOsVersion == object.phoneOsVersion else { return false }
      guard self.platformId == object.platformId else { return false }
      return true
    }

    public static func == (lhs: SubscribeRequest, rhs: SubscribeRequest) -> Bool {
        return lhs.isEqual(to: rhs)
    }
}
