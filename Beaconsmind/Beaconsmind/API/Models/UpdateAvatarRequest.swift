//
// BeaconsmindAPI
// Generated by SwagGen
//

import Foundation

public class UpdateAvatarRequest: APIModel {

    public var thumbnailUrl: String?

    public var url: String?

    public init(thumbnailUrl: String? = nil, url: String? = nil) {
        self.thumbnailUrl = thumbnailUrl
        self.url = url
    }

    public required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: StringCodingKey.self)

        thumbnailUrl = try container.decodeIfPresent("thumbnailUrl")
        url = try container.decodeIfPresent("url")
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: StringCodingKey.self)

        try container.encodeIfPresent(thumbnailUrl, forKey: "thumbnailUrl")
        try container.encodeIfPresent(url, forKey: "url")
    }

    public func isEqual(to object: Any?) -> Bool {
      guard let object = object as? UpdateAvatarRequest else { return false }
      guard self.thumbnailUrl == object.thumbnailUrl else { return false }
      guard self.url == object.url else { return false }
      return true
    }

    public static func == (lhs: UpdateAvatarRequest, rhs: UpdateAvatarRequest) -> Bool {
        return lhs.isEqual(to: rhs)
    }
}
