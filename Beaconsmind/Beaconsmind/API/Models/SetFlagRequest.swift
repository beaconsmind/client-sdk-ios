//
// BeaconsmindAPI
// Generated by SwagGen
//

import Foundation

public class SetFlagRequest: APIModel {

    public var value: Bool

    public init(value: Bool) {
        self.value = value
    }

    public required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: StringCodingKey.self)

        value = try container.decode("value")
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: StringCodingKey.self)

        try container.encode(value, forKey: "value")
    }

    public func isEqual(to object: Any?) -> Bool {
      guard let object = object as? SetFlagRequest else { return false }
      guard self.value == object.value else { return false }
      return true
    }

    public static func == (lhs: SetFlagRequest, rhs: SetFlagRequest) -> Bool {
        return lhs.isEqual(to: rhs)
    }
}
