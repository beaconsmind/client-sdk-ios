//
//  Copyright © 01.12.21 Beaconsmind. All rights reserved.
//

import Foundation
import Alamofire
import BugfenderSDK

protocol ContextManagerDelegate: AnyObject {

    func contextManager(_ manager: ContextManager, savedContext context: APIContext?)
    func contextManager(_ manager: ContextManager, restoredContext context: APIContext?)
    func contextManagerCouldNotRestoreContext(_ manager: ContextManager)
    func contextManagerClearedContext(_ manager: ContextManager)
}

protocol ContextManager {

    var hostname: String? { get }
    var context: APIContext? { get }

    func updateHostname(hostname: String)
    func updateHostname(hostname: String, token: String) throws
    func saveContext(context: APIContext) throws
    func restoreContext() throws
    func clearContext() throws
    func getUnauthenticatedAPIClient() -> APIClient?
    func getAPIClient() -> APIClient?
    func setDelegate(delegate: ContextManagerDelegate?)
}

final class ContextManagerImpl: ContextManager {

    private(set) var hostname: String?
    private(set) var context: APIContext?
    private var clients: [APIContext: APIClient] = [:]
    private let store: Store
    /// Needs to be internal so we can test the SDK
    weak var delegate: ContextManagerDelegate?

    init(store: Store, delegate: ContextManagerDelegate? = nil) {
        self.store = store
        self.delegate = delegate
    }

    func updateHostname(hostname: String, token: String) throws {
        guard
            !token.isEmpty,
            let userID = JWTDecoder.decode(jwtToken: token)[Constants.Param.nameid.rawValue] as? String
        else {
            throw SDKApiError.invalidContext
        }

        let context = APIContext(
            hostname: hostname.preprocessedHostame,
            accessToken: token,
            userID: userID
        )
        try saveContext(context: context)
    }

    func updateHostname(hostname: String) {
        self.hostname = hostname.preprocessedHostame
    }

    func saveContext(context: APIContext) throws {
        guard
            !context.hostname.isEmpty &&
            !context.accessToken.isEmpty &&
            !context.userID.isEmpty
        else {
            throw SDKApiError.invalidContext
        }
        do {
            let data = try JSONEncoder().encode(context)
            try store.saveContextData(data: data)

            self.context = context
            delegate?.contextManager(self, savedContext: context)
        } catch {
            throw SDKApiError.invalidContext
        }
    }

    func restoreContext() throws {
        guard let data = store.contextData else {
            delegate?.contextManagerCouldNotRestoreContext(self)
            return
        }
        do {
            context = try JSONDecoder().decode(APIContext.self, from: data)
            delegate?.contextManager(self, restoredContext: context)
        } catch {
            context = nil
            delegate?.contextManagerCouldNotRestoreContext(self)
        }
    }

    func clearContext() throws {
        do {
            try store.clearContextData()
            context = nil
            delegate?.contextManagerClearedContext(self)
        } catch {
            throw SDKApiError.invalidContext
        }
    }

    func getUnauthenticatedAPIClient() -> APIClient? {
        guard let hostname = hostname else {
            return nil
        }
        
        return APIClientImpl(
            baseURL: hostname,
            session: getSession(),
            delegate: self
        )
    }

    func getAPIClient() -> APIClient? {
        guard let context = context else {
            return nil
        }
        
        let pocToken = UserDefaults.standard.string(forKey: Constants.Key.pocToken.rawValue) != nil ? UserDefaults.standard.string(forKey: Constants.Key.pocToken.rawValue) : ""
        
        guard let client = clients[context] else {
            let client = APIClientImpl(
                baseURL: context.hostname,
                session: getSession(),
                delegate: self,
                defaultHeaders: ["Authorization": "Bearer \(context.accessToken)",
                "poc-token": pocToken ?? ""]
            )
            clients[context] = client
            return client
        }
        return client
    }

    func setDelegate(delegate: ContextManagerDelegate?) {
        self.delegate = delegate
    }
    
    private func getSession() -> Session {
        return Session(
            configuration: URLSessionConfiguration.default,
            eventMonitors: [TimingEventMonitor()]
        )
    }
}

class TimingEventMonitor: EventMonitor {
    func requestDidResume(_ request: Request) {
        let method = request.request?.httpMethod ?? "unknown"
        let url = request.request?.url?.absoluteString ?? "unknown"
        
        if let body = request.request?.httpBody, let jsonString = String(data: body, encoding: .utf8) {
            Logger.log("Started request \(method): \(url), body: \(jsonString)", category: .networking)
        } else {
            Logger.log("Started request \(method): \(url)", category: .networking)
        }
    }
    
    func request<Value>(_ request: DataRequest, didParseResponse response: DataResponse<Value, AFError>) {
        if let error = response.error {
            Logger.log("Request failed: \(request), error: \(error)", category: .networking, level: .error)
        } else {
            if let data = response.data, let jsonString = String(data: data, encoding: .utf8) {
                Logger.log("Request succeeded: \(request), response body: \(jsonString)", category: .networking)
            } else {
                Logger.log("Request succeeded: \(request)", category: .networking)
            }
        }
    }
}

extension ContextManagerImpl: APIClientDelegate {
    func handleUnauthenticatedResponse() {
        try? clearContext()
    }
}
