//
//  Copyright © 2022 beaconsmind. All rights reserved.
//

import CoreLocation
import UIKit

protocol BeaconLocationManagerDelegate: AnyObject {

    func onLocationManagerAuthorized(manager: BeaconLocationManager)
    func onLocationManagerError(region: CLRegion?, error: LocationError)
    func onLocationManagerRanged(regions: [CLBeacon], in region: CLBeaconRegion, position: LatLongModel?)
    func onLocationManagerEntered(region: CLBeaconRegion, position: LatLongModel?)
    func onLocationManagerLeft(region: CLBeaconRegion, position: LatLongModel?)
}

protocol BeaconLocationManager {

    var delegate: BeaconLocationManagerDelegate? { get set }
    var isAuthorized: Bool { get }

    func updateMonitoredRegions(regions: [CLBeaconRegion])
    func start() -> Bool
    func stop()
    func stopRanging(region: CLBeaconRegion)
    func startRanging(region: CLBeaconRegion)
    func restartMonitoringSignificantLocationChanges()
}

final class BeaconLocationManagerImpl: NSObject, BeaconLocationManager, CLLocationManagerDelegate {

    weak var delegate: BeaconLocationManagerDelegate?
    private let state: State
    private let prefix: String
    private let monitoringLimit: Int
    private var monitoringSignificantRestartTimer: Timer?
    private let monitoringSignificantRestartTimeInterval: TimeInterval

    private lazy var locationManager: CLLocationManager = {
        let manager = CLLocationManager()
        
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.distanceFilter = 1
        if #available(iOS 11.0, *) {
            manager.showsBackgroundLocationIndicator = true
        }
        manager.allowsBackgroundLocationUpdates = true
        manager.pausesLocationUpdatesAutomatically = false
        manager.activityType = .other
        manager.delegate = self
        
        return manager
    }()

    var isAuthorized: Bool {
        let status: CLAuthorizationStatus
        let accuracy: CLAccuracyAuthorization?
        
        if #available(iOS 14.0, *) {
            status = locationManager.authorizationStatus
            accuracy = locationManager.accuracyAuthorization
        } else {
            status = CLLocationManager.authorizationStatus()
            accuracy = nil
        }
        
        let result = [.authorizedAlways, .authorizedWhenInUse].contains(status)
        
        state.isLocationAuthorized = result
        state.locationAuthorization = status
        state.locationAccuracy = accuracy
        
        Logger.log("Authorization status: \(status.rawValue). Accuracy status: \(accuracy?.rawValue ?? -1)", category: .monitoring)
        
        return result
    }

    private var position: LatLongModel? {
        var result: LatLongModel? = nil
        if let coordinate = locationManager.location?.coordinate {
            result = LatLongModel(latitude: coordinate.latitude, longitude: coordinate.longitude)
        }
        state.lastLocation = result
        return result
    }

    init(
        state: State,
        prefix: String,
        monitoringLimit: Int = 20,
        monitoringSignificantRestartTimeInterval: TimeInterval = Constants.Interval.monitoringSignificantRestart
    ) {
        self.state = state
        self.prefix = prefix
        self.monitoringLimit = monitoringLimit
        self.monitoringSignificantRestartTimeInterval = monitoringSignificantRestartTimeInterval
        
        super.init()

        NotificationCenter.default.addObserver(self, selector: #selector(onAppEnteredBackground), name: UIApplication.didEnterBackgroundNotification, object: nil)
    }

    deinit {
        stop()
        NotificationCenter.default.removeObserver(self)
    }

    func start() -> Bool {
        guard CLLocationManager.locationServicesEnabled() else {
            Logger.log("Location services are not enabled.", category: .monitoring, level: .error)
            delegate?.onLocationManagerError(region: nil, error: .locationServicesNotEnabled)
            return false
        }

        guard isAuthorized else {
            Logger.log("Cannot start beacon location manager because permissions are not granted", category: .monitoring, level: .warning)
            return false
        }
        
        Logger.log("Starting beacon location manager", category: .monitoring)

        return true
    }

    func stop() {
        Logger.log("Stopping beacon location manager", category: .monitoring)
        
        stopMonitoring()
        stopRanging()
    }

    @objc private func onAppEnteredBackground() {
        Logger.log("App entered background", category: .monitoring)
        restartMonitoringSignificantLocationChanges()
    }
}

//
// MARK: - Monitoring
//

extension BeaconLocationManagerImpl {

    func updateMonitoredRegions(regions: [CLBeaconRegion]) {
        Logger.log("Updating monitored regions", category: .monitoring)
        
        guard isAuthorized else {
            Logger.log("Cannot update monitored regions because user is not authorized", category: .monitoring, level: .warning)
            return
        }

        guard CLLocationManager.isMonitoringAvailable(for: CLBeaconRegion.self) else {
            Logger.log("Monitoring is not possible with this device.", category: .monitoring, level: .error)
            delegate?.onLocationManagerError(region: nil, error: .monitoringNotAvailable)
            return
        }

        stopMonitoring()

        if regions.count > monitoringLimit {
            Logger.log("Reached monitoring limit \(monitoringLimit)", category: .monitoring, level: .error)
            delegate?.onLocationManagerError(region: nil, error: .monitoringLimitReached)
        }
        regions.prefix(monitoringLimit).forEach { region in
            region.notifyEntryStateOnDisplay = true
            region.notifyOnExit = true
            region.notifyOnEntry = true

            if !locationManager.monitoredRegions.contains(region) {
                Logger.log("Started monitoring region \(region.uuidString)", category: .monitoring)
                locationManager.startMonitoring(for: region)
            } else {
                Logger.log("Region \(region.uuidString) is already in monitoredRegions map", category: .monitoring)
            }
        }
    }

    private func stopMonitoring() {
        Logger.log("Stopping monitoring", category: .monitoring)
        locationManager.monitoredRegions.forEach {
            locationManager.stopMonitoring(for: $0)
            Logger.log("Stopped monitoring region \($0.identifier)", category: .monitoring)
        }
    }

    func startRanging(region: CLBeaconRegion) {
        Logger.log("Starting ranging region \(region.uuidString)", category: .monitoring)
        
        guard CLLocationManager.isRangingAvailable() else {
            Logger.log("Cannot start ranging region \(region.uuidString) because ranging is not available", category: .monitoring, level: .warning)
            delegate?.onLocationManagerError(region: nil, error: .rangingNotAvailable)
            return
        }
        if #available(iOS 13.0, *) {
            var constraint = CLBeaconIdentityConstraint(uuid: region.uuid)
            if let major = region.major, let minor = region.minor {
                constraint = CLBeaconIdentityConstraint(
                    uuid: region.uuid,
                    major: major.uint16Value,
                    minor: minor.uint16Value
                )
            }
            if locationManager.rangedBeaconConstraints.firstIndex(of: constraint) == nil {
                locationManager.startRangingBeacons(satisfying: constraint)
            }
        } else {
            if locationManager.rangedRegions.firstIndex(of: region) == nil {
                locationManager.startRangingBeacons(in: region)
            }
        }
        Logger.log("Started ranging region \(region.identifier)", category: .monitoring)
    }

    func stopRanging(region: CLBeaconRegion) {
        if #available(iOS 13.0, *) {
            var constraint = CLBeaconIdentityConstraint(uuid: region.uuid)
            if let major = region.major, let minor = region.minor {
                constraint = CLBeaconIdentityConstraint(
                    uuid: region.uuid,
                    major: major.uint16Value,
                    minor: minor.uint16Value
                )
            }
            if locationManager.rangedBeaconConstraints.firstIndex(of: constraint) != nil {
                locationManager.stopRangingBeacons(satisfying: constraint)
                Logger.log("Stopped ranging in region %@", category: .monitoring, region.identifier)
            }
        } else {
            if locationManager.rangedRegions.firstIndex(of: region) != nil {
                locationManager.stopRangingBeacons(in: region)
                Logger.log("Stopped ranging in region %@", category: .monitoring, region.identifier)
            }
        }
    }

    private func stopRanging() {
        if #available(iOS 13.0, *) {
            locationManager.rangedBeaconConstraints.forEach {
                locationManager.stopRangingBeacons(satisfying: $0)
                Logger.log("Stopped ranging in region: %@", category: .monitoring, $0.uuid.uuidString)
            }
        } else {
            locationManager.rangedRegions.compactMap { $0 as? CLBeaconRegion }.forEach {
                locationManager.stopRangingBeacons(in: $0)
                Logger.log("Stopped ranging in region: %@", category: .monitoring, $0.proximityUUID.uuidString)
            }
        }
    }
}

//
// MARK: - Location manager delegate
//

extension BeaconLocationManagerImpl {

    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        Logger.log("Authorization changed: %d", category: .monitoring, status.rawValue)
        if isAuthorized {
            delegate?.onLocationManagerAuthorized(manager: self)
        }
    }

    func locationManager(_ manager: CLLocationManager, didStartMonitoringFor region: CLRegion) {
        locationManager.requestState(for: region)
        Logger.log("Started monitoring for region: %@", category: .monitoring, region.identifier)
    }

    func locationManager(_ manager: CLLocationManager, monitoringDidFailFor region: CLRegion?, withError error: Error) {
        Logger.log("Monitoring failed for region: %@", category: .monitoring, level: .error, region?.identifier ?? error.localizedDescription)
        delegate?.onLocationManagerError(region: region, error: .monitoringFailed(error: error))
    }

    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        Logger.log("Monitoring failed: %@", category: .monitoring, level: .error, error.localizedDescription)
        delegate?.onLocationManagerError(region: nil, error: .monitoringFailed(error: error))
    }

    func locationManager(_ manager: CLLocationManager, didRangeBeacons beacons: [CLBeacon], in region: CLBeaconRegion) {
        Logger.log("Ranged beacons: %@", category: .monitoring, beacons.map {
            "\(region.uuidString)-\($0.major.stringValue)-\($0.minor.stringValue)"
        })
        delegate?.onLocationManagerRanged(regions: beacons, in: region, position: position)
    }

    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        Logger.log("Entered region (old API): %@", category: .monitoring, region.identifier)
        guard let beaconRegion = region as? CLBeaconRegion else {
            Logger.log("Entered wrong region: %@", category: .monitoring, level: .error, region.identifier)
            delegate?.onLocationManagerError(region: region, error: .wrongRegion(region: region))
            return
        }
        delegate?.onLocationManagerEntered(region: beaconRegion, position: position)
    }

    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        Logger.log("Left region (old API): %@", category: .monitoring, region.identifier)
        guard let beaconRegion = region as? CLBeaconRegion else {
            Logger.log("Left wrong region: %@", category: .monitoring, level: .error, region.identifier)
            delegate?.onLocationManagerError(region: region, error: .wrongRegion(region: region))
            return
        }
        delegate?.onLocationManagerLeft(region: beaconRegion, position: position)
    }

    func locationManager(_ manager: CLLocationManager, didDetermineState state: CLRegionState, for region: CLRegion) {
        Logger.log("Entered region: %@", category: .monitoring, region.identifier)
        guard let beaconRegion = region as? CLBeaconRegion else {
            Logger.log("Entered wrong region: %@", category: .monitoring, level: .error, region.identifier)
            delegate?.onLocationManagerError(region: region, error: .wrongRegion(region: region))
            return
        }

        switch state {
            case .inside:
                Logger.log("Entered region: %@", category: .monitoring, region.identifier)
                delegate?.onLocationManagerEntered(region: beaconRegion, position: position)

            case .outside:
                Logger.log("Left region: %@", category: .monitoring, region.identifier)
                delegate?.onLocationManagerLeft(region: beaconRegion, position: position)

            case .unknown:
                Logger.log("Unknown state for region: %@", category: .monitoring, region.identifier)
        }
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        Logger.log("Significant location changes triggered", category: .monitoring)
    }
}

//
// MARK: - Significant location change monitoring
//

extension BeaconLocationManagerImpl {

    func restartMonitoringSignificantLocationChanges() {
        Logger.log("Restarting monitoring due to significat location change", category: .monitoring)
        
        guard isAuthorized else {
            Logger.log("Cannot restart monitoring due to significat location change because permissions are not granted", category: .monitoring, level: .warning)
            return
        }
        stopMonitoringSignificantLocationChanges()

        locationManager.startMonitoringSignificantLocationChanges()
        monitoringSignificantRestartTimer = Timer.scheduledTimer(withTimeInterval: monitoringSignificantRestartTimeInterval, repeats: true, block: { [weak self] _ in
            self?.restartMonitoringSignificantLocationChanges()
        })
    }

    private func stopMonitoringSignificantLocationChanges() {
        locationManager.stopMonitoringSignificantLocationChanges()
        monitoringSignificantRestartTimer?.invalidate()
        monitoringSignificantRestartTimer = nil
    }
}
