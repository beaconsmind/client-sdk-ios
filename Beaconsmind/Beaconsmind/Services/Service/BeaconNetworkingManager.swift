//
//  Copyright © 2022 beaconsmind. All rights reserved.
//

import CoreLocation
import UIKit

protocol BeaconNetworkingManagerDelegate: AnyObject {

    func onNetworkingManager(manager: BeaconNetworkingManager, fetchedBeacons: [Beacon])
}

protocol BeaconNetworkingManager {

    func startFetching()
    func startReporting()
    func reportEvent(position: LatLongModel?, event: EventModel)

    /// TODO: Only exposed for testing
    var reportingState: RequestState { get }
    var eventsToReport: [EventModel] { get }
    var lastPositionToReport: LatLongModel? { get }
}

class BeaconNetworkingManagerImpl: BeaconNetworkingManager {

    private let apiClient: APIClient
    private let userID: String
    private let store: Store
    private weak var delegate: BeaconNetworkingManagerDelegate?

    /// Fetching
    private var fetchingState: RequestState = .initial
    private var fetchingTimer: Timer?
    private let fetchingInterval: TimeInterval
    private let beaconStore = BeaconPersistence()
    private var fetchedBeacons: [Beacon] = []
    private var fetchingBackgroundTaskID = UIBackgroundTaskIdentifier.invalid

    /// Reporting
    var reportingState: RequestState = .initial
    var eventsToReport: [EventModel] = []
    var lastPositionToReport: LatLongModel?
    private let reportInterval: TimeInterval
    private var reportTimer: Timer?
    private var reportingBackgroundTaskID = UIBackgroundTaskIdentifier.invalid

    init(
        apiClient: APIClient,
        delegate: BeaconNetworkingManagerDelegate?,
        userID: String,
        store: Store,
        reportInterval: TimeInterval = Constants.Interval.reportContacts,
        fetchingInterval: TimeInterval = Constants.Interval.refetchBeacons
    ) {
        self.apiClient = apiClient
        self.delegate = delegate
        self.userID = userID
        self.store = store
        self.reportInterval = reportInterval
        self.fetchingInterval = fetchingInterval
    }

    func reportEvent(position: LatLongModel?, event: EventModel) {
        Logger.log("Reporting event \(event.uuid)", category: .networking)
        
        lastPositionToReport = position ?? lastPositionToReport
        eventsToReport.append(event)
        
        /// Immediately report stored events to the backend to ensure that they won't be lost
        /// when the app is terminated by the system.
        reportStoredEvents()
    }

    deinit {
        stopFetchingTimer()
        stopReportingTimer()
    }
}

//
// MARK: - Fetching
//

extension BeaconNetworkingManagerImpl {

    func startFetching() {
        Logger.log("Start fetching.", category: .networking)
        
        fetchBeacons()
        stopFetchingTimer()
        fetchingTimer = Timer.scheduledTimer(withTimeInterval: fetchingInterval, repeats: true, block: { [weak self] _ in
            self?.fetchBeacons()
        })
    }

    private func stopFetchingTimer() {
        fetchingTimer?.invalidate()
        fetchingTimer = nil
    }

    private func fetchBeacons() {
        Logger.log("Fetching beacons", category: .networking)
        guard fetchingState != .working else {
            Logger.log("Already fetching beacons", category: .networking)
            return
        }

        fetchingBackgroundTaskID = UIApplication.shared.beginBackgroundTask(withName: Constants.ID.backgroundTaskFetching.rawValue) { [weak self] in
            if let id = self?.fetchingBackgroundTaskID {
                UIApplication.shared.endBackgroundTask(id)
            }
        }

        fetchingState = .working
        let request = API.Configuration.ConfigurationGetBeacons.Request()
        _ = apiClient.apiRequest(request) { [weak self] response in
            guard let self = self else {
                Logger.log("self reference is null", category: .general)
                return
            }
            switch response {
                case let .success(beacons):
                    self.fetchingState = .done

                    if let models = beacons.success {
                        let validBeacons = models.compactMap(Beacon.fromAPI(element:))
                        Logger.log("Fetched beacons", category: .networking, validBeacons.map { $0.description })
                        self.fetchedBeacons = validBeacons
                        self.beaconStore.saveBeacons(self.userID, beacons: validBeacons)
                        self.delegate?.onNetworkingManager(manager: self, fetchedBeacons: validBeacons)
                    }

                case let .failure(error):
                    self.fetchingState = .error
                    Logger.log("Error fetching beacons %@", category: .networking, level: .error, error.localizedDescription)

                    let storedBeacons = self.beaconStore.loadBeacons(userID: self.userID)
                    self.fetchedBeacons = storedBeacons
                    self.delegate?.onNetworkingManager(manager: self, fetchedBeacons: storedBeacons)
            }

            /// If the task hasn't been ended yet, end it.
            if self.fetchingBackgroundTaskID != .invalid {
                UIApplication.shared.endBackgroundTask(self.fetchingBackgroundTaskID)
                self.fetchingBackgroundTaskID = .invalid
            }
        }
    }
}

//
// MARK: - Reporting
//

extension BeaconNetworkingManagerImpl {

    func startReporting() {
        Logger.log("Start reporting.", category: .networking)
        
        reportStoredEvents()
        stopReportingTimer()
        reportTimer = Timer.scheduledTimer(withTimeInterval: reportInterval, repeats: true, block: { [weak self] _ in
            self?.reportStoredEvents()
        })
    }

    private func stopReportingTimer() {
        Logger.log("Stop reporting timer.", category: .networking)
        
        reportTimer?.invalidate()
        reportTimer = nil
    }

    private func reportStoredEvents() {
        Logger.log("Report stored events.", category: .networking)
        guard reportingState != .working && eventsToReport.count > 0 else {
            Logger.log("Already reporting or nothing to report.", category: .networking)
            return
        }
        cleanEventsToReport()
        reportEvents(events: eventsToReport)
    }

    private func reportEvents(events: [EventModel]) {
        Logger.log("Reporting \(events.count) events", category: .networking)
        
        reportingBackgroundTaskID = UIApplication.shared.beginBackgroundTask(withName: Constants.ID.backgroundTaskReporting.rawValue) { [weak self] in
            if let id = self?.reportingBackgroundTaskID {
                UIApplication.shared.endBackgroundTask(id)
            }
        }
        
        let model = PingBeaconsRequest(
            currentPosition: lastPositionToReport,
            deviceId: store.deviceToken ?? "",
            deviceOs: "iOS",
            events: events,
            userId: userID
        )
        reportingState = .working
        let request = API.Ping.PingPingMultipleBeacons.Request(body: model)
        _ = apiClient.apiRequest(request) { [weak self] response in
            guard let self = self else {
                Logger.log("self reference is null", category: .general)
                return
            }

            self.handleReportEventsResponse(
                events: events,
                response: response
            )

            /// If the task hasn't been ended yet, end it.
            if self.reportingBackgroundTaskID != .invalid {
                UIApplication.shared.endBackgroundTask(self.reportingBackgroundTaskID)
                self.reportingBackgroundTaskID = .invalid
            }
        }
    }

    private func handleReportEventsResponse(
        events: [EventModel],
        response: SDKApiResult<API.Ping.PingPingMultipleBeacons.Response>
    ) {
        switch response {
            case .failure(let error):
                reportingState = .error
                Logger.log("Error reporing events: %@", category: .networking, level: .error, error.localizedDescription)

            case .success:
                reportingState = .done
                eventsToReport.removeItems(from: events)
                Logger.log("Finished reporting events: %@", category: .monitoring, events.map { $0.description })
        }
    }

    private func cleanEventsToReport() {
        Logger.log("Cleaning events to report", category: .networking)
        
        var newEvents: [EventModel] = []
        var previous: EventModel?
        eventsToReport.forEach { event in
            if event.description != previous?.description {
                newEvents.append(event)
            }
            previous = event
        }
        eventsToReport = newEvents
    }
}
