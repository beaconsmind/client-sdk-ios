//
//  Copyright © 2022 beaconsmind. All rights reserved.
//

import Foundation

/// Collects beacon contact events and keeps a summary for each contacted beacon up-to-date
protocol BeaconContactsManager {

    var summaries: [BeaconContactsSummary] { get }

    func updateBeaconList(beacons: [Beacon])
    func ranged(beacon: Beacon, event: EventModel)
    func entered(beacon: Beacon, event: EventModel)
    func left(beacon: Beacon)
}

class BeaconContactsManagerImpl: BeaconContactsManager {

    private let maxEventCount = 100
    private var allEvents: [Beacon: [EventModel]] = [:]
    private var inRange: Set<Beacon> = []

    var summaries: [BeaconContactsSummary] = []

    func updateBeaconList(beacons: [Beacon]) {
        let toRemove = allEvents.keys.filter { !beacons.contains($0) }
        toRemove.forEach { allEvents.removeValue(forKey: $0) }

        beacons.forEach {
            if allEvents[$0] == nil {
                allEvents[$0] = []
            }
        }
        updateSummaries()
    }

    func entered(beacon: Beacon, event: EventModel) {
        ranged(beacon: beacon, event: event)
    }

    func left(beacon: Beacon) {
        inRange.remove(beacon)
        updateSummaries()
    }

    func ranged(beacon: Beacon, event: EventModel) {
        inRange.insert(beacon)
        if let beaconEvents = allEvents[beacon], !beaconEvents.contains(event) {
            var newBeaconEvents = beaconEvents
            newBeaconEvents.append(event)
            if newBeaconEvents.count > maxEventCount {
                newBeaconEvents = newBeaconEvents.sorted(by: { ($0.timestamp ?? 0) > ($1.timestamp ?? 0) }).dropLast(1)
            }
            allEvents[beacon] = newBeaconEvents
        } else {
            allEvents[beacon] = [event]
        }
        updateSummaries()
    }

    private func updateSummaries() {
        summaries = allEvents.map {
            BeaconContactsSummary(beacon: $0.key, events: $0.value, isInRange: inRange.contains($0.key))
        }
        .sorted { item1, item2 in
            if item1.isInRange != item2.isInRange {
                return item1.isInRange
            }
            if item1.timestamp == item2.timestamp {
                return item1.name < item2.name
            }
            if let timestamp1 = item1.timestamp, let timestamp2 = item2.timestamp {
                return timestamp1 > timestamp2
            }
            if item1.timestamp != nil {
                return true
            }
            return false
        }
    }
}
