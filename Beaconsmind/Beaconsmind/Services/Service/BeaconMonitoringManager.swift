//
//  Copyright © 02.03.22 Beaconsmind. All rights reserved.
//

import CoreLocation
import UIKit

/// Handles monitoring beacons in 20 batches and ranging the found ones if the app is in foreground
protocol BeaconMonitoringManager {

    var beaconContactsSummaries: [BeaconContactsSummary] { get }

    func start() -> Bool

    /// TODO: Only exposed for testing
    var beaconsAll: [Beacon] { get }
    var beaconsRanged: Set<Beacon> { get }
    var enteredRegions: Set<CLBeaconRegion> { get }
    var monitoredRegions: [CLBeaconRegion] { get }
    var regionsToMonitor: [CLBeaconRegion] { get }
    var nextIndexToMonitor: Int { get }
    var isLooping: Bool { get }

    func restart()
    func loopMonitoring()
}

final class BeaconMonitoringManagerImpl: BeaconMonitoringManager {

    private weak var delegate: BeaconListenerDelegate?
    private let apiClient: APIClient
    private let userID: String
    private let store: Store
    private let state: State
    private let suspendStatusRecorder = SuspendStatusRecorder()


    /// Monitoring state
    var beaconsAll: [Beacon] = []
    var beaconsRanged: Set<Beacon> = []
    var enteredRegions: Set<CLBeaconRegion> = []
    var monitoredRegions: [CLBeaconRegion] = []
    var regionsToMonitor: [CLBeaconRegion] = []
    var nextIndexToMonitor = 0

    private lazy var networkingService: BeaconNetworkingManager = {
        BeaconNetworkingManagerImpl(
            apiClient: apiClient,
            delegate: self,
            userID: userID,
            store: store
        )
    }()

    private var locationManager: BeaconLocationManager
    private let contactsManager: BeaconContactsManager
    private var monitoringLoopTimer: Timer?
    private let monitoringLoopTimeInterval: TimeInterval
    private var restartTimer: Timer?
    private let restartTimeInterval: TimeInterval
    private let monitoringLimit: Int

    var beaconContactsSummaries: [BeaconContactsSummary] {
        contactsManager.summaries
    }

    init(
        userID: String,
        apiClient: APIClient,
        delegate: BeaconListenerDelegate? = nil,
        store: Store,
        state: State,
        contactsManager: BeaconContactsManager = BeaconContactsManagerImpl(),
        monitoringLimit: Int = 20,
        monitoringLoopTimeInterval: TimeInterval = Constants.Interval.monitoringNextRegions,
        restartTimeInterval: TimeInterval = Constants.Interval.monitoringRestart,
        locationManager: BeaconLocationManager? = nil
    ) {
        self.userID = userID
        self.apiClient = apiClient
        self.delegate = delegate
        self.store = store
        self.state = state
        self.contactsManager = contactsManager
        self.monitoringLimit = monitoringLimit
        self.monitoringLoopTimeInterval = monitoringLoopTimeInterval
        self.restartTimeInterval = restartTimeInterval

        /// Setting up location manager
        if let locationManager = locationManager {
            self.locationManager = locationManager
        } else {
            self.locationManager = BeaconLocationManagerImpl(state: state, prefix: userID)
        }
        self.locationManager.delegate = self

        NotificationCenter.default.addObserver(self, selector: #selector(onAppEnteredBackground), name: UIApplication.didEnterBackgroundNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(onAppTermination), name: UIApplication.willTerminateNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(onAppWillSuspend), name: UIApplication.willSuspend, object: nil)

        self.suspendStatusRecorder.start()
    }

    deinit {
        stop()
        NotificationCenter.default.removeObserver(self)
    }

    func start() -> Bool {
        if locationManager.start() {
            Logger.log("Beacon monitoring started.", category: .monitoring)
            networkingService.startFetching()
            return true
        }
        Logger.log("Failed to start beacon monitoring.", category: .monitoring, level: .error)
        return false
    }

    @objc private func onAppEnteredBackground() {
        Logger.log("App entered background.", category: .monitoring)
        if isLooping {
            Logger.log("App entered background while monitoring.", category: .monitoring)
            locationManager.updateMonitoredRegions(regions: monitoredRegions)
        }
    }
    
    @objc private func onAppTermination() {
        Logger.log("App terminated.", category: .monitoring)
        if isLooping {
            Logger.log("App terminated while monitoring.", category: .monitoring)
            //we have a few seconds left to do, so we can do something quick
        }
    }
    
    @objc private func onAppWillSuspend() {
        Logger.log("App will suspend.", category: .monitoring)
        if isLooping {
            Logger.log("App will suspend while monitoring.", category: .monitoring)
            //we have a few seconds left to do, so we can do something quick
        }
    }
}

//
// MARK: - Networking manager delegate
//

extension BeaconMonitoringManagerImpl: BeaconNetworkingManagerDelegate {

    func onNetworkingManager(manager: BeaconNetworkingManager, fetchedBeacons: [Beacon]) {
        Logger.log("Fetched \(fetchedBeacons.count) beacons", category: .monitoring)
        
        beaconsAll = fetchedBeacons
        contactsManager.updateBeaconList(beacons: fetchedBeacons)
        restart()
    }
}

//
// MARK: - Location manager delegate
//

extension BeaconMonitoringManagerImpl: BeaconLocationManagerDelegate {
    func onLocationManagerAuthorized(manager: BeaconLocationManager) {
        Logger.log("Location manager authorized.", category: .monitoring)
        networkingService.startReporting()
    }

    func onLocationManagerError(region: CLRegion?, error: LocationError) {
        Logger.log("Location manager failed for region \(region?.identifier ?? "").", category: .monitoring, level: .error)
        delegate?.ranged(region: region, error: SDKApiError.location(error))
    }

    func onLocationManagerRanged(regions: [CLBeacon], in region: CLBeaconRegion, position: LatLongModel?) {
        Logger.log("Ranged \(regions.count) beacons in region: \(region.identifier) at position (\(position?.latitude ?? 0),\(position?.longitude ?? 0))", category: .monitoring)

        let beacons: [Beacon] = regions.compactMap { region in
            let result = beaconsAll.first(forBeacon: region)
            return result
        }
        beaconsRanged.formUnion(beacons)

        regions.forEach { region in
            Logger.log("Beacon ranged: \(region.uuidString).", category: .monitoring)
            guard let beacon = beaconsAll.first(forBeacon: region), beaconsRanged.contains(beacon) else {
                Logger.log("Ranged beacon \(region.uuidString) is not known.", category: .monitoring)
                return
            }
            let event = region.toEvent(event: .in)
            contactsManager.ranged(beacon: beacon, event: event)
            networkingService.reportEvent(position: position, event: event)
        }
        delegate?.ranged(beacons: regions)
    }

    func onLocationManagerEntered(region: CLBeaconRegion, position: LatLongModel?) {
        Logger.log("Entered region: \(region.identifier) at position (\(position?.latitude ?? 0),\(position?.longitude ?? 0))", category: .monitoring)
        guard !enteredRegions.contains(region) else {
            Logger.log("Entered region \(region.identifier) is already in the map.", category: .monitoring)
            return
        }
        enteredRegions.insert(region)
        locationManager.startRanging(region: region)
        if isLooping {
            updateRegionsToMonitorAndLoop()
        }
        delegate?.proximity(inRegion: region)
    }

    func onLocationManagerLeft(region: CLBeaconRegion, position: LatLongModel?) {
        Logger.log("Left region: \(region.identifier) at position (\(position?.latitude ?? 0),\(position?.longitude ?? 0))", category: .monitoring)
        let leftBeacons = beaconsAll.filter { $0.uuid.lowercased() == region.uuidString.lowercased() }
        leftBeacons.forEach { beacon in
            if beaconsRanged.contains(beacon), let beaconRegion = beacon.toRegion() {
                Logger.log("Removing left region from ranged beacons map.", category: .monitoring)
                beaconsRanged.remove(beacon)
                contactsManager.left(beacon: beacon)
                networkingService.reportEvent(position: position, event: beaconRegion.toEvent(event: .out))
            }
        }

        if enteredRegions.contains(region) {
            Logger.log("Removing left region from entered regions map.", category: .monitoring)
            enteredRegions.remove(region)
            locationManager.stopRanging(region: region)
            if isLooping {
                updateRegionsToMonitorAndLoop()
            }
            delegate?.proximity(leftRegion: region)
        }
    }
}

//
// MARK: - Monitoring loop
//

extension BeaconMonitoringManagerImpl {
    func restart() {
        Logger.log("Restarting monitoring.", category: .monitoring)
    
        stop()

        beaconsRanged = []
        enteredRegions = []

        guard beaconsAll.count > 0 else {
            Logger.log("Can't start monitoring, there are no beacons.", category: .monitoring, level: .warning)
            return
        }

        locationManager.restartMonitoringSignificantLocationChanges()
        if isLooping {
            updateRegionsToMonitorAndLoop()
        } else {
            regionsToMonitor = allRegionIDs.compactMap { $0.toBeaconRegion() }
            locationManager.updateMonitoredRegions(regions: regionsToMonitor)
        }

        startRestartTimer()
    }

    func loopMonitoring() {
        Logger.log("Loop monitoring.", category: .monitoring)
        
        /// Collecting next beacons, preparing next round and updating monitored regions
        let toFillCount = monitoringLimit - enteredRegions.count
        let nextRegionsResult = regionsToMonitor.nextItems(fromIndex: nextIndexToMonitor, size: toFillCount)

        /// Restart if needed
        if nextRegionsResult.items.count == 0 && beaconsAll.count > 0 {
            restart()
        } else {
            monitoredRegions = Array(enteredRegions) + nextRegionsResult.items
            nextIndexToMonitor = nextRegionsResult.nextIndex
            locationManager.updateMonitoredRegions(regions: monitoredRegions)
        }
    }

    private func stop() {
        Logger.log("Stopping monitoring", category: .monitoring)
        
        stopMonitoringLoopTimer()
        stopRestartTimer()
        locationManager.stop()
    }

    private func startMonitoringLoopTimer() {
        monitoringLoopTimer = Timer.scheduledTimer(withTimeInterval: monitoringLoopTimeInterval, repeats: true, block: { [weak self] _ in
            self?.loopMonitoring()
        })
    }

    private func stopMonitoringLoopTimer() {
        monitoringLoopTimer?.invalidate()
        monitoringLoopTimer = nil
    }

    private func startRestartTimer() {
        restartTimer = Timer.scheduledTimer(withTimeInterval: restartTimeInterval, repeats: true, block: { [weak self] _ in
            self?.restart()
        })
    }

    private func stopRestartTimer() {
        restartTimer?.invalidate()
        restartTimer = nil
    }
}

//
// MARK: - Helpers
//

extension BeaconMonitoringManagerImpl {

    var isLooping: Bool {
        allRegionIDs.count > monitoringLimit
    }

    private var allRegionIDs: [String] {
        beaconsAll.map { $0.uuid.lowercased() }.distinct
    }

    private func updateRegionsToMonitorAndLoop() {
        Logger.log("Update regions to monitor and loop.", category: .monitoring)
        
        let enteredRegionIDs = enteredRegions.map { $0.uuidString.lowercased() }.distinct
        let countBefore = regionsToMonitor.count
        regionsToMonitor = allRegionIDs
            .filter { !enteredRegionIDs.contains($0) }
            .compactMap { $0.toBeaconRegion() }

        if countBefore != regionsToMonitor.count {
            nextIndexToMonitor = 0
        }
        loopMonitoring()
    }
}





fileprivate extension UIApplication {
    static let willSuspend = Notification.Name("application-will-suspend")
}


///This class creates an empty background task and observes how long it can run before it is suspended. It should call the UIApplication.willSuspend notification a few seconds before suspending the application.
fileprivate class SuspendStatusRecorder {
    private var timer : Timer?
    private var task : UIBackgroundTaskIdentifier = UIBackgroundTaskIdentifier.invalid

    /// Start monitoring for suspend
    internal func start() {
        DispatchQueue.main.async {
            self.stop() // If already going.
            self.startTask()
            let timer = Timer(timeInterval: 1, repeats: true) { [weak self] (_) in
                // periodic timer running every 1 second that checks app's background time remaining
                self?.checkStatus()
            }
            RunLoop.main.add(timer, forMode: .common)
            NotificationCenter.default.addObserver(self, selector: #selector(self.onAppDidBecomeActive), name: UIApplication.didBecomeActiveNotification, object: nil)
        }
    }
    
    @objc func onAppDidBecomeActive() {
        self.endTask()
        self.startTask()
    }

    internal func stop() {
        if let timer = timer {
            timer.invalidate()
            self.timer = nil
        }
        endTask()
    }

    //if recognised that app will is about to be suspended, we call our custom UIApplication.willSuspend notification
    private func willExpire() {
        endTask() // Allow app to suspend
        NotificationCenter.default.post(name: UIApplication.willSuspend, object: nil)
    }

    private func checkStatus() {
        
        // by default, backgroundTimeRemaining is infinite before background time countdown is started
        // we check if there is only 5 seconds remaining background time
        if UIApplication.shared.applicationState == .background &&
           UIApplication.shared.backgroundTimeRemaining != Double.greatestFiniteMagnitude &&
            UIApplication.shared.backgroundTimeRemaining < 5.0 &&
           task != UIBackgroundTaskIdentifier.invalid
        {
            willExpire()
        }
    }

    private func endTask() {
        if task != UIBackgroundTaskIdentifier.invalid {
            UIApplication.shared.endBackgroundTask(task)
            self.task = UIBackgroundTaskIdentifier.invalid
        }
    }

    private func startTask() {
        //imaginary task that doesn’t actually start any sort of background task, but it is used so we can observe app suspending
        task = UIApplication.shared.beginBackgroundTask(expirationHandler: { [weak self] in
            // handler to be called shortly before the app’s remaining background time reaches 0
            self?.willExpire()
        })
    }
    
    deinit {
        stop()
    }
}
