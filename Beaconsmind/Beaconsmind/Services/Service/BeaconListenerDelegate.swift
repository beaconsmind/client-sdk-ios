//
//  Copyright © 02.03.22 Beaconsmind. All rights reserved.
//

import Foundation
import CoreLocation

/**
 Listener delegate which is used to get current beacon ranging data for all registered and detected beacons in the vicinity.
 */
public protocol BeaconListenerDelegate: AnyObject {
    /**
     A list of registered beacons currently detected by the iDevice
     - parameter beacons : List of `CLBeacon`s
     */
    func ranged(beacons: [CLBeacon])

    /**
     Method is invoked when a beacon enters the listening region of a device
     - parameter inRegion : `CLBeaconRegion` of the iBeacon.
     */
    func proximity(inRegion: CLBeaconRegion)

    /**
     Method is invoked when a beacon leaves the listening region of a device
     - parameter leftRegion : `CLBeaconRegion` of the iBeacon.
     */
    func proximity(leftRegion: CLBeaconRegion)

    /**
     Method is invoked when there is an error
     */
    func ranged(region: CLRegion?, error: Error)
}
