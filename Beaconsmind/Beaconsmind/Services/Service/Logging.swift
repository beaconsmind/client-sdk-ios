//
//  Copyright © 03.12.21 Beaconsmind. All rights reserved.
//

import OSLog
import BugfenderSDK

public enum LogLevel: String {
    /// Will log all levels
    /// Any developer-level debug info, repeating events
    case debug
    /// Will log all info/warning/error logs
    /// Bigger events/steps in the SDK lifecycle, for example: service start/stop, one-time events
    case info
    /// Will log all warning/error logs
    /// Allowed, but not-optimal, inconsistent state
    case warning
    /// Not allowed state
    case error
    /// No logging at all
    case silent

    var type: OSLogType? {
        switch self {
            case .debug:
                return .debug
            case .info:
                return .info
            case .warning:
                return .default
            case .error:
                return .error
            case .silent:
                return nil
        }
    }

    var order: Int {
        switch self {
            case .debug:
                return 2
            case .info:
                return 3
            case .warning:
                return 4
            case .error:
                return 5
            case .silent:
                return 999
        }
    }
}

extension LogLevel {
    func toBFLogLevel() -> BFLogLevel {
        switch self {
        case .debug:
            return .default
        case .info:
            return .info
        case .warning:
            return .warning
        case .error:
            return .error
        case .silent:
            return .default
        }
    }
}

/// Domain-specific category used for OSLog's category
enum LogCategory: String {
    /// General SDK logging
    case general
    /// Monitoring, ranging, beacon contacts
    case monitoring
    /// Networking, fetching and reporting beacons
    case networking
}

struct Logger {

    static var consoleLogLevel: LogLevel = .silent
    static var remoteLoggingLevel: LoggingLevel = .Silent
    static var remoteLoggingEnabled = false

    /// Logs message with OSLog in case log level is enabled
    /// - Parameters:
    ///   - message: Log message
    ///   - category: Domain-specific OSLog category
    ///   - level: Used LogLevel
    ///   - args: Optional arguments of the message
    static func log(_ message: String, category: LogCategory, level: LogLevel = .debug, filename: String = #fileID, function: String = #function, _ args: CVarArg...) {
        guard let type = level.type else {
            return
        }
        
        let text = String(format: message, locale: .current, arguments: args)
        let log = OSLog(
            subsystem: Constants.logSubsystemID,
            category: category.rawValue
        )
        os_log("%{public}@", log: log, type: type, text)
        
        if shouldLogToConsole(level: level) {
            NSLog("[\(category)] \(filename)#\(function) \(text)")
        }
        
        if remoteLoggingEnabled && shouldLogToBF(level: level) {
            BFLog("[\(category)] \(text)", args, level: level.toBFLogLevel(), filename: filename, funcname: function)
        }
    }
    
    static func enableRemoteLogging(key: String) {
        /// The API key should be provided by the backend.
        if Bugfender.appKey() == nil {
            Bugfender.activateLogger(key)
            Bugfender.enableCrashReporting()
            Bugfender.enableUIEventLogging()
        }
    }
    
    static func setUserId(userId: String) {
        Bugfender.setDeviceString(userId, forKey: "user_id")
    }
    
    static func setDeviceId(deviceId: String) {
        Bugfender.setDeviceString(deviceId, forKey: "device_uuid")
    }
    
    static func setPlatformId(platformId: String) {
        Bugfender.setDeviceString(platformId, forKey: "platform_id")
    }

    static func shouldLogToConsole(level: LogLevel) -> Bool {
        guard consoleLogLevel != .silent, level != .silent else {
            return false
        }
        return level.order >= consoleLogLevel.order
    }
    
    static func shouldLogToBF(level: LogLevel) -> Bool {
        guard remoteLoggingLevel != .Silent, level != .silent else {
            return false
        }
        return level.order >= remoteLoggingLevel.rawValue
    }
}
