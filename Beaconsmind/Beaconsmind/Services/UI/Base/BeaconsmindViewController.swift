//
//  Copyright © 29.01.22 Beaconsmind. All rights reserved.
//

import UIKit
import MBProgressHUD

open class BeaconsmindViewController: UIViewController {

    /// Error and retry
    var errorView = UIStackView()
    var errorViewTop: NSLayoutConstraint?
    var retryButton: OfferActionButton?
    var errorMessage: UILabel?

    // MARK: - Theming

    var styles: StyleSheet {
        BeaconsmindUI.default.styleSheet
    }

    var spacing: CGFloat {
        styles.spacingMultiplier
    }

    var strings: LocalizedStrings {
        BeaconsmindUI.default.localizedStrings
    }

    // MARK: - Loading

    public func startLoading() {
        showLoading()
    }

    public func stopLoading() {
        MBProgressHUD.hide(for: view, animated: true)
    }

    public func showLoading(_ text: String? = nil) {
        let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        hud.label.text = text
        hud.isUserInteractionEnabled = false

        if let color = BeaconsmindUI.default.styleSheet.loadingViewContentColor {
            hud.contentColor = color
        }

        if let color = BeaconsmindUI.default.styleSheet.loadingViewBackgroundColor {
            hud.bezelView.backgroundColor = color
        }

        hud.show(animated: true)
    }

    public func showMessage(
        message: String,
        okButtonText: String = "OK",
        callback: (() -> Void)? = nil
    ) {
        let alert = UIAlertController(
            title: nil,
            message: message,
            preferredStyle: .alert
        )

        let action = UIAlertAction(title: okButtonText, style: .default) { _ in
            callback?()
        }
        alert.addAction(action)

        present(alert, animated: true, completion: nil)
    }

    public func showQuestion(
        question: String,
        acceptButtonText: String = "OK",
        cancelButtonText: String = "Cancel",
        callback: @escaping (_ isAccepted: Bool) -> Void
    ) {
        let alert = UIAlertController(title: nil, message: question, preferredStyle: .alert)

        let ok = UIAlertAction(title: acceptButtonText, style: .default) { _ in
            callback(true)
        }

        let cancel = UIAlertAction(title: cancelButtonText, style: .cancel) { _ in
            callback(false)
        }

        alert.addAction(ok)
        alert.addAction(cancel)
        present(alert, animated: true, completion: nil)
    }
}

// MARK: - Error view and retry handling

extension BeaconsmindViewController {

    func setupErrorViewUI() {
        errorView.alignment = .center
        errorView.axis = .vertical
        errorView.distribution = .equalCentering
        errorView.spacing = 8 * spacing
        errorView.translatesAutoresizingMaskIntoConstraints = false
        if #available(iOS 11.0, *) {
            errorViewTop = errorView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor)
        } else {
            errorViewTop = errorView.topAnchor.constraint(equalTo: view.topAnchor)
        }
        errorViewTop?.isActive = true
        errorView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16 * spacing).isActive = true
        errorView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16 * spacing).isActive = true
    }

    func addErrorMessage(message: String) {
        let label = UILabel()
        label.textColor = styles.errorMessageColor
        label.text = message
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        errorView.addArrangedSubview(label)
        errorMessage = label
    }

    func displayError(message: String, viewSpecific: (() -> Void)? = nil) {
        hideError()
        errorView.isHidden = false
        addErrorMessage(message: message)
        errorViewTop?.constant = 12 * spacing
        viewSpecific?()
    }

    func hideError(viewSpecific: (() -> Void)? = nil) {
        errorView.isHidden = true
        retryButton?.removeFromSuperview()
        errorMessage?.removeFromSuperview()
        errorViewTop?.constant = 0
    }
}
