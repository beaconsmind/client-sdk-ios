//
//  Copyright © 07.02.22 . All rights reserved.
//

import UIKit

class BeaconsmindCell: UITableViewCell {

    // MARK: - Theming

    var styles: StyleSheet {
        BeaconsmindUI.default.styleSheet
    }

    var spacing: CGFloat {
        styles.spacingMultiplier
    }

    var strings: LocalizedStrings {
        BeaconsmindUI.default.localizedStrings
    }
}
