//
//  Copyright © 07.02.22 . All rights reserved.
//

import UIKit
import Alamofire

class OfferCell: BeaconsmindCell {

    private var offer: OfferResponse?

    private var containerView = UIView()
    private var pictureView = UIImageView()
    private var textView = UITextView()
    private var readMoreButton = UIButton()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    func update(offer: OfferResponse) {
        self.offer = offer
        updateUI()
    }

    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        contentView.layer.opacity = highlighted ? 0.9 : 1
    }
}

// MARK: - UI

extension OfferCell {

    private func updateUI() {
        resetUI()

        guard let offer = offer else {
            return
        }

        if let urlString = offer.imageUrl, let url = URL(string: urlString) {
            pictureView.af.setImage(withURL: url)
        }
        textView.attributedText = offer.text?.attributedHTMLString
    }

    private func resetUI() {
        selectionStyle = .none
        backgroundColor = .clear
        contentView.backgroundColor = .clear

        contentView.removeAllSubviews()

        guard offer != nil else {
            return
        }

        containerView = UIView()
        pictureView = UIImageView()
        textView = UITextView()
        readMoreButton = UIButton()

        contentView.addSubview(containerView)
        containerView.addSubview(pictureView)
        containerView.addSubview(textView)
        containerView.addSubview(readMoreButton)

        setupUI()
    }

    private func setupUI() {
        containerView.backgroundColor = styles.backgroundColor

        contentView.layer.shadowOpacity = 1
        contentView.layer.shadowOffset = .zero
        contentView.layer.shadowColor = styles.cellShadowColor.cgColor
        contentView.layer.shadowRadius = styles.cellShadowRadius

        containerView.translatesAutoresizingMaskIntoConstraints = false
        containerView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 8 * spacing).isActive = true
        containerView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -8 * spacing).isActive = true
        containerView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16 * spacing).isActive = true
        containerView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16 * spacing).isActive = true
        containerView.clipsToBounds = true
        containerView.layer.cornerRadius = styles.cellCornerRadius

        /// Image view
        pictureView.contentMode = .scaleAspectFill
        pictureView.clipsToBounds = true
        pictureView.translatesAutoresizingMaskIntoConstraints = false
        let height = UIScreen.main.bounds.width * 0.35
        pictureView.heightAnchor.constraint(equalToConstant: height).isActive = true
        pictureView.topAnchor.constraint(equalTo: containerView.topAnchor).isActive = true
        pictureView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor).isActive = true
        pictureView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor).isActive = true

        /// Text view
        let padding = 16 * spacing
        textView.backgroundColor = .white
        textView.isScrollEnabled = false
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.topAnchor.constraint(equalTo: pictureView.bottomAnchor).isActive = true
        textView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor).isActive = true
        textView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor).isActive = true
        textView.textContainerInset = UIEdgeInsets(top: padding, left: padding, bottom: 0, right: padding)
        textView.isEditable = false
        textView.isSelectable = true

        /// Read more button
        readMoreButton.setTitle(strings.readMoreTitle, for: .normal)
        readMoreButton.setTitleColor(styles.primaryButtonTitleColor, for: .normal)
        readMoreButton.layer.cornerRadius = styles.buttonCornerRadius
        readMoreButton.backgroundColor = styles.primaryButtonBackgroundColor
        readMoreButton.isUserInteractionEnabled = false
        readMoreButton.contentEdgeInsets = .init(top: 15, left: 30, bottom: 15, right: 30)
        readMoreButton.translatesAutoresizingMaskIntoConstraints = false
        readMoreButton.topAnchor.constraint(equalTo: textView.bottomAnchor, constant: 8).isActive = true
        readMoreButton.centerXAnchor.constraint(equalTo: containerView.centerXAnchor).isActive = true
        readMoreButton.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: -12 * spacing).isActive = true
    }
}
