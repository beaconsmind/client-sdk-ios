//
//  Copyright © 03.02.22 . All rights reserved.
//

import UIKit

class OfferActionButton: UIButton {

    enum Action {
        case read, redeem, fetch
    }

    var retryAction: Action = .read
}
