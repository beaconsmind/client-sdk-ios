//
//  Copyright © 29.01.22 Beaconsmind. All rights reserved.
//

import UIKit
import AlamofireImage

public protocol OfferDetailViewControllerDelegate: AnyObject {
    var shouldDismissAfterRedeemed: Bool { get }
    func onOfferDetailView(controller: OfferDetailViewController, didRedeemOfferID: Int)
}

public class OfferDetailViewController: BeaconsmindViewController {

    /// Scroll view and content
    private var scrollView = UIScrollView()
    private var scrollViewToErrorBottom: NSLayoutConstraint?
    private var contentView = UIView()
    private var imageView = UIImageView()
    private var textView = UITextView()
    private var deliveredLabel = UILabel()
    private var titleLabel = UILabel()

    /// Bottom buttons
    private var buttonContainer = UIStackView()
    private var ctaButton = UIButton()
    private var redeemButton = UIButton()

    private var dataSource: DataSource
    private weak var delegate: OfferDetailViewControllerDelegate?

    public init(offer: OfferResponse, delegate: OfferDetailViewControllerDelegate? = nil) {
        dataSource = .offer(offer)
        self.delegate = delegate
        super.init(nibName: nil, bundle: nil)
    }

    public init(id: Int, delegate: OfferDetailViewControllerDelegate? = nil) {
        dataSource = .id(id)
        self.delegate = delegate
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    public override func viewDidLoad() {
        super.viewDidLoad()
        updateScreen()
    }
}

// MARK: - Data source

extension OfferDetailViewController {

    private enum DataSource {
        case offer(OfferResponse)
        case id(Int)
    }

    private var offerID: Int {
        switch dataSource {
        case .offer(let offer):
            return offer.offerId
        case .id(let id):
            return id
        }
    }

    private func updateScreen() {
        switch dataSource {
        case .offer:
            updateContent()
            readOffer()

        case .id:
            resetUI()
            fetchOffer()
        }
    }

    private func updateDataSource(source: DataSource) {
        dataSource = source
        updateScreen()
    }
}

// MARK: - Actions

extension OfferDetailViewController {

    private func fetchOffer() {
        hideError()
        startLoading()

        do {
            let request = API.Offers.OffersGetOffer.Request(offerId: offerID)
            _ = try Beaconsmind.default.apiRequest(request) { [weak self] result in
                guard let self = self else {
                    return
                }

                self.stopLoading()
                switch result {
                case .success(let response):
                    if let offer = response.success {
                        self.updateDataSource(source: .offer(offer))
                    } else {
                        self.displayError(message: self.strings.refreshingOfferErrorMessage, retryAction: .fetch)
                    }

                case .failure:
                    self.displayError(message: self.strings.refreshingOfferErrorMessage, retryAction: .fetch)
                }
            }
        } catch {
            stopLoading()
            displayError(message: strings.refreshingOfferErrorMessage, retryAction: .fetch)
        }
    }

    private func readOffer() {
        hideError()
        startLoading()

        do {
            _ = try Beaconsmind.default.markOfferAsRead(offerID: offerID) { [weak self] result in
                guard let self = self else {
                    return
                }
                self.stopLoading()
                if case .failure = result {
                    self.displayError(message: self.strings.readingOfferErrorMessage, retryAction: .read)
                }
            }
        } catch  {
            displayError(message: strings.readingOfferErrorMessage, retryAction: .read)
        }
    }

    private func redeemOffer() {
        hideError()
        startLoading()

        do {
            _ = try Beaconsmind.default.markOfferAsRedeemed(offerID: offerID) { [weak self] result in
                guard let self = self else {
                    return
                }
                self.stopLoading()
                switch(result) {
                case .success:
                    self.showMessage(
                        message: self.strings.redeemSuccessMessage,
                        okButtonText: self.strings.alertOKTitle,
                        callback: {
                            if let delegate = self.delegate {
                                if delegate.shouldDismissAfterRedeemed {
                                    self.navigationController?.popViewController(animated: true)
                                }
                                delegate.onOfferDetailView(controller: self, didRedeemOfferID: self.offerID)
                            }
                        }
                    )
                    self.redeemButton.removeFromSuperview()
                    self.fetchOffer()
                case .failure:
                    self.displayError(message: self.strings.redeemingOfferErrorMessage, retryAction: .redeem)
                }
            }
        } catch  {
            displayError(message: strings.redeemingOfferErrorMessage, retryAction: .redeem)
        }
    }

    private func sendSilentClickedEvent() {
        _ = try? Beaconsmind.default.markOfferAsClicked(offerID: offerID)
    }
}

// MARK: - UI

extension OfferDetailViewController {

    private func resetUI() {
        edgesForExtendedLayout = []
        view.backgroundColor = styles.backgroundColor

        view.removeAllSubviews()

        /// Removing previous views and reinitailizing them
        errorView = UIStackView()
        scrollView = UIScrollView()
        contentView = UIView()
        imageView = UIImageView()
        textView = UITextView()
        deliveredLabel = UILabel()
        titleLabel = UILabel()
        buttonContainer = UIStackView()
        ctaButton = UIButton()
        redeemButton = UIButton()

        /// Readding views and setting them up
        view.addSubview(errorView)
        view.addSubview(scrollView)
        scrollView.addSubview(contentView)
        contentView.addSubview(imageView)
        contentView.addSubview(textView)
        contentView.addSubview(deliveredLabel)
        contentView.addSubview(titleLabel)

        if hasButtons {
            view.addSubview(buttonContainer)
        }

        setupErrorViewUI()
        setupContentUI()
        setupButtonsUI()
    }
}

// MARK: - Scroll view and content

extension OfferDetailViewController {

    private func updateContent() {
        resetUI()

        if case let .offer(offer) = dataSource {
            if let urlString = offer.imageUrl, let url = URL(string: urlString) {
                imageView.af.setImage(withURL: url, completion:  { _ in
                    
                    guard let image = self.imageView.image else {
                        return
                    }
                    self.imageView.heightAnchor.constraint(equalToConstant: self.view.frame.width / image.size.width * image.size.height).isActive = true
                    
                })
            }

            title = offer.title
            
            var attributes = [NSAttributedString.Key: AnyObject]()
            attributes[.foregroundColor] = styles.textViewTextColor
            attributes[.font] = UIFont(name: styles.textViewFont, size: 15)

            let attributedString = NSAttributedString(string: offer.text?.attributedHTMLString?.string ?? "", attributes: attributes)

            textView.attributedText = attributedString
            deliveredLabel.text = offer.delivered?.getStringFormat("dd.MM.yyyy HH:mm")
            titleLabel.text = offer.title ?? ""
        }
    }

    private func setupContentUI() {
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        contentView.translatesAutoresizingMaskIntoConstraints = false

        scrollView.showsVerticalScrollIndicator = false
        scrollView.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        scrollView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        scrollView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        scrollView.widthAnchor.constraint(equalTo: self.view.widthAnchor).isActive = true

        scrollViewToErrorBottom = scrollView.topAnchor.constraint(equalTo: errorView.bottomAnchor)
        scrollViewToErrorBottom?.priority = .defaultHigh
        scrollViewToErrorBottom?.isActive = true

        let scrollViewToTop: NSLayoutConstraint
        if #available(iOS 11.0, *) {
            scrollViewToTop = scrollView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor)
        } else {
            scrollViewToTop = scrollView.topAnchor.constraint(equalTo: view.topAnchor)
        }
        scrollViewToTop.priority = .defaultLow
        scrollViewToTop.isActive = true

        scrollView.bottomAnchor.constraint(
            equalTo: hasButtons ? buttonContainer.topAnchor : view.bottomAnchor,
            constant: -20
        ).isActive = true

        contentView.topAnchor.constraint(equalTo: scrollView.topAnchor).isActive = true
        contentView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor).isActive = true
        contentView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor).isActive = true
        contentView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor).isActive = true
        contentView.widthAnchor.constraint(equalTo: scrollView.widthAnchor).isActive = true

        /// Image view
        imageView.clipsToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
        imageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor).isActive = true
        imageView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor).isActive = true
        
        /// Title label
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 16).isActive = true
        titleLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 18).isActive = true
        titleLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor).isActive = true
        titleLabel.numberOfLines = 0
        titleLabel.lineBreakMode = .byWordWrapping
        titleLabel.font = UIFont(name: styles.titleFont, size: 18)
        titleLabel.textColor = .black
        
        let padding = 16 * spacing
        
        /// Text view
        textView.backgroundColor = .clear
        textView.isScrollEnabled = false
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.topAnchor.constraint(equalTo: titleLabel.bottomAnchor).isActive = true
        textView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor).isActive = true
        textView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor).isActive = true
        textView.textContainerInset = UIEdgeInsets(top: padding, left: padding, bottom: padding, right: padding)
        textView.isEditable = false
        textView.isSelectable = true
        
        ///Delievered label
        deliveredLabel.translatesAutoresizingMaskIntoConstraints = false
        deliveredLabel.topAnchor.constraint(equalTo: textView.bottomAnchor, constant: 10).isActive = true
        deliveredLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
        deliveredLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 10).isActive = true
        deliveredLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor).isActive = true
        deliveredLabel.font = UIFont.systemFont(ofSize: 15)
        deliveredLabel.textColor = .clear // Hide it for the moment, in case needed to place it back
    }
}

// MARK: - Bottom buttons

extension OfferDetailViewController {

    private var isRedeemable: Bool {
        guard case let .offer(offer) = dataSource else {
            return false
        }
        return offer.isRedeemable && !offer.isRedeemed
    }

    private var hasButtons: Bool {
        guard case let .offer(offer) = dataSource else {
            return false
        }
        return isRedeemable || offer.callToAction != nil
    }

    @objc private func onRedeemTapped(_ sender: Any) {
        showQuestion(
            question: strings.redeemQuestionMessage,
            acceptButtonText: strings.alertOKTitle,
            cancelButtonText: strings.alertCancelTitle
        ) { [weak self] accepted in
            if accepted {
                self?.redeemOffer()
            }
        }
    }

    @objc private func onCTATapped(_ sender: Any) {
        sendSilentClickedEvent()

        if
            case let .offer(offer) = dataSource,
            let urlString = offer.callToAction?.link,
            let url = URL(string: urlString)
        {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }

    private func setupButtonsUI() {
        guard
            case let .offer(offer) = dataSource,
            hasButtons
        else {
            return
        }

        /// Container
        buttonContainer.alignment = .center
        buttonContainer.axis = .horizontal
        buttonContainer.distribution = .equalCentering
        buttonContainer.spacing = 20 * spacing
        buttonContainer.translatesAutoresizingMaskIntoConstraints = false
        buttonContainer.heightAnchor.constraint(equalToConstant: styles.buttonHeight).isActive = true
        if #available(iOS 11.0, *) {
            buttonContainer.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -25 * spacing).isActive = true
        } else {
            buttonContainer.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -25 * spacing).isActive = true
        }
        buttonContainer.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true

        /// Call to action button
        if let cta = offer.callToAction {
            buttonContainer.addArrangedSubview(ctaButton)

            ctaButton.setTitle(cta.title, for: .normal)
            ctaButton.setTitleColor(styles.primaryButtonTitleColor, for: .normal)
            ctaButton.layer.cornerRadius = styles.buttonCornerRadius
            ctaButton.backgroundColor = styles.primaryButtonBackgroundColor
            ctaButton.addTarget(self, action: #selector(onCTATapped(_:)), for: .touchUpInside)
            ctaButton.contentEdgeInsets = .init(top: 15, left: 30, bottom: 15, right: 30)
        }

        /// Redeem button
        if isRedeemable {
            buttonContainer.addArrangedSubview(redeemButton)

            redeemButton.setTitle(strings.redeemButtonTitle, for: .normal)
            redeemButton.setTitleColor(styles.redeemButtonTitleColor, for: .normal)
            redeemButton.layer.cornerRadius = styles.buttonCornerRadius
            redeemButton.backgroundColor = styles.redeemButtonBackgroundColor
            redeemButton.addTarget(self, action: #selector(onRedeemTapped(_:)), for: .touchUpInside)
            redeemButton.contentEdgeInsets = .init(top: 15, left: 30, bottom: 15, right: 30)
        }
    }
}

// MARK: - View-specific error handling

extension OfferDetailViewController {

    func displayError(message: String, retryAction: OfferActionButton.Action) {
        displayError(message: message) { [weak self] in
            guard let self = self else {
                return
            }
            self.addRetryButton(action: retryAction)
            self.scrollViewToErrorBottom?.constant = 12 * self.spacing
        }
    }

    func hideError() {
        hideError { [weak self] in
            self?.scrollViewToErrorBottom?.constant = 0
        }
    }

    private func addRetryButton(action: OfferActionButton.Action) {
        let button = OfferActionButton()
        button.retryAction = action
        button.setTitle(strings.retryButtonTitle, for: .normal)
        button.setTitleColor(styles.primaryButtonTitleColor, for: .normal)
        button.layer.cornerRadius = styles.buttonCornerRadius
        button.backgroundColor = styles.primaryButtonBackgroundColor
        button.addTarget(self, action: #selector(onRetryTapped(_:)), for: .touchUpInside)
        button.contentEdgeInsets = .init(top: 15, left: 30, bottom: 15, right: 30)
        errorView.addArrangedSubview(button)
        retryButton = button
    }

    @objc private func onRetryTapped(_ sender: OfferActionButton) {
        hideError()
        switch sender.retryAction {
        case .read:
            readOffer()
        case .redeem:
            redeemOffer()
        case .fetch:
            fetchOffer()
        }
    }
}
