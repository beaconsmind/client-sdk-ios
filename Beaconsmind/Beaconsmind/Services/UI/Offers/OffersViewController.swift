//
//  Copyright © 07.02.22 . All rights reserved.
//

import UIKit

public class OffersViewController: BeaconsmindViewController {

    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .plain)
        tableView.backgroundColor = .clear
        tableView.dataSource = self
        tableView.delegate = self
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.estimatedRowHeight = 140
        tableView.separatorStyle = .none
        tableView.rowHeight = UITableView.automaticDimension
        tableView.register(OfferCell.self, forCellReuseIdentifier: cellID)
        tableView.contentInset.top = 8 * spacing
        tableView.contentInset.bottom = 100
        return tableView
    }()

    private var tableViewToErrorBottom: NSLayoutConstraint?

    private(set) var offers: [OfferResponse] = []

    private var cellID: String {
        String(describing: OfferCell.self)
    }

    public override func viewDidLoad() {
        super.viewDidLoad()
        title = strings.offersListTitle

        setupUI()
    }

    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        fetchOffers()
    }

    private func fetchOffers() {
        hideError()
        startLoading()
        do {
            let request = API.Offers.OffersGetOffers.Request()
            _ = try Beaconsmind.default.apiRequest(request) { [weak self] result in
                guard let self = self else {
                    return
                }
                self.stopLoading()
                switch result {
                case let .success(response):
                    self.offers = response.success ?? []
                    self.tableView.reloadData()
                case .failure:
                    self.displayError(message: self.strings.fetchingOffersErrorMessage)
                }
            }
        } catch {
            stopLoading()
            displayError(message: strings.fetchingOffersErrorMessage)
        }
    }
}

// MARK: - UI

extension OffersViewController {

    private func setupUI() {
        edgesForExtendedLayout = []
        view.backgroundColor = styles.backgroundColor

        view.addSubview(errorView)
        view.addSubview(tableView)

        setupErrorViewUI()
        setupTableViewUI()
    }

    private func setupTableViewUI() {
        tableViewToErrorBottom = tableView.topAnchor.constraint(equalTo: errorView.bottomAnchor)
        tableViewToErrorBottom?.priority = .defaultHigh
        tableViewToErrorBottom?.isActive = true

        let tableViewToTop: NSLayoutConstraint
        if #available(iOS 11.0, *) {
            tableViewToTop = tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor)
        } else {
            tableViewToTop = tableView.topAnchor.constraint(equalTo: view.topAnchor)
        }
        tableViewToTop.priority = .defaultLow
        tableViewToTop.isActive = true

        if #available(iOS 11.0, *) {
            tableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        } else {
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        }

        tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
    }
}

// MARK: - Table view data source and delegate

extension OffersViewController: UITableViewDataSource, UITableViewDelegate {

    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        offers.count
    }

    public func numberOfSections(in tableView: UITableView) -> Int {
        1
    }

    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath) as? OfferCell else {
            return UITableViewCell()
        }
        cell.update(offer: offers[indexPath.row])
        return cell
    }

    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let controller = OfferDetailViewController(offer: offers[indexPath.row], delegate: self)
        navigationController?.pushViewController(controller, animated: true)
    }
}

// MARK: - Offer detail view controller delegate

extension OffersViewController: OfferDetailViewControllerDelegate {

    public var shouldDismissAfterRedeemed: Bool {
        false
    }

    public func onOfferDetailView(controller: OfferDetailViewController, didRedeemOfferID: Int) {
        showMessage(message: "Enjoy your product")
    }
}

// MARK: - View-specific error handling

extension OffersViewController {

    func displayError(message: String) {
        displayError(message: message) { [weak self] in
            guard let self = self else {
                return
            }
            self.addRetryButton()
            self.tableViewToErrorBottom?.constant = 12 * self.spacing
        }
    }

    func hideError() {
        hideError { [weak self] in
            self?.tableViewToErrorBottom?.constant = 0
        }
    }

    private func addRetryButton() {
        let button = OfferActionButton()
        button.retryAction = .fetch
        button.setTitle(strings.retryButtonTitle, for: .normal)
        button.setTitleColor(styles.primaryButtonTitleColor, for: .normal)
        button.layer.cornerRadius = styles.buttonCornerRadius
        button.backgroundColor = styles.primaryButtonBackgroundColor
        button.addTarget(self, action: #selector(onRetryTapped(_:)), for: .touchUpInside)
        button.contentEdgeInsets = .init(top: 15, left: 30, bottom: 15, right: 30)
        errorView.addArrangedSubview(button)
        retryButton = button
    }

    @objc private func onRetryTapped(_ sender: OfferActionButton) {
        hideError()
        if sender.retryAction == .fetch {
            fetchOffers()
        }
    }
}
