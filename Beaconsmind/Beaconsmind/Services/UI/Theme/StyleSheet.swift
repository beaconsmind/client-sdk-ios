//
//  Copyright © 03.02.22 . All rights reserved.
//

import UIKit

public struct StyleSheet {
    /// Sets the background color of the screen. Defaults to .white.
    let backgroundColor: UIColor
    /// Sets redeem button's title color. Defaults to .white.
    let redeemButtonTitleColor: UIColor
    /// Sets redeem button's background color. Defaults to .black.
    let redeemButtonBackgroundColor: UIColor
    /// Sets primary action button's title color. Defaults to .white.
    let primaryButtonTitleColor: UIColor
    /// Sets primary action button's background color. Defaults to .black.
    let primaryButtonBackgroundColor: UIColor
    /// Sets all button's height. Defaults to 50.
    let buttonHeight: CGFloat
    /// Sets button's corner radius. Defaults to 5.
    let buttonCornerRadius: CGFloat
    /// Sets spacing multipler. It will multiply the default paddings on the whole screen. Defaults to 1. Min is 0.1, max is 3.
    let spacingMultiplier: CGFloat
    /// Sets optional error message's text color. Defaults to .black,
    let errorMessageColor: UIColor
    /// Sets loading view's content color. Optional.
    let loadingViewContentColor: UIColor?
    /// Sets loading view's background color. Optional.
    let loadingViewBackgroundColor: UIColor?
    /// Sets cell text color. Defaults to ".black"
    let cellTextColor: UIColor
    /// Sets cell shadow color. Defaults to ".black.withAlphaComponent(0.25)"
    let cellShadowColor: UIColor
    /// Sets cell shadow radius. Defaults to "5"
    let cellShadowRadius: CGFloat
    /// Sets cell corner radius. Defaults to "5"
    let cellCornerRadius: CGFloat
    /// Sets the textView font
    let textViewFont: String
    /// Sets the textView text Color
    let textViewTextColor: UIColor
    /// Sets the title font
    let titleFont: String

    public init(
        backgroundColor: UIColor = .white,
        redeemButtonTitleColor: UIColor = .white,
        redeemButtonBackgroundColor: UIColor = .black,
        primaryButtonTitleColor: UIColor = .white,
        primaryButtonBackgroundColor: UIColor = .black,
        buttonHeight: CGFloat = 50,
        buttonCornerRadius: CGFloat = 5,
        spacingMultiplier: CGFloat = 1,
        errorMessageColor: UIColor = .black,
        loadingViewContentColor: UIColor? = nil,
        loadingViewBackgroundColor: UIColor? = nil,
        cellTextColor: UIColor = .black,
        cellShadowColor: UIColor = .black.withAlphaComponent(0.25),
        cellShadowRadius: CGFloat = 5,
        cellCornerRadius: CGFloat = 5,
        textViewFont: String = "",
        textViewTextColor: UIColor = .white,
        titleFont: String = ""
    ) {
        self.backgroundColor = backgroundColor
        self.redeemButtonTitleColor = redeemButtonTitleColor
        self.redeemButtonBackgroundColor = redeemButtonBackgroundColor
        self.primaryButtonTitleColor = primaryButtonTitleColor
        self.primaryButtonBackgroundColor = primaryButtonBackgroundColor
        self.buttonCornerRadius = buttonCornerRadius
        self.buttonHeight = buttonHeight
        self.spacingMultiplier = min(3, max(0.1, spacingMultiplier))
        self.errorMessageColor = errorMessageColor
        self.loadingViewContentColor = loadingViewContentColor
        self.loadingViewBackgroundColor = loadingViewBackgroundColor
        self.cellTextColor = cellTextColor
        self.cellShadowColor = cellShadowColor
        self.cellShadowRadius = cellShadowRadius
        self.cellCornerRadius = cellCornerRadius
        self.textViewFont = textViewFont
        self.textViewTextColor = textViewTextColor
        self.titleFont = titleFont
    }
}
