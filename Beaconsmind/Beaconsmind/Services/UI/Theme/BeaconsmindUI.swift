//
//  Copyright © 04.02.22 . All rights reserved.
//

import Foundation

public class BeaconsmindUI {
    public static let `default` = BeaconsmindUI()

    /// `StyleSheet` optional instance to override the default UI styles used in the SDK
    public lazy var styleSheet: StyleSheet = StyleSheet()

    /// `LocalizedStrings` optional instance to override the default strings used in the SDK
    public lazy var localizedStrings: LocalizedStrings = LocalizedStrings()

    public init() {}
}
