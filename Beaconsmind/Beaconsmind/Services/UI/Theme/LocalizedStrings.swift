//
//  Copyright © 03.02.22 . All rights reserved.
//

import Foundation

public struct LocalizedStrings {
    /// Sets alerts' ok button text. Defaults to "OK".
    let alertOKTitle: String
    /// Sets alerts' cancel button text. Defaults to "Cancel".
    let alertCancelTitle: String
    /// Sets retry button text. Defaults to "Retry",
    let retryButtonTitle: String
    /// Sets redeem button text. Defaults to "Retry",
    let redeemButtonTitle: String
    /// Sets redeem question message. Defaults to "Do you want to redeem this Offer?".
    let redeemQuestionMessage: String
    /// Sets redeem success message. Defaults to "Offer redeemed!".
    let redeemSuccessMessage: String
    /// Sets optional error message when reading an offer fails. Defaults to "Couldn't redeem offer, please try again".
    let redeemingOfferErrorMessage: String
    /// Sets optional error message when reading an offer fails. Defaults to "Couldn't read offer, please try again".
    let readingOfferErrorMessage: String
    /// Sets optional error message when reading an offer fails. Defaults to "Couldn't refresh offer, please try again".
    let refreshingOfferErrorMessage: String
    /// Sets offer list screen's title. Defaults to "Offers".
    let offersListTitle: String
    /// Sets optional error message when fetching offers fail. Defaults to "Couldn't fetch offers, please try again".
    let fetchingOffersErrorMessage: String
    /// Sets redeemed string on offers list cell. Defaults to "Redeemed".
    let redeemed: String
    /// Sets not redeemed string on offers list cell. Defaults to "Not redeemed".
    let notRedeemed: String
    /// Sets validity prefix string on offers list cell. Defaults to "Validity: ".
    let offerCellValidityPrefix: String
    /// Sets text for read more button on cells. Defaults to "Read more".
    let readMoreTitle: String

    public init(
        alertOKTitle: String = "OK",
        alertCancelTitle: String = "Cancel",
        retryButtonTitle: String = "Retry",
        redeemButtonTitle: String = "Redeem",
        redeemQuestionMessage: String = "Do you want to redeem this offer?",
        redeemSuccessMessage: String = "Offer redeemed",
        redeemingOfferErrorMessage: String = "Couldn't redeem offer, please try again",
        readingOfferErrorMessage: String = "Couldn't read offer, please try again",
        refreshingOfferErrorMessage: String = "Couldn't refresh offer, please try again",
        offersListTitle: String = "Offers",
        fetchingOffersErrorMessage: String = "Couldn't fetch offers, please try again",
        redeemed: String = "Redeemed",
        notRedeemed: String = "Not redeemed",
        offerCellValidityPrefix: String = "Validity: ",
        readMoreTitle: String = "Read more"
    ) {
        self.alertOKTitle = alertOKTitle
        self.alertCancelTitle = alertCancelTitle
        self.retryButtonTitle = retryButtonTitle
        self.redeemButtonTitle = redeemButtonTitle
        self.redeemQuestionMessage = redeemQuestionMessage
        self.redeemSuccessMessage = redeemSuccessMessage
        self.redeemingOfferErrorMessage = redeemingOfferErrorMessage
        self.readingOfferErrorMessage = readingOfferErrorMessage
        self.refreshingOfferErrorMessage = refreshingOfferErrorMessage
        self.offersListTitle = offersListTitle
        self.fetchingOffersErrorMessage = fetchingOffersErrorMessage
        self.redeemed = redeemed
        self.notRedeemed = notRedeemed
        self.offerCellValidityPrefix = offerCellValidityPrefix
        self.readMoreTitle = readMoreTitle
    }
}
