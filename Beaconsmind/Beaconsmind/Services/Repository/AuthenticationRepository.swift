//
//  Copyright © 02.12.21 Beaconsmind. All rights reserved.
//

import Foundation

protocol AuthenticationRepository {

    func signup(
        username: String,
        firstName: String,
        lastName: String,
        password: String,
        confirmPassword: String,
        avatarThumbnailURL: String?,
        avatarURL: String?,
        language: String?,
        gender: String?,
        favoriteStoreID: Int?,
        birthDate: DateDay?,
        countryCode: String?,
        phoneNumber: String?,
        callback: @escaping (SDKApiResult<APIContext>) -> Void
    ) throws

    func login(
        username: String,
        password: String,
        callback: @escaping (SDKApiResult<APIContext>) -> Void
    ) throws

    func importAccount(
        id: String,
        email: String,
        firstName: String?,
        lastName: String?,
        birthDate: DateTime?,
        language: String?,
        gender: String?,
        deviceRegistration: DeviceRegistrationModel?,
        callback: @escaping (SDKApiResult<APIContext>) -> Void
    ) throws

    func externalLogin(
        externalAccessToken: String,
        providerType: ProviderType,
        language: String?,
        firstName: String?,
        lastName: String?,
        deviceRegistration: DeviceRegistrationModel?,
        callback: @escaping (SDKApiResult<APIContext>) -> Void
    ) throws
    
    func delete(
        callback: @escaping (SDKApiResult<Bool>) -> Void
    ) throws
}

final class AuthenticationRepositoryImpl: AuthenticationRepository {

    private let contextManager: ContextManager

    init(contextManager: ContextManager) {
        self.contextManager = contextManager
    }

    func signup(
        username: String,
        firstName: String,
        lastName: String,
        password: String,
        confirmPassword: String,
        avatarThumbnailURL: String?,
        avatarURL: String?,
        language: String? = nil,
        gender: String? = nil,
        favoriteStoreID: Int? = nil,
        birthDate: DateDay? = nil,
        countryCode: String? = nil,
        phoneNumber: String? = nil,
        callback: @escaping (SDKApiResult<APIContext>) -> Void
    ) throws {
        guard
            let hostname = contextManager.hostname,
            let apiClient = contextManager.getUnauthenticatedAPIClient()
        else {
            throw SDKApiError.notStarted
        }

        let model = CreateUserRequest(
            confirmPassword: confirmPassword,
            firstName: firstName,
            lastName: lastName,
            password: password,
            username: username,
            avatarThumbnailUrl: avatarThumbnailURL,
            avatarUrl: avatarURL,
            birthday: birthDate,
            countryCode: countryCode,
            favoriteStoreId: favoriteStoreID,
            gender: gender,
            language: language,
            phoneNumber: phoneNumber
        )
        let request = API.Accounts.AccountsCreateUser.Request(body: model)

        _ = apiClient.apiRequest(request) { [weak self] result in
            switch result {
            case let .success(value):
                if
                    let response = value.success,
                    let token = response.token?.accessToken,
                    let userID = JWTDecoder.decode(jwtToken: token)[Constants.Param.nameid.rawValue] as? String
                {
                    let context = APIContext(
                        hostname: hostname,
                        accessToken: token,
                        userID: userID,
                        tokenType: response.token?.tokenType,
                        expiresIn: response.token?.expiresIn
                    )
                    do {
                        try self?.contextManager.saveContext(context: context)
                        callback(.success(context))
                    } catch {
                        callback(.failure(SDKApiError.invalidContext))
                    }
                } else {
                    callback(.failure(SDKApiError.invalidContext))
                }
            case let .failure(error):
                    callback(.failure(error))
            }
        }
    }

    func login(
        username: String,
        password: String,
        callback: @escaping (SDKApiResult<APIContext>) -> Void
    ) throws {
        guard
            let hostname = contextManager.hostname,
            let apiClient = contextManager.getUnauthenticatedAPIClient()
        else {
            throw SDKApiError.notStarted
        }

        let request = API.Auth.AuthToken.Request(
            grantType: Constants.Param.password.rawValue,
            password: password,
            username: username
        )
        _ = apiClient.apiRequest(request) { [weak self] result in
            switch result {
            case let .success(value):
                if
                    let response = value.success,
                    let token = response.accessToken,
                    let userID = JWTDecoder.decode(jwtToken: token)[Constants.Param.nameid.rawValue] as? String
                {
                    let context = APIContext(
                        hostname: hostname,
                        accessToken: token,
                        userID: userID,
                        tokenType: response.tokenType,
                        expiresIn: response.expiresIn
                    )
                    do {
                        try self?.contextManager.saveContext(context: context)
                        callback(.success(context))
                    } catch {
                        callback(.failure(SDKApiError.invalidContext))
                    }
                } else {
                    callback(.failure(SDKApiError.invalidContext))
                }
            case let .failure(error):
                callback(.failure(error))
            }
        }
    }

    func importAccount(
        id: String,
        email: String,
        firstName: String?,
        lastName: String?,
        birthDate: DateTime?,
        language: String?,
        gender: String?,
        deviceRegistration: DeviceRegistrationModel?,
        callback: @escaping (SDKApiResult<APIContext>) -> Void
    ) throws {
        guard
            let hostname = contextManager.hostname,
            let apiClient = contextManager.getUnauthenticatedAPIClient()
        else {
            throw SDKApiError.notStarted
        }
        
        let model = ImportUserRequest(
            email: email,
            id: id,
            birthDate: birthDate,
            deviceRegistration: deviceRegistration,
            firstName: firstName,
            gender: gender,
            language: language,
            lastName: lastName
        )
        let request = API.Accounts.AccountsImportUser.Request(body: model)
        _ = apiClient.apiRequest(request) { [weak self] result in
            switch result {
            case let .success(value):
                if
                    let response = value.success,
                    let token = response.accessToken,
                    let userID = JWTDecoder.decode(jwtToken: token)[Constants.Param.nameid.rawValue] as? String
                {
                    let context = APIContext(
                        hostname: hostname,
                        accessToken: token,
                        userID: userID,
                        tokenType: response.tokenType,
                        expiresIn: response.expiresIn
                    )
                    do {
                        try self?.contextManager.saveContext(context: context)
                        callback(.success(context))
                    } catch {
                        callback(.failure(SDKApiError.invalidContext))
                    }
                } else {
                    callback(.failure(SDKApiError.invalidContext))
                }
            case let .failure(error):
                callback(.failure(error))
            }
        }
    }

    func externalLogin(
        externalAccessToken: String,
        providerType: ProviderType,
        language: String? = nil,
        firstName: String? = nil,
        lastName: String? = nil,
        deviceRegistration: DeviceRegistrationModel?,
        callback: @escaping (SDKApiResult<APIContext>) -> Void
    ) throws {
        guard
            let hostname = contextManager.hostname,
            let apiClient = contextManager.getUnauthenticatedAPIClient()
        else {
            throw SDKApiError.notStarted
        }

        let model = RegisterViaSocialProviderRequest(
            externalAccessToken: externalAccessToken,
            providerType: providerType,
            deviceRegistration: deviceRegistration,
            firstName: firstName,
            language: language,
            lastName: lastName
        )
        let request = API.Accounts.AccountsRegisterViaSocialProviders.Request(body: model)
        _ = apiClient.apiRequest(request) { [weak self] result in
            switch result {
                case let .success(value):
                    if
                        let response = value.success,
                        let token = response.accessToken,
                        let userID = JWTDecoder.decode(jwtToken: token)[Constants.Param.nameid.rawValue] as? String
                    {
                        let context = APIContext(
                            hostname: hostname,
                            accessToken: token,
                            userID: userID,
                            tokenType: response.tokenType,
                            expiresIn: response.expiresIn
                        )
                        do {
                            try self?.contextManager.saveContext(context: context)
                            callback(.success(context))
                        } catch {
                            callback(.failure(SDKApiError.invalidContext))
                        }
                    } else {
                        callback(.failure(SDKApiError.invalidContext))
                    }
                case let .failure(error):
                    callback(.failure(error))
            }
        }
    }
    
    func delete(
        callback: @escaping (SDKApiResult<Bool>) -> Void
    ) throws {
        guard
            let apiClient = contextManager.getAPIClient()
        else {
            throw SDKApiError.invalidContext
        }
        
        let request = API.Accounts.AccountsDeleteUser.Request()
        _ = apiClient.apiRequest(request, emptyResponseCodes: [200],  complete: { result in
                        
            switch result {
            case let .success(value):
                if value.successful {
                    callback(.success(true))
                } else {
                    callback(.failure(SDKApiError.invalidContext))
                }
            case let .failure(error):
                print(error)
                callback(.failure(error))
            }
        })
    }
}
