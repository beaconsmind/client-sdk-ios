//
//  Copyright © 02.12.21 Beaconsmind. All rights reserved.
//

import Foundation

protocol TouchpointsRepository {

    func saveTouchpoint(
        appVersion: String,
        type: TouchpointType,
        deviceID: String,
        callback: ((SDKApiResult<Bool>)->Void)?
    ) throws -> CancellableRequest?

    func saveAppDownloadTouchpoint(
        appVersion: String,
        deviceID: String,
        callback: ((SDKApiResult<Bool>)->Void)?
    ) throws
}

final class TouchpointsRepositoryImpl: TouchpointsRepository {

    private let contextManager: ContextManager

    init(contextManager: ContextManager) {
        self.contextManager = contextManager
    }

    @discardableResult
    func saveTouchpoint(
        appVersion: String,
        type: TouchpointType,
        deviceID: String,
        callback: ((SDKApiResult<Bool>)->Void)? = nil
    ) throws -> CancellableRequest? {
        guard let client = contextManager.getAPIClient() else {
            throw SDKApiError.invalidContext
        }

        let model = SaveTouchpointRequest(
            type: type,
            appVersion: appVersion,
            localDateTime: Date(),
            uniqueDeviceId: deviceID
        )
        let request = API.Touchpoints.TouchpointsSaveTouchpoint.Request(body: model)
        return client.apiRequest(request, emptyResponseCodes: [200]) { result in
            switch result {
            case let .success(readResponse):
                callback?(SDKApiResult.success(readResponse.successful))
            case let .failure(responseError):
                callback?(SDKApiResult.failure(responseError))
            }
        }
    }

    func saveAppDownloadTouchpoint(
        appVersion: String,
        deviceID: String,
        callback: ((SDKApiResult<Bool>)->Void)? = nil
    ) throws {
        guard let apiClient = contextManager.getUnauthenticatedAPIClient() else {
            throw SDKApiError.notStarted
        }
        let model = SaveTouchpointRequest(
            type: .AppDownload,
            appVersion: appVersion,
            localDateTime: Date(),
            uniqueDeviceId: deviceID
        )
        let request = API.Touchpoints.TouchpointsSaveTouchpoint.Request(body: model)
        _ = apiClient.apiRequest(request, emptyResponseCodes: [200]) { result in
            switch result {
            case let .success(value):
                callback?(.success(value.successful))
            case let .failure(error):
                callback?(.failure(error))
            }
        }
    }
}
