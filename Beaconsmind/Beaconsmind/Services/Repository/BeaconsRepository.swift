//
//  Copyright © 02.12.21 Beaconsmind. All rights reserved.
//

import Foundation

protocol BeaconsRepository {

    var beaconContactsSummaries: [BeaconContactsSummary] { get }

    func startListeningBeacons(delegate: BeaconListenerDelegate?) throws
    func stopListeningBeacons()
}

final class BeaconsRepositoryImpl: BeaconsRepository {

    private let store: Store
    private let state: State
    private let contextManager: ContextManager
    private var monitoringManager: BeaconMonitoringManager?

    var beaconContactsSummaries: [BeaconContactsSummary] {
        monitoringManager?.beaconContactsSummaries ?? []
    }

    init(contextManager: ContextManager, store: Store, state: State) {
        self.contextManager = contextManager
        self.store = store
        self.state = state
    }

    func startListeningBeacons(delegate: BeaconListenerDelegate?) throws {
        guard
            let context = contextManager.context,
            let client = contextManager.getAPIClient()
        else {
            Logger.log("Cannot start listening beacons due to invalid context", category: .general, level: .error)
            throw SDKApiError.invalidContext
        }

        guard Thread.isMainThread else {
            Logger.log("Cannot start listening beacons because it must be called on the main thread", category: .general, level: .error)
            throw SDKApiError.needsMainThread
        }

        let manager = BeaconMonitoringManagerImpl(
            userID: context.userID,
            apiClient: client,
            delegate: delegate,
            store: store,
            state: state
        )

        guard manager.start() else {
            Logger.log("Cannot start listening beacons because location services not enabled", category: .general, level: .warning)
            throw SDKApiError.location(.locationServicesNotEnabled)
        }

        monitoringManager = manager
        Logger.log("Started listening beacons for user id %@", category: .general, context.userID)
    }

    func stopListeningBeacons() {
        monitoringManager = nil
        Logger.log("Stopped listening beacons", category: .general)
    }
}
