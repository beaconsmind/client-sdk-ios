//
//  Copyright © 2022 beaconsmind. All rights reserved.
//

import Foundation
import CoreLocation

public typealias LocationPermissionRequestCompletionHandler = (Bool) -> Void

protocol LocationsRepository {

    func requestPermission(callback: LocationPermissionRequestCompletionHandler?)
    func getCountries(callback: @escaping (SDKApiResult<[CountryView]?>) -> Void) throws -> CancellableRequest?
    func getCitiesForCountry(countryID: Int, callback: @escaping (SDKApiResult<[CityView]?>) -> Void) throws -> CancellableRequest?
}

final class LocationsRepositoryImpl: NSObject, LocationsRepository {

    private let contextManager: ContextManager
    private var onAuthorizedCallback: LocationPermissionRequestCompletionHandler?

    private lazy var locationManager: CLLocationManager = {
        let manager = CLLocationManager()
        manager.delegate = self
        return manager
    }()

    private var isAuthorized: Bool {
        let status: CLAuthorizationStatus
        if #available(iOS 14.0, *) {
            status = locationManager.authorizationStatus
        } else {
            status = CLLocationManager.authorizationStatus()
        }
        return [.authorizedAlways, .authorizedWhenInUse].contains(status)
    }

    init(contextManager: ContextManager) {
        self.contextManager = contextManager
    }

    func requestPermission(callback: LocationPermissionRequestCompletionHandler?) {
        guard isAuthorized else {
            onAuthorizedCallback = callback
            locationManager.requestAlwaysAuthorization()
            return
        }
        callback?(true)
    }

    @discardableResult
    func getCountries(callback: @escaping (SDKApiResult<[CountryView]?>) -> Void) throws -> CancellableRequest? {
        guard let client = contextManager.getUnauthenticatedAPIClient() else {
            throw SDKApiError.notStarted
        }

        let request = API.Locations.LocationsGetCountries.Request()
        return client.apiRequest(request) { result in
            switch result {
                case let .success(readResponse):
                    callback(SDKApiResult.success(readResponse.success))
                case let .failure(responseError):
                    callback(SDKApiResult.failure(responseError))
            }
        }
    }

    @discardableResult
    func getCitiesForCountry(countryID: Int, callback: @escaping (SDKApiResult<[CityView]?>) -> Void) throws -> CancellableRequest? {
        guard let client = contextManager.getUnauthenticatedAPIClient() else {
            throw SDKApiError.notStarted
        }

        let request = API.Locations.LocationsGetCitiesForCountry.Request(countryId: countryID)
        return client.apiRequest(request) { result in
            switch result {
                case let .success(readResponse):
                    callback(SDKApiResult.success(readResponse.success))
                case let .failure(responseError):
                    callback(SDKApiResult.failure(responseError))
            }
        }
    }
}

extension LocationsRepositoryImpl: CLLocationManagerDelegate {

    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        onAuthorizedCallback?(isAuthorized)
    }
}
