//
//  Copyright © 2022 beaconsmind. All rights reserved.
//

import Foundation

protocol DashboardRepository {

    func getFeaturedItems(callback: @escaping (SDKApiResult<[FeaturedItemView]>) -> Void) throws -> CancellableRequest?
    func getBlogPosts(callback: @escaping (SDKApiResult<[BlogPostView]>) -> Void) throws -> CancellableRequest?
}

final class DashboardRepositoryImpl: DashboardRepository {

    private let contextManager: ContextManager

    init(contextManager: ContextManager) {
        self.contextManager = contextManager
    }

    func getFeaturedItems(callback: @escaping (SDKApiResult<[FeaturedItemView]>) -> Void) throws -> CancellableRequest? {
        guard let client = contextManager.getAPIClient() else {
            throw SDKApiError.invalidContext
        }

        let request = API.Dashboard.DashboardGetFeaturedItems.Request()
        return client.apiRequest(request) { response in
            switch response {
                case let .success(result):
                    callback(SDKApiResult.success(result.success ?? []))
                case let .failure(error):
                    callback(SDKApiResult.failure(error))
            }
        }
    }

    func getBlogPosts(callback: @escaping (SDKApiResult<[BlogPostView]>) -> Void) throws -> CancellableRequest? {
        guard let client = contextManager.getAPIClient() else {
            throw SDKApiError.invalidContext
        }

        let request = API.Dashboard.DashboardGetBlogPosts.Request()
        return client.apiRequest(request) { response in
            switch response {
                case let .success(result):
                    callback(SDKApiResult.success(result.success ?? []))
                case let .failure(error):
                    callback(SDKApiResult.failure(error))
            }
        }
    }
}
