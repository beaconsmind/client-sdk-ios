//
//  Copyright © 02.12.21 Beaconsmind. All rights reserved.
//

import Foundation

protocol OffersRepository {

    func markOfferAsRead(offerID: Int, callback: ((SDKApiResult<Bool>)->Void)?) throws -> CancellableRequest?
    func markOfferAsReceived(offerID: Int, callback: ((SDKApiResult<Bool>)->Void)?) throws -> CancellableRequest?
    func markOfferAsRedeemed(offerID: Int, callback: ((SDKApiResult<Bool>)->Void)?) throws -> CancellableRequest?
    func markOfferAsClicked(offerID: Int, callback: ((SDKApiResult<Bool>)->Void)?) throws -> CancellableRequest?
}

final class OffersRepositoryImpl: OffersRepository {

    private let contextManager: ContextManager

    init(contextManager: ContextManager) {
        self.contextManager = contextManager
    }

    @discardableResult
    func markOfferAsRead(offerID: Int, callback: ((SDKApiResult<Bool>)->Void)?) throws -> CancellableRequest? {
        guard
            let context = contextManager.context,
            let client = contextManager.getAPIClient()
        else {
            throw SDKApiError.invalidContext
        }

        let model = OfferStatusRequest(offerId: offerID, userId: context.userID)
        let request = API.Offers.OffersRead.Request(body: model)
        return client.apiRequest(request, emptyResponseCodes: [200]) { result in
            switch result {
            case let .success(readResponse):
                callback?(SDKApiResult.success(readResponse.successful))
            case let .failure(responseError):
                callback?(SDKApiResult.failure(responseError))
            }
        }
    }

    @discardableResult
    func markOfferAsReceived(offerID: Int, callback: ((SDKApiResult<Bool>)->Void)?) throws -> CancellableRequest? {
        guard
            let context = contextManager.context,
            let client = contextManager.getAPIClient()
        else {
            throw SDKApiError.invalidContext
        }

        let model = OfferStatusRequest(offerId: offerID, userId: context.userID)
        let request = API.Offers.OffersReceived.Request(body: model)
        return client.apiRequest(request, emptyResponseCodes: [200]) { result in
            switch result {
            case let .success(readResponse):
                callback?(SDKApiResult.success(readResponse.successful))
            case let .failure(responseError):
                callback?(SDKApiResult.failure(responseError))
            }
        }
    }

    @discardableResult
    func markOfferAsRedeemed(offerID: Int, callback: ((SDKApiResult<Bool>)->Void)?) throws -> CancellableRequest? {
        guard
            let context = contextManager.context,
            let client = contextManager.getAPIClient()
        else {
            throw SDKApiError.invalidContext
        }

        let model = OfferStatusRequest(offerId: offerID, userId: context.userID)
        let request = API.Offers.OffersRedeemed.Request(body: model)
        return client.apiRequest(request, emptyResponseCodes: [200]) { result in
            switch result {
            case let .success(readResponse):
                callback?(SDKApiResult.success(readResponse.successful))
            case let .failure(responseError):
                callback?(SDKApiResult.failure(responseError))
            }
        }
    }
    
    @discardableResult
    func markOfferAsClicked(offerID: Int, callback: ((SDKApiResult<Bool>)->Void)?) throws -> CancellableRequest? {
        guard
            let context = contextManager.context,
            let client = contextManager.getAPIClient()
        else {
            throw SDKApiError.invalidContext
        }

        let model = OfferStatusRequest(offerId: offerID, userId: context.userID)
        let request = API.Offers.OffersClicked.Request(body: model)
        return client.apiRequest(request, emptyResponseCodes: [200]) { result in
            switch result {
            case let .success(readResponse):
                callback?(SDKApiResult.success(readResponse.successful))
            case let .failure(responseError):
                callback?(SDKApiResult.failure(responseError))
            }
        }
    }
}
