//
//  Copyright © 2022 beaconsmind. All rights reserved.
//

import Foundation

protocol ProductsRepository {

    func getProductCategories(callback: @escaping (SDKApiResult<[ProductCategoryModel]>) -> Void) throws -> CancellableRequest?
    func getProducts(categoryID: Int, callback: @escaping (SDKApiResult<[ProductModel]>) -> Void) throws -> CancellableRequest?
}

final class ProductsRepositoryImpl: ProductsRepository {

    private let contextManager: ContextManager

    init(contextManager: ContextManager) {
        self.contextManager = contextManager
    }

    func getProductCategories(callback: @escaping (SDKApiResult<[ProductCategoryModel]>) -> Void) throws -> CancellableRequest? {
        guard let client = contextManager.getAPIClient() else {
            throw SDKApiError.invalidContext
        }

        let request = API.Products.ProductsGetCategories.Request()
        return client.apiRequest(request) { response in
            switch response {
                case let .success(result):
                    callback(SDKApiResult.success(result.success ?? []))
                case let .failure(error):
                    callback(SDKApiResult.failure(error))
            }
        }
    }

    func getProducts(categoryID: Int, callback: @escaping (SDKApiResult<[ProductModel]>) -> Void) throws -> CancellableRequest? {
        guard let client = contextManager.getAPIClient() else {
            throw SDKApiError.invalidContext
        }

        let request = API.Products.ProductsGetProductsByCategory.Request(categoryId: categoryID)
        return client.apiRequest(request) { response in
            switch response {
                case let .success(result):
                    callback(SDKApiResult.success(result.success ?? []))
                case let .failure(error):
                    callback(SDKApiResult.failure(error))
            }
        }
    }
}
