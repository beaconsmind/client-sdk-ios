//
//  Copyright © 2023 beaconsmind. All rights reserved.
//

import Foundation

protocol PurchaseHistoriesRepository {

    func getPurchaseHistory(callback: @escaping (SDKApiResult<UserMonthPurchasesViewDataPage?>) -> Void) throws -> CancellableRequest?
    
    func getPurchaseDetails(purchaseHistoryId: Int, callback: @escaping (SDKApiResult<PurchaseHistoryView?>) -> Void) throws -> CancellableRequest?
}

final class PurchaseHistoriesRepositoryImpl: PurchaseHistoriesRepository {
    
    private let contextManager: ContextManager
    
    init(contextManager: ContextManager) {
        self.contextManager = contextManager
    }
    
    func getPurchaseHistory(callback: @escaping (SDKApiResult<UserMonthPurchasesViewDataPage?>) -> Void) throws -> CancellableRequest? {
        guard let client = contextManager.getAPIClient() else {
            throw SDKApiError.invalidContext
        }
        
        let request = API.PurchaseHistories.PurchaseHistoriesGetUserMonthlyPurchases.Request()
        return client.apiRequest(request) { response in
            switch response {
            case let .success(result):
                callback(SDKApiResult.success(result.success ?? UserMonthPurchasesViewDataPage(count: 0)))
            case let .failure(error):
                callback(SDKApiResult.failure(error))
            }
        }
    }
    
    func getPurchaseDetails(purchaseHistoryId: Int, callback: @escaping (SDKApiResult<PurchaseHistoryView?>) -> Void) throws -> CancellableRequest?{
        
        guard let client = contextManager.getAPIClient() else {
            throw SDKApiError.invalidContext
        }
        
        let request = API.PurchaseHistories.PurchaseHistoriesGetPurchaseHistoryDetails.Request(purchaseHistoryId: purchaseHistoryId )
        
        return client.apiRequest(request) { response in
            switch response {
            case let .success(result):
                callback(SDKApiResult.success(result.success))
            case let .failure(error):
                callback(SDKApiResult.failure(error))
            }
        }
        
    }
    
}
