//
//  Copyright © 2022 beaconsmind. All rights reserved.
//

import UIKit
import CoreLocation

protocol DiagnosticsRepository {

    func start()
    func report(force: Bool)
    func getAppConfiguration()
}

extension AppConfigurationResponse {
    func getDictionary() -> [String:Any?] {
        return ["loggingEnabled": loggingEnabled,
                "loggingLevel" : loggingLevel.rawValue,
                "metricsEnabled" : metricsEnabled,
                "bugFenderKey" : bugFenderKey,
                "metricsIntervalSeconds": metricsIntervalSeconds,
                "pocPublicKey": pocPublicKey]
    }
    
    convenience init?(dic: [String:Any?]) {
        guard let rawLoggingLevel = dic["loggingLevel"] as? Int,
              let loggingEnabled = dic["loggingEnabled"] as? Bool,
              let loggingLevel = LoggingLevel(rawValue: rawLoggingLevel),
              let metricsEnabled = dic["metricsEnabled"] as? Bool
        else {
            return nil
        }
                
        let bugFenderKey = dic["bugFenderKey"] as? String
        let metricsIntervalSeconds = dic["metricsIntervalSeconds"] as? Int
        let pocPublicKey = dic["pocPublicKey"] as? String
        
        self.init(loggingEnabled: loggingEnabled,
                  loggingLevel: loggingLevel,
                  metricsEnabled: metricsEnabled,
                  bugFenderKey: bugFenderKey,
                  metricsIntervalSeconds: metricsIntervalSeconds,
                  pocPublicKey: pocPublicKey)
    }
}


class DiagnosticsRepositoryImpl: NSObject, DiagnosticsRepository {
    
    
    private let contextManager: ContextManager
    private let store: Store
    private let state: State

    private var reportTimer: Timer?
    private let reportInitialDelay: TimeInterval
    
    private var metricsEnabled: Bool = false
    private var metricsIntervalSeconds: Int
    private var configurationFetchingState: RequestState = .initial
    
    private var reportingBackgroundTaskID = UIBackgroundTaskIdentifier.invalid

    init(
        contextManager: ContextManager,
        store: Store,
        state: State,
        reportInitialDelay: TimeInterval = Constants.Interval.diagnosticsReportInitialDelay
    ) {
        self.contextManager = contextManager
        self.store = store
        self.state = state
        self.reportInitialDelay = reportInitialDelay
        self.metricsIntervalSeconds = Int(Constants.Interval.diagnosticsReportInterval)
        
        super.init()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.powerStateDidChange), name: NSNotification.Name.NSProcessInfoPowerStateDidChange, object: nil)
        
        if let configDic = UserDefaults.standard.object(forKey: Constants.Key.appConfig.rawValue) as? [String:Any?], let config = AppConfigurationResponse(dic: configDic) {
            self.setConfig(config: config)
        }
    }
    
    @objc func powerStateDidChange(notification: NSNotification) {
        Logger.log("Power state changed. isLowerPowerModeEnabled: \(state.isLowPowerModeEnabled.description)", category: .general)
        /// Send diagnostic report when the battery power mode changes.
        report(force: true)
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
        
        reportTimer?.invalidate()
        reportTimer = nil
    }

    func getAppConfiguration() {
        self.getAppConfiguration(completion: nil)
    }
    
    private func getAppConfiguration(completion: (()->Void)?) {
        guard configurationFetchingState != .working else {
            completion?()
            return
        }
        
        guard let apiClient = contextManager.getUnauthenticatedAPIClient() else {
            completion?()
            return
        }
        
        configurationFetchingState = .working
        
        let request = API.Configuration.ConfigurationGetAppConfiguration.Request(userId: nil, phoneOS: nil, bundleId: nil)
        _ = apiClient.apiRequest(request) { [weak self] response in
            guard let self = self else {
                return
            }
            switch response {
            case let .success(config):
                
                self.configurationFetchingState = .done
                
                if let config = config.success {
                    UserDefaults.standard.set(config.getDictionary(), forKey: Constants.Key.appConfig.rawValue)
                    self.setConfig(config: config)
                }
                
                
            case let .failure(error):
                self.configurationFetchingState = .error
                Logger.log("Error fetching configuration %@", category: .networking, level: .error, error.localizedDescription)
            }
            
            completion?()
        }
    }
    
    private func setConfig(config: AppConfigurationResponse) {

        if let key = config.bugFenderKey {
            Logger.enableRemoteLogging(key: key)
        }
        
        Logger.remoteLoggingLevel = config.loggingLevel
        Logger.remoteLoggingEnabled = config.loggingEnabled
        
        self.metricsEnabled = config.metricsEnabled
        self.metricsIntervalSeconds = config.metricsIntervalSeconds ?? Int(Constants.Interval.diagnosticsReportInterval)
        
        self.reportTimer?.invalidate()
        
        self.reportTimer = Timer.scheduledTimer(withTimeInterval: TimeInterval(self.metricsIntervalSeconds), repeats: true) { [weak self] _ in
            self?.report(force: true)
        }
    }
    

    func start() {
        /// Fetch app configuration
        getAppConfiguration {

            /// Reporting right away to make sure we have some diagnostics
            self.report(force: true)

            /// Reporting with an initial delay to make sure we have location permissions evaluated
            DispatchQueue.main.asyncAfter(deadline: .now() + self.reportInitialDelay) { [weak self] in
                self?.report(force: true)
            }
        }
        
        /// Get app configuration timer
        Timer.scheduledTimer(withTimeInterval: Constants.Interval.appConfigFetchInterval, repeats: true) { [weak self] _ in
            self?.getAppConfiguration(completion: nil)
        }
    }

    func report(force: Bool = false) {
        
        guard metricsEnabled else {
            return
        }
        
        Logger.log("Sending diagnostic reports. Force: \(force.description)", category: .networking)
        
        // Check if enough time has passed since the last invocation so we don't spam the server.
        let lastInvocationTime = UserDefaults.standard.object(forKey: Constants.Key.lastReportInvocationTime.rawValue) as? Date ?? Date.distantPast
        let currentTime = Date()
        let timeSinceLastInvocation = Int(currentTime.timeIntervalSince(lastInvocationTime))
        guard force || timeSinceLastInvocation >= metricsIntervalSeconds  else {
            Logger.log("Aborting diagnostic report because time since last invocation \(timeSinceLastInvocation) is less than report interval", category: .networking)
            return
        }
        
        // Store the current time as the new last invocation time.
        UserDefaults.standard.set(currentTime, forKey: Constants.Key.lastReportInvocationTime.rawValue)
                                  
        reportingBackgroundTaskID = UIApplication.shared.beginBackgroundTask(withName: Constants.ID.backgroundTaskDiagnosticReporting.rawValue) { [weak self] in
            if let id = self?.reportingBackgroundTaskID {
                UIApplication.shared.endBackgroundTask(id)
            }
        }
        
        
        guard
            let client = contextManager.getUnauthenticatedAPIClient()
        else {
            return
        }

        let model = AppDiagnosticsRequest(
            metrics: getMetrics(),
            platformDeviceId: store.deviceToken,
            uniqueDeviceId: store.deviceID,
            userId: contextManager.context?.userID,
            utcDateTime: DateTime()
        )
        let request = API.Diagnostics.DiagnosticsSaveDiagnostics.Request(body: model)

        _ = client.apiRequest(request, emptyResponseCodes: [200]) { [weak self] result in
            guard let self = self else {
                Logger.log("self reference is null", category: .general)
                return
            }

            switch result {
                case.success:
                    Logger.log("Diagnostics sent", category: .general, level: .debug)
                case let .failure(error):
                    Logger.log("Diagnostics sending error", category: .general, level: .error, error.localizedDescription)
            }
            
            /// If the task hasn't been ended yet, end it.
            if self.reportingBackgroundTaskID != .invalid {
                UIApplication.shared.endBackgroundTask(self.reportingBackgroundTaskID)
                self.reportingBackgroundTaskID = .invalid
            }
        }
    }

    func getMetrics() -> [Metric] {
        /// TODO: add deviceId/hostname (ask Augustin again which to clarify)
        
        var result: [Metric] = [
            Metric(key: .platform, value: SDKPlatform.current.rawValue),
            Metric(key: .osVersion, value: UIDevice.current.systemVersion),
            Metric(key: .sdkVersion, value: Beaconsmind.sdkVersion),
            Metric(key: .manufacturer, value: UIDevice.current.manufacturer),
            Metric(key: .model, value: UIDevice.current.deviceModel),
        ]

        if let batteryPercentage = state.batteryPercentage {
            let level = String(format: "%.0f", batteryPercentage)
            result.append(Metric(key: .battery, value: level))
        }
        
        if let batteryState = state.batteryState {
            result.append(Metric(key: .batteryState, value: batteryState.toString()))
        }
        
        result.append(Metric(key: .batteryLowPowerModeEnabled, value: state.isLowPowerModeEnabled.description))

        if let latitude = state.lastLocation?.latitude, let longitude = state.lastLocation?.longitude {
            result.append(Metric(key: .location, value: "latitude: \(latitude), longitude: \(longitude)"))
        }
        
        if let missingPermissions = state.missingPermissions {
            result.append(Metric(key: .missingPermissions, value: missingPermissions))
        }
        
        if let locationAuthorization = state.locationAuthorization {
            result.append(Metric(key: .locationAuthorization, value: locationAuthorization.toString()))
        }
        
        if let locationAccuracy = state.locationAccuracy {
            result.append(Metric(key: .locationAccuracy, value: locationAccuracy.toString()))
        }

        return result
    }
}

extension CLAuthorizationStatus {
    func toString() -> String {
        let prefix = "CLAuthorizationStatus"
        
        switch self {
        case .notDetermined:
            return "\(prefix).notDetermined"
        case .restricted:
            return "\(prefix).restricted"
        case .denied:
            return "\(prefix).denied"
        case .authorizedAlways:
            return "\(prefix).authorizedAlways"
        case .authorizedWhenInUse:
            return "\(prefix).authorizedWhenInUse"
        @unknown default:
            return "\(prefix).unknown"
        }
    }
}

extension CLAccuracyAuthorization {
    func toString() -> String {
        let prefix = "CLAccuracyAuthorization"
        
        switch self {
        case .fullAccuracy:
            return "\(prefix).fullAccuracy"
        case .reducedAccuracy:
            return "\(prefix).reducedAccuracy"
        @unknown default:
            return "\(prefix).unknown"
        }
    }
}

extension UNAuthorizationStatus {
    func toString() -> String {
        let prefix = "UNAuthorizationStatus"
        
        switch self {
        case .notDetermined:
            return "\(prefix).notDetermined"
        case .denied:
            return "\(prefix).denied"
        case .authorized:
            return "\(prefix).authorised"
        case .provisional:
            return "\(prefix).provisional"
        case .ephemeral:
            return "\(prefix).ephermal"
        @unknown default:
            return "\(prefix).unknown"
        }
    }
}

extension UIDevice.BatteryState {
    func toString() -> String {
        switch self {
        case .unknown:
            return "unknown"
        case .unplugged:
            return "unplugged"
        case .charging:
            return "charging"
        case .full:
            return "full"
        @unknown default:
            return "unknown"
        }
    }
}
