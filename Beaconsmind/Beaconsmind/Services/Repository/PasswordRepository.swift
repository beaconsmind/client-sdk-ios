//
//  Copyright © 2022 beaconsmind. All rights reserved.
//

import Foundation

protocol PasswordRepository {

    func changePassword(
        newPassword: String,
        oldPassword: String,
        callback: @escaping (SDKApiResult<PasswordChangedResponse?>) -> Void
    ) throws -> CancellableRequest?

    func resetPasswordRequest(
        email: String,
        callback: @escaping (SDKApiResult<PasswordChangedResponse?>) -> Void
    ) throws -> CancellableRequest?

    func resetPassword(
        email: String,
        password: String,
        code: String?,
        callback: @escaping (SDKApiResult<PasswordChangedResponse?>) -> Void
    ) throws -> CancellableRequest?
    
    func forgotPasswordRequest(
        email: String,
        doNotSendEmail: Bool,
        callback: @escaping (SDKApiResult<EmailSentResponse?>) -> Void
    ) throws -> CancellableRequest?
    
}

final class PasswordRepositoryImpl: PasswordRepository {

    private let contextManager: ContextManager

    init(contextManager: ContextManager) {
        self.contextManager = contextManager
    }

    func changePassword(
        newPassword: String,
        oldPassword: String,
        callback: @escaping (SDKApiResult<PasswordChangedResponse?>) -> Void
    ) throws -> CancellableRequest? {
        guard let client = contextManager.getAPIClient() else {
            throw SDKApiError.notStarted
        }

        let model = ChangePasswordRequest(
            newPassword: newPassword,
            oldPassword: oldPassword
        )
        let request = API.Password.PasswordChangePassword.Request(body: model)
        return client.apiRequest(request) { response in
            switch response {
                case let .success(result):
                    callback(SDKApiResult.success(result.success))
                case let .failure(error):
                    callback(SDKApiResult.failure(error))
            }
        }
    }

    func resetPasswordRequest(email: String, callback: @escaping (SDKApiResult<PasswordChangedResponse?>) -> Void) throws -> CancellableRequest? {
        guard let client = contextManager.getUnauthenticatedAPIClient() else {
            throw SDKApiError.notStarted
        }

        let request = API.Password.PasswordResetPassword.Request(body: ResetPasswordRequest(email: email))
        return client.apiRequest(request, emptyResponseCodes: [200]) { response in
            switch response {
                case let .success(result):
                    callback(SDKApiResult.success(result.success))
                case let .failure(error):
                    callback(SDKApiResult.failure(error))
            }
        }
    }

    func resetPassword(
        email: String,
        password: String,
        code: String?,
        callback: @escaping (SDKApiResult<PasswordChangedResponse?>) -> Void
    ) throws -> CancellableRequest? {
        guard let client = contextManager.getUnauthenticatedAPIClient() else {
            throw SDKApiError.notStarted
        }

        let model = ResetPasswordRequest(
            email: email,
            password: password,
            token: code
        )
        let request = API.Password.PasswordResetPassword.Request(body: model)
        return client.apiRequest(request, emptyResponseCodes: [200]) { response in
            switch response {
                case let .success(result):
                    callback(SDKApiResult.success(result.success))
                case let .failure(error):
                    callback(SDKApiResult.failure(error))
            }
        }
    }
    
    func forgotPasswordRequest(
        email: String,
        doNotSendEmail: Bool,
        callback: @escaping (SDKApiResult<EmailSentResponse?>) -> Void) throws -> CancellableRequest? {
        
            guard let client = contextManager.getUnauthenticatedAPIClient() else {
                throw SDKApiError.notStarted
            }
            
            let model = ForgotPasswordRequest(doNotSendEmail: doNotSendEmail, email: email)
            let request = API.Password.PasswordForgotPassword.Request(body: model)
            
            return client.apiRequest(request, emptyResponseCodes: [200]) { response in
                switch response {
                    case let .success(result):
                        callback(SDKApiResult.success(result.success))
                    case let .failure(error):
                        callback(SDKApiResult.failure(error))
                }
            }
            
    }
    
}
