//
//  Copyright © 02.12.21 Beaconsmind. All rights reserved.
//

import UIKit

protocol NotificationsRepository {

    func registerForPushNotifications()

    func subscribeToNotifications(deviceToken: String, platformType: PlatformType, callback: ((SDKApiResult<API.Notifications.NotificationsSubscribe.Response>) -> Void)?) throws
    func unsubscribeFromNotifications(callback: ((SDKApiResult<API.Notifications.NotificationsUnsubscribe.Response>) -> Void)?) throws

    func getReedemableNotifications(callback: @escaping (SDKApiResult<[NotificationResponse]>) -> Void) throws -> CancellableRequest?
    func getNonReedemableNotifications(callback: @escaping (SDKApiResult<[NotificationResponse]>) -> Void) throws -> CancellableRequest?
}

final class NotificationsRepositoryImpl: NotificationsRepository {

    private let contextManager: ContextManager
    private let store: Store
    private var state: State

    init(contextManager: ContextManager, store: Store, state: State) {
        self.contextManager = contextManager
        self.store = store
        self.state = state
    }

    func registerForPushNotifications() {
        UNUserNotificationCenter.current()
            .requestAuthorization(
                options: [.alert, .sound, .badge]) { granted, _ in
                    guard granted else {
                        self.state.notificationAuthorization = .denied
                        return
                    }

                    UNUserNotificationCenter.current().getNotificationSettings { settings in
                        
                        self.state.notificationAuthorization = settings.authorizationStatus
                        
                        guard settings.authorizationStatus == .authorized else { return }
                        DispatchQueue.main.async {
                            UIApplication.shared.registerForRemoteNotifications()
                        }
                    }
                }
    }

    func subscribeToNotifications(
        deviceToken: String,
        platformType: PlatformType,
        callback: ((SDKApiResult<API.Notifications.NotificationsSubscribe.Response>) -> Void)?
    ) throws {
        guard let client = contextManager.getAPIClient() else {
            throw SDKApiError.invalidContext
        }
        
        /// If new token differs from the one that is already stored,
        /// we need to make sure to send unsubscribe request to the server
        /// so that it doesn't try to send push notifications to the outdated token.
        if (store.deviceToken != nil && store.deviceToken != deviceToken) {
            Logger.log("New token differs from the one that is already stored. Unsubscribing from the old one.", category: .general)
            
            let model = UnsubscribeRequest(platformType: platformType, platformId: store.deviceToken)
            let request = API.Notifications.NotificationsUnsubscribe.Request(body: model)

            _ = client.apiRequest(request, emptyResponseCodes: [200]) { _ in
                Logger.log("Unsubscribed from the old token.", category: .general)
            }
        }
        
        /// Save the device token and platform type in the persistent storage.
        try store.saveDeviceToken(string: deviceToken)
        try store.savePlatformType(type: platformType)

        let model = SubscribeRequest(
            phoneOS: .iOS,
            platformType: platformType,
            phoneManufacturer: UIDevice.current.manufacturer,
            phoneModel: UIDevice.current.deviceModel,
            phoneOsVersion: UIDevice.current.osVersion,
            platformId: deviceToken
        )
        let request = API.Notifications.NotificationsSubscribe.Request(body: model)

        _ = client.apiRequest(request, emptyResponseCodes: [200]) { result -> Void in
            callback?(result)
        }
    }

    func unsubscribeFromNotifications(
        callback: ((SDKApiResult<API.Notifications.NotificationsUnsubscribe.Response>) -> Void)?
    ) throws {
        guard let client = contextManager.getAPIClient() else {
            throw SDKApiError.invalidContext
        }
        
        /// Retrieve deviceToken and platformType from the persistent storage.
        let deviceToken = store.deviceToken
        let platformType = store.platformType

        let model = UnsubscribeRequest(platformType: platformType ?? .Apns, platformId: deviceToken)
        let request = API.Notifications.NotificationsUnsubscribe.Request(body: model)

        _ = client.apiRequest(request, emptyResponseCodes: [200]) { result -> Void in
            callback?(result)
        }
    }

    func getReedemableNotifications(callback: @escaping (SDKApiResult<[NotificationResponse]>) -> Void) throws -> CancellableRequest? {
        guard let client = contextManager.getAPIClient() else {
            throw SDKApiError.invalidContext
        }

        let request = API.Notifications.NotificationsGetRedeemable.Request()
        return client.apiRequest(request) { response in
            switch response {
                case let .success(result):
                    callback(SDKApiResult.success(result.success ?? []))
                case let .failure(error):
                    callback(SDKApiResult.failure(error))
            }
        }
    }

    func getNonReedemableNotifications(callback: @escaping (SDKApiResult<[NotificationResponse]>) -> Void) throws -> CancellableRequest? {
        guard let client = contextManager.getAPIClient() else {
            throw SDKApiError.invalidContext
        }

        let request = API.Notifications.NotificationsGetNonRedeemable.Request()
        return client.apiRequest(request) { response in
            switch response {
                case let .success(result):
                    callback(SDKApiResult.success(result.success ?? []))
                case let .failure(error):
                    callback(SDKApiResult.failure(error))
            }
        }
    }
}
