//
//  Copyright © 2022 beaconsmind. All rights reserved.
//

import Foundation

protocol StoreRepository {

    func getStores(callback: @escaping (SDKApiResult<[StoreModel]>) -> Void) throws -> CancellableRequest?
    func setStoreFavorite(storeID: Int, isFavorite: Bool, callback: ((SDKApiResult<Bool>)->Void)?) throws -> CancellableRequest?
}

final class StoreRepositoryImpl: StoreRepository {

    private let contextManager: ContextManager

    init(contextManager: ContextManager) {
        self.contextManager = contextManager
    }

    func getStores(callback: @escaping (SDKApiResult<[StoreModel]>) -> Void) throws -> CancellableRequest? {
        guard let client = contextManager.getAPIClient() else {
            throw SDKApiError.invalidContext
        }

        let request = API.Stores.StoresGetStores.Request()
        return client.apiRequest(request) { response in
            switch response {
                case let .success(result):
                    callback(SDKApiResult.success(result.success ?? []))
                case let .failure(error):
                    callback(SDKApiResult.failure(error))
            }
        }
    }
    func setStoreFavorite(storeID: Int, isFavorite: Bool, callback: ((SDKApiResult<Bool>) -> Void)?) throws -> CancellableRequest? {
        guard
            let client = contextManager.getAPIClient()
        else {
            throw SDKApiError.invalidContext
        }
        let model = SetFlagRequest(value: isFavorite)
        let request = API.Stores.StoresSetFavorite.Request(storeId: storeID, body: model)
        return client.apiRequest(request, emptyResponseCodes: [200]) { result in
            switch result {
            case let .success(readResponse):
                callback?(SDKApiResult.success(readResponse.successful))
            case let .failure(responseError):
                callback?(SDKApiResult.failure(responseError))
            }
        }
    }
}
