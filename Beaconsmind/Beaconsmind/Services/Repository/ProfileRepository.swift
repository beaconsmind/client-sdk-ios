//
//  Copyright © 18.12.21 Beaconsmind. All rights reserved.
//

import Foundation

protocol ProfileRepository {

    func getProfile(callback: @escaping (SDKApiResult<ProfileResponse?>) -> Void) throws -> CancellableRequest?

    func updateProfile(
        firstName: String,
        lastName: String,
        avatarThumbnailURL: String?,
        avatarURL: String?,
        birthDate: DateTime?,
        city: String?,
        country: String?,
        disablePushNotifications: Bool?,
        favoriteStoreID: Int?,
        gender: String?,
        houseNumber: String?,
        landlinePhone: String?,
        language: String?,
        phoneNumber: String?,
        street: String?,
        zipCode: String?,
        callback: @escaping (SDKApiResult<ProfileResponse?>) -> Void
    ) throws -> CancellableRequest?

    func updateAvatar(url: String?, thumbnailURL: String?, callback: @escaping (SDKApiResult<Void?>) -> Void) throws -> CancellableRequest?

    func getCustomerCard(callback: @escaping (SDKApiResult<CustomerCardModel?>) -> Void) throws -> CancellableRequest?
}

final class ProfileRepositoryImpl: ProfileRepository {

    private let contextManager: ContextManager

    init(contextManager: ContextManager) {
        self.contextManager = contextManager
    }

    @discardableResult
    func getProfile(callback: @escaping (SDKApiResult<ProfileResponse?>) -> Void) throws -> CancellableRequest? {
        guard let client = contextManager.getAPIClient() else {
            throw SDKApiError.invalidContext
        }

        let request = API.Accounts.AccountsGetProfile.Request()
        return client.apiRequest(request) { result in
            switch result {
            case let .success(readResponse):
                callback(SDKApiResult.success(readResponse.success))
            case let .failure(responseError):
                callback(SDKApiResult.failure(responseError))
            }
        }
    }

    @discardableResult
    func updateProfile(
        firstName: String,
        lastName: String,
        avatarThumbnailURL: String?,
        avatarURL: String?,
        birthDate: DateTime?,
        city: String?,
        country: String?,
        disablePushNotifications: Bool?,
        favoriteStoreID: Int?,
        gender: String?,
        houseNumber: String?,
        landlinePhone: String?,
        language: String?,
        phoneNumber: String?,
        street: String?,
        zipCode: String?,
        callback: @escaping (SDKApiResult<ProfileResponse?>) -> Void
    ) throws -> CancellableRequest? {
        guard let client = contextManager.getAPIClient() else {
            throw SDKApiError.invalidContext
        }

        let model = UpdateProfileRequest(
            avatarThumbnailUrl: avatarThumbnailURL,
            avatarUrl: avatarURL,
            birthDate: birthDate,
            city: city,
            country: country,
            disablePushNotifications: disablePushNotifications?.asAPIString,
            favoriteStoreId: favoriteStoreID,
            firstName: firstName,
            gender: gender,
            houseNumber: houseNumber,
            landlinePhone: landlinePhone,
            language: language,
            lastName: lastName,
            phoneNumber: phoneNumber,
            street: street,
            zipCode: zipCode
        )
        let request = API.Accounts.AccountsUpdateProfile.Request(body: model)
        return client.apiRequest(request) { result in
            switch result {
            case let .success(readResponse):
                callback(SDKApiResult.success(readResponse.success))
            case let .failure(responseError):
                callback(SDKApiResult.failure(responseError))
            }
        }
    }

    func updateAvatar(url: String?, thumbnailURL: String?, callback: @escaping (SDKApiResult<Void?>) -> Void) throws -> CancellableRequest? {
        guard let client = contextManager.getAPIClient() else {
            throw SDKApiError.invalidContext
        }

        let model = UpdateAvatarRequest(thumbnailUrl: thumbnailURL, url: url)
        let request = API.Profile.ProfileUpdateAvatar.Request(body: model)
        return client.apiRequest(request) { result in
            switch result {
                case let .success(readResponse):
                    callback(SDKApiResult.success(readResponse.success))
                case let .failure(responseError):
                    callback(SDKApiResult.failure(responseError))
            }
        }
    }

    func getCustomerCard(callback: @escaping (SDKApiResult<CustomerCardModel?>) -> Void) throws -> CancellableRequest? {
        guard let client = contextManager.getAPIClient() else {
            throw SDKApiError.invalidContext
        }

        let request = API.Profile.ProfileGetCustomerCard.Request()
        return client.apiRequest(request) { response in
            switch response {
                case let .success(result):
                    callback(SDKApiResult.success(result.success))
                case let .failure(error):
                    callback(SDKApiResult.failure(error))
            }
        }
    }
}
