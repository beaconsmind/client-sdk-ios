//
//  Copyright © 2022 beaconsmind. All rights reserved.
//

import Foundation
import UIKit

protocol ImageRepository {

    func uploadImage(image: UIImage, callback: @escaping (SDKApiResult<ImageUploadResponse?>) -> Void) throws -> CancellableRequest?
}

final class ImageRepositoryImpl: ImageRepository {

    private let contextManager: ContextManager

    init(contextManager: ContextManager) {
        self.contextManager = contextManager
    }

    func uploadImage(image: UIImage, callback: @escaping (SDKApiResult<ImageUploadResponse?>) -> Void) throws -> CancellableRequest? {
        guard let client = contextManager.getUnauthenticatedAPIClient() else {
            throw SDKApiError.notStarted
        }

        guard let fileData = image.jpegData(compressionQuality: Constants.imageCompressionQuality) else {
            throw SDKApiError.invalidImage
        }
        let file = UploadFile(type: .data(fileData), fileName: "test.jpg", mimeType: "image/jpg")

        let settings = UploadSettings(
            generateThumbnail: true,
            generateGrayscale: false,
            generateThumbnailGrayscale: false,
            fileName: "blob",
            mimeType: "application/json"
        )
        let request = API.Image.ImageCreate.Request(image: file, settings: settings)
        return client.apiRequest(request) { result in
            switch result {
                case let .success(readResponse):
                    callback(SDKApiResult.success(readResponse.success))
                case let .failure(responseError):
                    callback(SDKApiResult.failure(responseError))
            }
        }
    }
}
