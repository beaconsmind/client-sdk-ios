import Foundation

protocol IDeviceInfo {
    var manufacturer: String { get }
    var osVersion: String { get }
    var deviceName: String { get }
    var deviceModel: String { get }
    var platform: String { get }
}
