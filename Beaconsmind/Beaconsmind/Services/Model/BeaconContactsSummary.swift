//
//  Copyright © 2022 beaconsmind. All rights reserved.
//

import Foundation

/// Helper model to describe a beacon contacts summary
public struct BeaconContactsSummary {

    /// Store's name
    public let store: String
    /// Beacon's name
    public let name: String
    /// Beacon's uuid
    public let uuid: String
    /// Beacon's major
    public let major: String
    /// Beacon's minor
    public let minor: String
    /// Beacon's signal strength
    public let rssi: Double?
    /// Beacon's average signal strength (based on last contacts)
    public let averageRSSI: Double?
    /// Last beacon contact timestamp
    public let timestamp: Int?
    /// Contact frequency per minute
    public let frequency: Int
    /// Contact count
    public let contactCount: Int
    /// Returns if beacon is in range
    public let isInRange: Bool

    private let beacon: Beacon
    private let events: [EventModel]

    init(beacon: Beacon, events: [EventModel], isInRange: Bool) {
        self.beacon = beacon
        self.events = events
        self.isInRange = isInRange

        self.name = beacon.name ?? ""
        self.store = beacon.store ?? ""
        self.uuid = beacon.uuid
        self.major = "\(beacon.major)"
        self.minor = "\(beacon.minor)"
        self.contactCount = events.count

        let firstEvent = events.sorted { ($0.timestamp) ?? 0 < ($1.timestamp ?? 0) }.first
        let lastEvent = events.sorted { ($0.timestamp ?? 0) > ($1.timestamp ?? 0) }.first
        if
            let lastTime = lastEvent?.timestamp,
            let firstTime = firstEvent?.timestamp,
            firstTime < lastTime
        {
            frequency = Int(Float(Float((Float(lastTime - firstTime) / Float(contactCount)) / 1000) * 60))
        } else {
            frequency = 0
        }

        timestamp = lastEvent?.timestamp
        rssi = lastEvent?.rssi

        if events.count > 0 {
            averageRSSI = events.reduce(into: 0) { result, event in
                result += event.rssi ?? 0
            } / Double(events.count)
        } else {
            averageRSSI = nil
        }
    }
}
