//
//  Copyright © 02.03.22 Beaconsmind. All rights reserved.
//

import Foundation
import CoreLocation

struct Beacon: Codable, Hashable, CustomStringConvertible {

    enum CodingKeys: String, CodingKey {
        case uuid, major, minor, name, store
    }

    let uuid: String
    let major: Int
    let minor: Int
    let name: String?
    let store: String?

    var isValid: Bool {
        UUID(uuidString: uuid) != nil && major > 0 && minor > 0
    }

    var description: String {
        "UUID: \(uuid.lowercased()), Major: \(major),  Minor: \(minor)"
    }
}

extension Beacon: Equatable {

    static public func ==(lhs: Beacon, rhs: Beacon) -> Bool {
        guard
            let lhsName = lhs.name,
            let lhsStore = lhs.store,
            let rhsName = rhs.name,
            let rhsStore = rhs.store
        else {
            return lhs.description == rhs.description
        }
        return lhs.description == rhs.description && lhsName == rhsName && lhsStore == rhsStore
    }
}

extension Beacon {

    static func fromAPI(element: StoreBeaconInfo) -> Beacon? {
        let formatter = NumberFormatter()
        formatter.locale = Locale.init(identifier: "en_US")

        if
            let uuid = element.uuid,
            let major = element.major,
            let minor = element.minor,
            let intMajor = formatter.number(from: major)?.intValue,
            let intMinor = formatter.number(from: minor)?.intValue
        {
            return Beacon(uuid: uuid, major: intMajor, minor: intMinor, name: element.name, store: element.store)
        }

        return nil
    }

    func toRegion() -> CLBeaconRegion? {
        guard let uuid = UUID(uuidString: uuid) else {
            return nil
        }

        let major: Int? = major.isValidMajorMinor ? major : nil
        let minor: Int? = minor.isValidMajorMinor ? minor : nil

        if #available(iOS 13.0, *) {
            if let major = major, let minor = minor {
                return CLBeaconRegion(
                    uuid: uuid,
                    major: UInt16(major),
                    minor: UInt16(minor),
                    identifier: "\(uuid):\(major):\(minor)"
                )
            }
            
            return CLBeaconRegion(uuid: uuid, identifier: "\(uuid)")
        } else {
            if let major = major, let minor = minor {
                return CLBeaconRegion(
                    proximityUUID: uuid,
                    major: UInt16(major),
                    minor: UInt16(minor),
                    identifier: "\(uuid):\(major):\(minor)"
                )
            }
            return CLBeaconRegion(proximityUUID: uuid, identifier: "\(uuid)")
        }
    }
}

extension Array where Element == Beacon {

    func first(inRegion region: CLBeaconRegion) -> Beacon? {
        first { beacon in
            region.uuidString.lowercased() == beacon.uuid.lowercased() &&
            region.major?.intValue == beacon.major &&
            region.minor?.intValue == beacon.minor
        }
    }

    func first(forBeacon clBeacon: CLBeacon) -> Beacon? {
        first { beacon in
            clBeacon.uuidString.lowercased() == beacon.uuid.lowercased() &&
            clBeacon.major.intValue == beacon.major &&
            clBeacon.minor.intValue == beacon.minor
        }
    }
}
