//
//  Copyright © 2022 beaconsmind. All rights reserved.
//

import Foundation
import CoreLocation

/// Location-related errors
public enum LocationError: Error {

    case locationServicesNotEnabled
    case monitoringNotAvailable
    case monitoringFailed(error: Error)
    case rangingNotAvailable
    case wrongRegion(region: CLRegion)
    case monitoringLimitReached
}

extension LocationError: LocalizedError {

    public var errorDescription: String? {
        switch self {

            case .locationServicesNotEnabled:
                return "[LocationError] Location services are not enabled."

            case .monitoringNotAvailable:
                return "[LocationError] Monitoring is not available with the device."

            case .monitoringFailed(let error):
                return "[LocationError] Monitoring failed: \(error.localizedDescription)"

            case .rangingNotAvailable:
                return "[LocationError] Ranging is not available with the device."

            case .wrongRegion(let region):
                return "[LocationError] Can't cast region to CLBeaconRegion with identifier: \(region.identifier)"

            case .monitoringLimitReached:
                return "[LocationError] Monitoring limit reached, will use the first 20 only."
        }
    }
}
