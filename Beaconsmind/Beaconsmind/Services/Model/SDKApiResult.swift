import Foundation

/**
 A result enum returned by `BeaconsmindSDK` and `BeaconsmindAPI` when working on API requests or other async operations.
 Use `switch case` to pattern match the response
 */
public enum SDKApiResult<TResult> {
    /**
     A success case having `TResult` as content
     */
    case success(_: TResult)
    /**
     An error case where an error has happened. `SDKApiError`
     */
    case failure(_ : SDKApiError)
}
