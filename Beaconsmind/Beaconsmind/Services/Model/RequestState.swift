//
//  Copyright © 27.12.21 Beaconsmind. All rights reserved.
//

import Foundation

enum RequestState {
    case initial
    case done
    case error
    case working
}
