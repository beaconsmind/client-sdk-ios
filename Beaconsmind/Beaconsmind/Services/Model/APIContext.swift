//
//  Copyright © 2022 Beaconsmind. All rights reserved.
//

import Foundation

public struct APIContext: Codable, Hashable {

    public let hostname: String
    public let accessToken: String
    public let userID: String
    public let tokenType: String?
    public let expiresIn: String?

    public init(
        hostname: String,
        accessToken: String,
        userID: String,
        tokenType: String? = nil,
        expiresIn: String? = nil
    ) {
        self.hostname = hostname
        self.accessToken = accessToken
        self.userID = userID
        self.tokenType = tokenType
        self.expiresIn = expiresIn
    }
}
