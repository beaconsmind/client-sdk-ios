//
//  Copyright © 2022 beaconsmind. All rights reserved.
//

import Foundation

enum Event: String {
    case `in` = "IN"
    case out = "OUT"
}

extension EventModel {

    var description: String {
        "beacon: \(uuid)-\(major)-\(minor), event: \(event)"
    }
}
