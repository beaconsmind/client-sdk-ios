//
//  Copyright © 03.12.21 Beaconsmind. All rights reserved.
//

import UIKit

enum Constants {

    enum Param: String {
        case password
        case nameid
    }

    enum ID: String {
        case storeName = "com.beaconsmind.sdk.store"
        case backgroundTaskFetching = "BeaconsmindBackgroundFetchingTaskID"
        case backgroundTaskReporting = "BeaconsmindBackgroundReportingTaskID"
        case backgroundTaskDiagnosticReporting = "BeaconsmindBackgroundDiagnosticReportingTaskID"
    }

    enum Key: String {
        case authToken = "com.beaconsmind.beacons.authentication.token"
        case beaconsStore = "com.beaconsmind.beacons.list"
        case apiReportService = "com.beaconsmind.last-beacon-timestamp"
        case lastReportInvocationTime = "com.beaconsmind.last-report-invocation-timestamp"
        case appConfig = "com.beaconsmind.app-config"
        case pocToken = "com.beaconsmind.poc-token"
    }

    static let logSubsystemID = "com.beaconsmind.sdk"
    static let imageCompressionQuality: CGFloat = 0.8

    struct Interval {
        static let refetchBeacons = 30.minutes
        static let reportContacts = 4.seconds
        static let monitoringNextRegions = 4.seconds
        static let monitoringRestart = 30.minutes
        static let monitoringSignificantRestart = 15.seconds
        static let diagnosticsReportInterval = 1.hour
        static let appConfigFetchInterval = 1.hour
        static let diagnosticsReportInitialDelay = 10.seconds
    }
}
