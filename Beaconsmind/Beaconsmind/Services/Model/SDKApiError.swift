//
//  Copyright © 2022 beaconsmind. All rights reserved.
//

import Foundation

/**
 Error enum which represents errors that can occur when consuming the Services provided by `BeaconsmindSDK` and `BeaconsmindAPI`.
 */
public enum SDKApiError: Error {
    /**
     An unknown error has occured
     */
    case unknown

    /**
     `self` got deallocated
     */
    case wrongSelf

    /**
     SDK is not started, yet. Call `start`
     */
    case notStarted

    /**
     Needs main thread error
     */
    case needsMainThread

    /**
     An a generic error has occured
     */
    case generic(_ : Error)

    /**
     Location manager error
     - parameter : `LocationError`
     */
    case location(_ : LocationError)

    /**
     General API request response error wrapper
     - parameter : `APIClientError` used by the `BeaconsmindAPI`
     */
    case network(_ : APIClientError)

    /**
     General parsing error when trying to parse serialized objects
     - parameter : `String` generic error description returned by the parser
     */
    case parse(_ : String)

    /**
     An API error has occured where HTTP status code and payload are known.
     - parameter code: `Int` HTTP status code.
     - parameter payload: `String` response payload, usually JSON encoded.
     */
    case apiError(_ code: Int, _ payload: String)
    
    /**
     An API error has occured where payload is known.
     - parameter payload: `String` response payload, usually JSON encoded.
     */
    case apiErrorMessage(_ payload: String)

    /**
     An SDK eror where an invalid `APIContext` has been passed to the SDK.
     */
    case invalidContext

    /**
     An SDK eror that represents a persistent store issue with `UserDefaults`
     */
    case invalidStore

    /**
     An SDK eror that represents an invalid image parameter error
     */
    case invalidImage
}

extension SDKApiError: LocalizedError {

    public var errorDescription: String? {
        switch self {

            case .unknown:
                return "[SDKApiError] An unknown error occured."

            case .wrongSelf:
                return "[SDKApiError] The object got deallocated before handling a completion."

            case .notStarted:
                return "[SDKApiError] The SDK has not been started, yet. Please call the .start() method first."

            case .needsMainThread:
                return "[SDKApiError] Wrong thread has been used, please start your request on the main thread."

            case .generic(let error):
                return "[SDKApiError] A generic error occured. Details: \(error.localizedDescription)"

            case .location(let error):
                return error.errorDescription

            case .network(let error):
                return "[SDKApiError] A networking error occured. Details: \(error.localizedDescription)"

            case .parse(let message):
                return "[SDKApiError] A parsing error occured. Details: \(message)"

            case .apiError(let code, let payload):
                return "[SDKApiError] An API error occured with code: \(code), payload: \(payload)"

            case .invalidContext:
                return "[SDKApiError] Invalid context, you need to authenticate first."

            case .invalidStore:
                return "[SDKApiError] Your store is invalid, the value you're looking for might still be empty."

            case .invalidImage:
                return "[SDKApiError] The passed image is invalid"
            
            case .apiErrorMessage(let payload):
                return payload
        }
    }
}
