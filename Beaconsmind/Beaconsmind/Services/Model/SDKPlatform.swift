//
//  Copyright © 2022 beaconsmind. All rights reserved.
//

import Foundation

enum SDKPlatform: String {
    case iOS
    case flutter
    case reactNative
    case cordova

    static var current: SDKPlatform {
        if NSClassFromString("RCTEventEmitter") != nil {
            return .reactNative
        }
        if NSClassFromString("FlutterMethodCall") != nil || NSClassFromString("FlutterEngine") != nil {
            return .flutter
        }
        if NSClassFromString("CDVPlugin") != nil {
            return .cordova
        }
        return .iOS
    }
}
