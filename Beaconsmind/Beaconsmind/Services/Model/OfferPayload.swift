//
//  Copyright © 02.03.22 Beaconsmind. All rights reserved.
//

import Foundation

/**
 Upon receiving push notification, this is the parsed payload from the `userData` received via APNS.
 */
public class OfferPayload: Decodable {

    enum CodingKeys: String, CodingKey {
        case offerID = "offerId"
        case offerCallToActionTitle
        case offerCallToActionLink
        case offerImageURL = "offerImageUrl"
        case isRedeemable = "isRedeemable"
    }

    public let offerID: Int
    public let offerCallToActionTitle: String?
    public let offerCallToActionLink: String?
    public let offerImageURL: String
    public let isRedeemable: Bool

    public static func tryParse(data: [AnyHashable : Any]) throws -> OfferPayload {
        do {
            let data = try JSONSerialization.data(withJSONObject: data, options: .fragmentsAllowed)
            return try JSONDecoder().decode(OfferPayload.self, from: data)
        } catch {
            if error is SDKApiError {
                throw error
            }
            throw SDKApiError.parse(error.localizedDescription)
        }
    }
}
