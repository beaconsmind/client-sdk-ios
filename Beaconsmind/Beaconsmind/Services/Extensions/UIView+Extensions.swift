//
//  Copyright © 04.02.22 . All rights reserved.
//

import UIKit

extension UIView {

    func removeAllSubviews() {
        for subview in subviews {
            subview.removeFromSuperview()
        }
    }
}
