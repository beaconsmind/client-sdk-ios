//
//  Copyright © 03.12.21 Beaconsmind. All rights reserved.
//

import CoreLocation

extension String {

    var preprocessedHostame: String {
        if starts(with: "http://") {
            return replacingOccurrences(of: "http://", with: "https://")
        }
        if starts(with: "https://") {
            return self
        }
        return "https://" + self + "/"
    }

    var attributedHTMLString: NSAttributedString? {
        guard let data = data(using: .utf8) else {
            return nil
        }
        do {
            return try NSAttributedString(
                data: data,
                options: [
                    .documentType: NSAttributedString.DocumentType.html,
                    .characterEncoding:String.Encoding.utf8.rawValue
                ],
                documentAttributes: nil
            )
        } catch {
            return nil
        }
    }

    var htmlString: String {
        attributedHTMLString?.string ?? ""
    }

    func toBeaconRegion() -> CLBeaconRegion? {
        guard let uuid = UUID(uuidString: self) else {
            return nil
        }
        if #available(iOS 13.0, *) {
            return CLBeaconRegion(uuid: uuid, identifier: "\(uuid)")
        } else {
            return CLBeaconRegion(proximityUUID: uuid, identifier: "\(uuid)")
        }
    }
}
