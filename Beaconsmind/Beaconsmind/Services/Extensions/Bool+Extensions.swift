//
//  Copyright © 29.12.21 Beaconsmind. All rights reserved.
//

import Foundation

extension Bool {

    var asAPIString: String {
        self ? "True" : "False"
    }
}
