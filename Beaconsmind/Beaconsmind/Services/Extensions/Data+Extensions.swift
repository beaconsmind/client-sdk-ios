//
//  Copyright © 03.12.21 Beaconsmind. All rights reserved.
//

import Foundation

extension Data {

    var asAPNSToken: String {
        map { String(format: "%02.2hhx", $0) }.joined()
    }
}
