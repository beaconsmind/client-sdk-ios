//
//  Copyright © 2022 beaconsmind. All rights reserved.
//

import CoreLocation

extension CLBeaconRegion {

    var uuidString: String {
        if #available(iOS 13.0, *) {
            return uuid.uuidString.lowercased()
        } else {
            return proximityUUID.uuidString.lowercased()
        }
    }

    func toBeacon(name: String?, store: String?) -> Beacon {
        Beacon(
            uuid: uuidString,
            major: major?.intValue ?? -1,
            minor: minor?.intValue ?? -1,
            name: name,
            store: store
        )
    }

    func toEvent(event: Event) -> EventModel {
        let uuidString: String
        if #available(iOS 13.0, *) {
            uuidString = uuid.uuidString
        } else {
            uuidString = proximityUUID.uuidString
        }
        let majorString: String = major?.stringValue ?? ""
        let minorString: String = minor?.stringValue ?? ""
        return EventModel(
            event: event.rawValue,
            major: majorString,
            minor: minorString,
            uuid: uuidString,
            distance: 0,
            rssi: 0,
            timestamp: Date().millisecondsSince1970
        )
    }
}
