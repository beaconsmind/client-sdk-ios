//
//  Copyright © 2022 beaconsmind. All rights reserved.
//

import Foundation

extension Int {

    var isValidMajorMinor: Bool {
        UInt16(self) == self
    }
}
