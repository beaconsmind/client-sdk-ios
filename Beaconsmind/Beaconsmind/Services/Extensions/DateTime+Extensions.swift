//
//  Copyright © 2023 beaconsmind. All rights reserved.
//

import Foundation

extension DateTime {
    
    func getStringFormat(_ template: String) -> String {
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = template
        return dateFormatterPrint.string(from: self)
    }
}
