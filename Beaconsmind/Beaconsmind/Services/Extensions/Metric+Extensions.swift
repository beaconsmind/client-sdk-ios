//
//  Copyright © 2022 beaconsmind. All rights reserved.
//

import Foundation

extension Metric {

    enum Key: String {
        case platform
        case osVersion
        case model
        case manufacturer
        case missingPermissions
        case battery
        case batteryState
        case batteryLowPowerModeEnabled
        case sdkVersion
        case location
        case locationAuthorization
        case locationAccuracy
    }

    convenience init(key: Key, value: String?) {
        self.init(key: key.rawValue, value: value)
    }
}
