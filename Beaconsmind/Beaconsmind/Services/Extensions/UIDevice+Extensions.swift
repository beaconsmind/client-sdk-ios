//
//  Copyright © 03.12.21 Beaconsmind. All rights reserved.
//

import UIKit

extension UIDevice: IDeviceInfo {

    var osVersion: String {
        systemVersion
    }

    var deviceName: String {
        name
    }

    var deviceModel: String {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        return identifier
    }

    var manufacturer: String {
        "Apple"
    }

    var platform: String {
        "iOS"
    }
}
