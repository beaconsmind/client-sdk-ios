//
//  Copyright © 2022 beaconsmind. All rights reserved.
//

import Foundation

extension Array where Element: Hashable {

    var distinct: Array {
        Array(Set(self))
    }
}

extension Array {

    func nextItems(fromIndex from: Int, size: Int) -> (items: [Element], nextIndex: Int) {
        if size <= 0 || count == 0 {
            return ([], 0)
        }

        var start = from
        if from < 0 || from >= count {
            start = 0
        }

        var nextIndex = start
        var result: [Element] = []
        while result.count < size {
            let next = Swift.min(size - result.count, count - start)
            result += self[start..<start + next]
            nextIndex = Swift.min(start + next, count)
            start += next
            if start >= count {
                start = 0
            }
        }
        return (result, nextIndex)
    }
}

extension Array where Element: Equatable {

    mutating func removeItems(from: Array) {
        from.forEach { toRemove in
            if let index = firstIndex(where: { $0 == toRemove }) {
                remove(at: index)
            }
        }
    }
}
