//
//  Copyright © 2022 beaconsmind. All rights reserved.
//

import CoreLocation

extension CLBeacon {

    var uuidString: String {
        if #available(iOS 13.0, *) {
            return uuid.uuidString.lowercased()
        } else {
            return proximityUUID.uuidString.lowercased()
        }
    }

    func toEvent(event: Event) -> EventModel {
        let uuidString: String
        var timestamp = Date().millisecondsSince1970
        if #available(iOS 13.0, *) {
            uuidString = uuid.uuidString
            timestamp = self.timestamp.millisecondsSince1970
        } else {
            uuidString = proximityUUID.uuidString
        }
        return EventModel(
            event: event.rawValue,
            major: "\(major)",
            minor: "\(minor)",
            uuid: uuidString,
            distance: 0,
            rssi: Double(rssi),
            timestamp: timestamp
        )
    }
}
