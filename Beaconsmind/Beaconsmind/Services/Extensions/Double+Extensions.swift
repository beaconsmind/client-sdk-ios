//
//  Copyright © 03.12.21 Beaconsmind. All rights reserved.
//

import Foundation

extension Double {
    var millisecond: TimeInterval  { return self / 1000 }
    var milliseconds: TimeInterval { return self / 1000 }
    var ms: TimeInterval           { return self / 1000 }

    var second: TimeInterval       { return self }
    var seconds: TimeInterval      { return self }

    var minute: TimeInterval       { return self * 60 }
    var minutes: TimeInterval      { return self * 60 }

    var hour: TimeInterval         { return self * 3600 }
    var hours: TimeInterval        { return self * 3600 }

    var day: TimeInterval          { return self * 3600 * 24 }
    var days: TimeInterval         { return self * 3600 * 24 }
}
