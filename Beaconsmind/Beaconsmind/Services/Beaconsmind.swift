//
//  Copyright © 03.12.21 Beaconsmind. All rights reserved.
//

import UIKit
import CoreLocation

public protocol BeaconsmindDelegate: AnyObject {

    func beaconsmind(_ beaconsmind: Beaconsmind, onContextChanged context: APIContext?)
}

public class Beaconsmind {

    private var appVersion: String?
    private var isInDeveloperMode = false

    private let store: Store
    private let state: State
    private let contextManager: ContextManager
    private let authenticationRepository: AuthenticationRepository
    private let beaconsRepository: BeaconsRepository
    private let dashboardRepository: DashboardRepository
    private let imageRepository: ImageRepository
    private let locationsRepository: LocationsRepository
    private let notificationsRepository: NotificationsRepository
    private let offersRepository: OffersRepository
    private let passwordRepository: PasswordRepository
    private let productsRepository: ProductsRepository
    private let profileRepository: ProfileRepository
    private let storeRepository: StoreRepository
    private let purchaseHistoriesRepository: PurchaseHistoriesRepository
    private let touchpointsRepository: TouchpointsRepository
    private let diagnosticsRepository: DiagnosticsRepository


    /**
     Singleton SDK instance to make developer's lives easier
     */
    public static let `default` = Beaconsmind()

    private weak var delegate: BeaconsmindDelegate?

    public static let sdkVersion = "3.9.7"

    /// Internal helpers to be able to mock the SDK for testing
    init(
        store: Store,
        state: State,
        contextManager: ContextManager
    ) {
        self.store = store
        self.state = state
        self.contextManager = contextManager

        authenticationRepository = AuthenticationRepositoryImpl(contextManager: contextManager)
        beaconsRepository = BeaconsRepositoryImpl(contextManager: contextManager, store: store, state: state)
        dashboardRepository = DashboardRepositoryImpl(contextManager: contextManager)
        imageRepository = ImageRepositoryImpl(contextManager: contextManager)
        locationsRepository = LocationsRepositoryImpl(contextManager: contextManager)
        notificationsRepository = NotificationsRepositoryImpl(contextManager: contextManager, store: store, state: state)
        offersRepository = OffersRepositoryImpl(contextManager: contextManager)
        passwordRepository = PasswordRepositoryImpl(contextManager: contextManager)
        productsRepository = ProductsRepositoryImpl(contextManager: contextManager)
        profileRepository = ProfileRepositoryImpl(contextManager: contextManager)
        storeRepository = StoreRepositoryImpl(contextManager: contextManager)
        purchaseHistoriesRepository = PurchaseHistoriesRepositoryImpl(contextManager: contextManager)
        touchpointsRepository = TouchpointsRepositoryImpl(contextManager: contextManager)
        diagnosticsRepository = DiagnosticsRepositoryImpl(contextManager: contextManager, store: store, state: state)

        /// Making sure to set context manager delegate once `self` is initialized properly
        contextManager.setDelegate(delegate: self)
    }

    convenience init() {
        let store = StoreImpl(storeName: Constants.ID.storeName.rawValue)
        let state = State()
        let contextManager = ContextManagerImpl(store: store)
        self.init(store: store, state: state, contextManager: contextManager)
    }

    // MARK: - Start

    /// SDK's entry point. It will automatically try to restore the sdk context and will register to app lifecycle events to do tracking silently. The passed delegate will be notified whenever the SDK's state changes.
    /// - parameter delegate: `BeaconsmindDelegate` instance to be notified about state changes.
    /// - parameter appVersion: `String` client-side app's version string
    /// - parameter hostname: `String` hostname of your Beaconsmind service hostname.
    /// - parameter token: `String` optional JWT token to initialize the SDK with
    public func start(
        delegate: BeaconsmindDelegate,
        appVersion: String,
        hostname: String,
        token: String? = nil
    ) throws {
        self.delegate = delegate
        self.appVersion = appVersion
        if let token = token {
            try contextManager.updateHostname(hostname: hostname, token: token)
        } else {
            updateHostname(hostname: hostname)
        }
        try contextManager.restoreContext()
        startAutomaticTouchpointSaving()
        diagnosticsRepository.start()
        
    }

    /// **For development purposes only.**
    /// Alternative start method to kickstart sdk testing and integration.
    /// Invokes start and sets the sdk in development mode.
    ///
    /// SDK's entry point. It will automatically try to restore the sdk context and will register to app lifecycle events to do tracking silently. The passed delegate will be notified whenever the SDK's state changes.
    /// - parameter delegate: `BeaconsmindDelegate` instance to be notified about state changes.
    /// - parameter appVersion: `String` client-side app's version string
    /// - parameter hostname: `String` hostname of your Beaconsmind service hostname.
    /// - parameter token: `String` optional JWT token to initialize the SDK with
    public func startDevelop(
        delegate: BeaconsmindDelegate,
        appVersion: String,
        hostname: String,
        token: String? = nil
    ) throws {
        isInDeveloperMode = true
        try start(delegate: delegate, appVersion: appVersion, hostname: hostname)
        Beaconsmind.default.setMinLogLevel(level: .debug)
        Logger.log("Initialized using explicit permissions mode, use this for development only.", category: .general)
    }

    // MARK: - Authentication

    /**
     This method is used to create a customer account on your running instance. Will return `APIContext` object that is used to execute authenticated calls to the Beaconsmind services.
     In case customer with same username exists, and was authenticated via Facebook, this will link the account and allow customer to log in with password. If the customer is successfully created, and device information is available, messages triggered by user registration will be sent.
     - parameter username: `String` username.
     - parameter firstName: `String` user's first name.
     - parameter lastName: `String` user's last name.
     - parameter password: `String` password.
     - parameter confirmPassword: `String` password confirmation.
     - parameter avatarThumbnailURL: `String?` customer's avatar thumbnail url.
     - parameter avatarURL: `String?` customer's avatar url.
     - parameter language: `String` user's language code.
     - parameter gender: `String` user's gender.
     - parameter favoriteStoreID: `Int` user's favorite store id.
     - parameter birthDate: `DateDay` user's birth date.
     - parameter countryCode: `String` country code in a ISO 3166-1 alpha-3 format.
     - parameter phoneNumber: `String` user's phonenumber.
     - parameter callback: `SDKApiResult<APIContext>` callback which returns you the Context if entered credentials are correct
     - throws `SDKApiError.notStarted` if `start` has not been called, yet
     */
    public func signup(
        username: String,
        firstName: String,
        lastName: String,
        password: String,
        confirmPassword: String,
        avatarThumbnailURL: String? = nil,
        avatarURL: String? = nil,
        language: String? = nil,
        gender: String? = nil,
        favoriteStoreID: Int? = nil,
        birthDate: DateDay? = nil,
        countryCode: String? = nil,
        phoneNumber: String? = nil,
        callback: @escaping (SDKApiResult<APIContext>) -> Void
    ) throws {
        try authenticationRepository.signup(
            username: username,
            firstName: firstName,
            lastName: lastName,
            password: password,
            confirmPassword: confirmPassword,
            avatarThumbnailURL: avatarThumbnailURL,
            avatarURL: avatarURL,
            language: language,
            gender: gender,
            favoriteStoreID: favoriteStoreID,
            birthDate: birthDate,
            countryCode: countryCode,
            phoneNumber: phoneNumber
        ) { [weak self] result in
            if case .success = result {
                _ = try? self?.saveTouchpoint(type: .Registration)
            }
            callback(result)
        }
    }

    /**
     This method is used to log in with your credentials on your running instance. Will return `APIContext` object that is used to execute authenticated calls to the Beaconsmind services.
     - parameter username: `String` username.
     - parameter password: `String` password.
     - parameter callback: `SDKApiResult<APIContext>` callback which returns you the Context if entered credentials are correct
     */
    public func login(
        username: String,
        password: String,
        callback: @escaping (SDKApiResult<APIContext>) -> Void
    ) throws {
        try authenticationRepository.login(
            username: username,
            password: password
        ) { [weak self] result in
            if case .success = result {
                _ = try? self?.saveTouchpoint(type: .Login)
            }
            callback(result)
        }
    }

    /**
     When the customer is using 3rd party authentication and not relying on Beaconsmind to keep the customer's private data, this endpoint is used to import the customer into Beaconsmind and to obtain a token with which the app can send tracking data to us. The token does NOT provide access to any personal customer info. If personal data is present in the request, it will only be updated the first time the endpoint is invoked for that customer.
     Once the authentication is done by the app, the username is sent to Beaconsmind using /api/accounts/import to receive a JWT for access to Beaconsmind backend. In this case, Beaconsmind will not allow any personal data to be accessed and it’s the apps responsibility to only send personal data the first time /api/accounts/import is used.
     When using import, editing profile is disabled.
     Will return `APIContext` object that is used to execute authenticated calls to the Beaconsmind services.
     - parameter id: `String` user id.
     - parameter email: `String` user's email address.
     - parameter firstName: `String` user's first name (Optional).
     - parameter lastName: `String` user's last name (Optional).
     - parameter birthDate: `DateTime` user's birth date (Optional).
     - parameter language: `String` user's language code (Optional).
     - parameter gender: `String` user's gender (Optional).
     - parameter deviceRegistration: `DeviceRegistrationModel` device registration with id and platform.
     - parameter callback: `SDKApiResult<APIContext>` callback which returns you the Context if the entered credentials are correct.
     */
    public func importAccount(
        id: String,
        email: String,
        firstName: String? = nil,
        lastName: String? = nil,
        birthDate: DateTime? = nil,
        language: String? = nil,
        gender: String? = nil,
        deviceRegistration: DeviceRegistrationModel,
        callback: @escaping (SDKApiResult<APIContext>) -> Void
    ) throws {
        try authenticationRepository.importAccount(
            id: id,
            email: email,
            firstName: firstName,
            lastName: lastName,
            birthDate: birthDate,
            language: language,
            gender: gender,
            deviceRegistration: deviceRegistration
        ) { [weak self] result in
            if case .success = result {
                _ = try? self?.saveTouchpoint(type: .Login)
            }
            callback(result)
        }
    }

    /**
     When the customer is using 3rd party authentication and not relying on Beaconsmind to keep the customer's private data, this endpoint is used to import the customer into Beaconsmind and to obtain a token with which the app can send tracking data to us. The token does NOT provide access to any personal customer info. If personal data is present in the request, it will only be updated the first time the endpoint is invoked for that customer.
     Once the authentication is done by the app, the username is sent to Beaconsmind using /api/accounts/import to receive a JWT for access to Beaconsmind backend. In this case, Beaconsmind will not allow any personal data to be accessed and it’s the apps responsibility to only send personal data the first time /api/accounts/import is used.
     When using import, editing profile is disabled.
     Will return `APIContext` object that is used to execute authenticated calls to the Beaconsmind services.
     - parameter id: `String` user id.
     - parameter email: `String` user's email address.
     - parameter firstName: `String` user's first name (Optional).
     - parameter lastName: `String` user's last name (Optional).
     - parameter birthDate: `DateTime` user's birth date (Optional).
     - parameter language: `String` user's language code (Optional).
     - parameter gender: `String` user's gender (Optional).
     - parameter callback: `SDKApiResult<APIContext>` callback which returns you the Context if the entered credentials are correct.
     */
    public func importAccount(
        id: String,
        email: String,
        firstName: String? = nil,
        lastName: String? = nil,
        birthDate: DateTime? = nil,
        language: String? = nil,
        gender: String? = nil,
        callback: @escaping (SDKApiResult<APIContext>) -> Void
    ) throws {
        try authenticationRepository.importAccount(
            id: id,
            email: email,
            firstName: firstName,
            lastName: lastName,
            birthDate: birthDate,
            language: language,
            gender: gender,
            deviceRegistration: nil
        ) { [weak self] result in
            if case .success = result {
                _ = try? self?.saveTouchpoint(type: .Login)
            }
            callback(result)
        }
    }

    /**
     Create customer account, authenticated with a social Provider (currently supports: Facebook, Google, and Apple).
     In case customer with same token exists, and was authenticated via password, this will link the account and allow customer to log in with password as well. If the customer is successfully created, and device information is available, messages triggered by user registration will be sent. External Access Token must contain email and ideally first and last name. Note: Apple provides the first and last name in the callback (not in the token), so please provide names in the request for Apple.
     - Parameters:
       - externalAccessToken: third-party token
       - providerType: `ProviderType` 0 (Facebook) 1 (Google) 2 (Apple)
       - language: `String` user's language code (Optional).
       - firstName: `String` user's first name (Optional).
       - lastName: `String` user's last name (Optional).
       - deviceRegistration: `DeviceRegistrationModel` device registration with id and platform.
     */
     public func externalLogin(
        externalAccessToken: String,
        providerType: ProviderType,
        language: String? = nil,
        firstName: String? = nil,
        lastName: String? = nil,
        deviceRegistration: DeviceRegistrationModel,
        callback: @escaping (SDKApiResult<APIContext>) -> Void
    ) throws {
        try authenticationRepository.externalLogin(
            externalAccessToken: externalAccessToken,
            providerType: providerType,
            language: language,
            firstName: firstName,
            lastName: lastName,
            deviceRegistration: deviceRegistration
        ) { [weak self] result in
            if case .success = result {
                _ = try? self?.saveTouchpoint(type: .Login)
            }
            callback(result)
        }
    }

    /**
     Create customer account, authenticated with a social Provider (currently supports: Facebook, Google, and Apple).
     In case customer with same token exists, and was authenticated via password, this will link the account and allow customer to log in with password as well. If the customer is successfully created, and device information is available, messages triggered by user registration will be sent. External Access Token must contain email and ideally first and last name. Note: Apple provides the first and last name in the callback (not in the token), so please provide names in the request for Apple.
     - Parameters:
     - externalAccessToken: third-party token
     - providerType: `ProviderType` 0 (Facebook) 1 (Google) 2 (Apple)
     - language: `String` user's language code (Optional).
     - firstName: `String` user's first name (Optional).
     - lastName: `String` user's last name (Optional).
     */
    public func externalLogin(
        externalAccessToken: String,
        providerType: ProviderType,
        language: String? = nil,
        firstName: String? = nil,
        lastName: String? = nil,
        callback: @escaping (SDKApiResult<APIContext>) -> Void
    ) throws {
        try authenticationRepository.externalLogin(
            externalAccessToken: externalAccessToken,
            providerType: providerType,
            language: language,
            firstName: firstName,
            lastName: lastName,
            deviceRegistration: nil
        ) { [weak self] result in
            if case .success = result {
                _ = try? self?.saveTouchpoint(type: .Login)
            }
            callback(result)
        }
    }

    // MARK: - Context helpers

    /**
     Logging out by clearing current context and updating the client-side through the delegate
     */
    @available(*, deprecated, message: "This function will not unsubscribe from push notifications. Please use the new logout(callback:) method instead")
    
    public func logout() throws {
        try contextManager.clearContext()
        stopListeningBeacons()
    }
    
    

    /**
     Logging out by unsubscribing from push notifications, clearing the current context and updating the client-side through the delegate
     - Parameter callback ((SDKApiResult<Bool>) -> Void)? notifies the caller about the result (optional error or was successful)
     */
    public func logout(callback: ((SDKApiResult<Bool>) -> Void)? = nil) throws {
        try notificationsRepository.unsubscribeFromNotifications { [weak self] response in
            guard let self = self else {
                callback?(.failure(.wrongSelf))
                return
            }
            switch response {
                case .failure(let error):
                    callback?(.failure(error))
                case .success:
                    do {
                        try self.contextManager.clearContext()
                    } catch {
                        callback?(.failure(.invalidStore))
                    }
                    self.stopListeningBeacons()
                    callback?(.success(true))
            }
        }
    }
    
    /**
     Deleting account and clearing current context and updating the client-side through the delegate
     */
    public func deleteAccount(callback: ((SDKApiResult<Bool>) -> Void)? = nil) throws {
        try authenticationRepository.delete { [weak self] response in
            switch response {
            case .failure(let error):
                callback?(.failure(error))
            case .success( _):
                try? self?.contextManager.clearContext()
                self?.stopListeningBeacons()
            }
        }
    }

    /// Requesting SDK context
    /// - Returns: Currently stored SDK context
    public func getAPIContext() -> APIContext? {
        contextManager.context
    }

    /// Updates hostname of your Beaconsmind service hostname.
    /// - parameter hostname: `String` hostname of your Beaconsmind service hostname.
    public func updateHostname(hostname: String) {
        contextManager.updateHostname(hostname: hostname)
    }

    // MARK: - Beacons

    /**
     Start listening beacons near your Apple Device.
     *NOTES:*
     - Invoke this method on main thread to ensure proper `CLLocationManager` initialization.
     - Any existing delegates will be overridden by the new one.
     - Does not automatically ask for location permission and might throw an error in case the user hasn't yet authorized the app to receive location updates
     To properly function in the background, applications need to set `NSLocationAlwaysAndWhenInUseUsageDescription` String value in the `Info.plist`.
     - parameter delegate: (optional) object implementing `BeaconListenerDelegate` to read ranging information while Application is in foreground.
     - throws `SDKApiError.invalidContext` if context is invalid.
     */
    public func startListeningBeacons(delegate: BeaconListenerDelegate? = nil) throws {
        try beaconsRepository.startListeningBeacons(delegate: delegate ?? self)
    }

    /**
     Stop listening for beacons near your Apple Device.
     */
    public func stopListeningBeacons() {
        beaconsRepository.stopListeningBeacons()
    }

    /**
     Returns a beacon contacts summary for all beacons sorted by their last contact (if any)
     */
    public var beaconContactsSummaries: [BeaconContactsSummary] {
        beaconsRepository.beaconContactsSummaries
    }

    // MARK: - Dashboard

    /**
     Returns featured items from the suite
     */
    public func getFeaturedItems(callback: @escaping (SDKApiResult<[FeaturedItemView]>) -> Void) throws -> CancellableRequest? {
        try dashboardRepository.getFeaturedItems(callback: callback)
    }

    /**
     Returns blog posts from the suite
     */
    public func getBlogPosts(callback: @escaping (SDKApiResult<[BlogPostView]>) -> Void) throws -> CancellableRequest? {
        try dashboardRepository.getBlogPosts(callback: callback)
    }

    // MARK: - Images

    /**
     Uploading a binary image string and returns a thumbnail and image path
     */
    public func uploadImage(image: UIImage, callback: @escaping (SDKApiResult<ImageUploadResponse?>) -> Void) throws -> CancellableRequest? {
        try imageRepository.uploadImage(image: image, callback: callback)
    }

    // MARK: - Locations

    /**
     Requests location permission and calls the given callback with the result asynchronously
     */
    public func requestLocationPermission(callback: LocationPermissionRequestCompletionHandler?) {
        locationsRepository.requestPermission(callback: callback)
    }

    /**
     Returns list of countries from the suite
     */
    public func getCountries(callback: @escaping (SDKApiResult<[CountryView]?>) -> Void) throws -> CancellableRequest? {
        try locationsRepository.getCountries(callback: callback)
    }

    /**
     Returns list of cities for a given `countryID` from the suite
     */
    public func getCitiesForCountry(countryID: Int, callback: @escaping (SDKApiResult<[CityView]?>) -> Void) throws -> CancellableRequest? {
        try locationsRepository.getCitiesForCountry(countryID: countryID, callback: callback)
    }

    // MARK: - Notifications

    /**
     Uses Apple's `UserNotifications` framework to register for remote notifications. Once notifications are enabled, the application delegate's `didRegisterForRemoteNotificationsWithDeviceToken` callback will be called with the device token. See `register(deviceToken:)` to upload the token to Beaconsmind to receive offer notifications.
     */
    public func registerForPushNotifications() {
        notificationsRepository.registerForPushNotifications()
    }

    /**
     Use the device push token returned by APNS in the `application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data)` Application delegate
     callback to register the device for Beaconsmind services in order to receive beacon pings.
     - parameter deviceToken: APNS device token `Data`
     - parameter platformType: Push notification service type `PlatformType`
     - parameter callback : (optional) `SDKApiResult` enum callback to listen for the API result.
     */
    public func register(
        deviceToken: Data,
        platformType: PlatformType = .Apns,
        callback: ((SDKApiResult<API.Notifications.NotificationsSubscribe.Response>) -> Void)? = nil
    ) throws {
        try register(deviceToken: deviceToken.asAPNSToken, platformType: platformType, callback: callback)
    }

    /**
     Use a hex string device push token to register the device for Beaconsmind services in order to receive beacon pings.
     - parameter deviceToken: APNS token hex `String`
     - parameter platformType: Push notification service type `PlatformType`
     - parameter callback : (optional) `SDKApiResult` enum callback to listen for the API result.
     */
    public func register(
        deviceToken: String,
        platformType: PlatformType = .Apns,
        callback: ((SDKApiResult<API.Notifications.NotificationsSubscribe.Response>) -> Void)? = nil
    ) throws {
        try notificationsRepository.subscribeToNotifications(deviceToken: deviceToken, platformType: platformType, callback: callback)
    }

    /**
     Notifies the SDK about the received push notification. It will mark the offer as received.
     - parameter offer: `OfferPayload` Parsed offer payload
     - throws `SDKApiError.invalidContext` in case there's no current SDK context, yet
     */
    public func handleReceivedPushNotification(offer: OfferPayload) throws {
        try markOfferAsReceived(offerID: offer.offerID, callback: nil)
    }

    /**
     Returns reedemable notifications from the suite
     */
    public func getReedemableNotifications(callback: @escaping (SDKApiResult<[NotificationResponse]>) -> Void) throws -> CancellableRequest? {
        try notificationsRepository.getReedemableNotifications(callback: callback)
    }

    /**
     Returns non-reedemable notifications from the suite
     */
    public func getNonReedemableNotifications(callback: @escaping (SDKApiResult<[NotificationResponse]>) -> Void) throws -> CancellableRequest? {
        try notificationsRepository.getNonReedemableNotifications(callback: callback)
    }

    // MARK: - Offers

    /**
     Register the Offer as Read.
     - parameter offerID: `Int` Offer Id
     - parameter callback: (optional) `SDKApiResult<Bool>` containing  `API.Offers.OffersRead` API result
     - returns `CancellableRequest` cancellation handle which allows you to cancel the request.
     - throws either `SDKApiError.invalidContext` or `SDKApiError.parse` depending on the issues
     */
    @discardableResult
    public func markOfferAsRead(offerID: Int, callback: ((SDKApiResult<Bool>)->Void)? = nil) throws -> CancellableRequest? {
        try offersRepository.markOfferAsRead(offerID: offerID, callback: callback)
    }

    /**
     Register the Offer as Received. Automatically called if an valid Offer ID is found  when `handleReceivedPushNotification(userInfo:context)` is called upon receiving APNS payload in the app.
     - parameter offerID: `Int` Offer Id
     - parameter callback: (optional) `SDKApiResult<Bool>` containing  `API.Offers.OffersReceived` result for
     - returns `CancellableRequest` cancellation handle which allows you to cancel the request.
     - throws either `SDKApiError.invalidContext` or `SDKApiError.parse` depending on the issues
     */
    @discardableResult
    public func markOfferAsReceived(offerID: Int, callback: ((SDKApiResult<Bool>)->Void)? = nil) throws -> CancellableRequest? {
        try offersRepository.markOfferAsReceived(offerID: offerID, callback: callback)
    }

    /**
     Register the Offer as Redeemed.
     - parameter offerID: `Int` Offer Id
     - parameter callback: (optional) `SDKApiResult<Bool>` containing  `API.Offers.OffersRedeemed` result for
     - returns `CancellableRequest` cancellation handle which allows you to cancel the request.
     - throws either `SDKApiError.invalidContext` or `SDKApiError.parse` depending on the issues
     */
    @discardableResult
    public func markOfferAsRedeemed(offerID: Int, callback: ((SDKApiResult<Bool>)->Void)? = nil) throws -> CancellableRequest? {
        try offersRepository.markOfferAsRedeemed(offerID: offerID, callback: callback)
    }
    
    /**
     Register the Offer as Clicked.
     - parameter offerID: `Int` Offer Id
     - parameter callback: (optional) `SDKApiResult<Bool>` containing  `API.Offers.OffersClicked` API result
     - returns `CancellableRequest` cancellation handle which allows you to cancel the request.
     - throws either `SDKApiError.invalidContext` or `SDKApiError.parse` depending on the issues
     */
    @discardableResult
    public func markOfferAsClicked(offerID: Int, callback: ((SDKApiResult<Bool>)->Void)? = nil) throws -> CancellableRequest? {
        try offersRepository.markOfferAsClicked(offerID: offerID, callback: callback)
    }

    // MARK: - Passwords

    /// Changes password
    /// - Parameters:
    ///   - confirmPassword: new password confirmation
    ///   - newPassword: new password
    ///   - oldPassword: old password
    ///   - callback: success/error callback
    /// - Returns: cancellable request
    public func changePassword(
        newPassword: String,
        oldPassword: String,
        callback: @escaping (SDKApiResult<PasswordChangedResponse?>) -> Void
    ) throws -> CancellableRequest? {
        try passwordRepository.changePassword(
            newPassword: newPassword,
            oldPassword: oldPassword,
            callback: callback
        )
    }

    /// Sends a password reset request
    /// - Parameter callback: success/error callback
    /// - Parameter email: email to send password reset to
    /// - Returns: cancellable request
    public func resetPasswordRequest(email: String, callback: @escaping (SDKApiResult<PasswordChangedResponse?>) -> Void) throws -> CancellableRequest? {
        try passwordRepository.resetPasswordRequest(email: email, callback: callback)
    }

    /// Resets password
    /// - Parameters:
    ///   - email: user's email
    ///   - password: new password
    ///   - code: received code
    ///   - confirmPassword: new password confirmation
    ///   - callback: success/error callback
    /// - Returns: cancellable request
    public func resetPassword(
        email: String,
        password: String,
        code: String?,
        confirmPassword: String?,
        callback: @escaping (SDKApiResult<PasswordChangedResponse?>) -> Void
    ) throws -> CancellableRequest? {
        try passwordRepository.resetPassword(
            email: email,
            password: password,
            code: code,
            callback: callback
        )
    }

    /// Forgot password
    /// - Parameters:
    ///   - email: user's email
    ///   - doNotSendEmail: Do not send email flag
    ///   - callback: success/error callback
    /// - Returns: cancellable request
    public func forgotPassword(
        email: String,
        doNotSendEmail: Bool,
        callback: @escaping (SDKApiResult<EmailSentResponse?>) -> Void
    ) throws -> CancellableRequest? {
        try passwordRepository.forgotPasswordRequest(
            email: email,
            doNotSendEmail: doNotSendEmail,
            callback: callback)
    }
    
    // MARK: - Products

    /**
     Returns products categories from the suite
     */
    public func getProductCategories(callback: @escaping (SDKApiResult<[ProductCategoryModel]>) -> Void) throws -> CancellableRequest? {
        try productsRepository.getProductCategories(callback: callback)
    }

    /**
     Returns products for a given category from the suite
     */
    public func getProducts(categoryID: Int, callback: @escaping (SDKApiResult<[ProductModel]>) -> Void) throws -> CancellableRequest? {
        try productsRepository.getProducts(categoryID: categoryID, callback: callback)
    }

    // MARK: - Profile

    /**
     Returns customer profile info and personal data.
     - parameter callback: `SDKApiResult<ProfileResponse?>` callback which returns you the profile
     */
    public func getProfile(callback: @escaping (SDKApiResult<ProfileResponse?>) -> Void) throws -> CancellableRequest? {
        try profileRepository.getProfile(callback: callback)
    }

    /**
     Update customer profile info and personal data.
     - parameter firstName: `String` customer's first name.
     - parameter lastName: `String` customer's last name.
     - parameter avatarThumbnailURL: `String?` customer's avatar thumbnail url.
     - parameter avatarURL: `String?` customer's avatar url.
     - parameter birthDate: `DateTime?` customer's birth date.
     - parameter city: `String?` customer's city.
     - parameter country: `String?` customer's country.
     - parameter disablePushNotifications: `Bool?` customer's push notification state.
     - parameter favoriteStoreID: `Int?` customer's favorite store id.
     - parameter gender: `String?` customer's gender.
     - parameter houseNumber: `String?` customer's house number.
     - parameter landlinePhone: `String?` customer's landline phone number.
     - parameter language: `String?` customer's language.
     - parameter phoneNumber: `String?` customer's phone number.
     - parameter street: `String?` customer's street.
     - parameter zipCode: `String?` customer's zip code.
     - parameter callback: `SDKApiResult<ProfileResponse?>` callback which returns you the new profile
     */
    public func updateProfile(
        firstName: String,
        lastName: String,
        avatarThumbnailURL: String? = nil,
        avatarURL: String? = nil,
        birthDate: DateTime? = nil,
        city: String? = nil,
        country: String? = nil,
        disablePushNotifications: Bool? = nil,
        favoriteStoreID: Int? = nil,
        gender: String? = nil,
        houseNumber: String? = nil,
        landlinePhone: String? = nil,
        language: String? = nil,
        phoneNumber: String? = nil,
        street: String? = nil,
        zipCode: String? = nil,
        callback: @escaping (SDKApiResult<ProfileResponse?>) -> Void
    ) throws -> CancellableRequest? {
        try profileRepository.updateProfile(
            firstName: firstName,
            lastName: lastName,
            avatarThumbnailURL: avatarThumbnailURL,
            avatarURL: avatarURL,
            birthDate: birthDate,
            city: city,
            country: country,
            disablePushNotifications: disablePushNotifications,
            favoriteStoreID: favoriteStoreID,
            gender: gender,
            houseNumber: houseNumber,
            landlinePhone: landlinePhone,
            language: language,
            phoneNumber: phoneNumber,
            street: street,
            zipCode: zipCode,
            callback: callback
        )
    }

    /// Updates profile avatar
    /// - Parameters:
    ///   - url: avatar image url
    ///   - thumbnailURL: avatar thumbnail image url
    ///   - callback: completion handler
    /// - Returns: cancellable request reference
    public func updateAvatar(url: String?, thumbnailURL: String?, callback: @escaping (SDKApiResult<Void?>) -> Void) throws -> CancellableRequest? {
        try profileRepository.updateAvatar(url: url, thumbnailURL: thumbnailURL, callback: callback)
    }

    /**
     Returns logged in user's customer card details
     */
    public func getCustomerCard(callback: @escaping (SDKApiResult<CustomerCardModel?>) -> Void) throws -> CancellableRequest? {
        try profileRepository.getCustomerCard(callback: callback)
    }

    // MARK: - Store

    /**
     Returns all stores from the suite
     */
    public func getStores(callback: @escaping (SDKApiResult<[StoreModel]>) -> Void) throws -> CancellableRequest? {
        try storeRepository.getStores(callback: callback)
    }

    /**
     Set store as favorite.
     - parameter storeID: `Int` Store Id
     - parameter isFavorite: `Bool` Favorite flag
     - parameter callback: (optional) `SDKApiResult<Bool>` containing  `API.Stores.StoresSetFavorite` API result
     - returns `CancellableRequest` cancellation handle which allows you to cancel the request.
     - throws either `SDKApiError.invalidContext` or `SDKApiError.parse` depending on the issues
     */

    public func setStoreFavorite(storeID: Int, isFavorite: Bool, callback: ((SDKApiResult<Bool>)->Void)? = nil) throws -> CancellableRequest? {
        try storeRepository.setStoreFavorite(storeID: storeID, isFavorite: isFavorite, callback: callback)
    }
    
    // MARK: - Touchpoints

    /// Save touchpoint of the mobile application. Touchpoints are application events reported to the suite, like 'Application Downloaded', 'Application Opened', etc.
    /// When tracking AppDownload, a unique device id has to be present to allow the suite to ignore future requests. If the application developer decides to implement single AppDownload reporting on the application side, a random UUID can be sent instead. For other touchpoints, user needs to be authenticated and Bearer JWT needs to be passed with the request.
    /// - Parameters:
    ///     - parameter type: `TouchpointType` Type of the touchpoint event
    ///     - parameter callback (optional): `SDKApiResult<Bool>` containing  `API.Touchpoints.TouchpointsSaveTouchpoint` API result
    ///     - returns `CancellableRequest` cancellation handle which allows you to cancel the request.
    ///     - throws `SDKApiError.invalidContext` in case wrong context is used
    ///     - throws `SDKApiError.invalidStore` in case store is invalid
    @discardableResult
    public func saveTouchpoint(
        type: TouchpointType,
        callback: ((SDKApiResult<Bool>)->Void)? = nil
    ) throws -> CancellableRequest? {
        guard let appVersion = self.appVersion else {
            callback?(.failure(.notStarted))
            return nil
        }
        guard let deviceID = store.deviceID else {
            throw SDKApiError.invalidStore
        }
        return try touchpointsRepository.saveTouchpoint(
            appVersion: appVersion,
            type: type,
            deviceID: deviceID,
            callback: callback
        )
    }

    /// Save AppDownload touchpoint
    private func saveAppDownloadTouchpoint(callback: ((SDKApiResult<Bool>)->Void)? = nil) throws {
        guard let appVersion = self.appVersion else {
            throw SDKApiError.notStarted
        }
        guard let deviceID = store.deviceID else {
            throw SDKApiError.invalidStore
        }
        try touchpointsRepository.saveAppDownloadTouchpoint(
            appVersion: appVersion,
            deviceID: deviceID,
            callback: callback
        )
    }
    
    // MARK: - Purchase Histories
    /**
     Returns users list of purchases
     */
    public func getPurchaseHistory(callback: @escaping (SDKApiResult<UserMonthPurchasesViewDataPage?>) -> Void) throws -> CancellableRequest? {
        try purchaseHistoriesRepository.getPurchaseHistory(callback: callback)
    }
    
    public func getPurchaseDetails(purchaseHistoryId: Int, callback: @escaping (SDKApiResult<PurchaseHistoryView?>) -> Void) throws -> CancellableRequest? {
        
        try purchaseHistoriesRepository.getPurchaseDetails(purchaseHistoryId: purchaseHistoryId, callback: callback)
    }
    
    // MARK: - QR activation
    
    public func getPublicKey() -> String?{
        
        if let configDic = UserDefaults.standard.object(forKey: Constants.Key.appConfig.rawValue) as? [String:Any?], let config = AppConfigurationResponse(dic: configDic) {
            return config.pocPublicKey ?? ""
        }
        return nil
    }
    
    public func setPocToken(pocToken: String?){
        
        UserDefaults.standard.set(pocToken, forKey: Constants.Key.pocToken.rawValue)
        
    }
    
    // MARK: - API Client calls

    /// Makes and returns a cancellable api request
    /// - Parameters:
    ///   - request: The API request to make
    ///   - complete: A closure that gets the parsed `SDKApiResult` from the raw `APIResponse`
    /// - Returns: Cancellable api request
    public func apiRequest<T>(
        _ request: APIRequest<T>,
        emptyResponseCodes: Set<Int> = [],
        complete: @escaping (SDKApiResult<T>) -> Void
    ) throws -> CancellableRequest? {
        guard let client = contextManager.getAPIClient() else {
            throw SDKApiError.invalidContext
        }

        return client.apiRequest(
            request,
            emptyResponseCodes: emptyResponseCodes,
            complete: complete
        )
    }

    // MARK: - Logging

    /// Updates used minimum log level
    /// - Parameter level: Minimum log level to use. Default is `.silent`
    public func setMinLogLevel(level: LogLevel) {
        Logger.consoleLogLevel = level
    }
}

extension Beaconsmind: BeaconListenerDelegate {
    public func proximity(inRegion: CLBeaconRegion) {
        Logger.log("Beaconsmind.proximity inRegion \(inRegion.uuidString)", category: .monitoring)
        diagnosticsRepository.report(force: false)
    }
    
    public func proximity(leftRegion: CLBeaconRegion) {
        Logger.log("Beaconsmind.proximity leftRegion \(leftRegion.uuidString)", category: .monitoring)
        diagnosticsRepository.report(force: false)
    }
    
    public func ranged(region: CLRegion?, error: Error) {
        Logger.log("Beaconsmind.ranged \(region?.identifier ?? "") \(error)", category: .monitoring)
    }
    
    public func ranged(beacons: [CLBeacon]) {
        Logger.log("Beaconsmind.ranged \(beacons.count) beacons", category: .monitoring)
    }
}

extension Beaconsmind: ContextManagerDelegate {

    func contextManager(_ manager: ContextManager, savedContext context: APIContext?) {
        delegate?.beaconsmind(self, onContextChanged: context)
        
        /// When user signs in, we want to send the diagnostics to the server.
        diagnosticsRepository.report(force: true)

        /// Now that there is a valid context, the device token can be sent to Beaconsmind (if there's a stored one)
        if let deviceToken = store.deviceToken, let platformType = store.platformType {
            do {
                try notificationsRepository.subscribeToNotifications(
                    deviceToken: deviceToken,
                    platformType: platformType,
                    callback: nil
                )
            } catch {
                Logger.log("Valid context but device token is missing so can't register device token", category: .general, level: .warning)
            }
        }
        
        if let userId = context?.userID {
            Logger.setUserId(userId: userId)
            self.diagnosticsRepository.getAppConfiguration()
        }
        
        if let deviceId = store.deviceID {
            Logger.setDeviceId(deviceId: deviceId)
        }
        
        if let platformId = store.deviceToken {
            Logger.setPlatformId(platformId: platformId)
        }

        /// In developer mode, permissions are requested explicitly
        if isInDeveloperMode {
            locationsRepository.requestPermission(callback: nil)
            registerForPushNotifications()
        }
    }

    func contextManager(_ manager: ContextManager, restoredContext context: APIContext?) {
        delegate?.beaconsmind(self, onContextChanged: context)
        
        if let userId = context?.userID {
            Logger.setUserId(userId: userId)
            self.diagnosticsRepository.getAppConfiguration()
        }
        
        if let deviceId = store.deviceID {
            Logger.setDeviceId(deviceId: deviceId)
        }
        
        if let platformId = store.deviceToken {
            Logger.setPlatformId(platformId: platformId)
        }

        /// In developer mode, permissions are requested explicitly
        if isInDeveloperMode {
            locationsRepository.requestPermission(callback: nil)
            registerForPushNotifications()
        }
    }

    func contextManagerCouldNotRestoreContext(_ manager: ContextManager) {
        delegate?.beaconsmind(self, onContextChanged: nil)
    }

    func contextManagerClearedContext(_ manager: ContextManager) {
        delegate?.beaconsmind(self, onContextChanged: nil)
    }
}

// MARK: - Start automatic touchpoint saving

extension Beaconsmind {

    private func startAutomaticTouchpointSaving() {
        /// Saving app download touchpoint if needed
        if !store.didSaveAppDownloadTouchpoint {
            try? saveAppDownloadTouchpoint() { [weak self] result in
                    if case let .success(saved) = result, saved {
                        try? self?.store.saveAppDownloadSentTouchpoint()
                    }
                }
        }

        /// Observing app lifecycle events
        /// https://developer.apple.com/documentation/uikit/uiapplicationdelegate
        /// Why are there no `removeObserver` calls in the deinit? According to this, we don't need it above iOS 9
        /// https://developer.apple.com/documentation/foundation/notificationcenter/1413994-removeobserver
        NotificationCenter.default.addObserver(
            forName: UIApplication.didBecomeActiveNotification,
            object: nil,
            queue: OperationQueue.main) { [weak self] notification in
                _ = try? self?.saveTouchpoint(type: .AppOpen)
            }
        
        NotificationCenter.default.addObserver(
            forName: UIApplication.willTerminateNotification,
            object: nil,
            queue: OperationQueue.main) { notification in
                Logger.log("onWillTerminateNotification", category: .general)
            }
    }
}

