//
//  Copyright © 01.12.21 Beaconsmind. All rights reserved.
//

import Foundation

/// Stores given properties on the secure keychain
protocol Store {

    var deviceID: String? { get }
    var deviceToken: String? { get }
    var didSaveAppDownloadTouchpoint: Bool { get }
    var contextData: Data? { get }
    var platformType: PlatformType? { get }

    func saveAppDownloadSentTouchpoint() throws
    func saveContextData(data: Data) throws
    func clearContextData() throws
    func saveDeviceToken(string: String) throws
    func savePlatformType(type: PlatformType) throws
}

final class StoreImpl: Store {

    private enum Key: String {
        case deviceID
        case deviceTokenString
        case sentAppDownloadTouchpoint
        case sdkContext
        case platformType
    }

    private let keychain: KeychainWrapper

    init(storeName: String) {
        self.keychain = KeychainWrapper(storeName: storeName)
    }

    // MARK: - Device

    var deviceID: String? {
        let key = Key.deviceID.rawValue
        if let deviceID = try? keychain.string(key: key) {
            return deviceID
        }

        do {
            let uuid = UUID().uuidString
            try keychain.set(key: key, string: uuid)
            return uuid
        } catch {
            return nil
        }
    }

    var deviceToken: String? {
        try? keychain.string(key: Key.deviceTokenString.rawValue)
    }

    func saveDeviceToken(string: String) throws {
        try keychain.set(key: Key.deviceTokenString.rawValue, string: string)
    }

    var platformType: PlatformType? {
        guard let intValue = try? keychain.int(key: Key.platformType.rawValue) else {
            return nil
        }
        return PlatformType(rawValue: intValue)
    }

    func savePlatformType(type: PlatformType) throws {
        try keychain.set(key: Key.platformType.rawValue, int: type.rawValue)
    }

    // MARK: - Touchpoints

    var didSaveAppDownloadTouchpoint: Bool {
        guard let storedValue = try? keychain.bool(key: Key.sentAppDownloadTouchpoint.rawValue) else {
            return false
        }
        return storedValue
    }

    func saveAppDownloadSentTouchpoint() throws {
        try keychain.set(key: Key.sentAppDownloadTouchpoint.rawValue, bool: true)
    }

    // MARK: - Context

    func saveContextData(data: Data) throws {
        try keychain.set(key: Key.sdkContext.rawValue, data: data)
    }

    var contextData: Data? {
        try? keychain.data(key: Key.sdkContext.rawValue)
    }

    func clearContextData() throws {
        try keychain.clear(key: Key.sdkContext.rawValue)
    }
}
