//
//  Copyright © 02.03.22 Beaconsmind. All rights reserved.
//

import Foundation

/// Fetched beacons are stored in the insecure user defaults
class BeaconPersistence {

    private let encoder = JSONEncoder()
    private let decoder = JSONDecoder()

    func saveBeacons(_ userID: String, beacons: [Beacon]) {
        if let encoded = try? encoder.encode(beacons) {
            UserDefaults.standard.set(encoded, forKey: key(userID: userID))
        }
    }

    func loadBeacons(userID: String) -> [Beacon] {
        guard let encoded = UserDefaults.standard.data(forKey: key(userID: userID)) else {
            return []
        }
        if let decoded = try? decoder.decode([Beacon].self, from: encoded) {
            return decoded
        }
        return []
    }

    private func key(userID: String) -> String {
        Constants.Key.beaconsStore.rawValue + ".\(userID)"
    }
}
