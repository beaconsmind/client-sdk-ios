//
//  Copyright © 02.12.21 Beaconsmind. All rights reserved.
//

import Foundation

private enum KeychainError: Error {
    case create
    case read
    case update
    case delete
    case wrongValue
}

final class KeychainWrapper {

    private let storeName: String

    init(storeName: String) {
        self.storeName = storeName
    }

    // MARK: - Getters

    func data(key: String) throws -> Data? {
        try read(key: key)
    }

    func string(key: String) throws -> String? {
        guard let data = try read(key: key) else {
            return nil
        }
        return String(data: data, encoding: .utf8) as String?
    }

    func bool(key: String) throws -> Bool? {
        guard let number = try object(key: key) as? NSNumber else {
            return nil
        }
        return number.boolValue
    }

    func int(key: String) throws -> Int? {
        guard let number = try object(key: key) as? NSNumber else {
            return nil
        }
        return number.intValue
    }

    private func object(key: String) throws -> NSCoding? {
        guard let data = try read(key: key) else {
            return nil
        }
        return NSKeyedUnarchiver.unarchiveObject(with: data) as? NSCoding
    }

    // MARK: - Setters

    func set(key: String, string: String) throws {
        guard let data = string.data(using: .utf8) else {
            throw KeychainError.wrongValue
        }
        try set(key: key, data: data)
    }

    func set(key: String, bool: Bool) throws {
        try set(key: key, value: NSNumber(value: bool))
    }

    func set(key: String, int: Int) throws {
        try set(key: key, value: NSNumber(integerLiteral: int))
    }

    private func set(key: String, value: NSCoding) throws {
        let data = NSKeyedArchiver.archivedData(withRootObject: value)
        try set(key: key, data: data)
    }

    func set(key: String, data: Data) throws {
        if try read(key: key) != nil {
            try update(key: key, value: data)
        } else {
            try create(key: key, value: data)
        }
    }

    func clear(key: String) throws {
        try delete(key: key)
    }
}

// MARK: - Accessing keychain through low-level API

extension KeychainWrapper {

    private func create(key: String, value: Data) throws {
        let status = SecItemAdd([
            kSecClass: kSecClassGenericPassword,
            kSecAttrAccount: key,
            kSecAttrService: storeName,
            kSecAttrAccessible: kSecAttrAccessibleAfterFirstUnlock,
            kSecValueData: value,
        ] as NSDictionary, nil)

        guard status == errSecSuccess else {
            throw KeychainError.create
        }
    }

    private func read(key: String) throws -> Data? {
        var result: AnyObject?
        let status = SecItemCopyMatching([
            kSecClass: kSecClassGenericPassword,
            kSecAttrAccount: key,
            kSecAttrService: storeName,
            kSecReturnData: true,
        ] as NSDictionary, &result)

        switch status {
        case errSecSuccess:
            return result as? Data
        case errSecItemNotFound:
            return nil
        default:
            throw KeychainError.read
        }
    }

    private func update(key: String, value: Data) throws {
        let status = SecItemUpdate([
            kSecClass: kSecClassGenericPassword,
            kSecAttrAccount: key,
            kSecAttrService: storeName,
        ] as NSDictionary, [
            kSecValueData: value,
        ] as NSDictionary)

        guard status == errSecSuccess else {
            throw KeychainError.update
        }
    }

    private func delete(key: String) throws {
        let status = SecItemDelete([
            kSecClass: kSecClassGenericPassword,
            kSecAttrAccount: key,
            kSecAttrService: storeName,
        ] as NSDictionary)

        guard status == errSecSuccess else {
            throw KeychainError.delete
        }
    }
}
