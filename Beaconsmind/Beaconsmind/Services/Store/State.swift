//
//  Copyright © 2022 beaconsmind. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

class State {
    var locationAuthorization: CLAuthorizationStatus?
    var locationAccuracy: CLAccuracyAuthorization?
    var notificationAuthorization: UNAuthorizationStatus?
    
    /// Returns missing permissions that are required for beacons monitoring to work
    /// as comma-separated string, e.g.: "authorizedAlways,fullAccuracy" when both
    /// location authorization and accuracy are missing.
    var missingPermissions: String? {
        guard let locationAuthorization = locationAuthorization, let locationAccuracy = locationAccuracy, let notificationAuthorization = notificationAuthorization else {
            return nil
        }
        
        var mp = ""
        
        if (locationAuthorization != .authorizedAlways && locationAccuracy != .fullAccuracy) {
            mp =  "\(CLAuthorizationStatus.authorizedAlways.toString()),\(CLAccuracyAuthorization.fullAccuracy.toString())"
        }
        
        else if (locationAuthorization != .authorizedAlways) {
            mp = CLAuthorizationStatus.authorizedAlways.toString()
        }
        
        else if (locationAccuracy != .fullAccuracy) {
            mp = CLAccuracyAuthorization.fullAccuracy.toString()
        }
        
                
        if notificationAuthorization != .authorized {
            if !mp.isEmpty {
                mp.append(",")
            }
            mp.append(UNAuthorizationStatus.authorized.toString())
        }
        
        return mp.isEmpty ? nil : mp
    }

    init() {
        UIDevice.current.isBatteryMonitoringEnabled = true
        
        /// Query initial location permission status so that it will be reported by
        /// DiagnosticsRepository even if beacons are not being scanned e.g.
        /// when location permission is denied or there are no beacons to monitor.
        if #available(iOS 14.0, *) {
            let locationManager = CLLocationManager()
            locationAuthorization = locationManager.authorizationStatus
            locationAccuracy = locationManager.accuracyAuthorization
        } else {
            locationAuthorization = CLLocationManager.authorizationStatus()
            locationAccuracy = nil
        }
        
        isLocationAuthorized = [.authorizedAlways, .authorizedWhenInUse].contains(locationAuthorization)
        
        UNUserNotificationCenter.current().getNotificationSettings { settings in
            self.notificationAuthorization = settings.authorizationStatus
        }
    }
    
    var isLocationAuthorized: Bool? {
        didSet {
            if let isLocationAuthorized = isLocationAuthorized, !isLocationAuthorized {
                lastLocation = nil
            }
        }
    }
    var lastLocation: LatLongModel?
    var batteryPercentage: Float? {
        if UIDevice.current.isBatteryMonitoringEnabled {
            return UIDevice.current.batteryLevel * 100
        }
        return nil
    }
    
    var batteryState: UIDevice.BatteryState? {
        if UIDevice.current.isBatteryMonitoringEnabled {
            return UIDevice.current.batteryState
        }
        return nil
    }
    
    var isLowPowerModeEnabled: Bool {
        return ProcessInfo.processInfo.isLowPowerModeEnabled
    }
}
