//
//  Copyright © 2022 beaconsmind. All rights reserved.
//

import XCTest
@testable import Beaconsmind

class StateTests: BaseTestCase {

    private let sut = State()

    func test_disablingLocation_clearsLastLocation() {
        XCTAssertNil(sut.isLocationAuthorized)
        XCTAssertNil(sut.lastLocation)

        sut.isLocationAuthorized = true
        sut.lastLocation = mockPosition

        XCTAssertTrue(sut.isLocationAuthorized!)
        XCTAssertEqual(sut.lastLocation, mockPosition)

        sut.isLocationAuthorized = false

        XCTAssertFalse(sut.isLocationAuthorized!)
        XCTAssertNil(sut.lastLocation)
    }
}
