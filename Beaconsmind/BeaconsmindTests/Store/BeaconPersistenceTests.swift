//
//  Copyright © 04.12.21 Beaconsmind. All rights reserved.
//

import XCTest
@testable import Beaconsmind

class BeaconPersistenceTests: BaseTestCase {

    private let sut = BeaconPersistence()

    func testSavingRestoringBeacons() {
        XCTAssertEqual(sut.loadBeacons(userID: mockUserID), [])
        sut.saveBeacons(mockUserID, beacons: [mockBeacon()])
        XCTAssertEqual(sut.loadBeacons(userID: mockUserID), [mockBeacon()])
    }
}
