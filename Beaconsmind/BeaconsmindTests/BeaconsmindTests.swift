//
//  Copyright © 04.12.21 Beaconsmind. All rights reserved.
//

import XCTest
@testable import Beaconsmind

class BeaconsmindTests: BaseTestCase {

    private let deviceToken = "test.token"

    private lazy var sut: Beaconsmind = {
        mockBeaconsmind(contextManager: mockContextManager())
    }()

    private func makeSUT(
        expectation: XCTestExpectation,
        results: [SDKApiResult<MockAPIClientResponse>]
    ) -> Beaconsmind {
        let contextManager = mockContextManager(
            context: nil,
            apiClient: MockAPIClient(
                expectation: expectation,
                results: results
            ),
            delegate: nil
        )
        return mockBeaconsmind(contextManager: contextManager)
    }

    func test_startRestoresEmptyContext() throws {
        let expectation = XCTestExpectation(description: "Should not restore")
        let delegate = MockBeaconsmindDelegate(expectation: expectation)
        try sut.start(delegate: delegate, appVersion: "1.0", hostname: "test.host.name")
        wait(for: [expectation], timeout: 0.1)
    }

    func test_subscribeToNotifications_storesTokenWithoutContext() {
        XCTAssertNil(mockStore.deviceToken)
        do {
            try sut.register(deviceToken: deviceToken) { result in
                XCTFail()
            }
        } catch {
            guard case SDKApiError.invalidContext = error else {
                XCTFail("Should return an invalid context error")
                return
            }
        }
        XCTAssertEqual(mockStore.deviceToken, deviceToken)
    }

    func test_contextManagerDelegate_sendsStoredTokenOnSavedContext() throws {
        XCTAssertNil(mockStore.deviceToken)
        try mockStore.saveDeviceToken(string: deviceToken)
        try mockStore.savePlatformType(type: .Apns)

        let loginResponse = try API.Auth.AuthToken.Response(
            statusCode: 200,
            data: JSONEncoder().encode(mockTokenResponse),
            decoder: JSONDecoder()
        )
        let deviceTokenResponse = try API.Notifications.NotificationsSubscribe.Response(
            statusCode: 200,
            data: JSONEncoder().encode(mockSubscribeRequest),
            decoder: JSONDecoder()
        )

        let expectation = XCTestExpectation(description: "Both calls should succeed")
        let sut = makeSUT(
            expectation: expectation,
            results: [
                .success(loginResponse),
                .success(deviceTokenResponse)
            ]
        )
        XCTAssertNil(sut.getAPIContext())
        try sut.login(username: "", password: "", callback: { result in
            if case .failure = result {
                XCTFail()
            }
        })

        wait(for: [expectation], timeout: 0.1)
        XCTAssertNotNil(sut.getAPIContext())
    }
}
