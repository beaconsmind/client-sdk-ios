//
//  Copyright © 2022 beaconsmind. All rights reserved.
//

@testable import Beaconsmind

extension Beacon {

    var storeInfo: StoreBeaconInfo {
        StoreBeaconInfo(
            cashbox: false,
            beaconId: nil,
            beaconType: nil,
            description: description,
            major: "\(major)",
            minor: "\(minor)",
            name: name,
            store: store,
            uuid: uuid
        )
    }
}
