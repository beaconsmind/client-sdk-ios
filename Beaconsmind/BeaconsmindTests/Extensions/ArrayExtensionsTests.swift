//
//  Copyright © 2022 beaconsmind. All rights reserved.
//

import XCTest
@testable import Beaconsmind

class ArrayExtensionsTests: BaseTestCase {

    func test_nextItems_returnsWholeArray_withZeroStartAndArraySize() {
        let sut = [0, 1, 2, 3, 4]
        let result = sut.nextItems(fromIndex: 0, size: sut.count)
        XCTAssertEqual(result.items, sut)
        XCTAssertEqual(result.nextIndex, 5)
    }

    func test_nextItems_emptyWithWrongParameters() {
        let sut = [0, 1, 2, 3, 4]

        let resultTooBigFromIndex = sut.nextItems(fromIndex: 1500, size: 5)
        XCTAssertEqual(resultTooBigFromIndex.items, [0, 1, 2, 3, 4])
        XCTAssertEqual(resultTooBigFromIndex.nextIndex, 5)

        let resultNegativeFromIndex = sut.nextItems(fromIndex: -1, size: 5)
        XCTAssertEqual(resultNegativeFromIndex.items, [0, 1, 2, 3, 4])
        XCTAssertEqual(resultNegativeFromIndex.nextIndex, 5)

        let resultNegativeSize = sut.nextItems(fromIndex: 1, size: -5)
        XCTAssertEqual(resultNegativeSize.items, [])
        XCTAssertEqual(resultNegativeSize.nextIndex, 0)
    }

    func test_nextItems_cutsSliceOnlyIfArrayIsBigEnough() {
        let sut = [0, 1, 2, 3, 4]
        let result = sut.nextItems(fromIndex: 1, size: 2)
        XCTAssertEqual(result.items, [1, 2])
        XCTAssertEqual(result.nextIndex, 3)
    }

    func test_nextItems_slidesIfArrayIsNotBigEnough() {
        let sut = [0, 1, 2, 3, 4]
        let result = sut.nextItems(fromIndex: 2, size: 12)
        XCTAssertEqual(result.items, [2, 3, 4, 0, 1, 2, 3, 4, 0, 1, 2, 3])
        XCTAssertEqual(result.nextIndex, 4)
    }

    func test_edgeCases() {
        let sutEmpty: [Int] = []
        let empty = sutEmpty.nextItems(fromIndex: 1, size: 4)
        XCTAssertEqual(empty.items, [])
        XCTAssertEqual(empty.nextIndex, 0)

        let zero = [0].nextItems(fromIndex: 0, size: 4)
        XCTAssertEqual(zero.items, [0, 0, 0, 0])
        XCTAssertEqual(zero.nextIndex, 1)

        let lastStartIndex = [0, 1, 2, 3, 4, 5, 6].nextItems(fromIndex: 6, size: 4)
        XCTAssertEqual(lastStartIndex.items, [6, 0, 1, 2])
        XCTAssertEqual(lastStartIndex.nextIndex, 3)

        let tooBigStartIndex = [0, 1, 2, 3, 4, 5, 6].nextItems(fromIndex: 7, size: 4)
        XCTAssertEqual(tooBigStartIndex.items, [0, 1, 2, 3])
        XCTAssertEqual(tooBigStartIndex.nextIndex, 4)

        let normal = [0, 1, 2, 3, 4, 5, 6].nextItems(fromIndex: 0, size: 4)
        XCTAssertEqual(normal.items, [0, 1, 2, 3])
        XCTAssertEqual(normal.nextIndex, 4)
    }

    func test_removeItems_empty() {
        var sut: [Int] = []
        sut.removeItems(from: [1, 2, 3])
        XCTAssertEqual(sut, [])
    }

    func test_removeItems_all() {
        var sut: [Int] = [1, 2, 3, 1, 2, 3]
        sut.removeItems(from: [1, 2, 3, 3, 2, 1])
        XCTAssertEqual(sut, [])
    }

    func test_removeItems_partialKeepsOrder() {
        var sut: [Int] = [1, 2, 3, 2, 3, 1]
        sut.removeItems(from: [3, 2, 1])
        XCTAssertEqual(sut, [2, 3, 1])
    }
}
