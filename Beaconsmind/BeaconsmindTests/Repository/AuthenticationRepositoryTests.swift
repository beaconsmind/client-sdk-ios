//
//  Copyright © 07.12.21 Beaconsmind. All rights reserved.
//

import XCTest
@testable import Beaconsmind

class AuthenticationRepositoryTests: BaseTestCase {

    func test_loginSuccess_savesContext() throws {
        let expectation = XCTestExpectation(description: "Should save context")
        let response = try API.Auth.AuthToken.Response(
            statusCode: 200,
            data: JSONEncoder().encode(mockTokenResponse),
            decoder: JSONDecoder()
        )
        let sut = makeSUT(
            results: [.success(response)],
            savedExpectation: expectation,
            savedShouldBeNil: false
        )
        try sut.login(username: "", password: "") { result in
            switch result {
            case .success(let response):
                XCTAssertEqual(self.mockUserID, response.userID)
                XCTAssertEqual(self.mockAccessToken, response.accessToken)
            case .failure:
                XCTFail()
            }
        }
        wait(for: [expectation], timeout: 0.1)
    }

    func test_signupSuccess_savesContext() throws {
        let expectation = XCTestExpectation(description: "Should save context")
        let response = try API.Accounts.AccountsCreateUser.Response(
            statusCode: 200,
            data: JSONEncoder().encode(mockCreateUserResponse),
            decoder: JSONDecoder()
        )
        let sut = makeSUT(
            results: [.success(response)],
            savedExpectation: expectation,
            savedShouldBeNil: false
        )
        try sut.signup(
            username: "",
            firstName: "",
            lastName: "",
            password: "",
            confirmPassword: "",
            avatarThumbnailURL: nil,
            avatarURL: nil,
            language: nil,
            gender: nil,
            favoriteStoreID: nil,
            birthDate: nil,
            countryCode: nil
        ) { result in
            switch result {
            case .success(let response):
                XCTAssertEqual(self.mockUserID, response.userID)
                XCTAssertEqual(self.mockAccessToken, response.accessToken)
            case .failure:
                XCTFail()
            }
        }
        wait(for: [expectation], timeout: 0.1)
    }

    private func makeSUT(
        results: [SDKApiResult<MockAPIClientResponse>],
        savedExpectation: XCTestExpectation? = nil, savedShouldBeNil: Bool = true,
        restoredExpectation: XCTestExpectation? = nil, restoredShouldBeNil: Bool = true,
        couldNotRestoreExpectation: XCTestExpectation? = nil,
        clearedContextExpectation: XCTestExpectation? = nil
    ) -> AuthenticationRepository {
        updateMockContextManagerDelegate(
            savedExpectation: savedExpectation,
            savedShouldBeNil: savedShouldBeNil,
            restoredExpectation: restoredExpectation,
            restoredShouldBeNil: restoredShouldBeNil,
            couldNotRestoreExpectation: couldNotRestoreExpectation,
            clearedContextExpectation: clearedContextExpectation
        )
        let contextManager = mockContextManager(
            apiClient: MockAPIClient(results: results),
            delegate: mockContextManagerDelegate
        )
        return AuthenticationRepositoryImpl(contextManager: contextManager)
    }
}
