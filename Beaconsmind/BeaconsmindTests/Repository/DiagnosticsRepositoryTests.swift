//
//  Copyright © 2022 beaconsmind. All rights reserved.
//

import XCTest
import UIKit
@testable import Beaconsmind

class DiagnosticsRepositoryTests: BaseTestCase {
    private lazy var sut: DiagnosticsRepositoryImpl = {
        DiagnosticsRepositoryImpl(contextManager: mockContextManager(), store: mockStore, state: mockState)
    }()

    func test_getMetrics() {
        mockState.locationAuthorization = .authorizedAlways
        mockState.locationAccuracy = .fullAccuracy
        
        let metrics = sut.getMetrics()

        XCTAssertEqual(metrics[0].key, "platform")
        XCTAssertEqual(metrics[0].value, SDKPlatform.current.rawValue)
        XCTAssertEqual(metrics[1].key, "osVersion")
        XCTAssertEqual(metrics[1].value, UIDevice.current.systemVersion)
        XCTAssertEqual(metrics[2].key, "sdkVersion")
        XCTAssertEqual(metrics[2].value, Beaconsmind.sdkVersion)
        XCTAssertEqual(metrics[3].key, "manufacturer")
        XCTAssertEqual(metrics[3].value, UIDevice.current.manufacturer)
        XCTAssertEqual(metrics[4].key, "model")
        XCTAssertEqual(metrics[4].value, UIDevice.current.deviceModel)
        XCTAssertEqual(metrics[5].key, "batteryLowPowerModeEnabled")
        XCTAssertEqual(metrics[5].value, "false")
        XCTAssertEqual(metrics[6].key, "locationAuthorization")
        XCTAssertEqual(metrics[6].value, "authorizedAlways")
        XCTAssertEqual(metrics[7].key, "locationAccuracy")
        XCTAssertEqual(metrics[7].value, "fullAccuracy")
    }
    
    func test_getMetrics_whenBothPermissionsAreMissing() {
        mockState.locationAuthorization = .authorizedWhenInUse
        mockState.locationAccuracy = .reducedAccuracy
        
        let metrics = sut.getMetrics()

        XCTAssertEqual(metrics[6].key, "missingPermissions")
        XCTAssertEqual(metrics[6].value, "authorizedAlways,fullAccuracy")
        XCTAssertEqual(metrics[7].key, "locationAuthorization")
        XCTAssertEqual(metrics[7].value, "authorizedWhenInUse")
        XCTAssertEqual(metrics[8].key, "locationAccuracy")
        XCTAssertEqual(metrics[8].value, "reducedAccuracy")
    }
    
    func test_getMetrics_whenLocationAccuracyIsMissing() {
        mockState.locationAuthorization = .authorizedAlways
        mockState.locationAccuracy = .reducedAccuracy
        
        let metrics = sut.getMetrics()

        XCTAssertEqual(metrics[6].key, "missingPermissions")
        XCTAssertEqual(metrics[6].value, "fullAccuracy")
        XCTAssertEqual(metrics[7].key, "locationAuthorization")
        XCTAssertEqual(metrics[7].value, "authorizedAlways")
        XCTAssertEqual(metrics[8].key, "locationAccuracy")
        XCTAssertEqual(metrics[8].value, "reducedAccuracy")
    }
    
    func test_getMetrics_whenLocationAuthorizationIsMissing() {
        mockState.locationAuthorization = .authorizedWhenInUse
        mockState.locationAccuracy = .fullAccuracy
        
        let metrics = sut.getMetrics()

        XCTAssertEqual(metrics[6].key, "missingPermissions")
        XCTAssertEqual(metrics[6].value, "authorizedAlways")
        XCTAssertEqual(metrics[7].key, "locationAuthorization")
        XCTAssertEqual(metrics[7].value, "authorizedWhenInUse")
        XCTAssertEqual(metrics[8].key, "locationAccuracy")
        XCTAssertEqual(metrics[8].value, "fullAccuracy")
    }
}
