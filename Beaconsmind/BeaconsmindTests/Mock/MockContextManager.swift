//
//  Copyright © 07.12.21 Beaconsmind. All rights reserved.
//

@testable import Beaconsmind

class MockContextManager: ContextManager {

    private weak var delegate: ContextManagerDelegate?

    private var originalContext: APIContext?
    private(set) var hostname: String?
    private(set) var context: APIContext?
    private var apiClient: APIClient?

    init(
        hostname: String?,
        context: APIContext? = nil,
        apiClient: APIClient? = nil,
        delegate: ContextManagerDelegate? = nil
    ) {
        self.hostname = hostname
        self.context = context
        self.originalContext = context
        self.apiClient = apiClient
        self.delegate = delegate
    }

    func saveContext(context: APIContext) throws {
        self.context = context
        delegate?.contextManager(self, savedContext: context)
    }

    func restoreContext() throws {
        guard let context = originalContext else {
            delegate?.contextManagerCouldNotRestoreContext(self)
            return
        }
        self.context = context
        delegate?.contextManager(self, restoredContext: context)
    }

    func clearContext() throws {
        self.context = nil
        delegate?.contextManagerClearedContext(self)
    }

    func getAPIClient() -> APIClient? {
        apiClient
    }

    func updateHostname(hostname: String) {
        self.hostname = hostname
    }

    func updateHostname(hostname: String, token: String) throws {
        self.hostname = hostname
    }

    func getUnauthenticatedAPIClient() -> APIClient? {
        apiClient
    }

    func setDelegate(delegate: ContextManagerDelegate?) {
        self.delegate = delegate
    }
}
