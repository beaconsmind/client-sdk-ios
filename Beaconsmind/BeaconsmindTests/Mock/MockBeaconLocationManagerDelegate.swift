//
//  Copyright © 2022 beaconsmind. All rights reserved.
//

@testable import Beaconsmind
import CoreLocation

class MockBeaconLocationManagerDelegate: BeaconLocationManagerDelegate {

    var authorized = false
    var errors: [Error] = []
    var ranged: [CLBeacon] = []
    var entered: [CLBeaconRegion] = []
    var left: [CLBeaconRegion] = []

    func onLocationManagerAuthorized(manager: BeaconLocationManager) {
        authorized = true
    }

    func onLocationManagerError(region: CLRegion?, error: LocationError) {
        errors.append(error)
    }

    func onLocationManagerRanged(regions: [CLBeacon], in region: CLBeaconRegion, position: LatLongModel?) {
        ranged.append(contentsOf: regions)
    }

    func onLocationManagerEntered(region: CLBeaconRegion, position: LatLongModel?) {
        entered.append(region)
    }

    func onLocationManagerLeft(region: CLBeaconRegion, position: LatLongModel?) {
        left.append(region)
    }
}
