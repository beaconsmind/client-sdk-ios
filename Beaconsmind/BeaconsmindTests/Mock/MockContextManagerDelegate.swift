//
//  Copyright © 04.12.21 Beaconsmind. All rights reserved.
//

import XCTest
@testable import Beaconsmind

class MockContextManagerDelegate: ContextManagerDelegate {

    private let savedExpectation: XCTestExpectation?
    private let savedShouldBeNil: Bool
    private let restoredExpectation: XCTestExpectation?
    private let restoredShouldBeNil: Bool
    private let couldNotRestoreExpectation: XCTestExpectation?
    private let clearedContextExpectation: XCTestExpectation?

    init(savedExpectation: XCTestExpectation? = nil, savedShouldBeNil: Bool = true,
         restoredExpectation: XCTestExpectation? = nil, restoredShouldBeNil: Bool = true,
         couldNotRestoreExpectation: XCTestExpectation? = nil,
         clearedContextExpectation: XCTestExpectation? = nil
    ) {
        self.savedExpectation = savedExpectation
        self.savedShouldBeNil = savedShouldBeNil
        self.restoredExpectation = restoredExpectation
        self.restoredShouldBeNil = restoredShouldBeNil
        self.couldNotRestoreExpectation = couldNotRestoreExpectation
        self.clearedContextExpectation = clearedContextExpectation
    }

    func contextManager(_ manager: ContextManager, savedContext context: APIContext?) {
        if savedShouldBeNil {
            XCTAssertNil(context)
        } else {
            XCTAssertNotNil(context)
        }
        savedExpectation?.fulfill()
    }

    func contextManager(_ manager: ContextManager, restoredContext context: APIContext?) {
        if restoredShouldBeNil {
            XCTAssertNil(context)
        } else {
            XCTAssertNotNil(context)
        }
        restoredExpectation?.fulfill()
    }

    func contextManagerCouldNotRestoreContext(_ manager: ContextManager) {
        couldNotRestoreExpectation?.fulfill()
    }

    func contextManagerClearedContext(_ manager: ContextManager) {
        clearedContextExpectation?.fulfill()
    }
}
