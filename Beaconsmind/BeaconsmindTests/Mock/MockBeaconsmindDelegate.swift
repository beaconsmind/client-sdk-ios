//
//  Copyright © 04.12.21 Beaconsmind. All rights reserved.
//

import XCTest
@testable import Beaconsmind

class MockBeaconsmindDelegate: BeaconsmindDelegate {

    private let expectation: XCTestExpectation?
    private let shouldBeNil: Bool

    init(expectation: XCTestExpectation? = nil, shouldBeNil: Bool = true) {
        self.expectation = expectation
        self.shouldBeNil = shouldBeNil
    }

    func beaconsmind(_ beaconsmind: Beaconsmind, onContextChanged context: APIContext?) {
        if shouldBeNil {
            XCTAssertNil(context)
        } else {
            XCTAssertNotNil(context)
        }
        expectation?.fulfill()
    }
}
