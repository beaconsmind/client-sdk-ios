//
//  Copyright © 07.12.21 Beaconsmind. All rights reserved.
//

import Alamofire
import XCTest
@testable import Beaconsmind

class MockAPIClient: APIClient {

    private let expectation: XCTestExpectation?
    private var results: [SDKApiResult<MockAPIClientResponse>]

    init(
        expectation: XCTestExpectation? = nil,
        results: [SDKApiResult<MockAPIClientResponse>]
    ) {
        self.results = results
        self.expectation = expectation
    }

    func apiRequest<T>(
        _ request: APIRequest<T>,
        emptyResponseCodes: Set<Int>,
        complete: @escaping (SDKApiResult<T>) -> Void
    ) -> CancellableRequest? {
        let result = results.removeFirst()
        switch result {
        case .success(let response):
            complete(.success(response.getValue() as! T))
        case .failure(let error):
            complete(.failure(error))
        }
        if results.isEmpty {
            expectation?.fulfill()
        }
        return nil
    }
}

/// Unfortunately we need mocked responses to confirm to a common protocol to put them in an array
protocol MockAPIClientResponse {
    func getValue() -> Self
}

extension API.Auth.AuthToken.Response: MockAPIClientResponse {
    func getValue() -> API.Auth.AuthToken.Response {
        self
    }
}

extension API.Notifications.NotificationsSubscribe.Response: MockAPIClientResponse {
    func getValue() -> API.Notifications.NotificationsSubscribe.Response {
        self
    }
}

extension API.Accounts.AccountsCreateUser.Response: MockAPIClientResponse {
    func getValue() -> API.Accounts.AccountsCreateUser.Response {
        self
    }
}

extension API.Configuration.ConfigurationGetBeacons.Response: MockAPIClientResponse {
    func getValue() -> API.Configuration.ConfigurationGetBeacons.Response {
        self
    }
}

extension API.Ping.PingPingMultipleBeacons.Response: MockAPIClientResponse {
    func getValue() -> API.Ping.PingPingMultipleBeacons.Response {
        self
    }
}
