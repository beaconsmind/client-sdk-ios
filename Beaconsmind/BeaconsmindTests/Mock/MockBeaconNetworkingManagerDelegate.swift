//
//  Copyright © 2022 beaconsmind. All rights reserved.
//

@testable import Beaconsmind

class MockBeaconNetworkingManagerDelegate: BeaconNetworkingManagerDelegate {

    var fetchedBeacons: [Beacon] = []

    func onNetworkingManager(manager: BeaconNetworkingManager, fetchedBeacons: [Beacon]) {
        self.fetchedBeacons = fetchedBeacons
    }
}
