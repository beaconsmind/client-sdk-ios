//
//  Copyright © 2022 beaconsmind. All rights reserved.
//

import CoreLocation

@testable import Beaconsmind

class MockBeaconListenerDelegate: BeaconListenerDelegate {

    var ranged: [CLBeacon] = []
    var inRegionEvents: [CLBeaconRegion] = []
    var leftRegionEvents: [CLBeaconRegion] = []
    var errorEvents: [Error] = []

    func ranged(beacons: [CLBeacon]) {
        ranged += beacons
    }

    func proximity(inRegion: CLBeaconRegion) {
        inRegionEvents.append(inRegion)
    }

    func proximity(leftRegion: CLBeaconRegion) {
        leftRegionEvents.append(leftRegion)
    }

    func ranged(region: CLRegion?, error: Error) {
        errorEvents.append(error)
    }
}
