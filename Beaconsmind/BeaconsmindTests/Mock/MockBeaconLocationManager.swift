//
//  Copyright © 2022 beaconsmind. All rights reserved.
//

@testable import Beaconsmind
import CoreLocation

class MockBeaconLocationManager: BeaconLocationManager {
    var isAuthorizedMock = false
    var isStarted = false
    var locationManager = CLLocationManager()
    var delegate: BeaconLocationManagerDelegate?
    var position: LatLongModel?
    var stopped = false
    var monitoredRegions: [CLBeaconRegion] = []
    var stoppedRangingRegion: CLBeaconRegion?
    var startedRangingRegion: CLBeaconRegion?
    var restartedMonitoringSignificantLocationChanges = false
    var updatedMonitoredRegions = false

    var isAuthorized: Bool {
        isAuthorizedMock
    }

    func start() -> Bool {
        isStarted = true
        isAuthorizedMock = true
        return true
    }

    func updateMonitoredRegions(regions: [CLBeaconRegion]) {
        monitoredRegions = regions
        updatedMonitoredRegions = true
    }

    func stop() {
        stopped = true
    }

    func stopRanging(region: CLBeaconRegion) {
        stoppedRangingRegion = region
    }

    func startRanging(region: CLBeaconRegion) {
        startedRangingRegion = region
    }

    func restartMonitoringSignificantLocationChanges() {
        restartedMonitoringSignificantLocationChanges = true
    }
}
