//
//  Copyright © 04.12.21 Beaconsmind. All rights reserved.
//

import Foundation
@testable import Beaconsmind

class MockStore: Store {

    var deviceID: String? = nil
    var didSaveAppDownloadTouchpoint: Bool = false
    var contextData: Data? = nil
    var deviceToken: String?
    var platformType: PlatformType? = nil

    func saveAppDownloadSentTouchpoint() throws {
        didSaveAppDownloadTouchpoint = true
    }

    func saveContextData(data: Data) throws {
        contextData = data
    }

    func clearContextData() throws {
        contextData = nil
    }

    func saveDeviceToken(string: String) throws {
        deviceToken = string
    }

    func savePlatformType(type: PlatformType) throws {
        platformType = type
    }
}
