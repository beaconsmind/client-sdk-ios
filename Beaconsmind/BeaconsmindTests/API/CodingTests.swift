//
//  Copyright © 17.12.21 Beaconsmind. All rights reserved.
//

import XCTest
@testable import Beaconsmind

class CodingTests: BaseTestCase {

    struct Test: Decodable {
        private enum CodingKeys: String, CodingKey {
            case date
        }
        let date: Date
    }

    func test_decodingISO8601DatesShouldSucceed() throws {
        let sut = JSONDecoder()
        sut.dateDecodingStrategy = .custom(dateDecoder)
        let expected: TimeInterval = 1639753810

        let strings = [
            "2021-12-17T15:10:10",
            "2021-12-17T15:10:10.000Z",
            "2021-12-17T15:10:10.0000000",
            "2021-12-17T15:10:10+00000"
        ]
        try strings.forEach { string in
            let data = "{\"date\":\"\(string)\"}".data(using: .utf8)!
            let decoded = try sut.decode(Test.self, from: data)
            XCTAssertEqual(decoded.date.timeIntervalSince1970, expected)
        }
    }
}
