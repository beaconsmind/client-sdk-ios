//
//  Copyright © 10.12.21 Beaconsmind. All rights reserved.
//

import CoreLocation
import XCTest
import Mockingbird
@testable import Beaconsmind

class BaseTestCase: XCTestCase {

    let mockAccessToken = "eyJhbGciOiJodHRwOi8vd3d3LnczLm9yZy8yMDAxLzA0L3htbGRzaWctbW9yZSNobWFjLXNoYTI1NiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1laWRlbnRpZmllciI6IjQ0NDk5OSIsIm5hbWVpZCI6IjQ0NDk5OSIsIm5iZiI6MTYzODkwMzk2NiwiZXhwIjoxNjQ2Njc5OTY2LCJpc3MiOiJodHRwczovL3Rlc3QtbGF0ZXN0LmJtcy5iZWFjb25zbWluZC5jb20iLCJhdWQiOiI1MDBmNTVjYWQwOTY0NzI3OGExYjBiMjA0ZDlkZmYwNCJ9.Hth0mBLdRCf_KmqfReppDn79nTKz28DNGUzzvx9zmso"
    let mockSubscribeRequest = SubscribeRequest(
        phoneOS: .iOS,
        platformType: .Apns,
        phoneManufacturer: "Apple",
        phoneModel: "iPhone SE",
        phoneOsVersion: "15.1",
        platformId: "iOS"
    )
    let mockExpiresIn = "3600"
    let mockHostname = "com.sdk.hostname"
    let mockUserID = "444999"
    let mockTokenType = "bearer"
    let mockStore: Store = MockStore()
    let mockState: State = State()

    override func setUp() {
        super.setUp()
        UserDefaults.standard.dictionaryRepresentation().keys.forEach { UserDefaults.standard.removeObject(forKey: $0) }
    }

    lazy var mockContext: APIContext = {
        APIContext(
            hostname: mockHostname,
            accessToken: mockAccessToken,
            userID: mockUserID,
            tokenType: mockTokenType,
            expiresIn: mockExpiresIn
        )
    }()

    func mockContextManager(
        context: APIContext? = nil,
        apiClient: APIClient? = nil,
        delegate: ContextManagerDelegate? = nil
    ) -> ContextManager {
        MockContextManager(
            hostname: mockHostname,
            context: context,
            apiClient: apiClient,
            delegate: delegate
        )
    }

    var mockContextManagerDelegate: ContextManagerDelegate?
    func updateMockContextManagerDelegate(
        savedExpectation: XCTestExpectation? = nil, savedShouldBeNil: Bool = true,
        restoredExpectation: XCTestExpectation? = nil, restoredShouldBeNil: Bool = true,
        couldNotRestoreExpectation: XCTestExpectation? = nil,
        clearedContextExpectation: XCTestExpectation? = nil
    ) {
        mockContextManagerDelegate = MockContextManagerDelegate(
            savedExpectation: savedExpectation,
            savedShouldBeNil: savedShouldBeNil,
            restoredExpectation: restoredExpectation,
            restoredShouldBeNil: restoredShouldBeNil,
            couldNotRestoreExpectation: couldNotRestoreExpectation,
            clearedContextExpectation: clearedContextExpectation
        )
    }

    func mockBeaconsmind(contextManager: ContextManager) -> Beaconsmind {
        Beaconsmind(
            store: mockStore,
            state: mockState,
            contextManager: contextManager
        )
    }

    let mockContactsManager: BeaconContactsManager = BeaconContactsManagerImpl()

    lazy var mockTokenResponse: TokenResponse = {
        TokenResponse(
            accessToken: mockAccessToken,
            expiresIn: mockExpiresIn,
            tokenType: mockTokenType,
            userId: mockUserID
        )
    }()

    lazy var mockCreateUserResponse: CreateUserResponse = {
        CreateUserResponse(
            city: "",
            country: "First name",
            disablePushNotifications: false,
            favoriteStore: "",
            firstName: "Last name",
            fullName: "",
            gender: "",
            houseNumber: "",
            id: "",
            joinDate: Date(),
            landlinePhone: "",
            language: "",
            lastName: "",
            newsLetterSubscription: false,
            phoneNumber: "",
            street: "",
            url: "",
            userName: "",
            zipCode: "",
            avatarThumbnailUrl: "",
            avatarUrl: "",
            birthDate: Date(),
            claims: [],
            clubId: ""
        )
    }()

    let mockBeaconUUID = UUID().uuidString
    let mockBeaconMajor = 111
    let mockBeaconMinor = 222
    let mockOfferID = 1750
    let mockSentDateTime: DateTime = Date()
    let mockOfferText = "Mocked Offer Text"
    let mockOfferTitle = "Mocked Offer Title"
    let mockBeaconRSSI: Double = -46
    let mockBeaconTimestamp: Int = 10000
    let mockBeaconName = "Mock beacon name"
    let mockBeaconStore = "Mock beacon store"
    let mockIsInRange = false
    let mockLatitude = 0.8
    let mockLongitude = 0.5
    lazy var mockPosition: LatLongModel = {
        LatLongModel(latitude: mockLatitude, longitude: mockLongitude)
    }()

    func mockBeacon(uuid: String? = nil, majorMinorDiff: Int = 0) -> Beacon {
        Beacon(
            uuid: uuid ?? mockBeaconUUID,
            major: mockBeaconMajor + majorMinorDiff,
            minor: mockBeaconMinor + majorMinorDiff,
            name: mockBeaconName,
            store: mockBeaconStore
        )
    }

    func mockBeacon(uuid: UUID, majorMinorDiff: Int = 0) -> CLBeacon {
        let major = NSNumber(integerLiteral: mockBeaconMajor + majorMinorDiff)
        let minor = NSNumber(integerLiteral: mockBeaconMinor + majorMinorDiff)

        let beacon = mock(CLBeacon.self)
        if #available(iOS 13.0, *) {
            given(beacon.uuid).willReturn(uuid)
            given(beacon.timestamp).willReturn(Date())
        } else {
            given(beacon.proximityUUID).willReturn(uuid)
        }
        given(beacon.major).willReturn(major)
        given(beacon.minor).willReturn(minor)
        given(beacon.rssi).willReturn(0)
        return beacon
    }

    lazy var mockSentMessageModel: SentMessageModel = {
        SentMessageModel(
            offerId: mockOfferID,
            sentOfferId: mockOfferID,
            sent: mockSentDateTime,
            text: mockOfferText,
            title: mockOfferTitle,
            userId: mockUserID
        )
    }()

    func mockEventModel(event: String = "IN", timestamp: Int? = nil) -> EventModel {
        EventModel(
            event: event,
            major: "\(mockBeaconMajor)",
            minor: "\(mockBeaconMinor)",
            uuid: mockBeaconUUID,
            distance: 0,
            rssi: mockBeaconRSSI,
            timestamp: timestamp ?? mockBeaconTimestamp
        )
    }

    lazy var mockRegion: CLBeaconRegion? = {
        if #available(iOS 13.0, *) {
            return mockRegion(uuid: mockBeaconUUID)
        }
        return nil
    }()

    @available(iOS 13.0, *)
    func mockRegion(uuid: String) -> CLBeaconRegion {
        CLBeaconRegion(
            uuid: UUID(uuidString: uuid)!,
            major: UInt16(mockBeaconMajor),
            minor: UInt16(mockBeaconMinor),
            identifier: mockBeacon().description
        )
    }
}
