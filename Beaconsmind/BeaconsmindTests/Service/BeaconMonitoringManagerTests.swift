//
//  Copyright © 04.12.21 Beaconsmind. All rights reserved.
//

import XCTest
@testable import Beaconsmind
import CoreLocation

class BeaconMonitoringManagerTests: BaseTestCase {

    private let delegate = MockBeaconListenerDelegate()

    private let locationManager = MockBeaconLocationManager()

    private func makeSUT(
        expectation: XCTestExpectation? = nil,
        results: [SDKApiResult<MockAPIClientResponse>] = []
    ) -> BeaconMonitoringManagerImpl {
        BeaconMonitoringManagerImpl(
            userID: mockUserID,
            apiClient: MockAPIClient(expectation: expectation, results: results),
            delegate: delegate,
            store: mockStore,
            state: mockState,
            contactsManager: mockContactsManager,
            monitoringLimit: 2,
            locationManager: locationManager
        )
    }

    func test_start_shouldFetchBeacons() throws {
        let expectation = XCTestExpectation(description: "Should fetch beacons")
        let result: [StoreBeaconInfo] = [
            mockBeacon().storeInfo,
            mockBeacon(majorMinorDiff: 1).storeInfo,
            mockBeacon(majorMinorDiff: 2).storeInfo,
            mockBeacon(majorMinorDiff: 3).storeInfo
        ]
        let response = try! API.Configuration.ConfigurationGetBeacons.Response(
            statusCode: 200,
            data: JSONEncoder().encode(result),
            decoder: JSONDecoder()
        )

        let sut = makeSUT(expectation: expectation, results: [.success(response)])
        XCTAssertEqual(sut.beaconsAll.count, 0)
        sut.start()
        XCTAssertEqual(sut.beaconsAll.count, 4)
    }

    func test_onLocationManagerError() {
        let sut = makeSUT()
        XCTAssertEqual(delegate.errorEvents.count, 0)
        sut.onLocationManagerError(region: nil, error: .monitoringNotAvailable)
        XCTAssertEqual(delegate.errorEvents.count, 1)
    }

    @available(iOS 13.0, *)
    func test_onLocationManagerRanged() {
        let sut = makeSUT()
        let uuid1 = UUID()
        let clBeacon1: CLBeacon = mockBeacon(uuid: uuid1)
        let beacon1: Beacon = mockBeacon(uuid: uuid1.uuidString)

        sut.beaconsAll = [beacon1]
        sut.beaconsRanged = []

        sut.onLocationManagerRanged(regions: [clBeacon1], in: mockRegion!, position: nil)
        XCTAssertEqual(sut.beaconsRanged.count, 1)
    }

    @available(iOS 13.0, *)
    func test_onLocationManagerEntered() throws {
        let expectation = XCTestExpectation(description: "Should send report")
        let response = try API.Ping.PingPingMultipleBeacons.Response(
            statusCode: 200,
            data: JSONEncoder().encode([mockSentMessageModel]),
            decoder: JSONDecoder()
        )
        let sut = makeSUT(expectation: expectation, results: [.success(response)])

        XCTAssertEqual(sut.enteredRegions.count, 0)
        sut.onLocationManagerEntered(region: mockRegion!, position: nil)
        XCTAssertEqual(sut.beaconsRanged.count, 0)
        XCTAssertEqual(sut.enteredRegions.count, 1)
        XCTAssertEqual(mockContactsManager.summaries.count, 0)
        XCTAssertEqual(delegate.inRegionEvents.count, 1)
    }

    //
    // MARK: - Looping-specific tests
    //

    func test_looping_onAppEnteredBackground_updatesMonitoredRegions() {
        XCTAssertFalse(locationManager.updatedMonitoredRegions)
        let sut = makeSUT()
        sut.beaconsAll = [
            mockBeacon(uuid: UUID().uuidString),
            mockBeacon(uuid: UUID().uuidString),
            mockBeacon(uuid: UUID().uuidString)
        ]
        NotificationCenter.default.post(name: UIApplication.didEnterBackgroundNotification, object: nil)
        XCTAssertTrue(locationManager.updatedMonitoredRegions)
    }

    @available(iOS 13.0, *)
    func test_looping_enteringAndLeavingRegion_loopsMonitoring() {
        let sut = makeSUT()
        sut.beaconsAll = [
            mockBeacon(),
            mockBeacon(uuid: UUID().uuidString),
            mockBeacon(uuid: UUID().uuidString),
            mockBeacon(uuid: UUID().uuidString)
        ]
        XCTAssertEqual(sut.nextIndexToMonitor, 0)
        sut.onLocationManagerEntered(region: mockRegion!, position: nil)
        XCTAssertEqual(sut.nextIndexToMonitor, 1)
        sut.onLocationManagerLeft(region: mockRegion!, position: nil)
        XCTAssertEqual(sut.nextIndexToMonitor, 2)
    }

    func test_looping_loopMonitoring_circular() {
        let sut = makeSUT()
        sut.beaconsAll = [
            mockBeacon(),
            mockBeacon(uuid: UUID().uuidString),
            mockBeacon(uuid: UUID().uuidString),
            mockBeacon(uuid: UUID().uuidString),
            mockBeacon(uuid: UUID().uuidString)
        ]
        XCTAssertEqual(sut.nextIndexToMonitor, 0)
        sut.loopMonitoring()
        XCTAssertEqual(sut.nextIndexToMonitor, 2)
        sut.loopMonitoring()
        XCTAssertEqual(sut.nextIndexToMonitor, 4)
        sut.loopMonitoring()
        XCTAssertEqual(sut.nextIndexToMonitor, 1)
        sut.loopMonitoring()
        XCTAssertEqual(sut.nextIndexToMonitor, 3)
        sut.loopMonitoring()
        XCTAssertEqual(sut.nextIndexToMonitor, 5)
        sut.loopMonitoring()
        XCTAssertEqual(sut.nextIndexToMonitor, 2)
    }

    //
    // MARK: - Flow tests
    //

    func test_restart_switchingFromLoopingToNonLooping() throws {
        let sut = makeSUT()
        sut.beaconsAll = [
            mockBeacon(),
            mockBeacon(uuid: UUID().uuidString),
            mockBeacon(uuid: UUID().uuidString),
            mockBeacon(uuid: UUID().uuidString),
            mockBeacon(uuid: UUID().uuidString)
        ]
        XCTAssertTrue(sut.isLooping)

        sut.beaconsAll = [
            mockBeacon()
        ]
        sut.restart()
        XCTAssertFalse(sut.isLooping)
    }

    @available(iOS 13.0, *)
    func test_flow_nonLooping_enteringRangingLeaving() throws {
        let expectation = XCTestExpectation(description: "Should send report")
        let response = try API.Ping.PingPingMultipleBeacons.Response(
            statusCode: 200,
            data: JSONEncoder().encode([mockSentMessageModel]),
            decoder: JSONDecoder()
        )
        let sut = makeSUT(expectation: expectation, results: [.success(response)])
        let uuid1 = UUID()
        let region1 = CLBeaconRegion(uuid: uuid1, identifier: "\(uuid1.uuidString)")
        let clBeacon1: CLBeacon = mockBeacon(uuid: uuid1)
        let beacon1: Beacon = mockBeacon(uuid: uuid1.uuidString)

        sut.beaconsAll = [beacon1]
        sut.beaconsRanged = []
        XCTAssertFalse(sut.isLooping)

        sut.onLocationManagerEntered(region: region1, position: nil)
        XCTAssertEqual(mockContactsManager.summaries.count, 0)
        XCTAssertEqual(sut.enteredRegions.count, 1)
        XCTAssertEqual(sut.beaconsRanged.count, 0)

        sut.onLocationManagerRanged(regions: [clBeacon1], in: region1, position: nil)
        XCTAssertEqual(mockContactsManager.summaries.count, 1)
        XCTAssertEqual(sut.enteredRegions.count, 1)
        XCTAssertEqual(sut.beaconsRanged.count, 1)

        sut.onLocationManagerLeft(region: region1, position: nil)
        XCTAssertEqual(sut.enteredRegions.count, 0)
        XCTAssertEqual(sut.beaconsRanged.count, 0)
        XCTAssertEqual(mockContactsManager.summaries.count, 1)
        XCTAssertEqual(delegate.leftRegionEvents.count, 1)
    }

    @available(iOS 13.0, *)
    func test_flow_looping_enteringRangingLeaving() throws {
        let expectation = XCTestExpectation(description: "Should send report")
        let response = try API.Ping.PingPingMultipleBeacons.Response(
            statusCode: 200,
            data: JSONEncoder().encode([mockSentMessageModel]),
            decoder: JSONDecoder()
        )
        let sut = makeSUT(expectation: expectation, results: [.success(response)])

        let uuid1 = UUID()
        let region1 = CLBeaconRegion(uuid: uuid1, identifier: "\(uuid1.uuidString)")
        let clBeacon1: CLBeacon = mockBeacon(uuid: uuid1)
        let beacon1: Beacon = mockBeacon(uuid: uuid1.uuidString)

        sut.beaconsAll = [
            beacon1,
            mockBeacon(uuid: UUID().uuidString),
            mockBeacon(uuid: UUID().uuidString)
        ]
        sut.beaconsRanged = []
        XCTAssertTrue(sut.isLooping)

        sut.onLocationManagerEntered(region: region1, position: nil)
        XCTAssertEqual(mockContactsManager.summaries.count, 0)
        XCTAssertEqual(sut.enteredRegions.count, 1)
        XCTAssertEqual(sut.beaconsRanged.count, 0)

        sut.onLocationManagerRanged(regions: [clBeacon1], in: region1, position: nil)
        XCTAssertEqual(mockContactsManager.summaries.count, 1)
        XCTAssertEqual(sut.enteredRegions.count, 1)
        XCTAssertEqual(sut.beaconsRanged.count, 1)

        sut.onLocationManagerLeft(region: region1, position: nil)
        XCTAssertEqual(sut.enteredRegions.count, 0)
        XCTAssertEqual(sut.beaconsRanged.count, 0)
        XCTAssertEqual(mockContactsManager.summaries.count, 1)
        XCTAssertEqual(delegate.leftRegionEvents.count, 1)
    }
}
