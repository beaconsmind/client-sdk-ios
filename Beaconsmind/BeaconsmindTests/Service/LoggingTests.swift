//
//  Copyright © 2022 beaconsmind. All rights reserved.
//

import XCTest
@testable import Beaconsmind

class LoggingTests: BaseTestCase {

    func test_defaultLevelIsSilent() {
        XCTAssertEqual(Logger.consoleLogLevel, .silent)
    }

    func test_shouldLog_doesNotLogWithSilentLogLevel() {
        XCTAssertFalse(Logger.shouldLogToConsole(level: .silent))
    }

    func test_shouldLog_doesNotLogHigherOrderThanLogLevel() {
        Logger.consoleLogLevel = .silent
        XCTAssertFalse(Logger.shouldLogToConsole(level: .error))
        XCTAssertFalse(Logger.shouldLogToConsole(level: .warning))
        XCTAssertFalse(Logger.shouldLogToConsole(level: .info))
        XCTAssertFalse(Logger.shouldLogToConsole(level: .debug))

        Logger.consoleLogLevel = .error
        XCTAssertFalse(Logger.shouldLogToConsole(level: .warning))
        XCTAssertFalse(Logger.shouldLogToConsole(level: .info))
        XCTAssertFalse(Logger.shouldLogToConsole(level: .debug))

        Logger.consoleLogLevel = .warning
        XCTAssertFalse(Logger.shouldLogToConsole(level: .info))
        XCTAssertFalse(Logger.shouldLogToConsole(level: .debug))

        Logger.consoleLogLevel = .info
        XCTAssertFalse(Logger.shouldLogToConsole(level: .debug))
    }

    func test_shouldLog_logsLowerOrEqualOrderThanLogLevel() {
        Logger.consoleLogLevel = .debug
        XCTAssertTrue(Logger.shouldLogToConsole(level: .debug))
        XCTAssertTrue(Logger.shouldLogToConsole(level: .info))
        XCTAssertTrue(Logger.shouldLogToConsole(level: .warning))
        XCTAssertTrue(Logger.shouldLogToConsole(level: .error))

        Logger.consoleLogLevel = .info
        XCTAssertTrue(Logger.shouldLogToConsole(level: .info))
        XCTAssertTrue(Logger.shouldLogToConsole(level: .warning))
        XCTAssertTrue(Logger.shouldLogToConsole(level: .error))

        Logger.consoleLogLevel = .warning
        XCTAssertTrue(Logger.shouldLogToConsole(level: .warning))
        XCTAssertTrue(Logger.shouldLogToConsole(level: .error))

        Logger.consoleLogLevel = .error
        XCTAssertTrue(Logger.shouldLogToConsole(level: .error))
    }
}
