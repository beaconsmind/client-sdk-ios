//
//  Copyright © 04.12.21 Beaconsmind. All rights reserved.
//

import XCTest
@testable import Beaconsmind

class ContextManagerTests: BaseTestCase {

    private let store = MockStore()
    private let wrongContext = APIContext(hostname: "", accessToken: "", userID: "test.user.id", tokenType: "", expiresIn: "")
    private let validContext = APIContext(hostname: "test.host", accessToken: "test.token", userID: "test.user.id", tokenType: "", expiresIn: "")

    func testSaveContext_success() throws {
        let expectation = XCTestExpectation(description: "Should save context")
        let sut = makeSUT(savedExpectation: expectation, savedShouldBeNil: false)
        try sut.saveContext(context: validContext)
        wait(for: [expectation], timeout: 0.1)
    }

    func testSaveContext_nonOAuthContext_throwsInvalidContext() throws {
        let sut = makeSUT()
        do {
            try sut.saveContext(context: wrongContext)
            XCTFail("Should not save wrong context")
        } catch {
            guard case SDKApiError.invalidContext = error else {
                XCTFail("Should return an invalid context error")
                return
            }
        }
    }

    func testRestoreContext_success() throws {
        let expectation = XCTestExpectation(description: "Should restore context")
        let sut = makeSUT(
            savedShouldBeNil: false,
            restoredExpectation: expectation,
            restoredShouldBeNil: false
        )
        try sut.saveContext(context: validContext)
        try sut.restoreContext()
        wait(for: [expectation], timeout: 0.1)
    }

    func testRestoreContext_empty_callsCouldNotRestoreDelegate() throws {
        let expectation = XCTestExpectation(description: "Should not restore empty context")
        let sut = makeSUT(couldNotRestoreExpectation: expectation)
        try sut.restoreContext()
        wait(for: [expectation], timeout: 0.1)
    }

    func testRestoreContext_nonDecodable_throwsInvalidContext() throws {
        let expectation = XCTestExpectation(description: "Should not restore wrong context")
        let sut = makeSUT(couldNotRestoreExpectation: expectation)
        store.contextData = "[WRONG DATA]".data(using: .utf8)
        try sut.restoreContext()
        wait(for: [expectation], timeout: 0.1)
    }

    func testRestoreContext_clearContext_success() throws {
        let expectation = XCTestExpectation(description: "Should clear context")
        let sut = makeSUT(clearedContextExpectation: expectation)
        try sut.clearContext()
        wait(for: [expectation], timeout: 0.1)
    }

    private func makeSUT(
        savedExpectation: XCTestExpectation? = nil, savedShouldBeNil: Bool = true,
        restoredExpectation: XCTestExpectation? = nil, restoredShouldBeNil: Bool = true,
        couldNotRestoreExpectation: XCTestExpectation? = nil,
        clearedContextExpectation: XCTestExpectation? = nil
    ) -> ContextManager {
        updateMockContextManagerDelegate(
            savedExpectation: savedExpectation,
            savedShouldBeNil: savedShouldBeNil,
            restoredExpectation: restoredExpectation,
            restoredShouldBeNil: restoredShouldBeNil,
            couldNotRestoreExpectation: couldNotRestoreExpectation,
            clearedContextExpectation: clearedContextExpectation
        )
        return ContextManagerImpl(
            store: store,
            delegate: mockContextManagerDelegate
        )
    }
}
