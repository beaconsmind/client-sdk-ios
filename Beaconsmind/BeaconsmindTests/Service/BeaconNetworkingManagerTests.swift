//
//  Copyright © 04.12.21 Beaconsmind. All rights reserved.
//

import XCTest
@testable import Beaconsmind

class BeaconNetworkingManagerTests: BaseTestCase {

    private func makeSUT(
        expectation: XCTestExpectation? = nil,
        results: [SDKApiResult<MockAPIClientResponse>]
    ) -> BeaconNetworkingManager {
        let manager = BeaconNetworkingManagerImpl(
            apiClient: MockAPIClient(expectation: expectation, results: results),
            delegate: MockBeaconNetworkingManagerDelegate(),
            userID: mockUserID,
            store: mockStore,
            reportInterval: 0.1.seconds,
            fetchingInterval: 2.seconds
        )
        manager.startReporting()
        return manager
    }

    func test_reportEvent_success() throws {
        let expectation = XCTestExpectation(description: "Should handle success nicely")
        let response = try API.Ping.PingPingMultipleBeacons.Response(
            statusCode: 200,
            data: JSONEncoder().encode([mockSentMessageModel]),
            decoder: JSONDecoder()
        )
        let sut = makeSUT(
            expectation: expectation,
            results: [.success(response)]
        )

        XCTAssertEqual(sut.reportingState, .initial)

        sut.reportEvent(position: mockPosition, event: mockEventModel())

        wait(for: [expectation], timeout: 0.1)

        XCTAssertEqual(sut.reportingState, .done)
        XCTAssertEqual(sut.lastPositionToReport, mockPosition)
    }

    func test_reportEvent_error() throws {
        let expectation = XCTestExpectation(description: "Should handle errors nicely")
        let sut = makeSUT(
            expectation: expectation,
            results: [.failure(.apiError(400, "Bad request"))]
        )

        XCTAssertEqual(sut.reportingState, .initial)

        sut.reportEvent(position: mockPosition, event: mockEventModel())

        wait(for: [expectation], timeout: 0.1)

        XCTAssertEqual(sut.reportingState, .error)
        XCTAssertEqual(sut.lastPositionToReport, mockPosition)
    }

    func test_storeEvent_getsSentAtOnce() throws {
        let expectation = XCTestExpectation(description: "Should handle success nicely")
        let response = try API.Ping.PingPingMultipleBeacons.Response(
            statusCode: 200,
            data: JSONEncoder().encode([mockSentMessageModel]),
            decoder: JSONDecoder()
        )
        let sut = makeSUT(
            expectation: expectation,
            results: [.success(response)]
        )

        XCTAssertEqual(sut.reportingState, .initial)
        XCTAssertEqual(sut.eventsToReport.count, 0)
        XCTAssertEqual(sut.lastPositionToReport, nil)

        sut.reportEvent(position: mockPosition, event: mockEventModel(timestamp: 20000))
        XCTAssertEqual(sut.eventsToReport.count, 1)

        sut.reportEvent(position: mockPosition, event: mockEventModel(timestamp: 25000))
        XCTAssertEqual(sut.eventsToReport.count, 2)

        wait(for: [expectation], timeout: 0.1)

        XCTAssertEqual(sut.reportingState, .done)
        XCTAssertEqual(sut.eventsToReport.count, 0)
        XCTAssertEqual(sut.lastPositionToReport, mockPosition)
    }
}
