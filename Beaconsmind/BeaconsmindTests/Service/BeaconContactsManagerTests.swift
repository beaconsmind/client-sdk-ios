//
//  Copyright © Beaconsmind. All rights reserved.
//

import XCTest
@testable import Beaconsmind

class BeaconContactsManagerTests: BaseTestCase {

    private let sut = BeaconContactsManagerImpl()

    func test_updateBeaconList() {
        XCTAssertEqual(sut.summaries.count, 0)

        sut.updateBeaconList(beacons: [mockBeacon()])
        XCTAssertEqual(sut.summaries.count, 1)
        XCTAssertEqual(sut.summaries.first?.uuid, mockBeacon().uuid)

        sut.updateBeaconList(beacons: [mockBeacon()])
        XCTAssertEqual(sut.summaries.count, 1)
    }

    func test_ranged_updatesSummaryCorrectly() {
        XCTAssertEqual(sut.summaries.count, 0)

        sut.ranged(beacon: mockBeacon(), event: mockEventModel())
        XCTAssertEqual(sut.summaries.first?.uuid, mockBeacon().uuid)
        XCTAssertTrue(sut.summaries.first!.isInRange)

        sut.ranged(beacon: mockBeacon(), event: mockEventModel(timestamp: 20000))
        XCTAssertEqual(sut.summaries.first?.timestamp, 20000)

        sut.ranged(beacon: mockBeacon(), event: mockEventModel(timestamp: 50))
        XCTAssertEqual(sut.summaries.first?.timestamp, 20000)
        XCTAssertTrue(sut.summaries.first!.isInRange)
    }

    func test_left_updatesSummaryCorrectly() {
        XCTAssertEqual(sut.summaries.count, 0)

        sut.entered(beacon: mockBeacon(), event: mockEventModel())
        XCTAssertTrue(sut.summaries.first!.isInRange)

        sut.left(beacon: mockBeacon())
        XCTAssertFalse(sut.summaries.first!.isInRange)
    }

    func test_entered_updatesSummaryCorrectly() {
        XCTAssertEqual(sut.summaries.count, 0)

        sut.entered(beacon: mockBeacon(), event: mockEventModel())
        XCTAssertEqual(sut.summaries.first?.uuid, mockBeacon().uuid)
        XCTAssertTrue(sut.summaries.first!.isInRange)

        sut.entered(beacon: mockBeacon(), event: mockEventModel(timestamp: 20000))
        XCTAssertEqual(sut.summaries.first?.timestamp, 20000)

        sut.entered(beacon: mockBeacon(), event: mockEventModel(timestamp: 50))
        XCTAssertEqual(sut.summaries.first?.timestamp, 20000)
        XCTAssertTrue(sut.summaries.first!.isInRange)
    }
}
