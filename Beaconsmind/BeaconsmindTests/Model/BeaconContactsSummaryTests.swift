//
//  Copyright © Beaconsmind. All rights reserved.
//

import XCTest
@testable import Beaconsmind

class BeaconContactsSummaryTests: BaseTestCase {

    func test_model() {
        let sut = BeaconContactsSummary(beacon: mockBeacon(), events: [
            mockEventModel(timestamp: 1000),
            mockEventModel(timestamp: 10000),
            mockEventModel(timestamp: 30000),
            mockEventModel(timestamp: 60000)
        ], isInRange: mockIsInRange)
        XCTAssertEqual(sut.uuid, mockBeaconUUID)
        XCTAssertEqual(sut.timestamp, 60000)
        XCTAssertEqual(sut.contactCount, 4)
        XCTAssertEqual(sut.rssi, mockBeaconRSSI)
        XCTAssertEqual(sut.averageRSSI, mockBeaconRSSI)
        XCTAssertEqual(sut.store, mockBeaconStore)
        XCTAssertEqual(sut.name, mockBeaconName)
        XCTAssertEqual(sut.frequency, 885)
        XCTAssertEqual(sut.minor, "\(mockBeaconMinor)")
        XCTAssertEqual(sut.major, "\(mockBeaconMajor)")
        XCTAssertFalse(sut.isInRange)

        let sutEmpty = BeaconContactsSummary(beacon: mockBeacon(), events: [], isInRange: false)
        XCTAssertEqual(sutEmpty.contactCount, 0)
        XCTAssertNil(sutEmpty.rssi)
        XCTAssertNil(sutEmpty.averageRSSI)
    }
}
