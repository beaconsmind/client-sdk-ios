Pod::Spec.new do |s|
  s.name             = 'Beaconsmind'
  s.version          = '3.9.7'
  s.summary          = 'Beaconsmind SDK library for Beaconsmind Services and API calls'

  s.description      = <<-DESC
Client SDK collection for connecting to Beaconsmind infrastructure to access APIs and initialize beacons.
                       DESC

  s.homepage         = 'https://gitlab.com/beaconsmind/client-sdk-ios'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Augustin Juricic' => 'augustinjuricic@beaconsmind.com' }
  s.source           = { :git => 'https://gitlab.com/beaconsmind/client-sdk-ios', :tag => s.version.to_s }
  s.ios.deployment_target = '12.0'
  s.swift_versions = ['4.0', '4.1', '4.2', '5.0']

  s.subspec 'SDK' do |spec|
    spec.source_files = 'Beaconsmind/Beaconsmind/Services/**/*{.swift}'
    spec.dependency 'Beaconsmind/API'
    spec.dependency 'MBProgressHUD', '~> 1.2.0'
    spec.dependency 'BugfenderSDK', '~> 1.11.0'
    spec.frameworks = ['UIKit', 'CoreLocation', 'UserNotifications']
  end

  s.subspec 'API' do |spec|
    spec.source_files = 'Beaconsmind/Beaconsmind/API/**/*{.swift}'
    spec.dependency 'Alamofire', '~> 5.4.4'
    spec.dependency 'AlamofireImage', '~> 4.1.0'
  end

end
