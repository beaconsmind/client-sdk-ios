# Changelog

# 3.9.7

- Added error handling when request is returned as success but is not successfull
- Added new case in SDKApiError to return only the error message

# 3.9.6

- Title label added to the OfferDetailViewController
- Stylesheet updated with Title font

# 3.9.5

- Removed the unnecessary POC public key in the APIRequest

# 3.9.4

- Rebuild API
- Added ability to save phone number
- Added stylesheet options `textViewFont` and `textViewTextColor`

# 3.9.3

- Updated BarcodeType Model to handle code128

# 3.9.2

- Rebuild API
- Updated API models
- `pocPublicKey` handled when nil on AppConfigurationResponse model

# 3.9.1

- Update API models
- Add support for POC auth header

# 3.9.0

- Update API models
- Support for setting favorite store
- Support for forgot password flow

# 3.8.4

- Add `delete` method.

# 3.8.3

- Add `countryCode` parameter to the `signUp` method.

# 3.8.2

- Add Cartage support.

# 3.8.1

- End background tasks when they are actually finished
- begin background tasks even if app is in the active state

# 3.8.0

- Extend diagnostic metrics with information whether notification permission is granted
- Send diagnostics data immediately after login
- Use backend to configure logging and diagnostics
- Added proxy framework with Objective-C headers to enable Xamarin binding
- Removed some legacy code

## 3.7.3

- Added `markOfferAsClicked` method [#54](https://gitlab.com/beaconsmind/client-sdk-ios/-/merge_requests/54).
  
## 3.7.2

- Fixed unsubscribing from push notification on logout [#46](https://gitlab.com/beaconsmind/client-sdk-ios/-/merge_requests/46).

## 3.7.1

- Fixed device token registration [#43](https://gitlab.com/beaconsmind/client-sdk-ios/-/merge_requests/43).

## 3.7.0

- Adding a new async logout function that unsubscribes from push notifications first and only then clears the context
- Deprecating the old logout function
- Breaking change: `startListeningBeacons` does no longer request location permission automatically

## 3.6.6

- Adding auto-logout in case of any 401 or 403 response

## 3.6.5

- Adding developer mode support with `Beaconsmind.default.startDevelop`.
- Updated `OfferPayload` to match actual model sent from the backend

## 3.6.4

- Adding option to pop `OfferDetailViewController` after redeeming with `OfferDetailViewControllerDelegate`

## 3.6.3

- Adding SDK diagnostics

## 3.6.2

- Making `rssi` and `averageRSSI` in `BeaconContactsSummary` optional

## 3.6.1

- Breaking change: Renaming `BeaconContactSummary` to `BeaconContactsSummary`
- Breaking change: Renaming `beaconsContactSummary` to `beaconContactsSummaries`

## 3.6.0

- Updating `Logging`.
- Breaking change: `Beaconsmind.isBeaconLoggingVerbose` is removed, use `Beaconsmind.default.setMinLogLevel(level:)` instead

## 3.5.0

- Breaking change: `OfferModel` is now called `NotificationResponse`
- Adding support to override the button's corner radius in the UI compontents
- Exposing image upload, locations and image related endpoints
- Adding changing password and password reset flows to the demo app
- Enhancing background monitoring
- Adding start with JWT token support

## 3.4.2

- Exposing dashboard, password, product, store and user related endpoints

## 3.4.1

- Breaking change: merging `ISdkContext` and internal oauth context to publicly exposed `APIContext`. `getOAuthContext()` is now called `getAPIContext()` and `userId` is `userID`
- Exposing `/externalLogin` endpoint

## 3.4.0

- Redesigning logic for monitoring and ranging beacons to respect Apple's limitations:
- The fetched beacon list gets monitored in 20 sized batches.
- Ranging gets started for entered regions only in case the app is in foreground.

## 3.3.1

- Adding `beaconsContactSummary` to expose last beacon contacts stored in memory
- Updating demo app to use `beaconsContactSummary` instead of fetching beacons twice
- Refetching beacons every 30 minutes in the SDK
- Sending beacon contact events with `PingPingMultipleBeacons`

## 3.3.0

- Removed previously deprecated old SDK API
- Renaming `BeaconPingPushPayload` to `OfferPayload` and simplifying affected API-s
- Also renamed `OfferId` to `OfferID` in `OfferPayload`
- Redesigning internal beacon ranging and reporting

## 3.2.1

- Adding device token registration overload with a hex `String` token
- Adding basic theming to the demo app's UI

## 3.2.0

- Adding offer list UI screen
- Adding offer details UI screen
- Adding `BeaconsmindUI` module for theming
- Updating demo app to use SPM instead of CocoaPods

## 3.1.1

- Added profile update support

## 3.1.0

- Cleaned up background monitoring/ranging

## 3.0.0

- Redesigned SDK API and updated documentation
- Added account import support
- Added saving touchpoints support
- Added create user support
- Added get profile support

## 2.0.0

- Updated `Alamofire` to version `5.4.4`.

## 1.0.1

- Renamed offer utility methods to include `Offer` in the name
	- `markAsRead` -> `markOfferAsRead`
	- `markAsReceived` -> `markOfferAsReceived`

- Added `markOfferAsRedeemed` helper method to Redeem offer
- Added `userId: String` property on `ISdkContext`
- Added `destroySession(key: String)` on `BeaconsmindAPI` to remove the saved session for restore token

## 1.0

- Initial release
