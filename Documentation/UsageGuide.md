# Usage guide

## Prerequisites

### Environment

As a Beaconsmind client, a contained environment is provided for Beaconsmind services being hosted and maintained by Beaconsmind.

The hostname of the provided environment is required to properly access Beaconsmind API and to use Beaconsmind SDK

> example: `https://adidas.bms.beaconsmind.com`

Please contact Beaconsmind support for the required information about acquiring a valid environment and hostname.

Beaconsmind SDK objects and methods are available in the `Beaconsmind` module after installing with Cocoapods or Carthage.

### App setup, required permissions

The SDK need the client app to have the following permissions enabled:
* Background: location updates, fetch and processing
* Push notifications needs to be setup correctly

<img src="Images/usage-permissions.png" width=800>

_Note_: The SDK has a `registerForPushNotifications` helper call to ask for push permissions and requesting always access for location tracking is done internally and automatically when calling startListeningBeacons.

Additionally, in your Info.plist file please add the following key to support background processing:
```xml
<key>BGTaskSchedulerPermittedIdentifiers</key>
<array>
    <string>$(PRODUCT_BUNDLE_IDENTIFIER)</string>
</array>
```

## 1. Initializing the SDK

The `APIContext` object is the centerpoint which unlocks services provided by Beaconsmind SDK.

`APIContext` acts as a container for a valid session. A valid session represents all the information needed to access Beaconsmind services e.g: environment endpoint and customer access token. To obtain a valid session:

- if using Beaconsmind to authenticate customers, log in with the correct customer credentials in the provided environment (see [link](#when-using-beaconsmind-authentication))

- if using own authentication mechanism, import customer information to Beaconsmind every time the customer is authenticated (see [link](#when-using-own-authentication-mechanism))

Storing and restoring the context is done by the SDK. When you call the SDK's entry point, the `start()` function, the SDK will automatically try to restore your context. The passed in `BeaconsmindDelegate` delegate's `onContextChanged` callback will be then notified about the result.

Similarly as you log in, import an account, log out or do anything that updates the context, the same delegate will be notified about any context change, so your app can react to it accordingly.

Once you have a valid session, please enable beacon ranging by calling `Beaconsmind.default.startListeningBeacons(delegate:)`. Since it won't automatically request location permission, make sure to wrap it in `Beaconsmind.default.requestLocationPermission`.

### When using Beaconsmind authentication

This approach implies that the customer data is kept in Beaconsmind backend. Use `Beaconsmind.default.login` method to perform customer authentication.

```swift
import Beaconsmind

try? Beaconsmind.default.login(
    username: "customer.email@gmail.com", 
    password: "strong-password-123456"
) { [weak self] result in
    switch result {
    case .success:
        Beaconsmind.default.requestLocationPermission { enabled in
            if enabled {
                DispatchQueue.main.async {
                    try? Beaconsmind.default.startListeningBeacons(delegate: self)
                }
            } else {
                print("TODO: No location permission given, update UI")
            }
        }

    case let .failure(error):
        // handle failure
    }
}
```

### When using own authentication mechanism

This approach implies that the customer data is kept in clients backend. The authentication is done by the client app before the SDK is initialized. Whenever customer is authenticated, use `Beaconsmind.default.importAccount` method to send basic customer data to Beaconsmind. The customer data is updated only the first time the method is called. The client app can track if the customer was imported before and omit personal data from following `importAccount` calls.

```swift
import Beaconsmind

try? Beaconsmind.default.importAccount(
    id: "unique-customer-identifier", // must not change for customer
    email: "customer.email@gmail.com",
    firstName: "optional-first-name",
    lastName: "optional-last-name",
    birthDate: nil,
    language: "en",
    gender: "male",
    deviceRegistration: nil
) { [weak self] result in
    guard let self = self else {
        return
    }
    switch result {
    case .success:
        Beaconsmind.default.requestLocationPermission { enabled in
            if enabled {
                DispatchQueue.main.async {
                    try? Beaconsmind.default.startListeningBeacons(delegate: self)
                }
            } else {
                print("TODO: No location permission given, update UI")
            }
        }

    case .failure:
        // handle failure
    }
}
```

## 2. Saving and restoring the Session

Whenever the customer is logged in or imported, the valid session is returned. Usually this happens when customer previously logged in and after some time closed the app. When restoring the app state, session will also have to be restored. As mentioned, this is done by the SDK.

When you call the SDK's entry point, the `start()` function, the SDK will automatically try to restore your context. The passed in `BeaconsmindDelegate` delegate's `onContextChanged` callback will be then notified about the result.

Similarly as you log in, import an account, log out or do anything that updates the context, the same delegate will be notified about any context change, so your app can react to it accordingly i.e. showing the login screen when context is destroyed due to token expiration.

*Note*: in case of an internal 401 or 403 response, the SDK will automatically clear the context and you will be notified about it through `onContextChanged`.

## 3. Beacon ranging

Basic functionality provided by the SDK is beacon ranging and proximity reporting. Based on these signals, the backend can react accordingly and sent marketing push notifications to the customer. Beacon ranging is enabled by calling `Beaconsmind.default.startListeningBeacons(delegate:)` after a valid session was obtained by calling `login` or `importAccount`.

Please follow our [Using beacons in general](../Documentation/Beacons.md) guide to learn more about using beacons.

## 4. Push notifications

Push notifications are being sent to Devices using Beaconsmind SDK to allow customers to receive Offers as results of physical proximity to Beacons. To learn more about how we handle device tokens, please refer to the [Token registration flow](./Flows/token-registration.md).

### Push notification prerequisites

This guide assumes basic understanding of APNS, specifically:

- Sandbox and Production APNS environments
- Push notification entitlements
- Generating APNS certificates
- User permissions to receive local and/or remote notifications

In order to familiarise with the specifics, please consult Apple [Documentation](https://developer.apple.com/documentation/usernotifications) or this excellent [Ray Wenderlich article](https://www.raywenderlich.com/11395893-push-notifications-tutorial-getting-started).

For Beaconsmind backend to properly send push notifications to Devices implementing the SDK, please provide the Beaconsmind developers the appropriate .p12 certificate and it's passkey for your Application ID.

Push notification entitlement is required for the Device to properly register for Push notifications via APNS. 

> Configuration of Application's Bundle ID on the Apple Developer Portal may also be required.

To enable Push notifications for your App in Xcode please do the following:
- Select the iOS Application's Project file in the Navigator
- Select `Signing & Capabilities`
- Select the `+ Capability`
- Add the `Push Notifications` capability to the Application

> **Due to Privacy concerns, requesting Remote (Push) notifications for the App on the User's device will require User consent.**

### Enabling push notification using Beaconsmind

To enable push notifications, Beaconsmind provides a helper method in the SDK `Beaconsmind.default.registerForPushNotifications()`.

Calling the method will automatically request for permission and enable `[.alert, .sound, .badge]` notifications. If App User accepts push, the SDK will enable push notifications.

As a result App delegate method `func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data)` will be invoked that contains the Device's APNS Token.

Please forward the token to the `Beaconsmind` SDK using `Beaconsmind.device.register(deviceToken:)`.

### Receiving push notifications

In the App Delegate method `application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any])` please forward the `userInfo` to the `Beaconsmind.default.handleReceivedPushNotification(` method. This will handle the scenario while the app is running and a beacon is ranged/push notification received.

Calling `handleReceivedPushNotification` will return the parsed Payload with the Offer ID triggered by the proximity to the Beacon and will report the Offer ID as `received` by using the Beaconsmind API.

### Receiving push notifications when app is in background

If the app is in the background and is opened by reacting to the Push Notification, the Push Message is received in the app at a different location - on the `application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?)` App Delegate call. This is the method invoked when the Application launches.

In the `launchOptions` Map, a value having a key `UIApplication.LaunchOptionsKey.remoteNotification` will be available with a valid APNS payload Application was started with. You need to store this notification until the `BeaconsmindDelegate` delegate restored the session and then you can handle it with `handleReceivedPushNotification()`.

### Basic example

```swift
import Beaconsmind

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    /// Other code..

    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        do {
            let apnsData = try Beaconsmind.default.handleReceivedPushNotification(userInfo: userInfo)
        } catch {}
    }

    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        do {
            try Beaconsmind.default.register(deviceToken: deviceToken)
        } catch {}
    }
}

extension AppDelegate: BeaconsmindDelegate {

    func beaconsmind(_ beaconsmind: Beaconsmind, onContextChanged context: APIContext?) {
        /// Other code..

        if context != nil {
            /// Making sure to only register for pushes when there's a context
            Beaconsmind.default.registerForPushNotifications()
        }
    }
}
```

## 5. Reporting interaction with received offers

When users receive pushes about a new offer, then reads the offer and optionally redeems the voucher in the offer, backend should be notified about such activity. To get offers, we use the `Beaconsmind.default.apiRequest` method, which is a direct interface toward the API offered by Beaconsmind.

## Get existing offers

Using the `Beaconsmind.default.apiRequest` method, SDK integrator is able to initiate Requests to Beaconsmind API. Since OpenAPI is used, API endpoints have been generated from the Schema and packaged in the `BeaconsmindAPI` for easy access

Requirements for interaction with API methods:

- `Request` - a namespaced object for every `API` request in the API schema. `API` object is available upon importing the `Beaconsmind` module

To fetch offers, `API.Offers.OffersGetOffers.Request` is used as a `Request` object. In the example:

```swift
import Beaconsmind

func loadOffers() {
    self.startLoading()

    do {
        let request = API.Offers.OffersGetOffers.Request()
        _ = try Beaconsmind.default.apiRequest(request) { [weak self] result in
            switch result {
            case let .success(offers):
                self?.offers = offers.success ?? []
                self?.tableView?.reloadData()
            case let .failure(error):
                self?.showMessage(error.localizedDescription)
            }
            self?.stopLoading()
        }
    } catch {
        stopLoading()
        print(error.localizedDescription)
    }
}
```

### Reporting interaction with offer to backend

`Beaconsmind` contains offer utilites to mark an offer as read an offer as received by an Offer ID. Offer ID can be extracted from push notification data, or from response when fetching list of offers.

Methods:

- `Beaconsmind.default.handleReceivedPushNotification(userInfo:)` should be called when push notification is received. The method will extract offer ID and call `markOfferAsReceived` in the background. If using this, explicit call to `markOfferAsReceived` is not needed. The method returns `BeaconPingPushPayload` which will contain parsed offer information.

- if not using `Beaconsmind.default.handleReceivedPushNotification` to handle incoming notifications, use `Beaconsmind.default.markOfferAsReceived(offerID: 123)` explicitly

- `Beaconsmind.default.markOfferAsRead(offerID: 123)` should be called after few seconds when offer details are opened

- `Beaconsmind.default.markOfferAsRedeemed(offerID: 123)` should be called when user explicitly Redeems and offer using Redeemed button (should be used only for vouchers)
