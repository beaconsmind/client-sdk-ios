# Installation guide

## Requirements

- An Apple computer running macOS to run Xcode and an active Apple Developer Account subscription ($99/yr).

- Xcode version 12 or later is required to build the project. Latest version is available for download on [AppStore](https://developer.apple.com/xcode/) 

Make sure you have the latest version of the Xcode command line tools installed:

```sh
xcode-select --install
```

- Ruby is required for dependency management. Should work on system ruby, but ruby 2.3.1 is recommended. To manage ruby version use one of the ruby version managers ([rvm](https://rvm.io/rvm/install), [rbenv](https://github.com/rbenv/rbenv#installation))

## Installation

Beaconsmind SDK source code is available through [CocoaPods](https://cocoapods.org) and [Carthage](https://github.com/Carthage/Carthage).

> The minimal version of iOS required for the SDK is `iOS 10`

### For installing via Cocoapods

Add the following code to your `Podfile` and run `pod install`.

```ruby
pod 'Beaconsmind', '~> 3.9.7'
```

### For installing via Carthage

Add the following line of code to your `Cartfile`.

```ruby
git "https://gitlab.com/beaconsmind/client-sdk-ios/"
```

And follow the standard setup guide for Carthage found [here](https://github.com/carthage/carthage#adding-frameworks-to-an-application)

## Example

To run the example project, clone the repo, and run `pod install` from the root directory. Open up the `BeaconsmindSdkDemo.xcworkspace` and run the app on your device or simulator
