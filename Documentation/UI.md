# Beaconsmind UI

## Theming

The SDK is using many default values that are exposed through the `BeaconsmindUI.default` singleton to the client-side. Should you need to override some defaults, simply update the style sheet or the localized strings used by the SDK:

```swift
BeaconsmindUI.default.styleSheet = StyleSheet(
    backgroundColor: randomColor,
    redeemButtonTitleColor: randomColor,
    redeemButtonBackgroundColor: randomColor,
    primaryButtonTitleColor: randomColor,
    primaryButtonBackgroundColor: randomColor,
    buttonHeight: CGFloat.random(in: 30...80),
    buttonCornerRadius: CGFloat.random(in: 0...12),
    spacingMultiplier: CGFloat.random(in: 0.5...2),
    errorMessageColor: randomColor,
    loadingViewContentColor: randomColor,
    loadingViewBackgroundColor: randomColor,
    cellTextColor: randomColor,
    cellShadowColor: randomColor.withAlphaComponent(0.25),
    cellShadowRadius: CGFloat.random(in: 3...10),
    cellCornerRadius: CGFloat.random(in: 2...12)
)

BeaconsmindUI.default.localizedStrings = LocalizedStrings(
    alertOKTitle: "Done",
    alertCancelTitle: "Back",
    retryButtonTitle: "Try again",
    redeemButtonTitle: "Get coupon",
    redeemQuestionMessage: "Do you want to get the coupon?",
    redeemSuccessMessage: "Success, enjoy!",
    redeemingOfferErrorMessage: "Something went wrong while getting your coupon, try again",
    readingOfferErrorMessage: "Something went wrong while reading your coupon, try again",
    refreshingOfferErrorMessage: "Something went wrong while refreshing your coupon, try again",
    offersListTitle: "Coupons",
    fetchingOffersErrorMessage: "Something went wrong while getting your coupons, try again",
    redeemed: "You already have this",
    notRedeemed: "You didn't get this, yet",
    offerCellValidityPrefix: "You can get it until: ",
    readMoreTitle: "Go to details"
)
```

## Screens

### Offer list

The SDK exposes a view controller to display the list of offers. Default colors and localized strings can be overrided via `BeaconsmindUI.default.styleSheet` and `BeaconsmindUI.default.localizedStrings`. Creating a view controller is very simple:

```swift
let controller = OffersViewController()
navigationController?.pushViewController(controller, animated: true)
```

If you're using `SwiftUI`, you can simply wrap our `UIKit` view controller in a `UIViewControllerRepresentable` and use it as a `SwiftUI` view.

```swift
import SwiftUI
import Beaconsmind

struct OffersView: UIViewControllerRepresentable {

    func makeUIViewController(context: Context) -> OffersViewController {
        OffersViewController()
    }

    func updateUIViewController(_ uiViewController: OffersViewController, context: Context) {}
}
```

### Offer details

We also expose a reusable and easily themable view controller to display and redeem offers. The basic usage is `OfferDetailViewController(offer: offer)` where offer is an `OfferResponse` object or `OfferDetailViewController(id: 1750)` where id is a valid offer id. In the second case the view controller will take care of fetching the offer and displaying potential loading and error states.

If you're using `SwiftUI`, you can simply wrap our `UIKit` view controller in a `UIViewControllerRepresentable` and use it as a `SwiftUI` view.

```swift
import SwiftUI
import Beaconsmind

struct OfferDetailView: UIViewControllerRepresentable {

    let offer: OfferResponse

    func makeUIViewController(context: Context) -> OfferDetailViewController {
        OfferDetailViewController(id: 1750)
    }

    func updateUIViewController(_ uiViewController: OfferDetailViewController, context: Context) {}
}
```
