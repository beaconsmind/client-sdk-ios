# Push notification token registration

The SDK supports APNS or custom push notification tokens as well. Since our `register(deviceToken:)` api call is authenticated, a valid `APIContext` needs to be present in the SDK for it to succeed.

## APNS token registration flow

The SDK stores and restores your APNS token under the hood silently. The only thing you need to do on the client-side is to call `register(deviceToken:)` in your `AppDelegate`'s `application:didRegisterForRemoteNotificationsWithDeviceToken`.

After that we will make sure to send it to our servers when there has been a new successful login or account import.

```mermaid
sequenceDiagram

    participant A as App<br>[AppDelegate]
    participant SDK as SDK<br>[Beaconsmind]
    participant S as Store<br>[Beaconsmind]
    participant N as NotificationRepository<br>[Beaconsmind]

    note over A: Obtaining token
    A-->A: registerForPushNotifications
    A-->A: didRegisterForRemoteNotificationsWithDeviceToken
    A->>SDK: register(deviceToken:)
    SDK->>S: store(deviceToken:)
    alt Has context already
      SDK->>N: register(deviceToken:)
    end
    note over A, SDK: Waiting for login, signup or account import
    A->>SDK: login, signup or account import
    alt Has stored device token
      SDK->>N: register(deviceToken:)
    end
```

## Custom push notification token

In case you want to use a custom push notification token, you can use the `importAccount` method and pass your custom `deviceRegistration` data.
