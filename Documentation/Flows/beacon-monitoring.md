# Beacon monitoring and ranging

## High level overview

When it comes to monitoring beacons, Apple has a maximum 20 limitation. To work around that limitation the SDK monitors only regions at first, then starts ranging beacons in the given regions as the user enters them (also stops as the user leaves). Additionally, it does monitoring in 20 batches with a timer.

Whenever a list of beacons are fetched, location manager will restart monitoring the grouped region UUIDs. When an entry event arrives, location manager automatically starts ranging all beacons in the entered region. When an exit event arrives, ait stops ranging all beacons in the region and triggers an OUT event for all of them. As a range event arrives, it triggers an IN event only for the given beacon.

```mermaid
sequenceDiagram

  participant BE as Backend
  participant N as Networking Manager
  participant B as Beacon Manager
  participant L as Location Manager
  actor A as User - App state

  B->>L: Authorize
  L->>B: Authorized
  B->>N: Start "fetching beacons" and "reporting contacts" timers

  par Influencing events
    loop Refetching every n minutes
      N-->B: Fetched beacons
      B->>L: Update monitoring<br><<UPDATE DIAGRAM>>
    end

    loop Reporting every n seconds
      N->>BE: Report stored events
    end

    loop Location manager events
      B->>L: Update monitoring<br><<UPDATE DIAGRAM>>
    end

    alt App state change
      A-->A: App state changes
      B->>L: Update monitoring<br><<UPDATE DIAGRAM>>
    end
  end
```

## Updating monitoring

On the previous diagram, this was mentioned as `UPDATE DIAGRAM`. Whenever the app state changes, the beacon list gets updated or as beacon contact events arrive, beacon manager asks location manager to update its monitored regions.

```mermaid
sequenceDiagram

  participant N as Networking Manager
  participant B as Beacon Manager
  participant L as Location Manager

  loop Monitoring next batch
    B->>L: Restart monitoring with the entered regions plus the next batch of new regions

    alt ENTER
      L->>B: Entered region
      L-->L: Starts ranging all beacons in region
      B-->B: Adds region to entered regions
      B->>L: <<UPDATE DIAGRAM>>

    else EXIT
      L->>B: Left region
      L-->L: Stops ranging all beacons in region
      B-->B: Removes region from entered regions
      B->>N: Reports OUT events for all
      B->>L: <<UPDATE DIAGRAM>>

    else RANGE
      L->>B: Ranged beacon
      B->>N: Reports IN event for beacon
    end
  end
```
