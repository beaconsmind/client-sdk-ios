# API documentation

## SDK setup

### Starting the SDK

SDK's entry point. It will automatically try to restore the sdk context and will register to app lifecycle events to do tracking silently. The passed delegate will be notified whenever the SDK's state changes.
- parameter delegate: `BeaconsmindDelegate` instance to be notified about state changes.
- parameter appVersion: `String` client-side app's version string
- parameter hostname: `String` hostname of your Beaconsmind service hostname.
- parameter token: `String` optional JWT token to initialize the SDK with
```swift
func start(
    delegate: BeaconsmindDelegate,
    appVersion: String,
    hostname: String,
    token: String? = nil
) throws
```

The passed `BeaconsmindDelegate` delegate will receive all context changes from the SDK and can update the client-side depending on its business logic. See the demo app for an example.
```swift
func beaconsmind(_ beaconsmind: Beaconsmind, onContextChanged context: APIContext?)
```

### Starting the SDK in developer mode

**For development purposes only.**
Alternative start method to kickstart sdk testing and integration. Invokes start and sets the sdk in development mode.
SDK's entry point. It will automatically try to restore the sdk context and will register to app lifecycle events to do tracking silently. The passed delegate will be notified whenever the SDK's state changes.
- parameter delegate: `BeaconsmindDelegate` instance to be notified about state changes.
- parameter appVersion: `String` client-side app's version string
- parameter hostname: `String` hostname of your Beaconsmind service hostname.
- parameter token: `String` optional JWT token to initialize the SDK with
```swift
func startDevelop(
    delegate: BeaconsmindDelegate,
    appVersion: String,
    hostname: String,
    token: String? = nil
) throws
```

### SDK current version

You can retrieve the current SDK version with
```swift
Beaconsmind.sdkVersion
```

## Authentication

### Signing up

This method is used to create a customer account on your running instance. Will return `APIContext` object that is used to execute authenticated calls to the Beaconsmind services.
In case customer with same username exists, and was authenticated via Facebook, this will link the account and allow customer to log in with password. If the customer is successfully created, and device information is available, messages triggered by user registration will be sent.
- parameter username: `String` username.
- parameter firstName: `String` user's first name.
- parameter lastName: `String` user's last name.
- parameter password: `String` password.
- parameter confirmPassword: `String` password confirmation.
- parameter avatarThumbnailURL: `String?` customer's avatar thumbnail url.
- parameter avatarURL: `String?` customer's avatar url.
- parameter language: `String` user's language code.
- parameter gender: `String` user's gender.
- parameter favoriteStoreID: `Int` user's favorite store id.
- parameter birthDate: `DateDay` user's birth date.
- parameter callback: `SDKApiResult<APIContext>` callback which returns you the Context if entered credentials are correct
- throws `SDKApiError.notStarted` if `start` has not been called, yet

```swift
func signup(
    username: String,
    firstName: String,
    lastName: String,
    password: String,
    confirmPassword: String,
    avatarThumbnailURL: String? = nil,
    avatarURL: String? = nil,
    language: String? = nil,
    gender: String? = nil,
    favoriteStoreID: Int? = nil,
    birthDate: DateDay? = nil,
    callback: @escaping (SDKApiResult<APIContext>) -> Void
) throws
```

### Logging in

This method is used to log in with your credentials on your running instance. Will return `APIContext` object that is used to execute authenticated calls to the Beaconsmind services.
- parameter username: `String` username.
- parameter password: `String` password.
- parameter callback: `SDKApiResult<APIContext>` callback which returns you the Context if entered credentials are correct
- throws `SDKApiError.notStarted` if `start` has not been called, yet

```swift
func login(
    username: String,
    password: String,
    callback: @escaping (SDKApiResult<APIContext>) -> Void
) throws
```

### Importing an account

When the customer is using 3rd party authentication and not relying on Beaconsmind to keep the customer's private data, this endpoint is used to import the customer into Beaconsmind and to obtain a token with which the app can send tracking data to us. The token does NOT provide access to any personal customer info.

If personal data is present in the request, it will only be updated the first time the endpoint is invoked for that customer.

Once the authentication is done by the app, the username is sent to Beaconsmind using /api/accounts/import to receive a JWT for access to Beaconsmind backend. In this case, Beaconsmind will not allow any personal data to be accessed and it’s the apps responsibility to only send personal data the first time /api/accounts/import is used.

When using import, editing profile is disabled.

Will return `APIContext` object that is used to execute authenticated calls to the Beaconsmind services.
- parameter id: `String` user id.
- parameter email: `String` user's email address.
- parameter firstName: `String` user's first name (Optional).
- parameter lastName: `String` user's last name (Optional).
- parameter birthDate: `DateTime` user's birth date (Optional).
- parameter language: `String` user's language code (Optional).
- parameter gender: `String` user's gender (Optional).
- parameter callback: `SDKApiResult<APIContext>` callback which returns you the Context if the entered credentials are correct.
- throws `SDKApiError.notStarted` if `start` has not been called, yet

```swift
func importAccount(
    id: String,
    email: String,
    firstName: String? = nil,
    lastName: String? = nil,
    birthDate: DateTime? = nil,
    language: String? = nil,
    gender: String? = nil,
    callback: @escaping (SDKApiResult<APIContext>) -> Void
) throws
```

#### Importing with a custom push notification token

In case you want to use a custom push notification token, you can pass your custom `deviceRegistration` data here.

Will return `APIContext` object that is used to execute authenticated calls to the Beaconsmind services.
- parameter id: `String` user id.
- parameter email: `String` user's email address.
- parameter firstName: `String` user's first name (Optional).
- parameter lastName: `String` user's last name (Optional).
- parameter birthDate: `DateTime` user's birth date (Optional).
- parameter language: `String` user's language code (Optional).
- parameter gender: `String` user's gender (Optional).
- parameter deviceRegistration: `DeviceRegistrationModel` device registration with id and platform.
- parameter callback: `SDKApiResult<APIContext>` callback which returns you the Context if the entered credentials are correct.
- throws `SDKApiError.notStarted` if `start` has not been called, yet

```swift
func importAccount(
    id: String,
    email: String,
    firstName: String? = nil,
    lastName: String? = nil,
    birthDate: DateTime? = nil,
    language: String? = nil,
    gender: String? = nil,
    deviceRegistration: DeviceRegistrationModel? = nil,
    callback: @escaping (SDKApiResult<APIContext>) -> Void
) throws
```

## Context helpers

### Logging out

Logging out by unsubscribing from push notifications, clearing the current context and updating the client-side through the delegate
- Parameter callback ((SDKApiResult<Bool>) -> Void)? notifies the caller about the result (optional error or was successful)

```swift
func logout(callback: ((SDKApiResult<Bool>) -> Void)? = nil) throws
```

### Logging out (Deprecated)

Logging out by clearing current context and updating the client-side through the delegate.

```swift
func logout() throws
```

### Requesting context

- Returns: Currently stored SDK context

```swift
func getOAuthContext() -> APIContext?
```

### Updating hostname

Updates hostname of your Beaconsmind service hostname.
- parameter hostname: `String` hostname of your Beaconsmind service hostname.
```swift
func updateHostname(hostname: String)
```

## Beacons

### Start listening beacons near your device

*NOTES:*
- Invoke this method on main thread to ensure proper `CLLocationManager` initialization.
- Any existing delegates will be overridden by the new one.
- Does not automatically ask for location permission and might throw an error in case the user hasn't yet authorized the app to receive location updates

To properly function in the background, applications need to set `NSLocationAlwaysAndWhenInUseUsageDescription` String value in the `Info.plist`.
- parameter delegate: (optional) object implementing `BeaconListenerDelegate` to read ranging information while Application is in foreground.
- throws `SDKApiError.invalidContext` if context is invalid.

```swift
func startListeningBeacons(delegate: BeaconListenerDelegate? = nil) throws
```

### Stop listening for beacons near your device

```swift
func stopListeningBeacons()
```

### Beacons contact summary

Returns a beacon contacts summary for all beacons sorted by their last contact (if any)
```swift
var beaconContactsSummaries: [BeaconContactsSummary]
```

## Dashboard

### Featured items

Returns featured items from the suite

```swift
func getFeaturedItems(callback: @escaping (SDKApiResult<[FeaturedItemModel]>) -> Void) throws -> CancellableRequest?
```

### Blog posts

Returns blog posts from the suite

```swift
func getBlogPosts(callback: @escaping (SDKApiResult<[BlogPostModel]>) -> Void) throws -> CancellableRequest?
```

## Images

### Uploading an image

Uploading a binary image string and returns a thumbnail and image path

```swift
func uploadImage(image: UIImage, callback: @escaping (SDKApiResult<ImageUploadResponse?>) -> Void) throws -> CancellableRequest?
```

## Locations

### Requesting location permissions

Requests location permission and calls the given callback with the result asynchronously.

```swift
func requestLocationPermission(callback: LocationPermissionRequestCompletionHandler?)
```

### Countries

Returns list of countries from the suite

```swift
func getCountries(callback: @escaping (SDKApiResult<[CountryView]?>) -> Void) throws -> CancellableRequest?
```

### Cities for a country

Returns list of cities for a given `countryID` from the suite

```swift
func getCitiesForCountry(countryID: Int, callback: @escaping (SDKApiResult<[CityView]?>) -> Void) throws -> CancellableRequest?
```

## Notifications (pushes and offers)

### Registering for push notifications

Uses Apple's `UserNotifications` framework to register for remote notifications. Once notifications are enabled, the application delegate's `didRegisterForRemoteNotificationsWithDeviceToken` callback will be called with the device token. See `register(deviceToken:)` to upload the token to Beaconsmind to receive offer notifications.

```swift
func registerForPushNotifications()
```

### Registering device token

Use the device push token returned by APNS in the `application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data)` Application delegate
callback to register the device for Beaconsmind services in order to receive beacon pings.
- parameter deviceToken: APS device token `Data`
- parameter platformType: Push notification service type `PlatformType`
- parameter callback : (optional) `SDKApiResult` enum callback to listen for the API result.

```swift
func register(
    deviceToken: Data,
    callback: ((SDKApiResult<API.Notifications.NotificationsSubscribe.Response>) -> Void)? = nil
) throws
```

You can also use a hex string device push token to register the device for Beaconsmind services in order to receive beacon pings.
- parameter deviceToken: APNS token hex `String`
- parameter platformType: Push notification service type `PlatformType`
- parameter callback : (optional) `SDKApiResult` enum callback to listen for the API result.
```swift
func register(
    deviceToken: String,
    callback: ((SDKApiResult<API.Notifications.NotificationsSubscribe.Response>) -> Void)? = nil
) throws
```

### Handling received push notification

Notifies the SDK about the received push notification. It will mark the offer as received.
- parameter offer: `OfferPayload` Parsed offer payload
- throws `SDKApiError.invalidContext` in case there's no current SDK context, yet
```swift
public func handleReceivedPushNotification(offer: OfferPayload) throws
```

The `OfferPayload` can be parsed from the notification content as follows:

```swift
do {
    let offer = try OfferPayload.tryParse(data: notification)
    try Beaconsmind.default.handleReceivedPushNotification(offer: offer)
} catch {
    /// When deserialization fails, it means that the notification does not come from Beaconsmind.
    print(error.localizedDescription)
}
```

The `handleReceivedPushNotification` method should be called by the app receives a push notification from Beaconsmind. You can use the following callbacks to handle received push notifications:
- [userNotificationCenter(_:willPresent:withCompletionHandler:)](https://developer.apple.com/documentation/usernotifications/unusernotificationcenterdelegate/1649518-usernotificationcenter) - Invoked when push notification is delivered while the app is in the foreground.
  ```swift
  func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping     (UNNotificationPresentationOptions) -> Void) {
      do {
          let notification = notification.request.content.userInfo
          
          let offer = try OfferPayload.tryParse(data: notification)
          try Beaconsmind.default.handleReceivedPushNotification(offer: offer)
      } catch {
          /// When deserialization fails, it means that the notification does not come from Beaconsmind.
          print(error.localizedDescription)
      }
      
      /// .list - Show the notification in Notification Center.
      /// .sound - Play the sound associated with the notification.
      if #available(iOS 14.0, *) {
          completionHandler([.list, .sound, .banner])
      } else {
          completionHandler([.sound, .alert])
      }
  }
  ```
- [userNotificationCenter(_:didReceive:withCompletionHandler:)](https://developer.apple.com/documentation/usernotifications/unusernotificationcenterdelegate/1649501-usernotificationcenter) - Invoked when user interacts with the delivered push notification, e.g. taps on it.
  ```swift
  func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
      /// Needs some delay to make sure the app is displayed before handling the notification
      DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [weak self] in
          do {
              let notification = response.notification.request.content.userInfo
              
              let offer = try OfferPayload.tryParse(data: notification)
              try Beaconsmind.default.handleReceivedPushNotification(offer: offer)
          } catch {
              /// When deserialization fails, it means that the notification does not come from Beaconsmind.
              print(error.localizedDescription)
          }
      }
      completionHandler()
  }
  ```
- [application(_:didReceiveRemoteNotification:fetchCompletionHandler:)](https://developer.apple.com/documentation/uikit/uiapplicationdelegate/1623013-application) - If remote notifications background mode is enabled, then it's invoked when push notification is delivered while the app is in the killed, suspended or background state. [Here is official guide](https://developer.apple.com/documentation/usernotifications/setting_up_a_remote_notification_server/pushing_background_updates_to_your_app) on how to enable background push notifications.
  ```swift 
  func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) async -> UIBackgroundFetchResult {
      do {
          let notification = userInfo
          
          let offer = try OfferPayload.tryParse(data: notification)
          try Beaconsmind.default.handleReceivedPushNotification(offer: offer)
      } catch {
          /// When deserialization fails, it means that the notification does not come from Beaconsmind.
          print(error.localizedDescription)
      }
      
      return .newData
  }
  ```

### Redeemable offer notifications

Returns reedemable notifications from the suite
```swift
func getReedemableNotifications(callback: @escaping (SDKApiResult<[OfferModel]>) -> Void) throws -> CancellableRequest?
```

### Non-redeemable offer notifications

Returns non-reedemable notifications from the suite

```swift
func getNonReedemableNotifications(callback: @escaping (SDKApiResult<[OfferModel]>) -> Void) throws -> CancellableRequest? 
```

## Offers

### Registering offer as read

- parameter offerID: `Int` Offer Id
- parameter callback: (optional) `SDKApiResult<Bool>` containing  `API.Offers.OffersRead` API result
- returns `CancellableRequest` cancellation handle which allows you to cancel the request.
- throws either `SDKApiError.invalidContext` or `SDKApiError.parse` depending on the issues

```swift
func markOfferAsRead(offerID: Int, callback: ((SDKApiResult<Bool>)->Void)? = nil) throws -> CancellableRequest?
```

### Registering offer as received

Automatically called if an valid Offer ID is found  when `handleReceivedPushNotification(userInfo:context)` is called upon receiving APNS payload in the app.
- parameter offerID: `Int` Offer Id
- parameter callback: (optional) `SDKApiResult<Bool>` containing  `API.Offers.OffersRead` result for
- returns `CancellableRequest` cancellation handle which allows you to cancel the request.
- throws either `SDKApiError.invalidContext` or `SDKApiError.parse` depending on the issues

```swift
func markOfferAsReceived(offerID: Int, callback: ((SDKApiResult<Bool>)->Void)? = nil) throws -> CancellableRequest?
```

### Registering offer as redeemed

- parameter offerID: `Int` Offer Id
- parameter callback: (optional) `SDKApiResult<Bool>` containing  `API.Offers.OffersRead` result for
- returns `CancellableRequest` cancellation handle which allows you to cancel the request.
- throws either `SDKApiError.invalidContext` or `SDKApiError.parse` depending on the issues

```swift
func markOfferAsRedeemed(offerID: Int, callback: ((SDKApiResult<Bool>)->Void)? = nil) throws -> CancellableRequest?
```

## Passwords

### Changing password

Parameters:
- confirmPassword: new password confirmation
- newPassword: new password
- oldPassword: old password
- callback: success/error callback
Returns: cancellable request

```swift
func changePassword(
    confirmPassword: String,
    newPassword: String,
    oldPassword: String,
    callback: @escaping (SDKApiResult<Void?>) -> Void
) throws -> CancellableRequest?
```

### Requesting password reset

Parameter callback: success/error callback
Parameter email: email to send password reset to
Returns: cancellable request

```swift
func resetPasswordRequest(email: String, callback: @escaping (SDKApiResult<Void?>) -> Void) throws -> CancellableRequest?
```

### Resetting password

Parameters:
- email: user's email
- password: new password
- code: received code
- confirmPassword: new password confirmation
- callback: success/error callback
Returns: cancellable request

```swift
func resetPassword(
    email: String,
    password: String,
    code: String?,
    confirmPassword: String?,
    callback: @escaping (SDKApiResult<Void?>) -> Void
) throws -> CancellableRequest?
```

## Products

### Product categories

Returns products categories from the suite

```swift
func getProductCategories(callback: @escaping (SDKApiResult<[CategoryModel]>) -> Void) throws -> CancellableRequest?
```

### Product list for a category

Returns products for a given category from the suite

```swift
func getProducts(categoryID: Int, callback: @escaping (SDKApiResult<[ProductModel]>) -> Void) throws -> CancellableRequest?
```

## Profile

### Getting profile
Returns customer profile info and personal data.
- parameter callback: `SDKApiResult<ProfileResponse?>` callback which returns you the profile

```swift
func getProfile(callback: @escaping (SDKApiResult<ProfileResponse?>) -> Void) throws -> CancellableRequest?
```

### Updating profile
Update customer profile info and personal data.
- parameter firstName: `String` customer's first name.
- parameter lastName: `String` customer's last name.
- parameter avatarThumbnailURL: `String?` customer's avatar thumbnail url.
- parameter avatarURL: `String?` customer's avatar url.
- parameter birthDate: `DateTime?` customer's birth date.
- parameter city: `String?` customer's city.
- parameter country: `String?` customer's country.
- parameter disablePushNotifications: `Bool?` customer's push notification state.
- parameter favoriteStore: `String?` customer's favorite store id.
- parameter gender: `String?` customer's gender.
- parameter houseNumber: `String?` customer's house number.
- parameter landlinePhone: `String?` customer's landline phone number.
- parameter language: `String?` customer's language.
- parameter phoneNumber: `String?` customer's phone number.
- parameter street: `String?` customer's street.
- parameter zipCode: `String?` customer's zip code.
- parameter callback: `SDKApiResult<ProfileResponse?>` callback which returns you the new profile

```swift
func updateProfile(
    firstName: String,
    lastName: String,
    avatarThumbnailURL: String? = nil,
    avatarURL: String? = nil,
    birthDate: DateTime?,
    city: String?,
    country: String?,
    disablePushNotifications: Bool?,
    favoriteStore: String?,
    gender: String?,
    houseNumber: String?,
    landlinePhone: String?,
    language: String?,
    phoneNumber: String?,
    street: String?,
    zipCode: String?,
    callback: @escaping (SDKApiResult<ProfileResponse?>) -> Void
) throws -> CancellableRequest?
```

### Updating profile avatar

Updates profile avatar with passed avatar image url and avatar thumbnail image url

```swift
func updateAvatar(url: String?, thumbnailURL: String?, callback: @escaping (SDKApiResult<Void?>) -> Void) throws -> CancellableRequest?
```

### Get customer card details

Returns logged in user's customer card details

```swift
func getCustomerCard(callback: @escaping (SDKApiResult<CustomerCardModel?>) -> Void) throws -> CancellableRequest?
```

## Store

### Get stores

Returns all stores from the suite

```swift
func getStores(callback: @escaping (SDKApiResult<[StoreModel]>) -> Void) throws -> CancellableRequest?
```

## Touchpoints

Save touchpoint of the mobile application. Touchpoints are application events reported to the suite, like 'Application Downloaded', 'Application Opened', etc.
When tracking AppDownload, a unique device id has to be present to allow the suite to ignore future requests. If the application developer decides to implement single AppDownload reporting on the application side, a random UUID can be sent instead. For other touchpoints, user needs to be authenticated and Bearer JWT needs to be passed with the request.
- Parameters:
- parameter type: `TouchpointType` Type of the touchpoint event
- parameter callback (optional): `SDKApiResult<Bool>` containing  `API.Touchpoints.TouchpointsSaveTouchpoint` API result
- returns `CancellableRequest` cancellation handle which allows you to cancel the request.
- throws `SDKApiError.invalidContext` in case wrong context is used
- throws `SDKApiError.invalidStore` in case store is invalid

```swift
func saveTouchpoint(
    type: TouchpointType,
    callback: ((SDKApiResult<Bool>)->Void)? = nil
) throws
```

### API Client requests

In case the SDK has not yet exposed an api, you can connect it manually with this helper by directly accessing our api client calls.

Makes and returns a cancellable api request
- parameter request: The API request to make
- parameter complete: A closure that gets the parsed `SDKApiResult` from the raw `APIResponse`
- Returns: Cancellable api request

```swift
func apiRequest<T>(
    _ request: APIRequest<T>,
    emptyResponseCodes: Set<Int> = [],
    complete: @escaping (SDKApiResult<T>) -> Void
) throws -> CancellableRequest?
```

## Logging

### Updating log level

Updates used minimum log level
- parameter level: Minimum log level to use. Default is `.silent`
```swift
func setMinLogLevel(level: LogLevel)
```
