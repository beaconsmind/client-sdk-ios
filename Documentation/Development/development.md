# Developer documentation

## Updating API models and endpoints

The following command will fetch the latest generated openapi models and endpoints, copy them to their correct place and replace some overridden local files.

```sh
./scripts/generate-api-schema.sh
```

## Opening the project

Since we support SPM and we have a Package.swift file, Xcode will try to open that when opening the folder. Instead, make sure to open `BeaconsmindSDKDemo.xcworkspace`.

## Releasing a new SDK version

## Releasing

1. Create and checkout a new release branch.
   
   ```sh
    git branch chore/release-x.y.z
    git checkout chore/release-x.y.z
   ```

2. Update the SDK version in the [Beaconsmind.podspec](../../Beaconsmind.podspec) file. That's how pods will know which tag to use.
   
   ```podspec
   Pod::Spec.new do |s|
     s.version          = 'x.y.z'
   ```

3. Update the SDK version in the [Beaconsmind.swift](../../Beaconsmind/Beaconsmind/Services/Beaconsmind.swift) file.
   
   ```swift
   public static let sdkVersion = "x.y.z"
   ```

4. Update the SDK version in the [Installing.md](../Installing.md) file. That's what client-side developers will copy when adding the library to their project. We want them to use always use the latest version.
   
   ```ruby
   pod 'Beaconsmind', '~> x.y.z'
   ```

5. Describe changes in the [Changelog.md](../Changelog.md) file.
   
   ```md
   ## x.y.z

    - Added a new cool feature.
    - Fixed some nasty bugs.
   ```

6. Stage and commit changes.
   
   ```sh
   git add --all
   git commit -m "chore: release x.y.z"
   ```

7. Submit a PR.

   ```sh
   git push --set-upstream origin chore/release-x.y.z
   ```
   
8. Gather approvals from the teammates.
   
9. Once approved, merge the PR to main.
    
10. Checkout and sync the main branch.
   
    ```sh
    git checkout main
    git pull
    ```

11. Create a new tag.

    ```sh
    git tag x.y.z
    ```

12. Push newly created tag to remote.

    ```sh
    git push --tags
    ```

13. Sign in to Cocoapods.

    ```sh
    pod trunk register your@email.com 'John Doe'  --description='your device name'
    ```

14. If needed, request permission from the project owner in order to publish an update to the pod registry. See [this](https://guides.cocoapods.org/making/getting-setup-with-trunk.html#adding-other-people-as-contributors) link for more details.

15. Deploy. Please note that it can take up to a half an hour until it gets public.

    ```sh
    pod trunk push Beaconsmind.podspec
    ```

16. Create GitLab `x.y.z` release.

## Releasing a new test version for the demo app

This step is manual. Set the correct version and build number in Xcode, select "Any iOS" as deploy target, go to product and select archive. Once the build is ready, you can push it to App Store Connect right away.

Once it finished processing, make sure to select "Manage" and select "No" when App Store Connect asks if you added any encryption changes (of course select yes if you did).

## Testing

Tests live under `BeaconsmindTests` and you can run it by hitting `Command+U` after selecting the normal `Beaconsmind` scheme. This is setup in the Package.swift's test target part.

## Development stucture

SDK files live under Beaconsmind/Beaconsmind. The API is generated so you want to focus on the Services folder during development.

### Basic flow

All public api's live in `Beaconsmind.swift` which delegates the implementation of them to repositories. The repositories reuse the generated api models and endpoints and calls passed in callbacks with the result of the requests.

### Internal services

All internal logic can be found here.

* Beacon monitoring, ranging (`BeaconLocationManager` and `BeaconMonitoringManager` plays together)
* Contacts summary
* Fetching beacons and sending reports
* Maintaining the context
* Logging
* Managing the keychain store (it's in a different folder, but logically belongs here)

### UI elements

The SDK exposes two basic UI elements (offer list and offer details) and a stylesheet and localizations to be overridden by the client-side. They can be found under `Beaconsmind/Services/UI`.