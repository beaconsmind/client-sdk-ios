# Beacons

Beacons feature uses `BLE` transmitters which can be picked up by iDevices. In `Beaconsmind`, `BLE` transmitters are used to present offers based on a physical location of a device.

Every beacon is defined by its `UUID`, major and minor numbers. A list of beacons is persisted and maintained on `Beaconsmind` environment for each client.

## Requirements

iOS devices can detect the proximity of the beacons only if the location services are enabled and if the iOS device user has approved the use of location services. In newer iOS versions, even though we request "Always enabled" authorisation, the OS will only ask for "When in use" and only at a later (random) point will ask the user whether he or she wants to allow "Always enabled". 

> **Important**: Without user approval iOS application is _not able_ to detect or range beacons while running in the background.

> In order to receive push notifications as a result of beacon ranging, users also need to approve remote notifications.

## Enable Location Services

### Step 1 - Privacy settings

To enable location services, use Xcode to add the following keys to the `Info.plist` of application integrating the SDK:

- The string entry with a key `NSLocationAlwaysAndWhenInUseUsageDescription` - which explains to the user (and Apple during the App review process) how the location is used while the app is in the background
- The string entry with a key `NSLocationWhenInUseUsageDescription` - which explains how location is used while the application is the foreground (while being used)

> **Note**: When editing the `Info.plist` via Xcode you may need to show `Raw Keys & Values` to properly add the values.

This step is required by Apple for privacy reasons. Please consult Apple [Documentation](https://developer.apple.com/documentation/corelocation/requesting_authorization_for_location_services) for further explanation.

### Step 2 - Enable Background Location Services

For the Application to range beacons in the background the Application Entitlements must include running the Location background mode. To enable Location background mode in Xcode please do the following:

- Select the iOS Application's Project file in the Navigator
- Select `Signing & Capabilities`
- Select the `+ Capability`
- Add the `Background Modes` capability
- Select the `Location updates`, `Background processing` and `Background fetch`

Background processing is required to send the Ping API call while the Beacon is ranged while the app is in the background. This step is important to allow push notifications for offers.

## SDK Usage - Start Listening for Beacons

Registering for beacons is very simple - once you're logged in, just call `Beaconsmind.default.startListeningBeacons()`. Executing this method will fetch the beacons available for your Beaconsmind environment and register them to the `CLLocationManager` for ranging.

> `startListeningBeacons` will not automatically request for location permission so make sure to catch any errors when trying to call it.

To avoid receiving errors, request for location permission first and only then start listening:

```swift
Beaconsmind.default.requestLocationPermission { enabled in
    if enabled {
        DispatchQueue.main.async {
            try? Beaconsmind.default.startListeningBeacons()
        }
    } else {
        print("TODO: No location permission given, update UI")
    }
}
```

Special attention:
> **IMPORTANT:** `startListeningBeacons` needs to execute on the main thread. Invoking the method in another thread will throw an error. Please use `DispatchQueue.main.async { ... }` to ensure invocation on the _main_ thread.

Please notice the `delegate: BeaconListenerDelegate? = nil` argument of the method - provide the object implementing the `BeaconListenerDelegate` to monitor the Beacon ranging while the application is active (in the foreground). This can be used for debugging purpouses during app development.

> Even though the beacons detection and raging service will work in the background, call the `startListeningBeacons` method every time you either log in or restore the `APIContext`.

The background ranging and pinging of the beacons is executed automatically by the SDK even if the app is in the background.

## SDK Usage - Stop Listening 

To stop listening for the beacons please call `Beaconsmind.default.stopListeningBeacons()`. Ranging beacons and automatic pings to `Beaconsmind` will terminate.

## __Important__ - Background ranging is non-deterministic

There are many limitations for background ranging on iOS due to Apple's privacy regulations. Basically, in background only monitoring is available and once the device gets close to a beacon, the app can be activated for a limited time to start ranging and handle some ranging events before the OS again stops it.

Background mode for location is controlled by iOS and due to heuristic/nondisclosed parameters background ranging _may not work_ consistenly. Even if Beacons are properly ranged, depending on the current state of the device, battery rating, data plan, or other running apps, the Ping API call might not register every time.

This cannot be properly managed from the application and is entirely managed by Apple's undisclosed algorithms for Background execution of apps in iOS.

> Depending on the usage scenario of background beacon detection, your app _may not pass_ App Store Review Process.

Please make sure that while describing the reasons for Location Background mode that it's clear enough for Apple that User's Privacy will be respected and the data sent/received by the app is not used to identify users.

## Logging

If you want to see the SDK's logs, you can use the `setMinLogLevel(level:)` method. By default logs are turned off, meaning the level is set to `.silent`. The following levels are supported:

* `.debug`: Any developer-level debug info, repeating events
* `.info`: Bigger events/steps in the SDK lifecycle, for example: service start/stop, one-time events
* `.warning`: Allowed, but not-optimal, inconsistent state
* `.error`: Not allowed state
* `.silent`: No logging at all

_Warning_: The messages will also show up in production builds and are quite verbose.

## SDK's internal flow

Under the hood we use the [`allowsBackgroundLocationUpdates`](https://developer.apple.com/documentation/corelocation/cllocationmanager/1620568-allowsbackgroundlocationupdates) flag of `CLLocationManager` to support receiving location events in the background. This flag is mostly used for navigation though and without `startUpdatingLocation`, background tracking is not working any more reliably from it unfortunately. Using it for tracking would end up in your app displaying the blue status bar saying "your app is actively using your location" to the user, which we of course want to avoid.

### Monitoring

When you call `startListeningBeacons`, we call `startMonitoring` on our `CLLocationManager` instance for the first 20 regions. The reason for this limitation is in [Apple's documentation](https://developer.apple.com/documentation/corelocation/cllocationmanager/1423656-startmonitoring).
```
An app can register up to 20 regions at a time. In order to report region changes in a timely manner, the region monitoring service requires network connectivity.
```

Of course we stop any previous beacon ranging and monitoring before we would start the new ones.

As an extra step, we call `requestState` on our `CLLocationManager` instance for each monitored region as the application goes to the background with `UIApplication.didEnterBackgroundNotification` to make sure we have at least one chance to start ranging if needed as the app goes to the background.

### Ranging

Once the `CLLocationManagerDelegate`'s `didDetermineState` callback notifies us that the device is in a specific region, we call [`startRangingBeacons`](https://developer.apple.com/documentation/corelocation/cllocationmanager/1620554-startrangingbeacons) on our `CLLocationManager` instance for all beacons inside the tracked region.

Of course we stop any previous beacon ranging before we would start the new ones.

### Timed updates

The SDK sets up some timers that are running while the app is in the foreground (as a side-effect, whenever any event wakes up the app in the background, those timers also get evaluated):

* Refetching beacons every 30 minutes
* Report beacon contacts every 5 seconds
* Monitoring the next 20 beacons (to work around Apple's max 20 limitation) every 3 seconds
* Restart monitoring completely every 30 minutes (trying to work around any missed exit events)
* Restart monitoring significant location changes every 15 seconds to get location updates more frequently
