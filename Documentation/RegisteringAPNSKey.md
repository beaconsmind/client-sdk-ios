# Registering a new APNS key on Apple’s developer portal 

### 1. Go to https://developer.apple.com and click on “Account”
### 2. Select “Certificates, Identifiers & Profiles” and click on “Keys” in the menu

<img src="Images/apns-reg-1.png" width=580 />
<img src="Images/apns-reg-2.png" width=220 />

### 3. Click on the “+” button to add a new key

<img src="Images/apns-reg-5.png" width=800 />

### 4. Enter a name for the new key and select “Apple Push Notifications service (APNs)”, click on “Continue”, then “Register”

<img src="Images/apns-reg-6.png" width=800 />
<img src="Images/apns-reg-7.png" width=800 />

### 5. Select the registered key to view its details

<img src="Images/apns-reg-3.png" width=800 />

### 6. Copy the “Key ID” and send it to us so we can register it on our side

<img src="Images/apns-reg-4.png" width=800 />

### 7. Also copy the “Team ID” from your membership info and send it to us so we can register it on our side

<img src="Images/apns-reg-8.png" width=400 />
<img src="Images/apns-reg-9.png" width=400 />