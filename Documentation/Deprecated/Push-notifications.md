> _*Deprecated_: Until version `3.0.0`, the SDK was using static functions to make our API-s as clean as possible, but this introduced some limitations, so we decided to deprecate this approach and start a more flexible, delegate-based setup.

# Push notifications

Push notifications are being sent to Devices implementing Beaconsmind SDK for sending Offers as results of physical proximity of Beacons.

## Initial setup

This guide assumes basic understanding of APNS, specifically:

- Sandbox and Production APNS environments
- Push notification entitlements
- Generating APNS certificates
- User permissions to receive local and/or remote notifications

In order to familiarise with the specifics, please consult Apple [Documentation](https://developer.apple.com/documentation/usernotifications) or this excellent [Ray Wenderlich article](https://www.raywenderlich.com/11395893-push-notifications-tutorial-getting-started).

For Beaconsmind backend to properly send push notifications to Devices implementing the SDK, please provide the Beaconsmind developers the appropriate .p12 certificate and it's passkey for your Application ID.

## Application setup

Push notification entitlement is required for the Device to properly register for Push notifications via APNS. 

> Configuration of Application's Bundle ID on the Apple Developer Portal may also be required.

To enable Push notifications for your App in Xcode please do the following:
- Select the iOS Application's Project file in the Navigator
- Select `Signing & Capabilities`
- Select the `+ Capability`
- Add the `Push Notifications` capability to the Application

> **Due to Privacy concerns, requesting Remote (Push) notifications for the App on the User's device will require User consent.**

## Enabling push notifications

To enable push notifications, BeaconsmindSDK provides a helper method on the SDK `BeaconsmindSDK.registerForPush()`.

```swift
import Beaconsmind 

// other code

BeaconsmindSDK.registerForPush()

// aftercode

```

Calling the method will automatically request for permission and enable `[.alert, .sound, .badge]` notifications. If App User accepts push, the SDK will enable push notifications.

As a result App delegate method `func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data)` will be invoked that contains the Device's APNS Token.

Please forward the token to the `BeaconsmindSDK` using `BeaconsmindSDK.register()`. `ISdkContext` is required to map the Device APNS token to the User.

Example code.

```swift
import Beaconsmind

// other code

if let ctx = SessionStorage.default.sdkContext {
	BeaconsmindSDK.register(deviceToken: deviceToken, context: ctx)	
}
```

## Receiving Push

In the App Delegate method `application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any])` please forward the `userInfo` to the `BeaconsmindSdk.receivePush(` method. This will handle the scenario while the app is running and a beacon is ranged/push notification received.

```swift
import Beaconsmind

// other code

func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
    guard let ctx = SessionHolder.default.sdkContext else { return }

    do {
        let apnsData = try BeaconsmindSDK.receivePush(userInfo: userInfo, context: ctx)
        print(apnsData)
    } catch let err {
        print(err.localizedDescription)
    }

    // Other code handling Push Notifications.
}
```

Calling `receivePush` will return the parsed Payload with the Offer ID triggered by the proximity to the Beacon and will report the Offer ID as `received` by using the Beaconsmind API.

## Parsing push which opens the app

If the app is in the background and is opened by reacting to the Push Notification, the Push Message is received in the app at a different location - on the `application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?)` App Delegate call. This is the method invoked when the Application launches.

In the `launchOptions` Map, a value having a key `UIApplication.LaunchOptionsKey.remoteNotification` will be available with a valid APNS payload Application was started with.

```swift
func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

    // Other Code

    let sessionKey = UserDefaults.standard.string(forKey: SdkSessionKey),
    let context = try? BeaconsmindSDK.restoreSession(key: sessionKey)

    if let remoteNotification = launchOptions?[UIApplication.LaunchOptionsKey.remoteNotification], let ctx = context {
        do {
            let apnsData = try BeaconsmindSDK.receivePush(userInfo: remoteNotification, context: ctx)
            // Do something with push
        } catch let err {
            print(err.localizedDescription)
        }
    }

    // Aftercode

    return true
}

```

This example code immediately reports the Offer as Read if an Existing session for this app is available. The `userInfo` parameter is the same as in the `didReceiveRemoteNotification` App Delegate call.
