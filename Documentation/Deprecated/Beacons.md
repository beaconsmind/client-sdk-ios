> _*Deprecated_: Until version `3.0.0`, the SDK was using static functions to make our API-s as clean as possible, but this introduced some limitations, so we decided to deprecate this approach and start a more flexible, delegate-based setup.

# Beacons

Beacons feature uses BLE transmitters which can be picked up by iDevices. In Beaconsmind, BLE transmitters are used to present offers based on a physical location of a device.

Every beacon is defined by it's UUID and major and minor numbers. A list of beacons is persisted and maintained on Beaconsmind environment for each client.

## Requirements

iOS devices can detect the proximity of the beacons only if the Location services are enabled and if the iOS device user has approved the use of Location services.

> **Important**: Without user approval iOS application is _not able_ to detect or range beacons while running in the background.

> In order to receive push notifications as a result of beacon ranging, users also need to approve Remote Notifications. See the guide about [push notifications](Push-notifications.md) for details.


## Enable Location Services

### Step 1 - Privacy settings

To enable location services, use Xcode to add the following keys to the `Info.plist` of application integrating the SDK:

- The string entry with a key `NSLocationAlwaysAndWhenInUseUsageDescription` - which explains to the user (and Apple during the App review process) how the location is used while the app is in the background
- The string entry with a key `NSLocationWhenInUseUsageDescription` - which explains how location is used while the application is the foreground (while being used)

> **Note**: When editing the `Info.plist` via Xcode you may need to show `Raw Keys & Values` to properly add the values.

This step is required by Apple for privacy reasons. Please consult Apple [Documentation](https://developer.apple.com/documentation/corelocation/requesting_authorization_for_location_services) for further explanation.

### Step 2 - Enable Background Location Services

For the Application to range beacons in the background the Application Entitlements must include running the Location background mode. To enable Location background mode in Xcode please do the following:

- Select the iOS Application's Project file in the Navigator
- Select `Signing & Capabilities`
- Select the `+ Capability`
- Add the `Background Modes` capability
- Select the `Location updates`, `Background processing` and `Background fetch`

Background processing is required to send the Ping API call while the Beacon is ranged while the app is in the background. This step is important to allow push notifications for Offers. See the guide about [push notifications](Push-notifications.md) for details.

## SDK Usage - Start Listening for Beacons

Registering for beacons is very simple - after getting the `ISdkContext` just call `BeaconsmindSDK.startListeningBeacons()`. Executing this method will fetch the beacons available for your Beaconsmind environment and register them to the `CLLocationManager` for ranging.

> `startListeningBeacons` will automatically request for Location permissions

Special attention:
> **IMPORTANT:** `startListeningBeacons` needs to execute on the main thread. Invoking the method in another thread will throw an error. Please use `DispatchQueue.main.async { ... }` to ensure invocation on the _main_ thread.

Please notice the `delegate: BeaconListenerDelegate? = nil` argument of the method - provide the object implementing the `BeaconListenerDelegate` to monitor the Beacon ranging while the application is active (in the foreground). This can be used for debugging purpouses during app development.

> Even though the beacons detection and raging service will work in the background, call the `startListeningBeacons` method every time you either log in or restore the `ISdkContext`

The background ranging and pinging of the beacons is executed automatically by the SDK even if the app is in the background.

## SDK Usage - Stop Listening 

To stop listening for the beacons please call `BeaconsmindSDK.stopListeningBeacons()`. Ranging beacons and automatic pings to Beaconsmind will terminate.

## Important info - Background ranging is non-deterministic

Background mode for Location is controlled by iOS and due to heuristic/nondisclosed parameters background ranging _may not work_ consistenly. Even if Beacons are properly ranged, depending on the current state of the Device, battery rating, data plan, or other running apps, the Ping API call might not register every time.

This cannot be properly managed from the application and is entirely managed by Apple's undisclosed algorithms for Background execution of apps in iOS.

> Depending on the usage scenario of background beacon detection, your app _may not pass_ App Store Review Process. 

Please make sure that while describing the reasons for Location Background mode that it's clear enough for Apple that User's Privacy will be respected and the data sent/received by the app is not used to identify users.

## Logging

For more info about the insight of the Beaconsmind Beacon services, set the `BeaconsmindSDK.isBeaconLoggingVerbose` switch to `true`. This could be especially useful during development since you will be able to see the frequency of beacon registration as well as the behavior of Beacons.

The status messages show up in production builds as well and are quite verbose, however, the error messages will be logged regardless of the switch.
