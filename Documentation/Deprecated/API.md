> _*Deprecated_: Until version `3.0.0`, the SDK was using static functions to make our API-s as clean as possible, but this introduced some limitations, so we decided to deprecate this approach and start a more flexible, delegate-based setup.

# SDK API documentation

## Public parameters

* `static var isBeaconLoggingVerbose: Bool`

  Toggle this field to make the Beacon detection and ranging more chatty, otherwise reports only errors. Use with caution in production releases.

## Authentication

### Login

This method is used to log in with your credentials on your running instance hostname. Will return `ISdkContext` object that is used to execute authenticated calls to the Beaconsmind services.
- parameter username: `String` username.
- parameter password: `String` password.
- parameter hostname: `String` hostname of your Beaconsmind service hostname.
- parameter callback: `SDKApiResult<ISdkContext>` callback which returns you the Context if entered credentials are correct.

```swift
static func login(username: String, password: String, hostname: String, callback: @escaping (SDKApiResult<ISdkContext>)
```

## Push notifications

### Registering for push notifications

Uses Apple's `UserNotifications` framework to register for remote notifications.  Once notifications are enabled, the application delegate's `didRegisterForRemoteNotificationsWithDeviceToken` callback will be called with the device token. See `register(deviceToken:context:callback)` to upload the token to Beaconsmind to receive offer notifications.

```swift
static func registerForPush()
```

### Registering the token on Beaconsmind

Use the device push token returned by APNS in the `application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data)` Application delegate callback to register the device for Beaconsmind services in order to receive beacon pings.
- parameter deviceToken: APS device token `Data`
- parameter forContext: `ISdkContext` representing the current login session
- parameter callback : (optional) `SDKApiResult` enum callback to listen for the API result.

```swift
static func register(deviceToken: Data, context: ISdkContext, callback: ((SDKApiResult<API.Accounts.AccountsUpdateDevice.Response>) -> Void)? = nil)
```

### Receiving push notifications

Upon receiving Offer push, use this method to parse the Payload to extract the Offer ID and automatically register the Offer as Received.
- parameter userInfo: `[AnyHashable: Any]` APNS payload received from the delegate
- parameter context: `ISdkContext` representing the current login session
- throws either `SDKApiError.invalidContext` or `SDKApiError.parse` depending on the issues

```swift
static func receivePush(userInfo: [AnyHashable: Any], context: ISdkContext) throws -> BeaconPingPushPayload
```

## Offers

### Marking offer as read

Register the Offer as Read.
- parameter offerId: `Int` Offer Id
- parameter context: `ISdkContext` representing the current login session
- parameter callback: (optional) `SDKApiResult<Bool>` containing  `API.Offers.OffersRead` API result
- returns `CancellableRequest` cancellation handle which allows you to cancel the request.
- throws either `SDKApiError.invalidContext` or `SDKApiError.parse` depending on the issues

```swift
static func markOfferAsRead(offerId: Int, context: ISdkContext, callback: ((SDKApiResult<Bool>)->Void)? = nil) throws -> CancellableRequest?
```

### Marking offer as received

Register the Offer as Received. Automatically called if an valid Offer ID is found  when `receivePush(userInfo:context)` is called upon receiving APNS payload in the app.
- parameter offerId: `Int` Offer Id
- parameter context: `ISdkContext` representing the current login session
- parameter callback: (optional) `SDKApiResult<Bool>` containing  `API.Offers.OffersRead` result for
- returns `CancellableRequest` cancellation handle which allows you to cancel the request.
- throws either `SDKApiError.invalidContext` or `SDKApiError.parse` depending on the issues

```swift
static func markOfferAsReceived(offerId: Int, context: ISdkContext, callback: ((SDKApiResult<Bool>)->Void)? = nil) throws -> CancellableRequest?
```

### Marking offer as redeemed

Register the Offer as Redeemed.
- parameter offerId: `Int` Offer Id
- parameter context: `ISdkContext` representing the current login session
- parameter callback: (optional) `SDKApiResult<Bool>` containing  `API.Offers.OffersRead` result for
- returns `CancellableRequest` cancellation handle which allows you to cancel the request.
- throws either `SDKApiError.invalidContext` or `SDKApiError.parse` depending on the issues

```swift
static func markOfferAsRedeemed(offerId: Int, context: ISdkContext, callback: ((SDKApiResult<Bool>)->Void)? = nil) throws -> CancellableRequest?
```

## Beacons

### Listening to beacons

Start listening beacons near your Apple Device. *NOTE:* Invoke this method on main thread to ensure proper `CLLocationManager` initialization.
To properly function in the background, applications need to set `NSLocationAlwaysAndWhenInUseUsageDescription` String value in the `Info.plist`.
- parameter context: `ISdkContext` object representing the Session
- parameter delegate: (optional) object implementing `BeaconListenerDelegate` to read ranging information while Application is in foreground.
- throws `SDKApiError.invalidContext` if context is invalid.

```swift
static func startListeningBeacons(context: ISdkContext, delegate: BeaconListenerDelegate? = nil) throws
```

### Stop listening

Stop listening for beacons near your Apple Device.

```swift
static func stopListeningBeacons()
```

## Session

### Storing

Save the session SDK context returned by the SDK. Returns the restore key you can call `restoreSession(key:)` to restore session.
- parameter session: `ISdkContext`  representing the current login session
- throws `SDKApiError.invalidContext` if context is invalid.

```swift
static func saveSession(_ session: ISdkContext) throws -> String
```

### Restoring

Restore the SDK context. Use the key returned by  `saveSession(_ session:)` if a valid `ISdkContext` was used.
- parameter key: `String` representing a Key `ISdkContext` was saved with after calling `saveSession(_:)`
- returns ready to use `ISdkContext`
- throws `SDKApiError.invalidContext` if context is invalid.

```swift
static func restoreSession(key: String) throws -> ISdkContext
```

### Destroying

Remove the SDK context. Use the key returned by `saveSession(_ session:)` if a valid `ISdkContext` was used.
- parameter key: `String` representing a Key `ISdkContext` was saved with after calling `saveSession(_:)`
- throws `SDKApiError.invalidContext` if the `key` is invalid.

```swift
static func destroySession(key: String) throws
```
