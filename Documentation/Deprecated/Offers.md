> _*Deprecated_: Until version `3.0.0`, the SDK was using static functions to make our API-s as clean as possible, but this introduced some limitations, so we decided to deprecate this approach and start a more flexible, delegate-based setup.

# Offers

Beacons ranging is used to pick up offers. In respect to offers, when users receive pushes about a new offer, read an offer and consume an offer, API calls need to be sent to recognize user activity.

To Get offers, mark offers as read, we use the `BeaconsmindAPI` part of the SDK which is a direct interface toward the API offered by Beaconsmind.

Other actions possible with Offers API are:

- Mark as Received
- Mark as Read
- Mark as Redeemed
- Get List of Offers

## BeaconsmindAPI

Using the `BeaconsmindAPI.apiRequest` method in the `Beaconsmind` module, SDK integrator is able to initiate Requests to BeaconsmindAPI.

Since OpenAPI is used, API endpoints have been generated from the Schema and packaged in the `BeaconsmindAPI` for easy access

Requirements:

- `ISdkContext` - which is used to identify a user.
- `Request` - a namespaced object for every `API` request in the API schema. `API` object is available upon importing the `Beaconsmind` module

## Getting Offers

To fetch offers, `API.Offers.OffersGetOffers.Request` is used as a `Request` object. In the example:

```swift
import Beaconsmind

func loadOffers() {
    guard let ctx = sessionHolder.sdkContext else {
        return
    }

    let offers = API.Offers.OffersGetOffers.Request()
    BeaconsmindAPI.apiRequest(offers, context: ctx) { (response) in
        switch response {
        case let .success(r):
        	// do something with offers list `[OfferResponse]`
        case let .failure(error):
        	// show error
        }
    }
}
```

Here, the Offers are fetched to be used for example in a list to be opened by users.

## Other Offer utilities

`BeaconsmindSDK` contains offer utilites to mark an offer as read an offer as received by an Offer ID. OfferId can be extracted from a Push notification, from a GetOffers request and User activity.

`BeaconsmindSDK.markOfferAsReceived()`, `BeaconsmindSDK.markOfferAsRead()` and `BeaconsmindSDK.markOfferAsRedeemed()` are used for Received, Read and Redeemed actions.
