> _*Deprecated_: Until version `3.0.0`, the SDK was using static functions to make our API-s as clean as possible, but this introduced some limitations, so we decided to deprecate this approach and start a more flexible, delegate-based setup.

# Setting up

As a Beaconsmind client, a contained environment is provided for Beaconsmind services being hosted and maintained by Beaconsmind.

The hostname of the provided environment is required to properly access Beaconsmind API and to use Beaconsmind SDK

> example: `beaconsmind-test.westeurope.azurecontainer.io`

Please contact Beaconsmind support for the required information about acquiring a valid environment and hostname.

Beaconsmind SDK objects and methods are available in the `Beaconsmind` module after installing with Cocoapods or Carthage.

## Using the SDK - SDK Context

The `ISdkContext` object is the centerpoint of the SDK which unlocks usage of API and services provided by Beaconsmind.

To get the `ISdkContext`, a valid session is required which is in turn initiated by logging in with the correct credentials in the provided environment.

Upon receiving the SDK context, please store the reference since it's used to identify the session and hostname for future API requests for the duration of the session

```swift
import Beaconsmind

BeaconsmindSDK.login(username: username, password: password, hostname: hostname) { [unowned self] (r) in
    switch r {
    case let .success(context):
    	self.context = context // store the reference
        break
    case let .failure(error):
        self.showMessage(error.localizedDescription)
        break
    }
}
```

## Saving and restoring the Session

`BeaconsmindSDK` provides 2 methods which can be used to save and restore the `ISdkContext`.

To save the context:

```swift
import Beaconsmind

// Other Code

guard
    let session = sdkContext,
    let key = try? BeaconsmindSDK.saveSession(session)
else {
    return
}

UserDefaults.standard.set(key, forKey: SdkSessionKey)
```

To restore the context:
```swift
import Beaconsmind

// Other Code

if
    let session = UserDefaults.standard.string(forKey: SdkSessionKey),
    let ctx = try? BeaconsmindSDK.restoreSession(key: session) {
    sdkContext = ctx
}
```

`BeaconsmindSDK.saveSession()` serializes `ISdkContext` and returns the UUID string which is used to restore the session using `BeaconsmindSDK.restoreSession()`.

The UUID string can be saved to `UserDefaults`, `CoreData` or any other persistence method available. A same UUID string cannot be used across multiple devices.
