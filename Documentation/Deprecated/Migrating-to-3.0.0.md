# Migration to 3.0.0

Until version `3.0.0`, the SDK was using static functions to make our API-s as clean as possible, but this introduced some limitations, so we decided to deprecate this approach and start a more flexible, delegate-based setup.

This document describes the main changes you need to do to upgrade your codebase to the new approach.

## Starting the SDK

In your `application(didFinishLaunchingWithOptions:)` appdelegate callback, start the SDK with the new `start(delegate:appVersion:hostname)` call. Make sure to also implement the `BeaconsmindDelegate` delegate according to your business logic.

```swift
@main
class AppDelegate: UIResponder, UIApplicationDelegate {

  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
      do {
          try Beaconsmind.default.start(
              delegate: self,
              appVersion: appVersion,
              hostname: hostname
          )
      } catch {}
      return true
  }
}

// MARK: - BeaconsmindDelegate

extension AppDelegate: BeaconsmindDelegate {

    func beaconsmind(_ beaconsmind: Beaconsmind, onContextChanged context: ISdkContext?) {
      // TODO: Add context update logic
    }
}
```

### Update Beaconsmind SKD calls

Update your SDK calls like this: `BeaconsmindSDK.<some old function>` to `Beaconsmind.default.<updated function>`.

### Update BeaconsmindAPI calls

Using `BeaconsmindAPI` is deprecated, but sending any request with the current context is exposed by the new `Beaconsmind.default.apiRequest` call. Example:

```swift
func loadOffers() {
    self.startLoading()

    do {
        let request = API.Offers.OffersGetOffers.Request()
        _ = try Beaconsmind.default.apiRequest(request) { [weak self] result in
            switch result {
            case let .success(offers):
                self?.offers = offers.success ?? []
                self?.tableView?.reloadData()
            case let .failure(error):
                self?.showMessage(error.localizedDescription)
            }
            self?.stopLoading()
        }
    } catch {
        stopLoading()
        print(error.localizedDescription)
    }
}
```

In case you still want to use `BeaconsmindAPI` directly in your app, you can now also get the context from the new `getOAuthContext` call.

```swift
guard let context = Beaconsmind.default.getOAuthContext() else { return }
```
