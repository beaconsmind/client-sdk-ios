// swift-tools-version:5.5
import PackageDescription

let package = Package(
    name: "Beaconsmind",
    platforms: [
        .iOS(.v11)
    ],
    products: [
        .library(
            name: "Beaconsmind",
            targets: ["Beaconsmind"]),
    ],
    dependencies: [
        .package(url: "https://github.com/Alamofire/Alamofire.git", .upToNextMajor(from: "5.4.4")),
        .package(url: "https://github.com/Alamofire/AlamofireImage", .upToNextMajor(from: "4.1.0")),
        .package(url: "https://github.com/jdg/MBProgressHUD", .upToNextMajor(from: "1.2.0")),
        .package(name: "Mockingbird", url: "https://github.com/birdrides/mockingbird.git", .upToNextMinor(from: "0.20.0")),
        .package(name: "BugfenderLibrary", url: "https://github.com/bugfender/BugfenderSDK-iOS", .upToNextMajor(from: "1.11.0")),
    ],
    targets: [
        .target(
            name: "Beaconsmind",
            dependencies: ["Alamofire", "AlamofireImage", "MBProgressHUD", "BugfenderLibrary"],
            path: "Beaconsmind/Beaconsmind",
            sources: ["Services", "API"]
        ),
        .testTarget(
            name: "BeaconsmindTests",
            dependencies: ["Beaconsmind", "MBProgressHUD", "Mockingbird"],
            path: "Beaconsmind/BeaconsmindTests"
        )
    ]
)
